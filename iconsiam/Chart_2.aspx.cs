﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace iconsiam
{
    public partial class Chart_2 : System.Web.UI.Page
    {
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }

        [WebMethod]
        public static List<object> GetChartData()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Month", typeof(string));
            dt.Columns.Add("YTD " + DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));
            dt.Columns.Add("YTD " + DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            for (int j = 0; j < 12; j++)
            {
                DataRow row1 = dt.NewRow();
                row1 = dt.NewRow();

                decimal sum1_ = 0;
                decimal sum2_ = 0;

                string mm = DateTime.Now.ToString("yyyy-01-01", EngCI);

                string date1 = Convert.ToDateTime(mm).AddMonths(j).ToString("MMM", EngCI);
                string date1_ = Convert.ToDateTime(mm).AddMonths(j).ToString("MM", EngCI);

                row1["Month"] = date1;

                //==========================================================================================
                var saleAmount = GetSaleAmount(dt.Columns[1].ColumnName.Substring(4, 4),date1,"new", HttpContext.Current.Session["s_com_code"].ToString());                
                if (saleAmount.Rows.Count != 0)
                {
                    decimal saleAmount_ = 0;
                    for (int a = 0; a < saleAmount.Rows.Count; a++)
                    {
                        if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                        {
                            saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                        }
                        else
                        {
                            saleAmount_ = saleAmount_ + 0;
                        }
                    }                   
                    row1[dt.Columns[1].ColumnName] = saleAmount_;
                    sum1_ = sum1_ + saleAmount_;
                }               
                //==========================================================================================
                var saleAmount2 = GetSaleAmount(dt.Columns[2].ColumnName.Substring(4, 4), date1, "old", HttpContext.Current.Session["s_com_code"].ToString());
                if (saleAmount2.Rows.Count != 0)
                {
                    decimal saleAmount2_ = 0;
                    for (int a = 0; a < saleAmount2.Rows.Count; a++)
                    {
                        if (saleAmount2.Rows[a]["Amount_year_old"].ToString() != "")
                        {
                            saleAmount2_ = Convert.ToDecimal(saleAmount2.Rows[a]["Amount_year_old"].ToString());
                        }
                        else
                        {
                            saleAmount2_ = 0;
                        }
                    }
                    row1[dt.Columns[2].ColumnName] = saleAmount2_;
                    sum2_ = sum2_ + saleAmount2_;
                }
                                                  
                //==========================================================================================

                dt.Rows.Add(row1);
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        //=====================================================================================
        [WebMethod]
        public static List<object> GetChartData_location()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Location", typeof(string));
            dt.Columns.Add("YTD " + DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));
            dt.Columns.Add("YTD " + DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var group_loca = GetGroupLocation(HttpContext.Current.Session["s_com_code"].ToString());
            if (group_loca.Rows.Count != 0)
            {
                for (int i = 0; i < group_loca.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Location"] = group_loca.Rows[i]["Location"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_location(dt.Columns[1].ColumnName.Substring(4, 4), group_loca.Rows[i]["Location"].ToString(),"new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }                                     
                    //==========================================================================================
                    var saleAmount2 = GetSaleAmount_location(dt.Columns[2].ColumnName.Substring(4, 4), group_loca.Rows[i]["Location"].ToString(),"old", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount2.Rows.Count != 0)
                    {
                        decimal saleAmount2_ = 0;
                        for (int a = 0; a < saleAmount2.Rows.Count; a++)
                        {
                            if (saleAmount2.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount2_ = saleAmount2_ + Convert.ToDecimal(saleAmount2.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount2_ = saleAmount2_ + 0;
                            }
                        }
                        row1[dt.Columns[2].ColumnName] = saleAmount2_;
                    }                                                            
                    //==========================================================================================

                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_location_donut1()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Location", typeof(string));
            dt.Columns.Add(DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));

            var group_loca = GetGroupLocation(HttpContext.Current.Session["s_com_code"].ToString());
            if (group_loca.Rows.Count != 0)
            {
                for (int i = 0; i < group_loca.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Location"] = group_loca.Rows[i]["Location"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_location(dt.Columns[1].ColumnName, group_loca.Rows[i]["Location"].ToString(),"new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }                                                        
                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_location_donut2()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Location", typeof(string));
            dt.Columns.Add(DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var group_loca = GetGroupLocation(HttpContext.Current.Session["s_com_code"].ToString());
            if (group_loca.Rows.Count != 0)
            {
                for (int i = 0; i < group_loca.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Location"] = group_loca.Rows[i]["Location"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_location(dt.Columns[1].ColumnName, group_loca.Rows[i]["Location"].ToString(),"old", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }                                       
                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }
        //=====================================================================================

        //=====================================================================================
        [WebMethod]
        public static List<object> GetChartData_floor()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Floor", typeof(string));
            dt.Columns.Add("YTD " + DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));
            dt.Columns.Add("YTD " + DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var Floor = GetFloor(HttpContext.Current.Session["s_com_code"].ToString());
            if (Floor.Rows.Count != 0)
            {
                for (int i = 0; i < Floor.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Floor"] = Floor.Rows[i]["Floor"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_floor(dt.Columns[1].ColumnName.Substring(4, 4), Floor.Rows[i]["Floor"].ToString(),"new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }
                                           
                    //==========================================================================================
                    var saleAmount2 = GetSaleAmount_floor(dt.Columns[2].ColumnName.Substring(4, 4), Floor.Rows[i]["Floor"].ToString(),"old", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount2.Rows.Count != 0)
                    {
                        decimal saleAmount2_ = 0;
                        for (int a = 0; a < saleAmount2.Rows.Count; a++)
                        {
                            if (saleAmount2.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount2_ = saleAmount2_ + Convert.ToDecimal(saleAmount2.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount2_ = saleAmount2_ + 0;
                            }
                        }
                        row1[dt.Columns[2].ColumnName] = saleAmount2_;
                    }                                            
                    //==========================================================================================

                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_Floor_donut1()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Floor", typeof(string));
            dt.Columns.Add(DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));

            var Floor = GetFloor(HttpContext.Current.Session["s_com_code"].ToString());
            if (Floor.Rows.Count != 0)
            {
                for (int i = 0; i < Floor.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Floor"] = Floor.Rows[i]["Floor"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_floor(dt.Columns[1].ColumnName, Floor.Rows[i]["Floor"].ToString(),"new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }                                           
                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_Floor_donut2()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Floor", typeof(string));
            dt.Columns.Add(DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var Floor = GetFloor(HttpContext.Current.Session["s_com_code"].ToString());
            if (Floor.Rows.Count != 0)
            {
                for (int i = 0; i < Floor.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Floor"] = Floor.Rows[i]["Floor"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_floor(dt.Columns[1].ColumnName, Floor.Rows[i]["Floor"].ToString(),"old", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }                                           
                    dt.Rows.Add(row1);
                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }
        //===========================================================================================

        //=====================================================================================
        [WebMethod]
        public static List<object> GetChartData_Category()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Category Leasing", typeof(string));
            dt.Columns.Add("YTD " + DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));
            dt.Columns.Add("YTD " + DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var Catego = GetCategoryLeasing(HttpContext.Current.Session["s_com_code"].ToString());
            if (Catego.Rows.Count != 0)
            {
                for (int i = 0; i < Catego.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Category Leasing"] = Catego.Rows[i]["Category"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_CategoryLeasing(dt.Columns[1].ColumnName.Substring(4, 4), Catego.Rows[i]["Category"].ToString(),"new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }                                          
                    //==========================================================================================
                    var saleAmount2 = GetSaleAmount_CategoryLeasing(dt.Columns[2].ColumnName.Substring(4, 4), Catego.Rows[i]["Category"].ToString(), "old" , HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount2.Rows.Count != 0)
                    {
                        decimal saleAmount2_ = 0;
                        for (int a = 0; a < saleAmount2.Rows.Count; a++)
                        {
                            if (saleAmount2.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount2_ = saleAmount2_ + Convert.ToDecimal(saleAmount2.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount2_ = saleAmount2_ + 0;
                            }
                        }
                        row1[dt.Columns[2].ColumnName] = saleAmount2_;
                    }                                       
                    //==========================================================================================

                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_Category_donut1()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Category Leasing", typeof(string));
            dt.Columns.Add(DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));

            var Catego = GetCategoryLeasing(HttpContext.Current.Session["s_com_code"].ToString());
            if (Catego.Rows.Count != 0)
            {
                for (int i = 0; i < Catego.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Category Leasing"] = Catego.Rows[i]["Category"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_CategoryLeasing(dt.Columns[1].ColumnName, Catego.Rows[i]["Category"].ToString(), "new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }
                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_Category_donut2()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Category Leasing", typeof(string));
            dt.Columns.Add(DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var Catego = GetCategoryLeasing(HttpContext.Current.Session["s_com_code"].ToString());
            if (Catego.Rows.Count != 0)
            {
                for (int i = 0; i < Catego.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Category Leasing"] = Catego.Rows[i]["Category"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_CategoryLeasing(dt.Columns[1].ColumnName, Catego.Rows[i]["Category"].ToString(), "old", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }                                           
                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }
        //===========================================================================================
        //=====================================================================================
        [WebMethod]
        public static List<object> GetChartData_IndustrtGroup()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Business Type", typeof(string));
            dt.Columns.Add("YTD " + DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));
            dt.Columns.Add("YTD " + DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var Catego = GetIndustryGroup(HttpContext.Current.Session["s_com_code"].ToString());
            if (Catego.Rows.Count != 0)
            {
                for (int i = 0; i < Catego.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Business Type"] = Catego.Rows[i]["Industry"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_IndustryGroup(dt.Columns[1].ColumnName.Substring(4, 4), Catego.Rows[i]["Industry"].ToString(),"new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }
                                           
                    //==========================================================================================
                    var saleAmount2 = GetSaleAmount_IndustryGroup(dt.Columns[2].ColumnName.Substring(4, 4), Catego.Rows[i]["Industry"].ToString(),"old", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount2.Rows.Count != 0)
                    {
                        decimal saleAmount2_ = 0;
                        for (int a = 0; a < saleAmount2.Rows.Count; a++)
                        {
                            if (saleAmount2.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount2_ = saleAmount2_ + Convert.ToDecimal(saleAmount2.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount2_ = saleAmount2_ + 0;
                            }
                        }
                        row1[dt.Columns[2].ColumnName] = saleAmount2_;
                    }
                                            
                    //==========================================================================================

                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_IndustrtGroup_donut1()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Business Type", typeof(string));
            dt.Columns.Add(DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));

            var Catego = GetIndustryGroup(HttpContext.Current.Session["s_com_code"].ToString());
            if (Catego.Rows.Count != 0)
            {
                for (int i = 0; i < Catego.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Business Type"] = Catego.Rows[i]["Industry"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_IndustryGroup(dt.Columns[1].ColumnName, Catego.Rows[i]["Industry"].ToString(),"new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }                                            

                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_IndustrtGroup_donut2()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Business Type", typeof(string));
            dt.Columns.Add(DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var Catego = GetIndustryGroup(HttpContext.Current.Session["s_com_code"].ToString());
            if (Catego.Rows.Count != 0)
            {
                for (int i = 0; i < Catego.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Business Type"] = Catego.Rows[i]["Industry"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_IndustryGroup(dt.Columns[1].ColumnName, Catego.Rows[i]["Industry"].ToString(),"old", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }

                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }
        //===========================================================================================
        //=====================================================================================
        [WebMethod]
        public static List<object> GetChartData_building()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Building", typeof(string));
            dt.Columns.Add("YTD " + DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));
            dt.Columns.Add("YTD " + DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var Catego = GetBuilding(HttpContext.Current.Session["s_com_code"].ToString());
            if (Catego.Rows.Count != 0)
            {
                for (int i = 0; i < Catego.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Building"] = Catego.Rows[i]["Building"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_Building(dt.Columns[1].ColumnName.Substring(4, 4), Catego.Rows[i]["Building"].ToString(),"new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }                        
                    //==========================================================================================
                    var saleAmount2 = GetSaleAmount_Building(dt.Columns[2].ColumnName.Substring(4, 4), Catego.Rows[i]["Building"].ToString(),"old", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount2.Rows.Count != 0)
                    {
                        decimal saleAmount2_ = 0;
                        for (int a = 0; a < saleAmount2.Rows.Count; a++)
                        {
                            if (saleAmount2.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount2_ = saleAmount2_ + Convert.ToDecimal(saleAmount2.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount2_ = saleAmount2_ + 0;
                            }
                        }
                        row1[dt.Columns[2].ColumnName] = saleAmount2_;
                    }

                    //==========================================================================================

                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_building_donut1()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Building", typeof(string));
            dt.Columns.Add(DateTime.Now.ToString("yyyy", EngCI), typeof(decimal));

            var Catego = GetBuilding(HttpContext.Current.Session["s_com_code"].ToString());
            if (Catego.Rows.Count != 0)
            {
                for (int i = 0; i < Catego.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Building"] = Catego.Rows[i]["Building"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_Building(dt.Columns[1].ColumnName, Catego.Rows[i]["Building"].ToString(),"new", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_yeay_now"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_yeay_now"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }
                        
                    dt.Rows.Add(row1);

                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChartData_building_donut2()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Building", typeof(string));
            dt.Columns.Add(DateTime.Now.AddYears(-1).ToString("yyyy", EngCI), typeof(decimal));

            var Catego = GetBuilding(HttpContext.Current.Session["s_com_code"].ToString());
            if (Catego.Rows.Count != 0)
            {
                for (int i = 0; i < Catego.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    row1["Building"] = Catego.Rows[i]["Building"].ToString();

                    //==========================================================================================
                    var saleAmount = GetSaleAmount_Building(dt.Columns[1].ColumnName, Catego.Rows[i]["Building"].ToString(),"old", HttpContext.Current.Session["s_com_code"].ToString());
                    if (saleAmount.Rows.Count != 0)
                    {
                        decimal saleAmount_ = 0;
                        for (int a = 0; a < saleAmount.Rows.Count; a++)
                        {
                            if (saleAmount.Rows[a]["Amount_year_old"].ToString() != "")
                            {
                                saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount_year_old"].ToString());
                            }
                            else
                            {
                                saleAmount_ = saleAmount_ + 0;
                            }
                        }
                        row1[dt.Columns[1].ColumnName] = saleAmount_;
                    }

                    dt.Rows.Add(row1);
                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }
        //===========================================================================================

        public static DataTable GetSaleAmount(string year , string month , string status_year , string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            if (status_year == "new")
            {
                strSQL += "   select * from tblJob_chart_YTD_Data where Month = '"+ month + "' and Year_now = '"+ year + "' and CompanyCode in ("+ Companycode + ")  ";
            }
            else
            {
                strSQL += "   select * from tblJob_chart_YTD_Data where Month = '" + month + "' and Year_old = '" + year + "' and CompanyCode in (" + Companycode + ")  ";
            }


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetGroupLocation(string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select Distinct Location  from tblJob_chart_YTD_Location where CompanyCode in (" + Companycode+")  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetFloor(string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select Distinct Floor from tblJob_chart_YTD_Floor where CompanyCode in ("+companycode+") ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetSaleAmount_location(string year, string location , string yearstatus , string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            if (yearstatus == "new")
            {
                strSQL += " select * from tblJob_chart_YTD_Location where Year_now = '" + year + "' and Location = '" + location + "' and CompanyCode in (" + companycode + ")  ";
            }
            else
            {
                strSQL += " select * from tblJob_chart_YTD_Location where Year_old = '" + year + "' and Location = '" + location + "' and CompanyCode in (" + companycode + ")  ";
            }                  

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetSaleAmount_floor(string year, string smart_floor , string statusyear , string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            if (statusyear == "new")
            {
                strSQL += "  select * from tblJob_chart_YTD_Floor where Year_now = '" + year + "' and Floor = '" + smart_floor + "' and CompanyCode in (" + companycode + ") ";
            }
            else
            {
                strSQL += "  select * from tblJob_chart_YTD_Floor where Year_old = '" + year + "' and Floor = '" + smart_floor + "' and CompanyCode in (" + companycode + ") ";
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public static DataTable GetCategoryLeasing(string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select Distinct Category from tblJob_chart_YTD_Category where CompanyCode in ("+companycode+")  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public static DataTable GetSaleAmount_CategoryLeasing(string year, string CategoryleasingNameEN , string statusyear , string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            if (statusyear == "new")
            {
                strSQL += "  select * from tblJob_chart_YTD_Category where Year_now = '" + year + "' and Category = '" + CategoryleasingNameEN + "' and CompanyCode in (" + companycode + ")  ";
            }
            else
            {
                strSQL += "  select * from tblJob_chart_YTD_Category where Year_old = '" + year + "' and Category = '" + CategoryleasingNameEN + "' and CompanyCode in (" + companycode + ")  ";
            }
                 
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }



        public static DataTable GetIndustryGroup(string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select distinct Industry from tblJob_chart_YTD_Industry where CompanyCode in ("+ companycode + ") ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public static DataTable GetSaleAmount_IndustryGroup(string year, string IndustryGroupNameEN , string statusyear , string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            if (statusyear == "new")
            {
                strSQL += "  select * from tblJob_chart_YTD_Industry where Year_now = '" + year + "' and Industry = '" + IndustryGroupNameEN + "' and CompanyCode in (" + companycode + ") ";
            }
            else
            {
                strSQL += "  select * from tblJob_chart_YTD_Industry where Year_old = '" + year + "' and Industry = '" + IndustryGroupNameEN + "' and CompanyCode in (" + companycode + ") ";
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public static DataTable GetBuilding(string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select distinct Building from tblJob_chart_YTD_Building where CompanyCode in ("+Companycode+") ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public static DataTable GetSaleAmount_Building(string year, string BuildingNameEN , string statusyear , string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            if (statusyear == "new")
            {
                strSQL += "  select* from tblJob_chart_YTD_Building where Year_now = '" + year + "' and Building = '" + BuildingNameEN + "' and CompanyCode in (" + Companycode + ")";
            }
            else
            {
                strSQL += "  select* from tblJob_chart_YTD_Building where Year_old = '" + year + "' and Building = '" + BuildingNameEN + "' and CompanyCode in (" + Companycode + ")";
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        //=============================================================================================

        protected void Page_Load(object sender, EventArgs e)
        {
            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            if (!Page.IsPostBack)
            {
                Label1.Text = DateTime.Now.AddYears(-1).ToString("yyyy", EngCI);
                Label2.Text = DateTime.Now.ToString("yyyy", EngCI);

                Label3.Text = DateTime.Now.AddYears(-1).ToString("yyyy", EngCI);
                Label4.Text = DateTime.Now.ToString("yyyy", EngCI);

                Label5.Text = DateTime.Now.AddYears(-1).ToString("yyyy", EngCI);
                Label6.Text = DateTime.Now.ToString("yyyy", EngCI);

                Label7.Text = DateTime.Now.AddYears(-1).ToString("yyyy", EngCI);
                Label8.Text = DateTime.Now.ToString("yyyy", EngCI);

                Label9.Text = DateTime.Now.AddYears(-1).ToString("yyyy", EngCI);
                Label10.Text = DateTime.Now.ToString("yyyy", EngCI);

                ddltype.Items.Insert(0, new ListItem("Location", "Location"));
                ddltype.Items.Insert(1, new ListItem("Floor", "Floor"));
                ddltype.Items.Insert(2, new ListItem("Category Leasing", "Category"));
                ddltype.Items.Insert(3, new ListItem("Business type", "Business"));
                ddltype.Items.Insert(4, new ListItem("Building", "Building"));

                if (HttpContext.Current.Session["code_theme"] != null)
                {
                    this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
                }
                else
                {
                    this.MyTheme = "#CCA9DA";
                }

                if (HttpContext.Current.Session["code_Navbar"] != null)
                {
                    this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
                }
                else
                {
                    this.NavbarColor = "#2d2339";
                }

            }
        }
        protected void btnsearch_Click(object sender, EventArgs e)
        {
            if(ddltype.SelectedValue == "Location")
            {
                div_location.Visible = true;
                div_floor.Visible = false;
                div_cat.Visible = false;
                div_business.Visible = false;
                div_building.Visible = false;
            }
            else if (ddltype.SelectedValue == "Floor")
            {
                div_location.Visible = false;
                div_floor.Visible = true;
                div_cat.Visible = false;
                div_business.Visible = false;
                div_building.Visible = false;

            }
            else if (ddltype.SelectedValue == "Category")
            {
                div_location.Visible = false;
                div_floor.Visible = false;
                div_cat.Visible = true;
                div_business.Visible = false;
                div_building.Visible = false;

            }
            else if (ddltype.SelectedValue == "Business")
            {
                div_location.Visible = false;
                div_floor.Visible = false;
                div_cat.Visible = false;
                div_business.Visible = true;
                div_building.Visible = false;

            }
            else if (ddltype.SelectedValue == "Building")
            {
                div_location.Visible = false;
                div_floor.Visible = false;
                div_cat.Visible = false;
                div_business.Visible = false;
                div_building.Visible = true;

            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }
        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Chart_2.aspx");
        }
    }

}