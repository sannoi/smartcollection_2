﻿using GhostscriptSharp;
using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class record_transaction_tenant : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        record_transactionDLL Serv = new record_transactionDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {

                    txtproduct_amount.Attributes.Add("onkeypress", "return isNumberKey2AndEnter(event,'" + txtproduct_amount.ClientID + "')");
                    txtservice_serCharge_amount.Attributes.Add("onkeypress", "return isNumberKey2AndEnter(event,'" + txtservice_serCharge_amount.ClientID + "')");
                    txtservice_serCharge_amount2.Attributes.Add("onkeypress", "return isNumberKey2AndEnter(event,'" + txtservice_serCharge_amount2.ClientID + "')");


                    DateTime t1 = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    DateTime t2 = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 19:00:00"));

                    if (TimeSpan.Compare(t1.TimeOfDay, t2.TimeOfDay) > 0)
                    {
                        txtdate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);
                    }
                    else
                    {
                        txtdate.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", EngCI);
                    }


                    bind_default();
                    bind_rep();
                }
            }
        }

        protected void bind_default()
        {
            ///// Binding Reason /////

            ddlreason.Items.Insert(0, new ListItem("มียอดขาย", ""));
            ddlreason.Items.Insert(1, new ListItem("ร้านปิด", "close"));
            ddlreason.Items.Insert(2, new ListItem("No Bill", "nobill"));
            ddlreason.Items.Insert(3, new ListItem("Other", "other"));


        }

        protected void bind_rep()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var contract = Serv.GetContract(Request.QueryString["id"].ToString());
                if (contract.Rows.Count != 0)
                {
                    lbroomnum.Text = "ROOM Number  : " + contract.Rows[0]["smart_room_no"].ToString();
                    lbshopname.Text = "Shop Name : " + contract.Rows[0]["ShopName"].ToString();

                    if (contract.Rows[0]["smart_record_keyin_type"].ToString() == "prod_sale")
                    {
                        HttpContext.Current.Session["smart_record_keyin_type"] = contract.Rows[0]["smart_record_keyin_type"].ToString();
                        txtservice_serCharge_amount.Visible = false;
                        txtservice_serCharge_amount2.Visible = false;
                    }
                    else if (contract.Rows[0]["smart_record_keyin_type"].ToString() == "prod_service")
                    {
                        HttpContext.Current.Session["smart_record_keyin_type"] = contract.Rows[0]["smart_record_keyin_type"].ToString();
                        lb1.Visible = true;
                        lb1.Text = "ยอดค่าบริการ";
                        txtservice_serCharge_amount.Visible = true;
                        txtservice_serCharge_amount2.Visible = false;
                    }
                    else
                    {
                        HttpContext.Current.Session["smart_record_keyin_type"] = contract.Rows[0]["smart_record_keyin_type"].ToString();
                        lb1.Visible = true;
                        lb1.Text = "ยอดค่า Service Charge";
                        txtservice_serCharge_amount.Visible = false;
                        txtservice_serCharge_amount2.Visible = true;
                    }


                    if (!string.IsNullOrEmpty(Request.QueryString["idhis"]))
                    {
                        var trans = Serv.GetTransaction_detail(Request.QueryString["idhis"].ToString(), Request.QueryString["id"].ToString());
                        if (trans.Rows.Count != 0)
                        {
                            txtdate.Text = Convert.ToDateTime(trans.Rows[0]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            txtproduct_amount.Text = Convert.ToDecimal(trans.Rows[0]["product_amount"].ToString()).ToString("#,##0.00");

                            txtdate.ReadOnly = true;

                            hdd_current_1.Value = trans.Rows[0]["product_amount"].ToString();

                            HttpContext.Current.Session["tran_status"] = trans.Rows[0]["status"].ToString();

                            if (trans.Rows[0]["no_type"].ToString() != "")
                            {
                                Panel3.Visible = true;
                                ddlreason.Text = trans.Rows[0]["no_type"].ToString();
                                txtdetal.Text = trans.Rows[0]["remark"].ToString();
                            }
                            else
                            {
                                Panel3.Visible = false;
                            }


                            if (trans.Rows[0]["flag_confirm"].ToString() == "y")
                            {
                                Panel2.Visible = false;
                            }
                            else
                            {
                                Panel2.Visible = true;

                            }

                            if (HttpContext.Current.Session["smart_record_keyin_type"].ToString() == "prod_service")
                            {
                                if (trans.Rows[0]["service_serCharge_amount"].ToString() == "0.00")
                                {
                                    txtservice_serCharge_amount.Text = "";
                                    hdd_current_2.Value = "0";
                                }
                                else
                                {
                                    txtservice_serCharge_amount.Text = Convert.ToDecimal(trans.Rows[0]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                    hdd_current_2.Value = trans.Rows[0]["service_serCharge_amount"].ToString();
                                }

                            }
                            else if (HttpContext.Current.Session["smart_record_keyin_type"].ToString() == "prod_ser_charge")
                            {
                                if (trans.Rows[0]["service_serCharge_amount"].ToString() == "0.00")
                                {
                                    txtservice_serCharge_amount2.Text = "";
                                    hdd_current_2.Value = "0";
                                }
                                else
                                {
                                    txtservice_serCharge_amount2.Text = Convert.ToDecimal(trans.Rows[0]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                    hdd_current_2.Value = trans.Rows[0]["service_serCharge_amount"].ToString();
                                }

                            }
                            else
                            {
                                hdd_current_2.Value = "0";
                            }

                        }
                    }

                    if (HttpContext.Current.Session["code_theme"] != null)
                    {
                        this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
                    }
                    else
                    {
                        this.MyTheme = "#CCA9DA";
                    }

                    if (HttpContext.Current.Session["code_Navbar"] != null)
                    {
                        this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
                    }
                    else
                    {
                        this.NavbarColor = "#2d2339";
                    }


                }
                else
                {
                    Response.Redirect("~/transaction_list_tenant.aspx", false);
                }
            }
            else
            {
                Response.Redirect("~/transaction_list_tenant.aspx", false);
            }

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "showalert", sb.ToString(), true);

        }


        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/transaction_list_tenant.aspx", false);
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            LogService l = new LogService();

            string existing = "n";
            dateconvert cc = new dateconvert();

            string d2 = DateTime.Now.ToString("yyyy-MM-dd", EngCI);
            string d1 = cc.con_date(txtdate.Text);
            int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
            if (result > 0)
            {
                txtdate.Focus();
                POPUPMSG("วันที่บันทึกยอดห้ามมากกว่าวันปัจจุบัน");
                return;
            }


            var cont = Serv.GetContract_ShopOper_Close(Request.QueryString["id"].ToString());
            if (cont.Rows.Count != 0)
            {
                //d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_close"]).ToString("yyyy-MM-dd", EngCI);
                //d1 = cc.con_date(txtdate.Text);
                //result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                //if (result > 0)
                //{
                //    txtdate.Focus();
                //    POPUPMSG("วันที่บันทึกยอดห้ามมากกว่าวันปิดร้าน");
                //    return;
                //}

                d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_open"]).ToString("yyyy-MM-dd", EngCI);
                d1 = cc.con_date(txtdate.Text);
                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                if (result < 0)
                {
                    txtdate.Focus();
                    POPUPMSG("วันที่บันทึกยอดห้ามน้อยกว่าวันเปิดร้าน");
                    return;
                }
            }




            if (txtproduct_amount.Text == "")
            {
                POPUPMSG("กรุณากรอกยอดขายรวม");
                txtproduct_amount.Focus();
                return;
            }

            if (string.IsNullOrEmpty(Request.QueryString["idhis"]))
            {
                var exist = Serv.GetTransactionByDate(cc.con_date(txtdate.Text), Request.QueryString["id"].ToString());
                if (exist.Rows.Count != 0)
                {
                    if (exist.Rows[0]["status"].ToString() == "y")
                    {
                        POPUPMSG("วันที่ที่เลือกมีการคีย์ยอดแล้ว");
                        txtdate.Focus();
                        return;
                    }
                    else
                    {
                        existing = exist.Rows[0]["id"].ToString();
                    }
                    HttpContext.Current.Session["tran_status"] = exist.Rows[0]["status"].ToString();
                }
            }


            if (txtproduct_amount.Text == "")
            {
                POPUPMSG("กรุณากรอกยอดขายรวม");
                txtproduct_amount.Focus();
                return;
            }

            int result3 = txtproduct_amount.Text.ToCharArray().Count(c => c == '.');
            if (result3 > 1)
            {
                POPUPMSG("รูปแบบของยอดค่าบริการไม่ถูกต้อง");
                txtproduct_amount.Focus();
                return;
            }

            if (HttpContext.Current.Session["smart_record_keyin_type"].ToString() == "prod_service")
            {
                if (txtservice_serCharge_amount.Text == "")
                {
                    txtservice_serCharge_amount.Text = "0";
                }

                int result3_ = txtservice_serCharge_amount.Text.ToCharArray().Count(c => c == '.');
                if (result3_ > 1)
                {
                    POPUPMSG("รูปแบบของยอดค่าบริการไม่ถูกต้อง");
                    txtservice_serCharge_amount.Focus();
                    return;
                }
            }
            else if (HttpContext.Current.Session["smart_record_keyin_type"].ToString() == "prod_ser_charge")
            {
                if (txtservice_serCharge_amount2.Text == "")
                {
                    txtservice_serCharge_amount2.Text = "0";
                }

                int result3_ = txtservice_serCharge_amount2.Text.ToCharArray().Count(c => c == '.');
                if (result3_ > 1)
                {
                    POPUPMSG("รูปแบบของยอดค่าบริการไม่ถูกต้อง");
                    txtservice_serCharge_amount2.Focus();
                    return;
                }
            }

            if (ddlreason.SelectedValue == "")
            {
                if (FileUpload1.HasFile == false)
                {
                    POPUPMSG("กรุณาเลือกไฟล์");
                    return;
                }

                int vat = 0;
                var v = Serv.GetVAT(Request.QueryString["id"].ToString().Substring(0, 3));
                if (v.Rows.Count != 0)
                {
                    vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
                }
                else
                {
                    vat = 7;
                }

                if (FileUpload1.HasFile) // || FileUpload2.HasFile || FileUpload3.HasFile
                {

                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Start Insert Transaction : " + Request.QueryString["id"].ToString(), "Web_Transaction_insert");

                    try
                    {
                        string x = "0";

                        if (HttpContext.Current.Session["smart_record_keyin_type"].ToString() == "prod_service")
                        {
                            if (txtservice_serCharge_amount.Text != "")
                            {
                                x = txtservice_serCharge_amount.Text.Replace(",", "")
                                     .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                    .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                    .Replace("+", "").Replace("-", "").Replace("฿", "");
                            }
                        }
                        else if (HttpContext.Current.Session["smart_record_keyin_type"].ToString() == "prod_ser_charge")
                        {
                            if (txtservice_serCharge_amount2.Text != "")
                            {
                                x = txtservice_serCharge_amount2.Text.Replace(",", "")
                                     .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                    .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                    .Replace("+", "").Replace("-", "").Replace("฿", "");
                            }
                        }




                        if (!string.IsNullOrEmpty(Request.QueryString["idhis"]))
                        {
                            if (HttpContext.Current.Session["tran_status"].ToString() == "y")
                            {
                                Serv.Update_transaction_recor_1(Request.QueryString["id"].ToString(), txtproduct_amount.Text.Replace(",", "")
                                     .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                    .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                    .Replace("+", "").Replace("-", "").Replace("฿", ""), x, "y", cc.con_date(txtdate.Text), ddlreason.SelectedValue
                                        , txtdetal.Text, Request.QueryString["idhis"].ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), HttpContext.Current.Session["s_userid"].ToString(), vat, "te", "web");
                            }
                            else
                            {

                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Update ยอดที่ไม่ส่งยอด : " + Request.QueryString["id"].ToString() + " = " + txtproduct_amount.Text
                                 , "Web_Transaction_insert");


                                Serv.Update_transaction_recor_createDate_Stamp(Request.QueryString["id"].ToString(), txtproduct_amount.Text.Replace(",", "")
                                     .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                    .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                    .Replace("+", "").Replace("-", "").Replace("฿", ""), x, "y", cc.con_date(txtdate.Text),
                                    ddlreason.SelectedValue, txtdetal.Text, Request.QueryString["idhis"].ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), HttpContext.Current.Session["s_userid"].ToString(), vat, "te"
                                    , "web");
                            }

                            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": First File Name : " + FileUpload1.FileName, "Web_Transaction_insert");

                            if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                            {
                                System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                            }

                            if (FileUpload1.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload1.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload1.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";


                                    Bitmap originalBMP = new Bitmap(FileUpload1.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();

                                    //FileUpload1.SaveAs(Server.MapPath(img_name1));


                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");

                                    Serv.tbltransaction_record_img(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    //string xx = DateTime.Now.ToString("ssssHHddMM") + filename1;

                                    //string img_name1 = "~/img/slip/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                                    //string img_name2 = HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;

                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                        Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    //string xx = DateTime.Now.ToString("ssssHHddMM") + filename1;

                                    //string img_name1 = "~/img/slip/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                                    //string img_name2 = HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;

                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";


                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {

                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {

                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                            }

                            if (FileUpload2.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload2.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload2.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                    Bitmap originalBMP = new Bitmap(FileUpload2.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();


                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                    //FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }

                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }

                            }

                            if (FileUpload3.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload3.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload3.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                                         Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                    Bitmap originalBMP = new Bitmap(FileUpload3.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();


                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert"); //FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";

                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }

                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);

                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);

                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);

                                }
                            }

                            if (FileUpload4.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload4.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload4.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                    Bitmap originalBMP = new Bitmap(FileUpload4.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                    //FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";

                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }

                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                            }

                            if (FileUpload5.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload5.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload5.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";
                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");


                                    Bitmap originalBMP = new Bitmap(FileUpload5.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();


                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                    //FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }

                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }


                            }

                            if (FileUpload6.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload6.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload6.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                    {
                                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                    }

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                    Bitmap originalBMP = new Bitmap(FileUpload6.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();


                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                    //FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";


                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }

                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(Request.QueryString["idhis"].ToString(), img_name3, tmp_, filename1);
                                }
                            }

                        }
                        else if (existing != "n")
                        {
                            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Update ยอดที่ไม่ส่งยอด TranID = " + existing + " | Contract Number = " + Request.QueryString["id"].ToString(), "Web_Transaction_insert");

                            Serv.Update_transaction_recor_createDate_Stamp(Request.QueryString["id"].ToString(), txtproduct_amount.Text.Replace(",", "")
                                 .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                    .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                    .Replace("+", "").Replace("-", "").Replace("฿", ""), x, "y", cc.con_date(txtdate.Text), ddlreason.SelectedValue
                              , txtdetal.Text, existing, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), HttpContext.Current.Session["s_userid"].ToString(), vat, "te", "web");

                            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": First File Name : " + FileUpload1.FileName, "Web_Transaction_insert");

                            if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                            {
                                System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                            }

                            if (FileUpload1.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload1.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload1.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";
                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");


                                    Bitmap originalBMP = new Bitmap(FileUpload1.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();

                                    //FileUpload1.SaveAs(Server.MapPath(img_name1));

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");

                                    Serv.tbltransaction_record_img(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";


                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }

                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload1.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                            }

                            if (FileUpload2.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload2.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload2.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";
                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");


                                    Bitmap originalBMP = new Bitmap(FileUpload2.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();



                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                    //FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }

                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";

                                    FileUpload2.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                            }

                            if (FileUpload3.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload3.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload3.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";
                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");


                                    Bitmap originalBMP = new Bitmap(FileUpload3.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                    //FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";

                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);

                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);

                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload3.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);

                                }
                            }

                            if (FileUpload4.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload4.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload4.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                    Bitmap originalBMP = new Bitmap(FileUpload4.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();


                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                    //FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";

                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                            }

                            if (FileUpload5.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload5.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload5.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");


                                    Bitmap originalBMP = new Bitmap(FileUpload5.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();


                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");

                                    //FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }


                            }

                            if (FileUpload6.HasFile)
                            {
                                string filename1 = Path.GetFileName(FileUpload6.FileName);
                                FileInfo fi1 = new FileInfo(FileUpload6.FileName);
                                string ext = fi1.Extension.ToLower();
                                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/img.png";

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                    Bitmap originalBMP = new Bitmap(FileUpload6.FileContent);
                                    // Calculate the new image dimensions
                                    int origWidth = originalBMP.Width;
                                    int origHeight = originalBMP.Height;

                                    int newWidth;
                                    int newHeight;

                                    if (origWidth > origHeight)
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }
                                    else
                                    {
                                        newWidth = 600;
                                        newHeight = 400;
                                    }


                                    // Create a new bitmap which will hold the previous resized bitmap
                                    Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                    // Create a graphic based on the new bitmap
                                    Graphics oGraphics = Graphics.FromImage(newBMP);

                                    // Set the properties for the new graphic file
                                    oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    // Draw the new graphic based on the resized bitmap
                                    oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                    newBMP.Save(Server.MapPath(img_name1));
                                    // Once finished with the bitmap objects, we deallocate them.
                                    originalBMP.Dispose();
                                    newBMP.Dispose();
                                    oGraphics.Dispose();


                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                        Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                    //FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_img(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".pdf" || ext == ".PDF")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/pdf.png";

                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/doc.png";


                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }

                                else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/xls.png";


                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else if (ext == ".txt")
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/txt.png";


                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                                else
                                {
                                    Random _r = new Random();
                                    String n = _r.Next(0, 999999).ToString("D6");
                                    string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                    string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                    string img_name3 = "/img/other.png";


                                    FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    Serv.tbltransaction_record_pdf(existing, img_name3, tmp_, filename1);
                                }
                            }

                        }
                        else
                        {

                            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert " + Request.QueryString["id"].ToString(), "Web_Transaction_insert");

                            var indent = Serv.Insert_transaction_record(Request.QueryString["id"].ToString(), txtproduct_amount.Text.Replace(",", "")
                                 .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                    .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                    .Replace("+", "").Replace("-", "").Replace("฿", ""), x, "y", cc.con_date(txtdate.Text),
                                ddlreason.SelectedValue, txtdetal.Text, vat, HttpContext.Current.Session["smart_record_keyin_type"].ToString(), HttpContext.Current.Session["s_userid"].ToString(), "te", "web");

                            if (indent.Rows.Count != 0)
                            {
                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": First File Name : " + FileUpload1.FileName, "Web_Transaction_insert");

                                if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                                {
                                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                                }

                                if (FileUpload1.HasFile)
                                {
                                    string filename1 = Path.GetFileName(FileUpload1.FileName);
                                    FileInfo fi1 = new FileInfo(FileUpload1.FileName);
                                    string ext = fi1.Extension.ToLower();
                                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/img.png";

                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                        Bitmap originalBMP = new Bitmap(FileUpload1.FileContent);
                                        // Calculate the new image dimensions
                                        int origWidth = originalBMP.Width;
                                        int origHeight = originalBMP.Height;

                                        int newWidth;
                                        int newHeight;

                                        if (origWidth > origHeight)
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }
                                        else
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }


                                        // Create a new bitmap which will hold the previous resized bitmap
                                        Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                        // Create a graphic based on the new bitmap
                                        Graphics oGraphics = Graphics.FromImage(newBMP);

                                        // Set the properties for the new graphic file
                                        oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                        // Draw the new graphic based on the resized bitmap
                                        oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                        newBMP.Save(Server.MapPath(img_name1));
                                        // Once finished with the bitmap objects, we deallocate them.
                                        originalBMP.Dispose();
                                        newBMP.Dispose();
                                        oGraphics.Dispose();

                                        //FileUpload1.SaveAs(Server.MapPath(img_name1));

                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                            Request.QueryString["id"].ToString(), "Web_Transaction_insert");

                                        Serv.tbltransaction_record_img(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);
                                    }
                                    else if (ext == ".pdf" || ext == ".PDF")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/pdf.png";



                                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);


                                    }
                                    else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/doc.png";


                                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }

                                    else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/xls.png";


                                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }
                                    else if (ext == ".txt")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/txt.png";


                                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }
                                    else
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/other.png";


                                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }
                                }

                                if (FileUpload2.HasFile)
                                {
                                    string filename1 = Path.GetFileName(FileUpload2.FileName);
                                    FileInfo fi1 = new FileInfo(FileUpload2.FileName);
                                    string ext = fi1.Extension.ToLower();
                                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/img.png";
                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");


                                        Bitmap originalBMP = new Bitmap(FileUpload2.FileContent);
                                        // Calculate the new image dimensions
                                        int origWidth = originalBMP.Width;
                                        int origHeight = originalBMP.Height;

                                        int newWidth;
                                        int newHeight;

                                        if (origWidth > origHeight)
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }
                                        else
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }


                                        // Create a new bitmap which will hold the previous resized bitmap
                                        Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                        // Create a graphic based on the new bitmap
                                        Graphics oGraphics = Graphics.FromImage(newBMP);

                                        // Set the properties for the new graphic file
                                        oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                        // Draw the new graphic based on the resized bitmap
                                        oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                        newBMP.Save(Server.MapPath(img_name1));
                                        // Once finished with the bitmap objects, we deallocate them.
                                        originalBMP.Dispose();
                                        newBMP.Dispose();
                                        oGraphics.Dispose();


                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                            Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                        //FileUpload2.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_img(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);
                                    }
                                    else if (ext == ".pdf" || ext == ".PDF")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/pdf.png";


                                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);
                                    }

                                    else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/doc.png";


                                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }

                                    else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/xls.png";


                                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }
                                    else if (ext == ".txt")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/txt.png";


                                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }
                                    else
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/other.png";


                                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }
                                }

                                if (FileUpload3.HasFile)
                                {
                                    string filename1 = Path.GetFileName(FileUpload3.FileName);
                                    FileInfo fi1 = new FileInfo(FileUpload3.FileName);
                                    string ext = fi1.Extension.ToLower();
                                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/img.png";
                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");


                                        Bitmap originalBMP = new Bitmap(FileUpload3.FileContent);
                                        // Calculate the new image dimensions
                                        int origWidth = originalBMP.Width;
                                        int origHeight = originalBMP.Height;

                                        int newWidth;
                                        int newHeight;

                                        if (origWidth > origHeight)
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }
                                        else
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }


                                        // Create a new bitmap which will hold the previous resized bitmap
                                        Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                        // Create a graphic based on the new bitmap
                                        Graphics oGraphics = Graphics.FromImage(newBMP);

                                        // Set the properties for the new graphic file
                                        oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                        // Draw the new graphic based on the resized bitmap
                                        oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                        newBMP.Save(Server.MapPath(img_name1));
                                        // Once finished with the bitmap objects, we deallocate them.
                                        originalBMP.Dispose();
                                        newBMP.Dispose();
                                        oGraphics.Dispose();

                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                            Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                        //FileUpload3.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_img(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);
                                    }
                                    else if (ext == ".pdf" || ext == ".PDF")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/pdf.png";


                                        FileUpload3.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);
                                    }

                                    else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/doc.png";


                                        FileUpload3.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }

                                    else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/xls.png";


                                        FileUpload3.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }
                                    else if (ext == ".txt")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/txt.png";


                                        FileUpload3.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }
                                    else
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/other.png";


                                        FileUpload3.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                    }
                                }

                                if (FileUpload4.HasFile)
                                {
                                    string filename1 = Path.GetFileName(FileUpload4.FileName);
                                    FileInfo fi1 = new FileInfo(FileUpload4.FileName);
                                    string ext = fi1.Extension.ToLower();
                                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/img.png";

                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                        Bitmap originalBMP = new Bitmap(FileUpload4.FileContent);
                                        // Calculate the new image dimensions
                                        int origWidth = originalBMP.Width;
                                        int origHeight = originalBMP.Height;

                                        int newWidth;
                                        int newHeight;

                                        if (origWidth > origHeight)
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }
                                        else
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }


                                        // Create a new bitmap which will hold the previous resized bitmap
                                        Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                        // Create a graphic based on the new bitmap
                                        Graphics oGraphics = Graphics.FromImage(newBMP);

                                        // Set the properties for the new graphic file
                                        oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                        // Draw the new graphic based on the resized bitmap
                                        oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                        newBMP.Save(Server.MapPath(img_name1));
                                        // Once finished with the bitmap objects, we deallocate them.
                                        originalBMP.Dispose();
                                        newBMP.Dispose();
                                        oGraphics.Dispose();

                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                            Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                        //FileUpload4.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_img(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);
                                    }
                                    else if (ext == ".pdf" || ext == ".PDF")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/pdf.png";

                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload4.SaveAs(Server.MapPath(img_name1));

                                    }
                                    else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/doc.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    }

                                    else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/xls.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    }
                                    else if (ext == ".txt")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/txt.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    }
                                    else
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/other.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload4.SaveAs(Server.MapPath(img_name1));
                                    }
                                }

                                if (FileUpload5.HasFile)
                                {
                                    string filename1 = Path.GetFileName(FileUpload5.FileName);
                                    FileInfo fi1 = new FileInfo(FileUpload5.FileName);
                                    string ext = fi1.Extension.ToLower();
                                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/img.png";

                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                        Bitmap originalBMP = new Bitmap(FileUpload5.FileContent);
                                        // Calculate the new image dimensions
                                        int origWidth = originalBMP.Width;
                                        int origHeight = originalBMP.Height;

                                        int newWidth;
                                        int newHeight;

                                        if (origWidth > origHeight)
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }
                                        else
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }


                                        // Create a new bitmap which will hold the previous resized bitmap
                                        Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                        // Create a graphic based on the new bitmap
                                        Graphics oGraphics = Graphics.FromImage(newBMP);

                                        // Set the properties for the new graphic file
                                        oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                        // Draw the new graphic based on the resized bitmap
                                        oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                        newBMP.Save(Server.MapPath(img_name1));
                                        // Once finished with the bitmap objects, we deallocate them.
                                        originalBMP.Dispose();
                                        newBMP.Dispose();
                                        oGraphics.Dispose();


                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                            Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                        //FileUpload5.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_img(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);
                                    }
                                    else if (ext == ".pdf" || ext == ".PDF")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/pdf.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload5.SaveAs(Server.MapPath(img_name1));

                                    }
                                    else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/doc.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    }

                                    else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/xls.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    }

                                    else if (ext == ".txt")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/txt.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    }
                                    else
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/other.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload5.SaveAs(Server.MapPath(img_name1));
                                    }
                                }

                                if (FileUpload6.HasFile)
                                {
                                    string filename1 = Path.GetFileName(FileUpload6.FileName);
                                    FileInfo fi1 = new FileInfo(FileUpload6.FileName);
                                    string ext = fi1.Extension.ToLower();
                                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;

                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/img.png";

                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                                        Bitmap originalBMP = new Bitmap(FileUpload6.FileContent);
                                        // Calculate the new image dimensions
                                        int origWidth = originalBMP.Width;
                                        int origHeight = originalBMP.Height;

                                        int newWidth;
                                        int newHeight;

                                        if (origWidth > origHeight)
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }
                                        else
                                        {
                                            newWidth = 600;
                                            newHeight = 400;
                                        }


                                        // Create a new bitmap which will hold the previous resized bitmap
                                        Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                                        // Create a graphic based on the new bitmap
                                        Graphics oGraphics = Graphics.FromImage(newBMP);

                                        // Set the properties for the new graphic file
                                        oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                        // Draw the new graphic based on the resized bitmap
                                        oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                                        newBMP.Save(Server.MapPath(img_name1));
                                        // Once finished with the bitmap objects, we deallocate them.
                                        originalBMP.Dispose();
                                        newBMP.Dispose();
                                        oGraphics.Dispose();


                                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                                            Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                                        //FileUpload6.SaveAs(Server.MapPath(img_name1));
                                        Serv.tbltransaction_record_img(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);
                                    }
                                    else if (ext == ".pdf" || ext == ".PDF")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/pdf.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload6.SaveAs(Server.MapPath(img_name1));

                                    }
                                    else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/doc.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    }

                                    else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/xls.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    }

                                    else if (ext == ".txt")
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/txt.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    }
                                    else
                                    {
                                        Random _r = new Random();
                                        String n = _r.Next(0, 999999).ToString("D6");
                                        string tmp_ = DateTime.Now.ToString("yyyy-MM-dd") + "/" + "w_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" + Request.QueryString["id"].ToString() + ext;
                                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                                        string img_name3 = "/img/other.png";


                                        Serv.tbltransaction_record_pdf(indent.Rows[0]["last_id"].ToString(), img_name3, tmp_, filename1);

                                        FileUpload6.SaveAs(Server.MapPath(img_name1));
                                    }
                                }

                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Error " + ex.ToString(), "Web_Transaction_insert");
                        return;

                    }

                    Response.Redirect("~/transaction_list_tenant.aspx", false);

                }
                else
                {
                    POPUPMSG("กรุณาเลือก File รูปภาพ !?!");
                    return;
                }



            }
            else
            {

                if (!string.IsNullOrEmpty(Request.QueryString["idhis"]))
                {

                    if (HttpContext.Current.Session["tran_status"].ToString() == "y")
                    {

                        Serv.Update_transaction_recor_1(Request.QueryString["id"].ToString(), "0", "0", "y", cc.con_date(txtdate.Text), ddlreason.SelectedValue
                                    , txtdetal.Text, Request.QueryString["idhis"].ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                    HttpContext.Current.Session["s_userid"].ToString(), 0, "te", "web");

                    }
                    else
                    {

                        Serv.Update_transaction_recor_createDate_Stamp(Request.QueryString["id"].ToString(), "0", "0", "y", cc.con_date(txtdate.Text), ddlreason.SelectedValue
                              , txtdetal.Text, Request.QueryString["idhis"].ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                              HttpContext.Current.Session["s_userid"].ToString(), 0, "te", "web");
                    }
                }
                else if (existing != "n")
                {
                    Serv.Update_transaction_recor_createDate_Stamp(Request.QueryString["id"].ToString(), "0", "0", "y", cc.con_date(txtdate.Text), ddlreason.SelectedValue
                                , txtdetal.Text, existing, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                HttpContext.Current.Session["s_userid"].ToString(), 0, "te", "web");

                }
                else
                {
                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert Nobill " + Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                    var indent = Serv.Insert_transaction_record(Request.QueryString["id"].ToString(), "0", "0", "y", cc.con_date(txtdate.Text), ddlreason.SelectedValue
                        , txtdetal.Text, 0, HttpContext.Current.Session["smart_record_keyin_type"].ToString(), HttpContext.Current.Session["s_userid"].ToString(), "te", "web");
                }

                Response.Redirect("~/transaction_list_tenant.aspx", false);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='transaction_list_tenant.aspx';", true);

            }



        }


        protected void ddlreason_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlreason.SelectedValue == "")
            {
                txtdetal.ReadOnly = true;


                txtproduct_amount.ReadOnly = false;
                txtservice_serCharge_amount.ReadOnly = false;
                txtservice_serCharge_amount2.ReadOnly = false;

                FileUpload1.Visible = true;
                FileUpload2.Visible = true;
                FileUpload3.Visible = true;
                FileUpload4.Visible = true;
                FileUpload5.Visible = true;
                FileUpload6.Visible = true;

                Label1.Visible = true;
            }
            else
            {
                txtproduct_amount.Text = "0";
                txtservice_serCharge_amount.Text = "0";
                txtservice_serCharge_amount2.Text = "0";
                txtdetal.ReadOnly = false;

                txtproduct_amount.ReadOnly = true;
                txtservice_serCharge_amount.ReadOnly = true;
                txtservice_serCharge_amount2.ReadOnly = true;

                FileUpload1.Visible = false;
                FileUpload2.Visible = false;
                FileUpload3.Visible = false;
                FileUpload4.Visible = false;
                FileUpload5.Visible = false;
                FileUpload6.Visible = false;

                Label1.Visible = false;

            }
        }

    }
}