﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="record_transaction_tenant.aspx.cs" Inherits="iconsiam.record_transaction_tenant" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }


        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10005;
        }

        function lockoutSubmit(button) {
            var oldValue = button.value;

            setTimeout(function () {
                button.setAttribute('disabled', true);
                button.value = 'ระบบกำลังดำเนินการ';
            }, 0);

            setTimeout(function () {
                button.value = oldValue;
                button.removeAttribute('disabled');
            }, 500000);
        }


        function showProgress() {
            var updateProgress = $get("<%= UpdateProg1.ClientID %>");
            updateProgress.style.display = "block";
        }


    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link href="css/ProgressNotifier.css" rel="stylesheet" type="text/css" />


    <style type="text/css">
        .test .ajax__calendar_container {
            padding: 10px;
            position: absolute;
            cursor: default;
            width: 300px;
            font-size: 15px;
            text-align: center;
            font-family: tahoma,erdana,helvetica;
            background-color: lemonchiffon;
            border: 1px solid #646464;
            z-index: 10000 !important;
            text-wrap: none;
        }

        .test .ajax__calendar_dayname {
            height: 17px;
            width: 34px;
            text-align: center;
        }

          .ThemeBtn {
            background-color: <%= MyTheme %> !important;
            Color: <%= NavbarColor %>;
        }

        .ThemeBroder {
            border: 1px solid <%= MyTheme %>;
        }
    </style>




    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Add Transaction
            </p>
        </div>
    </div>
    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">
            <asp:HiddenField ID="hdd_current_2" runat="server" />
            <asp:HiddenField ID="hdd_current_1" runat="server" />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Label ID="lbshopname" runat="server" Font-Size="18px" Text="-"></asp:Label>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Label ID="lbroomnum" runat="server" Font-Size="18px" Text="-"></asp:Label>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtdate" runat="server" class="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="txtdate_CalendarExtender" runat="server" BehaviorID="txtdate_CalendarExtender" TargetControlID="txtdate"
                        OnClientShown="calendarShown" Format="dd/MM/yyyy"></asp:CalendarExtender>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ยอดขายรวม VAT
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtproduct_amount" runat="server" placeholder="กรอกยอดขายรวม"  class="form-control ThemeBroder"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Label ID="lb1" Visible="false" runat="server" Text="-"></asp:Label>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtservice_serCharge_amount" runat="server" placeholder="กรอกยอดบริการ" class="form-control" Style="border: 1px solid #dcd135;"></asp:TextBox>
                    <asp:TextBox ID="txtservice_serCharge_amount2" runat="server" placeholder="กรอกยอด Service Charge" class="form-control" Visible="false" Style="border: 1px solid #dcd135;"></asp:TextBox>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    สถานะยอดขายวันนี้ : 
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddlreason" runat="server" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlreason_SelectedIndexChanged" class="form-control ThemeBroder">
                    </asp:DropDownList>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    หมายเหตุ : 
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtdetal" runat="server" ReadOnly="true" Style="border: 1px solid #dcd135;" TextMode="MultiLine" class="form-control" Height="150px"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <asp:Panel ID="Panel1" runat="server">
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <asp:FileUpload ID="FileUpload1" runat="server" class="form-control" />
                    </div>
                    <div class="col-md-1 col-lg-1">
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </div>
                </div>
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server">
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <asp:FileUpload ID="FileUpload2" runat="server" class="form-control" />
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel3" runat="server">
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <asp:FileUpload ID="FileUpload3" runat="server" class="form-control" />
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel4" runat="server">
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <asp:FileUpload ID="FileUpload4" runat="server" class="form-control" />
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel5" runat="server">
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <asp:FileUpload ID="FileUpload5" runat="server" class="form-control" />
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel6" runat="server">
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <asp:FileUpload ID="FileUpload6" runat="server" class="form-control" />
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <br />
            </asp:Panel>

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" OnClientClick="showProgress()" ForeColor="White" class="btn btn-success" Width="180px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" ForeColor="White" class="btn btn-danger" Width="180px" />
                </div>
            </div>

        </div>
    </div>


    <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
        <ProgressTemplate>
            <div class="ModalProgressContainer">
                <div class="ModalProgressContent" style="text-align: center; position: absolute; font-family: Tahoma; font-size: 10pt">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/img/loading_trans.gif" Width="200px" Height="20px" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>






</asp:Content>
