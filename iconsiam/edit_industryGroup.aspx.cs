﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class edit_industryGroup : System.Web.UI.Page
    {
        industryGroupDLL Serv = new industryGroupDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtgroupcode.Focus();
                    txtgroupcode.Attributes.Add("onkeypress", "return next_tools(event,'" + txtgroupname_en.ClientID + "')");
                    txtgroupname_en.Attributes.Add("onkeypress", "return next_tools(event,'" + txtgroupname_th.ClientID + "')");
                    txtgroupname_th.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsave.ClientID + "')");

                    bind_default();
                    dinb_data();
                }
            }
        }

        protected void bind_default()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));
        }

        protected void dinb_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var indus = Serv.getIndustryGroupById(Request.QueryString["id"].ToString());
                if (indus.Rows.Count != 0)
                {
                    txtgroupcode.Text = indus.Rows[0]["IndustryGroupCode"].ToString();
                    txtgroupname_en.Text = indus.Rows[0]["IndustryGroupNameEN"].ToString();
                    txtgroupname_th.Text = indus.Rows[0]["IndustryGroupNameTH"].ToString();
                    ddlstatus.SelectedValue = indus.Rows[0]["IsActive"].ToString();
                }
                else
                {
                    Response.Redirect("~/industryGroup.aspx");
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtgroupcode.Text != "" && txtgroupname_en.Text != "" && txtgroupname_th.Text != "")
            {
                var chk_exist = Serv.getIndustryGroupByGroupCode_nid(txtgroupcode.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), Request.QueryString["id"].ToString());
                var ex1 = Serv.getIndustryGroupByGroupNameEN_nid(txtgroupname_en.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), Request.QueryString["id"].ToString());
                var ex2 = Serv.getIndustryGroupByGroupNameTH_nid(txtgroupname_th.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), Request.QueryString["id"].ToString());
                if (ex1.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Industry Group Name(EN) ซ้ำ");
                    return;
                }
                else if (ex2.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Industry Group Name(TH) ซ้ำ");
                    return;
                }
                else if (chk_exist.Rows.Count != 0)
                {
                    POPUPMSG("Industry Group Code ซ้ำในระบบ");
                    txtgroupcode.Focus();
                }
                else
                {
                    Serv.UpdateIndustryGroup(txtgroupcode.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                        txtgroupname_en.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                        txtgroupname_th.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlstatus.SelectedValue, Request.QueryString["id"].ToString(),
                        HttpContext.Current.Session["s_userid"].ToString());

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='industryGroup.aspx';", true);

                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/industryGroup.aspx");
        }



    }
}