﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CompanyGroupMgm_edit_pic.aspx.cs" Inherits="iconsiam.CompanyGroupMgm_edit_pic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12  col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Edit Picture Group Company
            </p>
        </div>
    </div>

    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">
            <div class="row">
                <div class="col-md-9">
                    <asp:Image ID="imgCompany" runat="server" Width="150px"/>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9">
                    <asp:FileUpload ID="fileupload_logo" runat="server" />
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
          
            <div class="row">
                <div class="col-md-9">
                    <asp:TextBox ID="txtCompanyGroupName" MaxLength="10" runat="server" placeholder="Group Company Name" class="form-control" ReadOnly ="true"></asp:TextBox>
                </div>
            </div>
            <br />           
            
            <div class="row">
                <div class="col-md-9">
                    <asp:DropDownList ID="ddlTheme" runat="server" class="form-control" OnSelectedIndexChanged="ddlTheme_OnSelectedIndexChanged" AutoPostBack="true">             
                    </asp:DropDownList>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

           <div class="row">
                <div class="col-9 text-center mt-5 mb-5">
                       <asp:Image ID="Image1" runat="server" Width="70%" />
                </div>
            </div>
             <br />

            <br />
            <div class="row">
                <div class="col-md-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ForeColor="White" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" ForeColor="White" class="btn btn-danger" Width="114px" />
                </div>
            </div>

        </div>
    </div>

</asp:Content>
