﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EmailTemplate_CompanyGroup.aspx.cs" Inherits="iconsiam.EmailTemplate_CompanyGroup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Email Template Company Group
            </p>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtcompanygroup" runat="server" class="form-control" placeholder="Company"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" OnClick="btnsearch_Click" Text="Search"  class="btn ThemeBtn" />
                     <asp:Button ID="btncancel" runat="server" Text="Clear" OnClick="btncancel_Click" class="btn ThemeBtn" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="ComGroupID" HeaderText="Company Code" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />
                            <asp:BoundField DataField="ComGroupName" HeaderText="Company Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                            <asp:BoundField DataField="username" HeaderText="Username" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Et_UpdateDate" HeaderText="Update Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />

                            <asp:TemplateField HeaderText="Edit company Group" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddCompanygroup" runat="server" Value='<%# Eval("ComGroupID") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="Edit Template" OnClick="btnedit_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Edit company" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddCompanygroup2" runat="server" Value='<%# Eval("ComGroupID") %>' />
                                    <asp:Button ID="btnedit2" runat="server" Text="Company detail" OnClick="btnedit2_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
