﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class user_mgm : System.Web.UI.Page
    {
        user_mgmDLL Serv = new user_mgmDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_data();
                }
            }
        }

        protected void bind_default()
        {
            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("Active", "y"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "n"));

            ddlrole.Items.Clear();

            if (HttpContext.Current.Session["role"].ToString() == "super_admin")
            {
                ddlrole.Items.Insert(0, new ListItem("Select Role", ""));
                ddlrole.Items.Insert(1, new ListItem("Super Admin", "super_admin"));
                ddlrole.Items.Insert(2, new ListItem("Datacenter", "datacenter"));
                ddlrole.Items.Insert(3, new ListItem("Admin", "admin"));
                ddlrole.Items.Insert(4, new ListItem("Officer", "officer"));
                ddlrole.Items.Insert(5, new ListItem("Manager", "manager"));
                ddlrole.Items.Insert(6, new ListItem("VP", "vp"));
                ddlrole.Items.Insert(7, new ListItem("Management", "management"));
            }
            else
            {
                ddlrole.Items.Insert(0, new ListItem("Select Role", ""));
                //ddlrole.Items.Insert(1, new ListItem("Super Admin", "super_admin"));
                //ddlrole.Items.Insert(1, new ListItem("Datacenter", "datacenter"));
                ddlrole.Items.Insert(1, new ListItem("Admin", "admin"));
                ddlrole.Items.Insert(2, new ListItem("Officer", "officer"));
                ddlrole.Items.Insert(3, new ListItem("Manager", "manager"));
                ddlrole.Items.Insert(4, new ListItem("VP", "vp"));
                ddlrole.Items.Insert(5, new ListItem("Management", "management"));
            }

        }
        protected void bind_data()
        {
            string[] name = txtname.Text.Split(',');

            var user = Serv.GetUser(name, ddlrole.SelectedValue, ddlstatus.SelectedValue,
                HttpContext.Current.Session["s_com_code"].ToString());
            if (user.Rows.Count != 0)
            {
                GridView_List.DataSource = user;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }


        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/add_user.aspx");
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");

            Response.Redirect("~/edit_user.aspx?id=" + hdd_id.Value);
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/user_mgm.aspx");

        }
    }
}