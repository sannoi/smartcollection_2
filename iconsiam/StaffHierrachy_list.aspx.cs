﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class StaffHierrachy_list : System.Web.UI.Page
    {
        StaffHierrachy_listDLL Serv = new StaffHierrachy_listDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_rep();                   
                }
            }
        }

        protected void bind_default()
        {
            ddlteam.Items.Insert(0, new ListItem("AR", "ar"));
            ddlteam.Items.Insert(1, new ListItem("Sales", "ae"));



        }

        protected void bind_rep()
        {
            string[] name = txtname.Text.Split(',');

            var hier = Serv.GetHierrachy(txtmanager.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), name, ddlteam.SelectedValue, HttpContext.Current.Session["s_com_code"].ToString());

            if (hier.Rows.Count != 0)
            {
                GridView_List.DataSource = hier;
                GridView_List.DataBind();
            }
            else
            {

                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }



        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/add_hierrachy.aspx");
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");

            Serv.DeleteStaffHierrachy(hdd_id.Value);
            bind_rep();

        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/StaffHierrachy_list.aspx");

        }
    }
}