﻿using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class setting_confirmdate : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        setting_confirmdateDLL Serv = new setting_confirmdateDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtfirst_date.Attributes.Add("onkeypress", "return isNumberKey2AndEnter(event,'" + txtfirst_date.ClientID + "')");

                    txtsec_date.Attributes.Add("onkeypress", "return isNumberKey2AndEnter(event,'" + txtsec_date.ClientID + "')");

                    bind_default();
                }
            }
        }

        protected void bind_default()
        {
            var data = Serv.GetConfirmDate();
            if (data.Rows.Count != 0)
            {
                txtfirst_date.Text = data.Rows[0]["first_confirm"].ToString();
                txtsec_date.Text = data.Rows[0]["second_confirm"].ToString();
            }
            else
            {
                txtfirst_date.Text = "";
                txtsec_date.Text = "";
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {

            if(txtfirst_date.Text == "")
            {
                POPUPMSG("กรุณากรอกวันที่ต้องการส่ง Confirm รอบแรก");
                return;
            }

            if (txtsec_date.Text == "")
            {
                POPUPMSG("กรุณากรอกวันที่ต้องการส่ง Confirm รอบสอง");
                return;
            }

            if (txtfirst_date.Text == "0" || txtfirst_date.Text == "0")
            {
                POPUPMSG("วันที่ Confirm รอบแรกต้องไม่เท่ากับ 0");
                return;
            }

            if (txtsec_date.Text == "0" || txtsec_date.Text == "0")
            {
                POPUPMSG("วันที่ Confirm รอบสองต้องไม่เท่ากับ 0");
                return;
            }

            if (Convert.ToDecimal( txtfirst_date.Text) > 31)
            {
                POPUPMSG("วันที่ต้องการส่ง Confirm รอบแรกห้ามมาก 31");
                return;
            }
            if (Convert.ToDecimal(txtfirst_date.Text) <= 0)
            {
                POPUPMSG("วันที่ต้องการส่ง Confirm รอบแรกห้ามน้อยกว่าเท่ากับ 0");
                return;
            }

            if (Convert.ToDecimal(txtsec_date.Text) > 31)
            {
                POPUPMSG("วันที่ต้องการส่ง Confirm รอบสองห้ามมาก 31");
                return;
            }
            if (Convert.ToDecimal(txtsec_date.Text) <= 0)
            {
                POPUPMSG("วันที่ต้องการส่ง Confirm รอบสองห้ามน้อยกว่าเท่ากับ 0");
                return;
            }

            var data = Serv.GetConfirmDate();
            if (data.Rows.Count != 0)
            {
                Serv.update_confirmDate(txtfirst_date.Text, txtsec_date.Text, data.Rows[0]["id"].ToString());

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='setting_confirmdate.aspx';", true);
                //update
            }
            else
            {
                Serv.Insert_confirmDate(txtfirst_date.Text, txtsec_date.Text);
                //insert
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='setting_confirmdate.aspx';", true);
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }
    }
}