﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class edit_user : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        edit_userDLL Serv = new edit_userDLL();
        sentmail sentMail = new sentmail();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {

                    txtusername.Focus();
                    txtusername.Attributes.Add("onkeypress", "return next_tools(event,'" + txtemployeecode.ClientID + "')");
                    txtemployeecode.Attributes.Add("onkeypress", "return next_tools(event,'" + txtfname.ClientID + "')");
                    txtfname.Attributes.Add("onkeypress", "return next_tools(event,'" + txtlname.ClientID + "')");
                    txtlname.Attributes.Add("onkeypress", "return next_tools(event,'" + txtemail.ClientID + "')");
                    //txtemail.Attributes.Add("onkeypress", "return next_tools(event,'" + txtext.ClientID + "')");
                    txtext.Attributes.Add("onkeypress", "return next_tools(event,'" + txtmobile.ClientID + "')");
                    txtmobile.Attributes.Add("onkeypress", "return next_tools(event,'" + txtmobile.ClientID + "')");



                    bind_default();
                    bind_data();
                }
            }
        }

        protected void bind_default()
        {
            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("Active", "y"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "n"));


            //ddlnoshow_img.Items.Insert(0, new ListItem("ไม่แสดงภาพ", "y"));
            //ddlnoshow_img.Items.Insert(1, new ListItem("แสดงภาพ", "n"));


            ddlrole.Items.Clear();
            ddlrole.Items.Insert(0, new ListItem("Select Role", ""));
            ddlrole.Items.Insert(1, new ListItem("Super Admin", "super_admin"));
            ddlrole.Items.Insert(2, new ListItem("Datacenter", "datacenter"));
            ddlrole.Items.Insert(3, new ListItem("Admin", "admin"));
            ddlrole.Items.Insert(4, new ListItem("Officer", "officer"));
            ddlrole.Items.Insert(5, new ListItem("Manager", "manager"));
            ddlrole.Items.Insert(6, new ListItem("VP", "vp"));
            ddlrole.Items.Insert(7, new ListItem("Management", "management"));

            ddlteam.Items.Clear();
            ddlteam.Items.Insert(0, new ListItem("Select Group", ""));
            ddlteam.Items.Insert(1, new ListItem("AR", "ar"));
            ddlteam.Items.Insert(1, new ListItem("Sales", "ae"));
        }

        protected void bind_data()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    var user = Serv.getUserByUserid(Request.QueryString["id"].ToString());
                    if (user.Rows.Count != 0)
                    {
                        txtemail.Text = user.Rows[0]["email"].ToString();
                        txtemployeecode.Text = user.Rows[0]["employee_code"].ToString();
                        txtext.Text = user.Rows[0]["tel"].ToString();
                        txtfname.Text = user.Rows[0]["Fname"].ToString();
                        txtlname.Text = user.Rows[0]["Lname"].ToString();
                        txtmobile.Text = user.Rows[0]["mobile"].ToString();
                        txtusername.Text = user.Rows[0]["username"].ToString();
                        ddlrole.SelectedValue = user.Rows[0]["role"].ToString();
                        ddlstatus.SelectedValue = user.Rows[0]["flag_active"].ToString();
                        ddlteam.SelectedValue = user.Rows[0]["user_group"].ToString();
                        //ddlnoshow_img.SelectedValue = user.Rows[0]["noshow_img"].ToString();

                        if (user.Rows[0]["role"].ToString() != "admin" && user.Rows[0]["role"].ToString() != "management" && ddlrole.SelectedValue != "super_admin"
                            && ddlrole.SelectedValue != "datacenter")
                        {
                            ddlteam.Visible = true;
                        }
                        else
                        {
                            ddlteam.Visible = false;

                        }
                    }
                    else
                    {
                        Response.Redirect("~/user_mgm.aspx");
                    }
                }
            }
            catch
            {
                Response.Redirect("~/user_mgm.aspx");
            }

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtusername.Text == "")
            {
                POPUPMSG("Please Insert Username");
                return;
            }

            if (txtemployeecode.Text == "")
            {
                POPUPMSG("Please Insert Employee Code");
                return;
            }

            if (txtfname.Text == "")
            {
                POPUPMSG("Please Insert First Name");
                return;
            }

            if (txtlname.Text == "")
            {
                POPUPMSG("Please Insert Last Name");
                return;
            }

            if (txtemail.Text == "")
            {
                POPUPMSG("Please Insert Email");
                return;
            }

            if (txtext.Text == "")
            {
                POPUPMSG("Please Insert เบอร์ต่อ");
                return;
            }

            if (txtmobile.Text == "")
            {
                POPUPMSG("Please Insert Mobile Number");
                return;
            }
            if (ddlrole.SelectedValue == "")
            {
                POPUPMSG("Please Select Role");
                return;
            }

            if (ddlrole.SelectedValue == "officer" || ddlrole.SelectedValue == "manager" || ddlrole.SelectedValue == "vp")
            {
                if (ddlteam.SelectedValue == "")
                {
                    POPUPMSG("Please Select Role");
                    return;
                }

            }


            var u = Serv.getUserByUsername(txtusername.Text, Request.QueryString["id"].ToString());
            if (u.Rows.Count != 0)
            {
                /// ซ้ำ ///
                POPUPMSG("Username ซ้ำกับผู้ใช้งานในระบบ");
            }
            else
            {

                if (ddlrole.SelectedValue != "admin" && ddlrole.SelectedValue != "management")
                {

                    Serv.UpdateUser(txtfname.Text, txtlname.Text, ddlrole.SelectedValue, ddlteam.SelectedValue, txtusername.Text, txtemployeecode.Text,
                  "", txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), txtext.Text, txtmobile.Text, ddlstatus.SelectedValue,
                  DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), HttpContext.Current.Session["s_userid"].ToString(), Request.QueryString["id"].ToString(), "y");
                }
                else
                {
                    Serv.UpdateUser(txtfname.Text, txtlname.Text, ddlrole.SelectedValue, ddlrole.SelectedValue, txtusername.Text, txtemployeecode.Text,
                 "", txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), txtext.Text, txtmobile.Text, ddlstatus.SelectedValue,
                  DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), HttpContext.Current.Session["s_userid"].ToString(), Request.QueryString["id"].ToString(), "y");



                }


                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='user_mgm.aspx';", true);
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void btncancel_Click(object sender, EventArgs e)

        {
            Response.Redirect("~/user_mgm.aspx");
        }

        protected void ddlrole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrole.SelectedValue != "admin" && ddlrole.SelectedValue != "management")
            {
                ddlteam.Visible = true;
            }
            else
            {
                ddlteam.Visible = false;

            }
        }

        protected void btnresetpass_Click(object sender, EventArgs e)
        {
            Serv.UpdatePAss(ConfigurationManager.AppSettings["default_password"], Request.QueryString["id"].ToString());

            sentMail.CallMail(txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), "ระบบ Tenant Sales Data Collection : Reset Password", "Tenant Sales Data Collection",
                            txtfname.Text + " " + txtlname.Text, "[Tenant Sales Data Collection] Your User ID is ", "",
                           "Username : ",
                           "Password : ", "", "", "", "", "", txtusername.Text, ConfigurationManager.AppSettings["default_password"], "",
                           "", "", "", "icon", Request.QueryString["id"].ToString(),"","user");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='user_mgm.aspx';", true);
        }


    }
}