﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iconsiam.App_Code.DLL
{
    public class EmailTemplateDLL
    {
        public DataTable getComName(string CompanyName , string CompanyGroup)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select  distinct cd.*, u.username , em.UpdateDate as Et_UpdateDate  " +
                      "  from TblCompanyDetail cd  " +
                      "  inner join TblEmailTemplate em on em.Companycode = cd.Companycode " +
                      "  inner join TblCompanyGroupMapping cg on cg.CompanyCode = cd.Companycode  " +
                      "  left join tbluser u on em.UpdateId = u.userid " +
                      "  where ( CompanyNameTh like '%"+ CompanyName + "%' or CompanyNameEn like '%"+ CompanyName + "%' ) and cg.ComGroupID = '"+ CompanyGroup + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getComGroup(string CompanyGroup , string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select distinct a.*, c.username , b.UpdateDate as Et_UpdateDate  " +
                      "  from TblCompanyGroup a  " +
                      "  inner join TblEmailTemplate b on b.ComGroupID = CONVERT(varchar, a.ComGroupID) " +
                      "  inner join tbluser c on c.userid = b.UpdateId  " +
                      "  inner join TblCompanyGroupMapping d on d.ComGroupID = a.ComGroupID  " +
                      "  where  a.ComGroupName like '%"+ CompanyGroup + "%'  and a.IsActive = 'y' and d.CompanyCode in ("+ Companycode + ") ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getComName_template(string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select a.Companycode,a.CompanyNameTh,b.Link,b.Sendfromteam,b.Sendfromcompany,b.Tel,b.F_Email,b.F_Callcenter   " +
                      " from TblCompanyDetail a  " +
                      " inner join TblEmailTemplate b on b.Companycode = a.Companycode  " +
                      " where a.Companycode = '"+ companycode + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getComGroupName_template(string companygroup)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select a.ComGroupID,a.ComGroupName,b.Link,b.Sendfromteam,b.Sendfromcompany,b.Tel,b.F_Email,b.F_Callcenter " +
                      " from TblCompanyGroup a   " +
                      " inner join TblEmailTemplate b on b.ComGroupID = CONVERT(varchar, a.ComGroupID)    " +
                      " where a.ComGroupID = '"+ companygroup + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void UpdateCompany_Template(string Sendfromteam, string Sendfromcompany, string Tel, string F_Email ,
                                            string F_Callcenter , string UpdateId , string Companycode)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " update TblEmailTemplate set Sendfromteam = '"+ Sendfromteam + "' , Sendfromcompany = '"+ Sendfromcompany + "' , " +
                     " Tel = '"+ Tel + "' ,  F_Email = '"+ F_Email + "' , F_Callcenter = '"+ F_Callcenter + "' , UpdateDate = getdate() , UpdateId = '"+UpdateId + "' " +
                     " where Companycode = '"+ Companycode + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void UpdateCompanyGroup_Template(string Sendfromteam, string Sendfromcompany, string Tel, string F_Email,
                                            string F_Callcenter, string UpdateId, string Companygroup)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " update TblEmailTemplate set Sendfromteam = '" + Sendfromteam + "' , Sendfromcompany = '" + Sendfromcompany + "' , " +
                     " Tel = '" + Tel + "' ,  F_Email = '" + F_Email + "' , F_Callcenter = '" + F_Callcenter + "' , UpdateDate = getdate() , UpdateId = '" + UpdateId + "' " +
                     " where ComGroupID = '" + Companygroup + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable getTemplate_by_userid(string userid)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select distinct a.username,b.company_code,c.Link,c.Sendfromteam,c.Sendfromcompany,c.Tel,c.F_Email,c.F_Callcenter,d.ComGroupID,e.ComGroupName,f.ButtonColor " +
                      "  from tbluser a  " +
                      "  inner join tblmapp_User_Company b on b.userid = a.userid " +
                      "  inner join TblEmailTemplate c on c.Companycode = b.company_code   " +
                      "  inner join TblCompanyGroupMapping d on d.CompanyCode = c.Companycode "+
                      "  inner join TblCompanyGroup e on e.ComGroupID = d.ComGroupID "+
                      "  inner join TblTheme f on f.ThemeId = e.ThemeId "+
                      "  where a.userid = '" + userid + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getTemplate_by_userid_many_com(string companygroup)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select b.Link,b.Sendfromteam,b.Sendfromcompany,b.Tel,b.F_Email,b.F_Callcenter,c.ButtonColor from TblCompanyGroup a " +
                      " inner join TblEmailTemplate b on b.ComGroupID = CONVERT(varchar, a.ComGroupID)  " +
                      " inner join TblTheme c on c.ThemeId = a.ThemeId " +
                      " where a.ComGroupID = '"+ companygroup + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getTemplate_by_CompanyCode(string CompanyCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select a.ComGroupID,a.CompanyCode,b.Link,b.Sendfromteam,b.Sendfromcompany,b.Tel,b.F_Email,b.F_Callcenter,c.ComGroupName,d.ButtonColor  " +
                      " from TblCompanyGroupMapping a  " +
                      " inner join TblEmailTemplate b on b.Companycode = a.CompanyCode  " +
                      " inner join TblCompanyGroup c on c.ComGroupID = a.ComGroupID  " +
                      " inner join TblTheme d on d.ThemeId = c.ThemeId  " +
                      " where a.CompanyCode = '"+ CompanyCode + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getComcode_by_ContractNumber(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select CompanyCode from tblsmart_Contract where ContractNumber = '"+ ContractNumber + "' ";                     

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

    }
}