﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StaffHierrachy_listDLL
/// </summary>
public class StaffHierrachy_listDLL
{
    public StaffHierrachy_listDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetHierrachy(string manager_name, string[] officer_name, string team , string com)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string name_ = "";


        if (officer_name.Length != 0)
        {
            for (int i = 0; i < officer_name.Length; i++)
            {
                name_ += " (uf.username like '%" + officer_name[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                    " or uf.fname like '%" + officer_name[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or uf.lname like '%" + officer_name[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%')  or";
            }

            name_ = name_.Substring(0, name_.Length - 2);
        }



        if (team == "")
        {
            //strSQL += "  select s.id,case when s.team = 'ar' then 'AR' else 'Sales' end as team, uv.username as vp_name,um.username as manager_name,uf.username as officer_name, s.update_date " +
            //      " from tblStaffHierrachy s inner " +
            //      " join tbluser uv on s.vp_id = uv.userid " +
            //      " inner " +
            //      " join tbluser um on s.manager_id = um.userid " +
            //      " inner " +
            //      " join tbluser uf on s.officer_id = uf.userid " +
            //      " where (um.username like '%" + manager_name + "%' or um.Fname like '%" + manager_name + "%' or um.Lname like '%" + manager_name + "%') and " +
            //      " " + name_ + "   ";


            //NEW

            strSQL += " select distinct s.id,case when s.team = 'ar' then 'AR' else 'Sales' end  as team ,  " +
                        " uv.username as vp_name,  " +
                        " um.username as manager_name,uf.username as officer_name, s.update_date   " +
                        " from tblStaffHierrachy s   " +
                        " inner  join tbluser uv on s.vp_id = uv.userid   " +
                        " inner  join tbluser um on s.manager_id = um.userid    " +
                        " inner  join tbluser uf on s.officer_id = uf.userid  " +
                        " inner join tblmapp_User_Company mapu on mapu.userid = uv.userid  " +
                        " inner join tblmapp_User_Company mapu_um on mapu_um.userid = um.userid  " +
                        " inner join tblmapp_User_Company mapu_uf on mapu_uf.userid = uf.userid  " +
                        " inner join TblCompanyDetail comd on comd.Companycode = mapu.company_code  " +
                        " inner join TblCompanyDetail comd_um on comd.Companycode = mapu_um.company_code  " +
                        " inner join TblCompanyDetail comd_uf on comd.Companycode = mapu_uf.company_code  " +
                        " where (um.username like '%" + manager_name + "%' or um.Fname like '%" + manager_name + "%' or um.Lname like '%" + manager_name + "%') and   " +
                        " " + name_ + "   " +
                        " and (mapu.company_code in (" + com + ") or mapu_um.company_code in (" + com + ") or mapu_uf.company_code in (" + com + ") )  ";
        }
        else
        {
            //strSQL += "  select s.id,case when s.team = 'ar' then 'AR' else 'Sales' end as team, uv.username as vp_name,um.username as manager_name,uf.username as officer_name, s.update_date " +
            //      " from tblStaffHierrachy s inner " +
            //      " join tbluser uv on s.vp_id = uv.userid " +
            //      " inner " +
            //      " join tbluser um on s.manager_id = um.userid " +
            //      " inner " +
            //      " join tbluser uf on s.officer_id = uf.userid " +
            //      " where (um.username like '%" + manager_name + "%' or um.Fname like '%" + manager_name + "%' or um.Lname like '%" + manager_name + "%') and " +
            //      " " + name_ + " and s.team = '" + team + "' ";

            //NEW

            strSQL +=   " select distinct s.id,case when s.team = 'ar' then 'AR' else 'Sales' end  as team ,  " +
                        " uv.username as vp_name,  " +
                        " um.username as manager_name,uf.username as officer_name, s.update_date   " +
                        " from tblStaffHierrachy s   " +
                        " inner  join tbluser uv on s.vp_id = uv.userid   " +
                        " inner  join tbluser um on s.manager_id = um.userid    " +
                        " inner  join tbluser uf on s.officer_id = uf.userid  " +
                        " inner join tblmapp_User_Company mapu on mapu.userid = uv.userid  " +
                        " inner join tblmapp_User_Company mapu_um on mapu_um.userid = um.userid  " +
                        " inner join tblmapp_User_Company mapu_uf on mapu_uf.userid = uf.userid  " +
                        " inner join TblCompanyDetail comd on comd.Companycode = mapu.company_code  " +
                        " inner join TblCompanyDetail comd_um on comd.Companycode = mapu_um.company_code  " +
                        " inner join TblCompanyDetail comd_uf on comd.Companycode = mapu_uf.company_code  " +
                        " where (um.username like '%" + manager_name + "%' or um.Fname like '%" + manager_name + "%' or um.Lname like '%" + manager_name + "%') and   " +
                        " " + name_ + " and s.team = '" + team + "'   " +
                        " and (mapu.company_code in ("+ com + ") or mapu_um.company_code in ("+ com + ") or mapu_uf.company_code in ("+ com + ") )  ";

        }




        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void DeleteStaffHierrachy(string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  Delete from  tblStaffHierrachy where id = '" + id + "' ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }



}