﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;

namespace iconsiam
{
    public partial class CompanyGroupMgm : System.Web.UI.Page
    {
        CompanyGroupMgmDLL Serv = new CompanyGroupMgmDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_data();


                }
            }
        }

        protected void bind_data()
        {
            var comp = Serv.getComGroupName(txtcompanygroup.Text, ddlstatus.SelectedValue);
            if (comp.Rows.Count != 0)
            {
                GridView_List.DataSource = comp;
                GridView_List.DataBind();

                if (ddlstatus.SelectedValue == "y")
                {
                    GridView_List.Columns[5].Visible = false;
                }
                else
                {
                    GridView_List.Columns[5].Visible = true;
                }

            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void bind_default()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "y"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "n"));

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hddComGroupID = (HiddenField)row.FindControl("hddComGroupID");

            Response.Redirect("~/CompanyGroupMgm_edit.aspx?comgroupid=" + hddComGroupID.Value);

        }

        protected void btnedit2_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hddComGroupID = (HiddenField)row.FindControl("hddComGroupID_2");

            Response.Redirect("~/CompanyGroupMgm_edit_pic.aspx?comgroupid=" + hddComGroupID.Value);

        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_data();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CompanyGroupMgm_add.aspx");
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CompanyGroupMgm.aspx");
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hddComGroupID = (HiddenField)row.FindControl("hddComGroupID");

            Serv.DeleteComGroupName(hddComGroupID.Value);
            bind_data();

            POPUPMSG("บันทึกเรียบร้อย");

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

    }
}