﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class edit_categoryleasing : System.Web.UI.Page
    {
        categoryleasingDLL Serv = new categoryleasingDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtCategoryleasingNameEN.Focus();
                    txtCategoryleasingNameEN.Attributes.Add("onkeypress", "return next_tools(event,'" + txtCategoryleasingNameTH.ClientID + "')");
                    txtCategoryleasingNameTH.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsave.ClientID + "')");

                    bind_default();
                    bind_data();
                }
            }
        }
        protected void bind_default()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));
        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var indus = Serv.getcategoryleasingById(Request.QueryString["id"].ToString());
                if (indus.Rows.Count != 0)
                {
                    txtCategoryleasingNameEN.Text = indus.Rows[0]["CategoryleasingNameEN"].ToString();
                    txtCategoryleasingNameTH.Text = indus.Rows[0]["CategoryleasingNameTH"].ToString();
                    ddlstatus.SelectedValue = indus.Rows[0]["Flag_active"].ToString();
                }
                else
                {
                    Response.Redirect("~/categoryleasing.aspx");
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtCategoryleasingNameEN.Text != "" && txtCategoryleasingNameTH.Text != "")
            {

                var ex1 = Serv.getcategoryleasingByNameEN_nid(txtCategoryleasingNameEN.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), Request.QueryString["id"].ToString());
                var ex2 = Serv.getcategoryleasingByNameTH_nid(txtCategoryleasingNameTH.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), Request.QueryString["id"].ToString());
                if (ex1.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Category Leasing Name(EN) ซ้ำ");
                    return;
                }
                else if (ex2.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Category Leasing Name(TH) ซ้ำ");
                    return;
                }
                else
                {
                    Serv.Updatecategoryleasing(txtCategoryleasingNameTH.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), 
                        txtCategoryleasingNameEN.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlstatus.SelectedValue, 
                        HttpContext.Current.Session["s_userid"].ToString(),Request.QueryString["id"].ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='categoryleasing.aspx';", true);
                }

            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/categoryleasing.aspx");
        }


    }
}