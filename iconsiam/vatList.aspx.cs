﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;

namespace iconsiam
{
    public partial class vatList : System.Web.UI.Page
    {
        setting_vatDLL Serv = new setting_vatDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_data();                   
                }
            }
        }

        protected void bind_data()
        {
            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');


            var comp = Serv.getVatList(txtcompany.Text, comcode_);
            if (comp.Rows.Count != 0)
            {
                GridView_List.DataSource = comp;
                GridView_List.DataBind();

            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_data();
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hddid = (HiddenField)row.FindControl("hddid");

            Response.Redirect("~/setting_vat_edit.aspx?id=" + hddid.Value);
        }


        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/setting_vat.aspx");
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/vatList.aspx");

        }

    }
}