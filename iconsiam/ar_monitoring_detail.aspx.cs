﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class ar_monitoring_detail : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        ar_monitoring_detailDLL Serv = new ar_monitoring_detailDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtstartdate.Text = DateTime.Now.ToString("01/MM/yyyy", EngCI);
                    txtenddate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);
                    bind_rep();
                    bind_default();
                    bind_data();
                }
            }
        }

        protected void bind_default()
        {

            ddlstatus.Items.Insert(0, new ListItem("", ""));
            ddlstatus.Items.Insert(1, new ListItem("ยังไม่ส่งยอด", "n"));
            ddlstatus.Items.Insert(2, new ListItem("ส่งยอดแล้ว", "y"));
            ddlstatus.Items.Insert(3, new ListItem("ยืนยันยอดแล้ว", "c"));
            ddlstatus.Items.Insert(4, new ListItem("ยังไม่ยืนยันยอด", "nc"));
            ddlstatus.Items.Insert(5, new ListItem("แก้ไขยอด", "a"));

            var c = Serv.Get_Confirm_new(hdd_companycode.Value.Substring(0, 3));
            var c2 = Serv.Get_Confirm_new2(hdd_companycode.Value.Substring(0, 3));

            if (c.Rows.Count != 0)
            {
                if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (c2.Rows.Count != 0)
                {
                    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "n";

                        btnselect_all.Visible = false;
                        btndisselect_all.Visible = false;
                        btnconfirm.Visible = false;
                    }
                }
                else
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "n";

                    btnselect_all.Visible = false;
                    btndisselect_all.Visible = false;
                    btnconfirm.Visible = false;
                }

            }
            else
            {
                HttpContext.Current.Session["samrt_chkflag_confirm"] = "n";

                btnselect_all.Visible = false;
                btndisselect_all.Visible = false;
                btnconfirm.Visible = false;
            }

        }

        protected void bind_rep()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && !string.IsNullOrEmpty(Request.QueryString["h"]))
            {
                var contract = Serv.GetContract(Request.QueryString["id"].ToString());
                if (contract.Rows.Count != 0)
                {
                    hdd_companycode.Value = contract.Rows[0]["CompanyCode"].ToString();

                    lbcompanyname.Text = contract.Rows[0]["CompanyNameTH"].ToString();
                    lbbpname.Text = contract.Rows[0]["BusinessPartnerName"].ToString();
                    lbshopname.Text = contract.Rows[0]["ShopName"].ToString();

                    hdd_h.Value = Request.QueryString["h"].ToString();
                    if (Request.QueryString["h"].ToString() == "te")
                    {
                        btnadd.Visible = false;
                        btnimport.Visible = false;

                        divConfirm.Visible = false;

                    }
                    else
                    {
                        btnadd.Visible = true;
                        btnimport.Visible = true;
                        divConfirm.Visible = true;
                    }
                }
                else
                {
                    Response.Redirect("~/ar_monitoring.aspx");
                }
            }
            else
            {
                Response.Redirect("~/ar_monitoring.aspx");
            }

        }
        protected void GridView_List_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdd_status = (HiddenField)(e.Row.FindControl("hdd_status"));
                Button btnedit = (Button)(e.Row.FindControl("btnedit"));

                if (hdd_status.Value == "ยังไม่ส่งยอด")
                {
                    e.Row.ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[2].Text = "";
                    e.Row.Cells[3].Text = "";
                    btnedit.Text = "บันทึกยอด";
                }//
                else if (hdd_status.Value == "ขอแก้ไขยอด")
                {
                    e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;

                }

                HiddenField hdd_flag_confirm = (HiddenField)(e.Row.FindControl("hdd_flag_confirm"));
                if (hdd_flag_confirm.Value == "ยังไม่ยืนยันยอด")
                {
                    e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;
                }

            }


        }

        protected void GridView_List_confirm_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                CheckBox CheckBox1 = (CheckBox)(e.Row.FindControl("CheckBox1"));
                Button btnedit = (Button)(e.Row.FindControl("btnedit"));


                HiddenField hdd_status = (HiddenField)(e.Row.FindControl("hdd_status"));
                if (hdd_status.Value == "ยังไม่ส่งยอด")
                {
                    e.Row.ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[3].Text = "";
                    e.Row.Cells[4].Text = "";
                    btnedit.Text = "บันทึกยอด";

                    CheckBox1.Visible = false;
                }
                else if (hdd_status.Value == "ขอแก้ไขยอด")
                {
                    e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;

                    CheckBox1.Visible = false;

                }
                else
                {
                    CheckBox1.Checked = true;
                }

                //if (Request.QueryString["h"].ToString() == "te")
                //{
                //    CheckBox1.Checked = false;
                //}
                //else
                //{

                //}

                HiddenField hdd_flag_confirm = (HiddenField)(e.Row.FindControl("hdd_flag_confirm"));
                if (hdd_flag_confirm.Value == "ยังไม่ยืนยันยอด")
                {
                    e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                }
                else if (hdd_flag_confirm.Value == "ยืนยันยอดแล้ว")
                {
                    CheckBox1.Visible = false;

                }

            }
        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && !string.IsNullOrEmpty(Request.QueryString["h"]))
            {
                string contractNUmber = "";

                var c = Serv.getOldContract(Request.QueryString["id"].ToString());
                if (c.Rows.Count != 0)
                {
                    contractNUmber += "'" + c.Rows[0]["Contract_old_number"].ToString() + "',";

                    var c_ = Serv.getOldContract(c.Rows[0]["Contract_old_number"].ToString());
                    if (c_.Rows.Count != 0)
                    {
                        contractNUmber += "'" + c_.Rows[0]["Contract_old_number"].ToString() + "',";
                    }
                }

                contractNUmber += "'" + Request.QueryString["id"].ToString() + "'";


                dateconvert con_d = new dateconvert();
                var t = Serv.GetTransaction(contractNUmber, con_d.con_date(txtstartdate.Text),
                    con_d.con_date(txtenddate.Text), ddlstatus.SelectedValue);
                if (t.Rows.Count != 0)
                {
                    hdd_h.Value = Request.QueryString["h"].ToString();

                    if (Request.QueryString["h"].ToString() == "te")
                    {
                        GridView_List.DataSource = t;
                        GridView_List.DataBind();

                        GridView_List_confirm.DataSource = null;
                        GridView_List_confirm.DataBind();

                    }
                    else
                    {
                        if (HttpContext.Current.Session["samrt_chkflag_confirm"].ToString() == "n")
                        {
                            GridView_List.DataSource = t;
                            GridView_List.DataBind();


                            GridView_List_confirm.DataSource = null;
                            GridView_List_confirm.DataBind();
                        }
                        else
                        {
                            GridView_List_confirm.DataSource = t;
                            GridView_List_confirm.DataBind();

                            GridView_List.DataSource = null;
                            GridView_List.DataBind();
                        }

                    }


                }
                else
                {
                    GridView_List.DataSource = null;
                    GridView_List.DataBind();

                    GridView_List_confirm.DataSource = null;
                    GridView_List_confirm.DataBind();

                }
            }
            else
            {
                Response.Redirect("~/ar_monitoring.aspx");
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }

        protected void btnimport_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session["samrt_last_page"] = "ar_monitoring_detail.aspx?id=" + Request.QueryString["id"].ToString() + "&h=" + hdd_h.Value;

            Response.Redirect("~/record_transaction_import.aspx?id=" + Request.QueryString["id"].ToString());

        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session["samrt_last_page"] = "ar_monitoring_detail.aspx?id=" + Request.QueryString["id"].ToString() + "&h=" + hdd_h.Value;
            Response.Redirect("~/record_transaction.aspx?id=" + Request.QueryString["id"].ToString());
        }

        protected void btndetail_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_ContractNumber = (HiddenField)row.FindControl("hdd_ContractNumber");
            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HiddenField hdd_status = (HiddenField)row.FindControl("hdd_status");
            HttpContext.Current.Session["samrt_last_page"] = "ar_monitoring_detail.aspx?id=" + Request.QueryString["id"].ToString() + "&h=" + hdd_h.Value;

            Response.Redirect("~/transaction_detail.aspx?id=" + hdd_id.Value + "&cont=" + hdd_ContractNumber.Value + "&s=" + hdd_status.Value + "&type=v");
        }

        protected void btnselect_all_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView_List_confirm.Rows)
            {
                CheckBox CheckBox1 = (CheckBox)(row.FindControl("CheckBox1"));
                if (CheckBox1.Visible == true)
                {
                    CheckBox1.Checked = true;
                }
            }
        }

        protected void btndisselect_all_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView_List_confirm.Rows)
            {

                CheckBox CheckBox1 = (CheckBox)(row.FindControl("CheckBox1"));

                if (CheckBox1.Visible == true)
                {
                    CheckBox1.Checked = false;
                }

            }
        }

        protected void btnconfirm_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView_List_confirm.Rows)
            {

                HiddenField hdd_ContractNumber = (HiddenField)row.FindControl("hdd_ContractNumber");
                HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
                CheckBox CheckBox1 = (CheckBox)(row.FindControl("CheckBox1"));
                if (CheckBox1.Checked == true)
                {
                    Serv.Update_flagConfirm(hdd_ContractNumber.Value, hdd_id.Value);
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='ar_monitoring_detail.aspx?id=" + Request.QueryString["id"].ToString() +
                "&h=" + hdd_h.Value + "';", true);
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/" + HttpContext.Current.Session["samrt_last_page"].ToString());
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_ContractNumber = (HiddenField)row.FindControl("hdd_ContractNumber");
            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HiddenField hdd_status = (HiddenField)row.FindControl("hdd_status");
            HttpContext.Current.Session["samrt_last_page"] = "ar_monitoring_detail.aspx?id=" + Request.QueryString["id"].ToString() + "&h=" + hdd_h.Value;

            if (hdd_status.Value == "ยังไม่ส่งยอด")
            {
                Response.Redirect("~/record_transaction.aspx?id=" + hdd_ContractNumber.Value + "&idhis=" + hdd_id.Value);

            }
            else
            {
                Response.Redirect("~/transaction_detail.aspx?id=" + hdd_id.Value + "&cont=" + hdd_ContractNumber.Value + "&s=" + hdd_status.Value + "&type=e");
                //Response.Redirect("~/transaction_tenant_detail2.aspx?id=" + hdd_id.Value + "&cont=" + hdd_ContractNumber.Value + "&type=e");

            }

        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ar_monitoring_detail.aspx?id=" + Request.QueryString["id"].ToString()
                + "&h=" + Request.QueryString["h"].ToString());

        }

    }
}