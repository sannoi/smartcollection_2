﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for record_transactionDLL
/// </summary>
public class record_transactionDLL
{
    public record_transactionDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetContract(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no ,  s.CompanyCode, s.ContractNumber,s.smart_record_keyin_type, " +
                    " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, " +
                    " s.smart_icon_staff_id,s.* " +
                    " from tblsmart_Contract s inner " +
                    " join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                    " where s.ContractNumber = '" + ContractNumber + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetContract_mobile(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no ,  s.CompanyCode, s.ContractNumber,s.smart_record_keyin_type, " +
                    " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, " +
                    " s.smart_icon_staff_id,s.* " +
                    " from tblsmart_Contract s inner " +
                    " join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                    " where s.ContractNumber =  @ContractNumber ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetCompanyCodeByCompanyCode(string companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from SAPCompany where CompanyCode = '" + companycode + "'  and IsActive = '1' ";


        objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetCompanyCodeByCompanyCode_mobile(string companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "select * from SAPCompany where CompanyCode =  @companycode  and IsActive = '1' ";


        objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@companycode", companycode);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetTransaction_detail(string id, string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tbltransaction_record where id = '" + id + "' and contractnumber = '" + contractnumber + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetTransaction_detail_mobile(string id, string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tbltransaction_record where id =  @id  and contractnumber =  @contractnumber  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@id", id);
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetTransactionByDate(string record_date, string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tbltransaction_record where record_date between '" + record_date + " 00:00:00' and '" + record_date + " 23:59:59' " +
            " and ContractNumber = '" + contractnumber + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetTransactionByContractNumber(string contractnumber, string d1, string d2)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select id,contractnumber, product_amount, service_serCharge_amount, record_date,flag_confirm,status," +
            " flag_adjust,adjust_remark from tbltransaction_record " +
                    " where contractnumber in (" + contractnumber + ")  and record_date between '" + d1 + "' and '" + d2 + "' order by  record_date ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetTransactionByContractNumber_mobile(string contractnumber, string d1, string d2)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        var _with1 = objCmd;
        string[] contractnumber_list = contractnumber.ToString().Split(',');

        strSQL += " select t.id,t.ContractNumber, t.product_amount, t.service_serCharge_amount, t.record_date,t.flag_confirm,t.status," +
                    " t.flag_adjust,t.adjust_remark " +
                    " from tbltransaction_record t where " +
                    " t.record_date between CONVERT(datetime, @d1)  and CONVERT(datetime, @d2 ) " +
                    " and ContractNumber in  ( ";

        int i = 1;
        foreach (var contract in contractnumber_list)
        {
            strSQL += "@ContractNumber" + i + ",";
            _with1.Parameters.AddWithValue("@ContractNumber" + i, contract);
            i++;
        }

        strSQL = strSQL.Substring(0, strSQL.Length - 1);


        strSQL += " ) order by  t.record_date ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();

        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@d1", d1);
        _with1.Parameters.AddWithValue("@d2", d2);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getOldContract(string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select Contract_old_number from tblsmart_contract where ContractNumber = '" + contractnumber + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable getOldContract_mobile(string contractnumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select Contract_old_number from tblsmart_contract where ContractNumber =  @contractnumber  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUser(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += "  select * from tbluser where userid = '" + userid + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetUser_mobile(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += "  select * from tbluser where userid =  @userid  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@userid", userid);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetTransaction_detail_img(string trans_id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tbltransaction_record_img where transaction_id = '" + trans_id + "' ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetTransaction_detail_edit_img(string trans_id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select TOP 2* from tbledit_transaction_record_img where transaction_id = '" + trans_id + "' ORDER BY id DESC ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable Insert_transaction_record(string ContractNumber, string product_amount, string service_serCharge_amount, string status,
        string record_date, string no_type, string remark, int vat, string type, string create_id, string create_role, string key_in_channel)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        decimal service_serCharge_amount_ = 0;

        if (service_serCharge_amount != "")
        {
            service_serCharge_amount_ = Convert.ToDecimal(service_serCharge_amount);
        }
        else
        {
            service_serCharge_amount_ = 0;
        }

        decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

        decimal v2 = 0;

        if (type == "prod_service")
        {
            if (service_serCharge_amount != "")
            {
                v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
            }
        }




        strSQL = " insert into tbltransaction_record(ContractNumber,product_amount,service_serCharge_amount,status,record_date,no_type,remark," +
            " product_amount_vat,service_serCharge_amount_vat,create_date,create_id,create_role, key_in_channel ) " +
                    " values('" + ContractNumber + "'," + product_amount + "," + service_serCharge_amount_ + ",'" + status + "','" + record_date + "', " +
                    " '" + no_type + "','" + remark + "','" + v1 + "','" + v2 + "',getdate(),'" + create_id + "','" + create_role + "' , '" + key_in_channel + "'); " +
                    " SELECT @@identity as last_id ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;

    }

    public DataTable Insert_transaction_record_mobile(string ContractNumber, string product_amount, string service_serCharge_amount, string status,
        string record_date, string no_type, string remark, int vat, string type, string create_id, string create_role, string key_in_channel)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        decimal service_serCharge_amount_ = 0;

        if (service_serCharge_amount != "")
        {
            service_serCharge_amount_ = Convert.ToDecimal(service_serCharge_amount);
        }
        else
        {
            service_serCharge_amount_ = 0;
        }

        decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

        decimal v2 = 0;

        if (type == "prod_service")
        {
            if (service_serCharge_amount != "")
            {
                v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
            }
        }

        if (no_type != null && remark != null)
        {
            strSQL = " insert into tbltransaction_record(ContractNumber,product_amount,service_serCharge_amount,status,record_date,no_type,remark," +
            " product_amount_vat,service_serCharge_amount_vat,create_date,create_id,create_role, key_in_channel ) " +
                    " values(@ContractNumber ,@product_amount , @service_serCharge_amount_ , @status ,@record_date , " +
                    " @no_type , @remark , @v1 , @v2 , getdate() , @create_id , @create_role , @key_in_channel ); " +
                    " SELECT @@identity as last_id ; ";
        }
        else if (no_type != null && remark == null)
        {
            strSQL = " insert into tbltransaction_record(ContractNumber,product_amount,service_serCharge_amount,status,record_date,no_type," +
          " product_amount_vat,service_serCharge_amount_vat,create_date,create_id,create_role, key_in_channel ) " +
                  " values(@ContractNumber ,@product_amount , @service_serCharge_amount_ , @status ,@record_date , " +
                  " @no_type , @v1 , @v2 , getdate() , @create_id , @create_role , @key_in_channel ); " +
                  " SELECT @@identity as last_id ; ";
        }
        else if (no_type == null && remark == null)
        {
            strSQL = " insert into tbltransaction_record(ContractNumber,product_amount,service_serCharge_amount,status,record_date," +
          " product_amount_vat,service_serCharge_amount_vat,create_date,create_id,create_role, key_in_channel ) " +
                  " values(@ContractNumber ,@product_amount , @service_serCharge_amount_ , @status ,@record_date , " +
                  "  @v1 , @v2 , getdate() , @create_id , @create_role , @key_in_channel ); " +
                  " SELECT @@identity as last_id ; ";
        }
        else if (no_type == null && remark != null)
        {
            strSQL = " insert into tbltransaction_record(ContractNumber,product_amount,service_serCharge_amount,status,record_date,remark," +
            " product_amount_vat,service_serCharge_amount_vat,create_date,create_id,create_role, key_in_channel ) " +
                    " values(@ContractNumber ,@product_amount , @service_serCharge_amount_ , @status ,@record_date , " +
                    " @remark , @v1 , @v2 , getdate() , @create_id , @create_role , @key_in_channel ); " +
                    " SELECT @@identity as last_id ; ";
        }



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
        _with1.Parameters.AddWithValue("@product_amount", Convert.ToDecimal(product_amount));
        _with1.Parameters.AddWithValue("@service_serCharge_amount_", service_serCharge_amount_);
        _with1.Parameters.AddWithValue("@status", status);
        _with1.Parameters.AddWithValue("@record_date", record_date);

        if (no_type != null)
        {
            _with1.Parameters.AddWithValue("@no_type", no_type);
        }

        if (remark != null)
        {
            _with1.Parameters.AddWithValue("@remark", remark);
        }


        _with1.Parameters.AddWithValue("@v1", v1);
        _with1.Parameters.AddWithValue("@v2", v2);
        _with1.Parameters.AddWithValue("@create_id", create_id);
        _with1.Parameters.AddWithValue("@create_role", create_role);
        _with1.Parameters.AddWithValue("@key_in_channel", key_in_channel);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;

    }

    public void Update_transaction_record(string ContractNumber, string product_amount, string service_serCharge_amount, string status,
     string record_date, string no_type, string remark, string id, int vat, string update_id, string update_role, string key_in_channel)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;



        decimal service_serCharge_amount_ = 0;

        if (service_serCharge_amount != "")
        {
            service_serCharge_amount_ = Convert.ToDecimal(service_serCharge_amount);
        }
        else
        {
            service_serCharge_amount_ = 0;
        }

        decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

        decimal v2 = 0;

        if (service_serCharge_amount != "0")
        {
            if (service_serCharge_amount != "")
            {
                v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
            }
        }



        strSQL = " Update tbltransaction_record set " +
            " product_amount = '" + product_amount + "', " +
            " service_serCharge_amount = '" + service_serCharge_amount_ + "', " +
            " status = 'y' , record_date = '" + record_date + "', " +
            " no_type = '" + no_type + "',remark = '" + remark + "' " +

            " , update_date = getdate(), update_id = '" + update_id + "' , update_role = '" + update_role + "' " +

            " , product_amount_vat = '" + v1 + "', service_serCharge_amount_vat = '" + v2 + "' " +

            " ,  key_in_channel = '" + key_in_channel + "' " +

         " where ContractNumber  = '" + ContractNumber + "' and id = '" + id + "' ";



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_transaction_record_mobile(string ContractNumber, string product_amount, string service_serCharge_amount, string status,
     string record_date, string no_type, string remark, string id, int vat, string update_id, string update_role, string key_in_channel)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;



        decimal service_serCharge_amount_ = 0;

        if (service_serCharge_amount != "")
        {
            service_serCharge_amount_ = Convert.ToDecimal(service_serCharge_amount);
        }
        else
        {
            service_serCharge_amount_ = 0;
        }

        decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

        decimal v2 = 0;

        if (service_serCharge_amount != "0")
        {
            if (service_serCharge_amount != "")
            {
                v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
            }
        }


        strSQL = " Update tbltransaction_record set " +
            " product_amount = @product_amount , " +
            " service_serCharge_amount = @service_serCharge_amount_ , " +
            " status = 'y' , record_date = @record_date  ";

        if (no_type != null)
        {
            strSQL += " , no_type = @no_type ";
        }

        if (remark != null)
        {
            strSQL += " , remark = @remark  ";
        }

        strSQL += " , update_date = getdate(), update_id = @update_id  , update_role = @update_role  " +

          " , product_amount_vat = @v1 , service_serCharge_amount_vat = @v2  " +

          " ,  key_in_channel = @key_in_channel " +

       " where ContractNumber  = @ContractNumber  and id = @id  ";



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@product_amount", Convert.ToDecimal(product_amount));
        _with1.Parameters.AddWithValue("@service_serCharge_amount_", service_serCharge_amount_);
        _with1.Parameters.AddWithValue("@record_date", record_date);

        if (no_type != null)
        {
            _with1.Parameters.AddWithValue("@no_type", no_type);
        }

        if (remark != null)
        {
            _with1.Parameters.AddWithValue("@remark", remark);
        }


        //_with1.Parameters.AddWithValue("@no_type", no_type);
        //_with1.Parameters.AddWithValue("@remark", remark);
        _with1.Parameters.AddWithValue("@update_id", update_id);
        _with1.Parameters.AddWithValue("@update_role", update_role);
        _with1.Parameters.AddWithValue("@v1", v1);
        _with1.Parameters.AddWithValue("@v2", v2);
        _with1.Parameters.AddWithValue("@key_in_channel", key_in_channel);
        _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
        _with1.Parameters.AddWithValue("@id", id);
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_transaction_recor_1(string ContractNumber, string product_amount, string service_serCharge_amount, string status,
                string record_date, string no_type, string remark, string id, string update_date, string update_id, int vat, string update_role, string key_in_channel)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;



        decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

        decimal v2 = 0;

        if (service_serCharge_amount != "0")
        {
            if (service_serCharge_amount != "")
            {
                v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
            }
        }




        strSQL = " Update tbltransaction_record set " +
            " product_amount = '" + product_amount + "', " +
            " service_serCharge_amount = '" + service_serCharge_amount + "', " +
            " status = 'y' , record_date = '" + record_date + "', " +
            " no_type = '" + no_type + "',remark = '" + remark + "' " +

            " , update_date = '" + update_date + "',update_id = '" + update_id + "' , update_role = '" + update_role + "' " +

            " , product_amount_vat = '" + v1 + "', service_serCharge_amount_vat = '" + v2 + "' " +

            " , key_in_channel = '" + key_in_channel + "' " +

            " where ContractNumber  = '" + ContractNumber + "' and id = '" + id + "' ";



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_transaction_recor_createDate_Stamp(string ContractNumber, string product_amount, string service_serCharge_amount, string status,
                string record_date, string no_type, string remark, string id, string update_date, string update_id, int vat, string create_role, string key_in_channel)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;



        decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

        decimal v2 = 0;

        if (service_serCharge_amount != "0")
        {
            if (service_serCharge_amount != "")
            {
                v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
            }
        }




        strSQL = " Update tbltransaction_record set " +
            " product_amount = '" + product_amount + "', " +
            " service_serCharge_amount = '" + service_serCharge_amount + "', " +
            " status = 'y' , record_date = '" + record_date + "', " +
            " no_type = '" + no_type + "',remark = '" + remark + "' " +

            " , create_date = '" + update_date + "',create_id = '" + update_id + "' , create_role  = '" + create_role + "' " +

            " , product_amount_vat = '" + v1 + "', service_serCharge_amount_vat = '" + v2 + "' " +
            " , key_in_channel = '" + key_in_channel + "' " +

            " where ContractNumber  = '" + ContractNumber + "' and id = '" + id + "' ";



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void tbltransaction_record_img(string transaction_id, string img, string filename, string filename_display)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " insert into tbltransaction_record_img(transaction_id,img,filename,filename_display) " +
                    " values('" + transaction_id + "','" + img + "','" + filename + "','" + filename_display + "') ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void tbltransaction_record_img_mobile(string transaction_id, string img, string filename, string filename_display)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " insert into tbltransaction_record_img(transaction_id,img,filename,filename_display) " +
                    " values( @transaction_id , @img , @filename , @filename_display ) ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@transaction_id", transaction_id);
        _with1.Parameters.AddWithValue("@img", img);
        _with1.Parameters.AddWithValue("@filename", filename);
        _with1.Parameters.AddWithValue("@filename_display", filename_display);
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void tbl_edit_transaction_record_img(string transaction_id, string img, string filename, string filename_display, string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        strSQL = " insert into tbledit_transaction_record_img(transaction_id,img,filename,filename_display,create_id,create_date,update_id,update_date) " +
                    " values('" + transaction_id + "','" + img + "','" + filename + "','" + filename_display + "','" + userid + "',getdate(),'',getdate()) ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void tbl_edit_transaction_record_img_mobile(string transaction_id, string img, string filename, string filename_display, string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        strSQL = " insert into tbledit_transaction_record_img(transaction_id,img,filename,filename_display,create_id,create_date,update_id,update_date) " +
                    " values( @transaction_id , @img , @filename , @filename_display , @userid , getdate() , '' , getdate() ) ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@transaction_id", transaction_id);
        _with1.Parameters.AddWithValue("@img", img);
        _with1.Parameters.AddWithValue("@filename", filename);
        _with1.Parameters.AddWithValue("@filename_display", filename_display);
        _with1.Parameters.AddWithValue("@userid", userid);
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void tbltransaction_record_pdf(string transaction_id, string img, string pdf_file, string filename_display)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        strSQL = " insert into tbltransaction_record_img(transaction_id,img,filename,filename_display) " +
                    " values('" + transaction_id + "','" + img + "','" + pdf_file + "','" + filename_display + "') ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_Transaction(string adjust_remark, string id, string contractnumber, string request_adj_id, string request_adj_role)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " update tbltransaction_record set adjust_remark = '" + adjust_remark + "', flag_adjust = 'y', request_adj_id = '" + request_adj_id + "', request_adj_date = getdate(), " +
            " request_adj_role = '" + request_adj_role + "' ,flag_confirm = 'n' where id = '" + id + "' and contractnumber = '" + contractnumber + "' ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }
    public void Update_Transaction_mobile(string adjust_remark, string id, string contractnumber, string request_adj_id, string request_adj_role)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " update tbltransaction_record set adjust_remark = '" + adjust_remark + "', flag_adjust = 'y', request_adj_id = '" + request_adj_id + "', request_adj_date = getdate(), " +
            " request_adj_role = '" + request_adj_role + "' ,flag_confirm = 'n' where id = '" + id + "' and contractnumber = '" + contractnumber + "' ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@adjust_remark", adjust_remark);
        _with1.Parameters.AddWithValue("@id", id);
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.Parameters.AddWithValue("@request_adj_id", request_adj_id);
        _with1.Parameters.AddWithValue("@request_adj_role", request_adj_role);
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_Transaction_ar(string last_product_amount, string last_service_serCharge_amount, string product_amount,
        string service_serCharge_amount, string id, string contractnumber, int vat, string type, string update_id, string update_role)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        decimal service_serCharge_amount_ = 0;

        if (service_serCharge_amount != "")
        {
            service_serCharge_amount_ = Convert.ToDecimal(service_serCharge_amount);
        }
        else
        {
            service_serCharge_amount_ = 0;
        }



        decimal product_amount_ = 0;

        if (product_amount != "")
        {
            product_amount_ = Convert.ToDecimal(product_amount);
        }
        else
        {
            product_amount_ = 0;
        }








        decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

        decimal v2 = 0;

        if (type == "prod_service")
        {
            if (service_serCharge_amount != "")
            {
                v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
            }
        }




        if (product_amount_ != 0 || service_serCharge_amount_ != 0)
        {
            strSQL = " update tbltransaction_record set " +
          " last_product_amount = '" + last_product_amount + "', " +
          " last_service_serCharge_amount = '" + last_service_serCharge_amount + "', " +
          " product_amount = '" + product_amount + "', " +
          " service_serCharge_amount = '" + service_serCharge_amount + "', " +
          " update_date = getdate() , " +
          " update_id = '" + update_id + "', " +
          " no_type = '' , " +

          " update_role = '" + update_role + "', " +


             " product_amount_vat = '" + v1 + "', " +
          " service_serCharge_amount_vat = '" + v2 + "' " +

          " where id = '" + id + "' and contractnumber = '" + contractnumber + "' ; ";
        }
        else
        {
            strSQL = " update tbltransaction_record set " +
          " last_product_amount = '" + last_product_amount + "', " +
          " last_service_serCharge_amount = '" + last_service_serCharge_amount + "', " +
          " product_amount = '" + product_amount + "', " +
          " service_serCharge_amount = '" + service_serCharge_amount + "', " +

                    " update_date = getdate() , " +
          " update_id = '" + update_id + "', " +

          " update_role = '" + update_role + "', " +


             " product_amount_vat = '" + v1 + "', " +
          " service_serCharge_amount_vat = '" + v2 + "' " +

          " where id = '" + id + "' and contractnumber = '" + contractnumber + "' ; ";
        }





        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Update_Transaction_ar_adjust(string last_product_amount, string last_service_serCharge_amount, string product_amount,
      string service_serCharge_amount, string id, string contractnumber, string adjust_remark, int vat, string type, string update_id, string update_role)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        decimal service_serCharge_amount_ = 0;

        if (service_serCharge_amount != "")
        {
            service_serCharge_amount_ = Convert.ToDecimal(service_serCharge_amount);
        }
        else
        {
            service_serCharge_amount_ = 0;
        }




        decimal product_amount_ = 0;

        if (product_amount != "")
        {
            product_amount_ = Convert.ToDecimal(product_amount);
        }
        else
        {
            product_amount_ = 0;
        }








        decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

        decimal v2 = 0;

        if (type == "prod_service")
        {
            if (service_serCharge_amount != "")
            {
                v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
            }
        }



        if (product_amount_ != 0 || service_serCharge_amount_ != 0)
        {
            strSQL = " update tbltransaction_record set " +
                    " last_product_amount = '" + last_product_amount + "', " +
                    " last_service_serCharge_amount = '" + last_service_serCharge_amount + "', " +
                    " product_amount = '" + product_amount + "', " +
                    " service_serCharge_amount = '" + service_serCharge_amount + "', " +
                    " product_amount_vat = '" + v1 + "', " +
                    " service_serCharge_amount_vat = '" + v2 + "', " +

                    " no_type = '' , " +
                    " update_date = getdate() , " +
                    " update_id = '" + update_id + "', " +
                    " update_role = '" + update_role + "', " +

                    " adjust_remark = adjust_remark + ' แก้ไขเมื่อ" + adjust_remark + "', flag_adjust = 'n' " +

                    " where id = '" + id + "' and contractnumber = '" + contractnumber + "' ; ";
        }
        else
        {
            strSQL = " update tbltransaction_record set " +
                    " last_product_amount = '" + last_product_amount + "', " +
                    " last_service_serCharge_amount = '" + last_service_serCharge_amount + "', " +
                    " product_amount = '" + product_amount + "', " +
                    " service_serCharge_amount = '" + service_serCharge_amount + "', " +
                    " product_amount_vat = '" + v1 + "', " +
                    " service_serCharge_amount_vat = '" + v2 + "', " +

                    " update_date = getdate() , " +
                    " update_id = '" + update_id + "', " +
                    " update_role = '" + update_role + "', " +

                    " adjust_remark = adjust_remark + ' แก้ไขเมื่อ" + adjust_remark + "', flag_adjust = 'n' " +

                    " where id = '" + id + "' and contractnumber = '" + contractnumber + "' ; ";
        }




        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public void Update_Transaction_ar_nokey(string last_product_amount, string last_service_serCharge_amount, string product_amount,
      string service_serCharge_amount, string id, string contractnumber, int vat, string type, string update_id, string update_role)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        decimal service_serCharge_amount_ = 0;
        decimal product_amount_ = 0;

        if (service_serCharge_amount != "")
        {
            service_serCharge_amount_ = Convert.ToDecimal(service_serCharge_amount);
        }
        else
        {
            service_serCharge_amount_ = 0;
        }

        if (product_amount != "")
        {
            product_amount_ = Convert.ToDecimal(product_amount);
        }
        else
        {
            product_amount_ = 0;
        }

        decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

        decimal v2 = 0;

        if (type == "prod_service")
        {
            if (service_serCharge_amount != "")
            {
                v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
            }
        }

        if (product_amount_ != 0 || service_serCharge_amount_ != 0)
        {
            strSQL = " update tbltransaction_record set " +
         //" last_product_amount = '" + last_product_amount + "', " +
         //" last_service_serCharge_amount = '" + last_service_serCharge_amount + "', " +
         " product_amount = '" + product_amount + "', " +
         " service_serCharge_amount = '" + service_serCharge_amount_ + "', " +
         " product_amount_vat = '" + v1 + "', " +
         " service_serCharge_amount_vat = '" + v2 + "', " +

         " update_date = getdate() , " +
         " no_type = '' , " +
         " update_id = '" + update_id + "', " +
         " update_role = '" + update_role + "',  " +
          " status = 'y' " +

         " where id = '" + id + "' and contractnumber = '" + contractnumber + "' ; ";
        }
        else
        {
            strSQL = " update tbltransaction_record set " +
           //" last_product_amount = '" + last_product_amount + "', " +
           //" last_service_serCharge_amount = '" + last_service_serCharge_amount + "', " +
           " product_amount = '" + product_amount + "', " +
           " service_serCharge_amount = '" + service_serCharge_amount_ + "', " +
           " product_amount_vat = '" + v1 + "', " +
           " service_serCharge_amount_vat = '" + v2 + "', " +

           " update_date = getdate() , " +
           " update_id = '" + update_id + "', " +
           " update_role = '" + update_role + "',  " +

       //" adjust_remark = '" + adjust_remark + "', " +
       " status = 'y' " +

           " where id = '" + id + "' and contractnumber = '" + contractnumber + "' ; ";
        }



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }



    public void Update_Password(string username, string contractnumber, string password, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        strSQL = " update tblcontract_account set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) , flag_reset = 'n',update_date = getdate(), update_id = '" + update_id + "' " +
            " where username = '" + username + "' and contractnumber = '" + contractnumber + "' ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }
    public void Update_Password_mobile(string username, string contractnumber, string password, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        strSQL = " update tblcontract_account set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) , flag_reset = 'n',update_date = getdate(), update_id = '" + update_id + "' " +
            " where username = '" + username + "' and contractnumber = '" + contractnumber + "' ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@username", username);
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.Parameters.AddWithValue("@password", password);
        _with1.Parameters.AddWithValue("@update_id", update_id);
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable getTransaction_dup(string contractnumber, string date1)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tbltransaction_record where contractnumber = '" + contractnumber + "' and Substring(Convert(varchar(50),record_date,120),1,10) = '" + date1 + "' ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getTransaction_dup_mobile(string contractnumber, string date1)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tbltransaction_record where contractnumber = @contractnumber and Substring(Convert(varchar(50),record_date,120),1,10) = @date1 ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.Parameters.AddWithValue("@date1", date1);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetCompany(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tblsmart_Contract where  contractnumber = @ContractNumber ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetVAT(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tblsetting_vat where CompanyCode = '" + CompanyCode + "' ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetVAT_mobile(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tblsetting_vat where CompanyCode = @CompanyCode  ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@CompanyCode", CompanyCode);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void Confirm_transaction(string id_list, string contractnumber)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        strSQL = " update tbltransaction_record set flag_confirm = 'y' where ContractNumber = '" + contractnumber + "' and id in (" + id_list + ") ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }
    public void Confirm_transaction_mobile(string id_list, string contractnumber)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        var _with1 = objCmd;
        string[] id_list_ = id_list.ToString().Split(',');

        strSQL = " update tbltransaction_record set flag_confirm = 'y' where ContractNumber =  @contractnumber  and id in ( ";

        int i = 1;
        foreach (var id in id_list_)
        {
            strSQL += "@id_list" + i + ",";
            _with1.Parameters.AddWithValue("@id_list" + i, id);
            i++;
        }

        strSQL = strSQL.Substring(0, strSQL.Length - 1);

        strSQL = strSQL + " ) ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable getNoti(string contractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tblnoti_message where status = 'y' and   contractNumber = '" + contractNumber + "' ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable getNoti_mobile(string contractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tblnoti_message where status = 'y' and   contractNumber = @contractNumber  ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@contractNumber", contractNumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void UpdateNoti(string id, string contractnumber)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        strSQL = " update tblnoti_message set status = 'n' where ContractNumber = '" + contractnumber + "' and id = '" + id + "' ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }
    public void UpdateNoti_mobile(string id, string contractnumber)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;


        strSQL = " update tblnoti_message set status = 'n' where ContractNumber =  @contractnumber  and id =  @id  ; ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
        _with1.Parameters.AddWithValue("@id", id);
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable GetContract_ShopOper_Close(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select top(1) s.smart_shop_open,s.smart_shop_close from tblsmart_Contract s where s.ContractNumber = '" + ContractNumber + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    public DataTable GetContract_ShopOper_Close_mobile(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select top(1) s.smart_shop_open,s.smart_shop_close from tblsmart_Contract s where s.ContractNumber =  @ContractNumber  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }




}