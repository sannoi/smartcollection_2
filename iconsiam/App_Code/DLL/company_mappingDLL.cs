﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for company_mappingฏศศ
/// </summary>
public class company_mappingDLL
{

    //public DataTable GetCompany()
    //{

    //    SqlConnection objConn = new SqlConnection();
    //    SqlCommand objCmd = new SqlCommand();
    //    SqlDataAdapter dtAdapter = new SqlDataAdapter();

    //    DataSet ds = new DataSet();
    //    DataTable dt = null;
    //    string strSQL = "";

    //    strSQL += " select * from SAPCompany where IsActive = '1'  order by CompanyNameTH  ";

    //    objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
    //    var _with1 = objCmd;
    //    _with1.Connection = objConn;
    //    _with1.CommandText = strSQL;
    //    _with1.CommandType = CommandType.Text;
    //    dtAdapter.SelectCommand = objCmd;

    //    dtAdapter.Fill(ds);
    //    dt = ds.Tables[0];

    //    dtAdapter = null;
    //    objConn.Close();
    //    objConn = null;

    //    return dt;
    //}


    public DataTable getCompany(string Companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select Companycode, CompanyNameTh from TblCompanyDetail " +
            " where Companycode in (" + Companycode + ") order by CompanyNameTh;  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getGroupCompany()
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblCompanyGroup where IsActive = 'y'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetMapCompany(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (userid != "")
        {
            strSQL += " select  distinct company_code from tblmapp_User_Company where userid in (" + userid + ") ;  ";

        }
        else
        {
            strSQL += " select  distinct company_code from tblmapp_User_Company ;  ";

        }

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetMapping(string username, string comcode, string role)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (comcode == "")
        {
            strSQL += " select m.id, u.username,concat(u.fname,' ',u.lname) as name ,s.CompanyNameTh, m.update_date " +
                  " from tbluser u inner " +
                  " join tblmapp_User_Company m on u.userid = m.userid " +
                  " inner " +
                  " join TblCompanyDetail s on m.company_code = s.Companycode " +
                  " where u.username like '%" + username + "%' and u.user_group = '" + role + "' ; ";

        }
        else
        {
            strSQL += " select m.id, u.username,concat(u.fname,' ',u.lname) as name ,s.CompanyNameTh, m.update_date " +
               " from tbluser u inner " +
               " join tblmapp_User_Company m on u.userid = m.userid " +
               " inner " +
               " join TblCompanyDetail s on m.company_code = s.Companycode where u.username like '%" + username + "%' " +
               " and m.company_code = '" + comcode + "' and u.user_group = '" + role + "'  ";
        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetMapping_group(string username, string comcode, string role)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (comcode == "")
        {
            strSQL += " select m.id, u.username,concat(u.fname,' ',u.lname) as name ,s.CompanyNameTh, m.update_date " +
                  " from tbluser u inner " +
                  " join tblmapp_User_Company m on u.userid = m.userid " +
                  " inner " +
                  " join TblCompanyDetail s on m.company_code = s.Companycode " +
                  " where u.username like '%" + username + "%' and u.role = '" + role + "' ";

        }
        else
        {
            strSQL += " select m.id, u.username,concat(u.fname,' ',u.lname) as name ,s.CompanyNameTh, m.update_date " +
               " from tbluser u inner " +
               " join tblmapp_User_Company m on u.userid = m.userid " +
               " inner " +
               " join TblCompanyDetail s on m.company_code = s.Companycode where u.username like '%" + username + "%' " +
               " and m.company_code in (select CompanyCode from TblCompanyGroupMapping where ComGroupID = '" + comcode + "' )" +
               " and u.role = '" + role + "'  ";
        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public void Delete_Mapping(string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = "  delete from tblmapp_User_Company where id  = '" + id + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

}