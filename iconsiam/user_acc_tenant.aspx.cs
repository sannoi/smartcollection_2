﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class user_acc_tenant : System.Web.UI.Page
    {
        sentmail mail = new sentmail();
        user_acc_tenantDLL Serv = new user_acc_tenantDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_data();                  
                }
            }
        }

        protected void bind_default()
        {
            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("Active", "y"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "n"));


            ddlstatus2.Items.Clear();
            ddlstatus2.Items.Insert(0, new ListItem("Active", "y"));
            ddlstatus2.Items.Insert(1, new ListItem("Inactive", "n"));

        }

        protected void bind_data()
        {
            string[] name = txtname.Text.Split(',');
            string[] shopname = txtshopname.Text.Split(',');

            var user = Serv.GetUser_account(name, ddlstatus.SelectedValue, shopname, txtcontactnumber.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), HttpContext.Current.Session["s_com_code"].ToString());
            if (user.Rows.Count != 0)
            {
                GridView_List.DataSource = user;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void btnedit_Click(object sender, EventArgs e)
        {

            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");//
            HiddenField hdd_username = (HiddenField)row.FindControl("hdd_username");//hdd_flag_active
            HiddenField hdd_flag_active = (HiddenField)row.FindControl("hdd_flag_active");//

            HiddenField_id.Value = hdd_id.Value;
            HiddenField_user.Value = hdd_username.Value;

            txtusername.Text = HiddenField_user.Value;
            ddlstatus2.SelectedValue = hdd_flag_active.Value;

            Panel1.Visible = false;
            Panel3.Visible = true;
            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_data();
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HiddenField hdd_contractNumber = (HiddenField)row.FindControl("hdd_contractNumber");
            HiddenField hdd_username = (HiddenField)row.FindControl("hdd_username");

            Serv.UpdatePassword(ConfigurationManager.AppSettings["default_password"], hdd_id.Value,
                HttpContext.Current.Session["s_userid"].ToString());

            ///// Sent EMAIL ////

            var contact = Serv.GetContactPoint(hdd_contractNumber.Value);
            if (contact.Rows.Count != 0)
            {
                string name = "";
                string email = "";

                for (int j = 0; j < contact.Rows.Count; j++)
                {
                    if (contact.Rows[0]["IsMain"].ToString() == "y")
                    {

                        name = name + contact.Rows[j]["name"].ToString() + ",";
                        email = email + contact.Rows[j]["email"].ToString() + ",";


                    }
                }

                name = name.Substring(0, name.Length - 1);
                email = email.Substring(0, email.Length - 1);

                var cont = Serv.GetContractByNumber(hdd_contractNumber.Value);
                if (cont.Rows.Count != 0)
                {
                    string comname_ = "";
                    var comname = Serv.GetCompanyCodeByCompanyCode(cont.Rows[0]["CompanyCode"].ToString());
                    if (comname.Rows.Count != 0)
                    {
                        comname_ = comname.Rows[0]["CompanyNameEN"].ToString();
                    }

                    mail.CallMail_bcc_ar(email, "ระบบ Tenant Sales Data Collection  : Reset Password  ร้าน " + cont.Rows[0]["ShopName"].ToString() + " ห้อง " +
                        cont.Rows[0]["smart_room_no"].ToString(), "Tenant Sales Data Collection", name, "", comname_, "ชื่อร้าน", "ชื่อบริษัท",
                   "ชั้น", "ห้อง", "Username", "Password", "", cont.Rows[0]["ShopName"].ToString(),
                   cont.Rows[0]["BusinessPartnerName"].ToString(), cont.Rows[0]["smart_floor"].ToString(),
                   cont.Rows[0]["smart_room_no"].ToString(), hdd_username.Value, ConfigurationManager.AppSettings["default_password"], "icon", hdd_contractNumber.Value,"tenant");

                }


            }




            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='user_acc_tenant.aspx';", true);
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            var ux = Serv.GetUser_accountExist(txtusername.Text, HiddenField_id.Value);
            if (ux.Rows.Count != 0)
            {
                POPUPMSG("Username ที่ตั้งซ้ำในระบบ");
                return;
            }
            else
            {
                Serv.UpdateUsername(txtusername.Text, ddlstatus2.SelectedValue, HiddenField_id.Value, HttpContext.Current.Session["s_userid"].ToString());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='user_acc_tenant.aspx';", true);
            }

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btnreset2_Click(object sender, EventArgs e)
        {
            Serv.UpdatePassword(ConfigurationManager.AppSettings["default_password"], HiddenField_id.Value, HttpContext.Current.Session["s_userid"].ToString());

            ///// Sent EMAIL ////

            var contact = Serv.GetContactPoint(txtcontactnumber.Text);
            if (contact.Rows.Count != 0)
            {
                string name = "";
                string email = "";

                for (int j = 0; j < contact.Rows.Count; j++)
                {
                    if (contact.Rows[0]["IsMain"].ToString() == "y")
                    {

                        name = name + contact.Rows[j]["name"].ToString() + ",";
                        email = email + contact.Rows[j]["email"].ToString() + ",";


                    }
                }

                name = name.Substring(0, name.Length - 1);
                email = email.Substring(0, email.Length - 1);

                var cont = Serv.GetContractByNumber(txtcontactnumber.Text);
                if (cont.Rows.Count != 0)
                {
                    string comname_ = "";
                    var comname = Serv.GetCompanyCodeByCompanyCode(cont.Rows[0]["CompanyCode"].ToString());
                    if (comname.Rows.Count != 0)
                    {
                        comname_ = comname.Rows[0]["CompanyNameEN"].ToString();
                    }

                    mail.CallMail_bcc_ar(email, "ระบบ Tenant Sales Data Collection  : Reset Password  ร้าน " + cont.Rows[0]["ShopName"].ToString() + " ห้อง " +
                        cont.Rows[0]["smart_room_no"].ToString(), "Tenant Sales Data Collection", name, "", comname_, "ชื่อร้าน", "ชื่อบริษัท",
                   "ชั้น", "ห้อง", "Username", "Password", "", cont.Rows[0]["ShopName"].ToString(),
                   cont.Rows[0]["BusinessPartnerName"].ToString(), cont.Rows[0]["smart_floor"].ToString(),
                   cont.Rows[0]["smart_room_no"].ToString(), txtusername.Text, ConfigurationManager.AppSettings["default_password"], "icon", txtcontactnumber.Text,"tenant");

                }

            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='user_acc_tenant.aspx';", true);
        }

        protected void GridView_List_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Button btnreset = (Button)(e.Row.FindControl("btnreset"));
                btnreset.Attributes.Add("onclick", "return confirm('ท่านต้อง Reset Password หรือไม่');");
            }
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/user_acc_tenant.aspx");

        }
    }
}