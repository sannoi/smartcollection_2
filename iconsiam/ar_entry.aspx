﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ar_entry.aspx.cs" Inherits="iconsiam.App_Code.DLL.ar_entry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

    <style type="text/css">
        .test .ajax__calendar_container {
            z-index: 10000 !important;
            text-wrap: none;
        }

        .test .ajax__calendar_dayname {
            height: 17px;
            width: 34px;
            text-align: center;
        }
    </style>

    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                AR Entry :
                <asp:Label ID="lbfordate" runat="server" Visible="false" Text="-"></asp:Label>
            </p>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlcompany" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <%--    <asp:TextBox ID="txtstartdate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>
                    <asp:CalendarExtender ID="txtstartdate_CalendarExtender" runat="server"
                        BehaviorID="txtstartdate_CalendarExtender" TargetControlID="txtstartdate" Format="dd/MM/yyyy"></asp:CalendarExtender>--%>
                    <asp:TextBox ID="txtbpname" runat="server" placeholder="ชื่อบริษัทร้านค้า" class="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12">
            <div class="row">
                 <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                <%--     <asp:TextBox ID="txtenddate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>
                    <asp:CalendarExtender ID="txtenddate_CalendarExtender" runat="server"
                        BehaviorID="txtenddate_CalendarExtender" TargetControlID="txtenddate" Format="dd/MM/yyyy"></asp:CalendarExtender>--%>
                <asp:TextBox ID="txtshopname" runat="server" placeholder="Shop Name" class="form-control"></asp:TextBox>
            </div>
            <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                <asp:TextBox ID="txtroomno" runat="server" placeholder="Room No." class="form-control"></asp:TextBox>
            </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="Button1" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn" />
                    <asp:Button ID="Button2" runat="server" Text="Clear" OnClick="btncancel_Click" class="btn ThemeBtn" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        OnRowDataBound="GridView_List_RowDataBound"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="smart_room_no" HeaderText="Room No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="BusinessPartnerName" HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />

                            <asp:BoundField DataField="username" HeaderText="AR" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />


                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="300px">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_smart_record_person_type" runat="server" Value='<%# Eval("smart_record_person_type") %>' />
                                    <asp:HiddenField ID="hdd_ContractNumber" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("id") %>' />
                                    <asp:Button ID="btnrecord" runat="server" Text="รายการทั้งหมด / บันทึกยอด" class="btn btn-success" OnClick="btnrecord_Click" ForeColor="Black" Visible="true" />
                                    <asp:Button ID="btndetail" runat="server" Text="รายการทั้งหมด" class="btn" OnClick="btndetail_Click" Visible="true" ForeColor="Black"
                                        BackColor="#f8ca3e" BorderColor="#a78a65" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>
            </div>


        </div>
    </div>




</asp:Content>
