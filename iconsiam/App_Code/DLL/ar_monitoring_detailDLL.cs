﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ar_monitoring_detailDLL
/// </summary>
public class ar_monitoring_detailDLL
{
    public ar_monitoring_detailDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetContract(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no ,  s.ContractNumber, " +
                    " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, c.CompanyCode " +
                    " from tblsmart_Contract s inner " +
                    " join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                    " where s.ContractNumber = '" + ContractNumber + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getOldContract(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        strSQL += " select * from tblsmart_Contract where ContractNumber = '" + ContractNumber + "' and Contract_old_number != '' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetTransaction(string ContractNumber, string d1, string d2, string status)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (status == "")
        {
            strSQL += "select t.id, t.record_date ,t.ContractNumber,t.product_amount,t.service_serCharge_amount, case " +

                " when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' " +
                " when t.status = 'y' then 'ส่งยอดแล้ว' " +

                " else 'ยังไม่ส่งยอด' end as status , " +
                "  case when t.flag_confirm = 'y' then 'ยืนยันยอดแล้ว' else 'ยังไม่ยืนยันยอด' end as flag_confirm  " +
                " from tbltransaction_record t where ContractNUmber in  (" + ContractNumber + ") " +
                " and t.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59' " +
                " order by  t.record_date ";
        }
        else
        {
            if (status == "c" || status == "nc")
            {
                string s;
                if (status == "c")
                {
                    s = "y";
                }
                else
                {
                    s = "n";
                }

                strSQL += "select t.id, t.record_date ,t.ContractNumber,t.product_amount,t.service_serCharge_amount, case " +

                " when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' " +
                " when t.status = 'y' then 'ส่งยอดแล้ว' " +

                    " else 'ยังไม่ส่งยอด' end as status , " +
              "  case when t.flag_confirm = 'y' then 'ยืนยันยอดแล้ว' else 'ยังไม่ยืนยันยอด' end as flag_confirm  " +
              " from tbltransaction_record t where ContractNUmber in  (" + ContractNumber + ") " +
              " and t.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59' and t.flag_confirm = '" + s + "' ";

            }
            else if (status == "a")
            {
                strSQL += "select t.id, t.record_date ,t.ContractNumber,t.product_amount,t.service_serCharge_amount, case " +

                " when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' " +
                " when t.status = 'y' then 'ส่งยอดแล้ว' " +

                    " else 'ยังไม่ส่งยอด' end as status , " +
              "  case when t.flag_confirm = 'y' then 'ยืนยันยอดแล้ว' else 'ยังไม่ยืนยันยอด' end as flag_confirm  " +
              " from tbltransaction_record t where ContractNUmber in  (" + ContractNumber + ") " +
              " and t.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59' and t.flag_adjust = 'y' ";

            }
            else
            {
                strSQL += "select t.id, t.record_date ,t.ContractNumber,t.product_amount,t.service_serCharge_amount, case " +

                " when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' " +
                " when t.status = 'y' then 'ส่งยอดแล้ว' " +

                    " else 'ยังไม่ส่งยอด' end as status , " +
              "  case when t.flag_confirm = 'y' then 'ยืนยันยอดแล้ว' else 'ยังไม่ยืนยันยอด' end as flag_confirm  " +
              " from tbltransaction_record t where ContractNUmber in  (" + ContractNumber + ") " +
              " and t.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 23:59:59' and t.status = '" + status + "' ";

            }
        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }



    public DataTable Get_Confirm_new(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tbljob where job_name = 'jobconfirm'  and CompanyCode = '" + CompanyCode + "' ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable Get_Confirm_new2(string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tbljob where job_name = 'jobconfirm2'   and CompanyCode = '" + CompanyCode + "' ; ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }



    public void Update_flagConfirm(string ContractNumber, string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;





        strSQL = " update tbltransaction_record set  flag_confirm = 'y'  where id = '" + id + "' and contractnumber = '" + ContractNumber + "'; ";



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }










}