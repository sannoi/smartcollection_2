﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;

namespace iconsiam
{
    public partial class Default : System.Web.UI.Page
    {

        [WebMethod]
        public static List<object> GetChartData()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Date", typeof(string));

            var group_loca = GetGroupLocation(DateTime.Now.ToString("yyyy-MM-", EngCI) + "01", HttpContext.Current.Session["s_com_code"].ToString());
            if (group_loca.Rows.Count != 0)
            {
                for (int i = 0; i < group_loca.Rows.Count; i++)
                {
                    if (group_loca.Rows[i]["location"].ToString() != "Summary")
                    {
                        dt.Columns.Add(group_loca.Rows[i]["location"].ToString(), typeof(decimal));
                    }
                }
            }

            dt.Columns.Add("Sum", typeof(decimal));

            string d1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + "01";
            string d2 = DateTime.Now.ToString("yyyy-MM-", EngCI) +
                Convert.ToInt32(DateTime.DaysInMonth(Convert.ToInt32(DateTime.Now.ToString("yyyy", EngCI)), Convert.ToInt32(DateTime.Now.ToString("MM", EngCI)))).ToString("0#");

            double result = (Convert.ToDateTime(d2) - Convert.ToDateTime(d1)).TotalDays;           
            if (result != 0)
            {

                for (int j = 0; j <= result; j++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();
                    decimal Summ = 0;

                    string date1 = Convert.ToDateTime(d1).AddDays(j).ToString("yyyy-MM-dd", EngCI);
                    row1["Date"] = date1;

                    if (dt.Columns.Count != 0)
                    {

                        for (int inCx = 1; inCx < dt.Columns.Count - 1; inCx++)
                        {
                            var saleAmount = GetSaleAmount(date1, dt.Columns[inCx].ColumnName);
                            if (saleAmount.Rows.Count != 0)
                            {
                                decimal saleAmount_ = 0;
                                for (int a = 0; a < saleAmount.Rows.Count; a++)
                                {
                                    if (saleAmount.Rows[a]["Amount"].ToString() != "")
                                    {
                                        saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount"].ToString());
                                    }
                                    else
                                    {
                                        saleAmount_ = saleAmount_ + 0;
                                    }
                                    Summ = Summ + saleAmount_;
                                }
                                row1[dt.Columns[inCx].ColumnName] = saleAmount_;                                
                            }                          
                        }
                    }
                    row1["Sum"] = Summ;
                    dt.Rows.Add(row1);
                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }


        [WebMethod]
        public static List<object> GetChart_Floor()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Date", typeof(string));

            var floor = GetFloor(DateTime.Now.ToString("yyyy-MM-", EngCI) + "01", HttpContext.Current.Session["s_com_code"].ToString());
            if (floor.Rows.Count != 0)
            {
                for (int i = 0; i < floor.Rows.Count; i++)
                {
                    if (floor.Rows[i]["Floor"].ToString() != "Summary")
                    {
                        dt.Columns.Add(floor.Rows[i]["Floor"].ToString(), typeof(decimal));
                    }
                }
            }

            dt.Columns.Add("Sum", typeof(decimal));

            string d1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + "01";
            string d2 = DateTime.Now.ToString("yyyy-MM-", EngCI) +
                Convert.ToInt32(DateTime.DaysInMonth(Convert.ToInt32(DateTime.Now.ToString("yyyy", EngCI)), Convert.ToInt32(DateTime.Now.ToString("MM", EngCI)))).ToString("0#");

            double result = (Convert.ToDateTime(d2) - Convert.ToDateTime(d1)).TotalDays;// DateTime.Compare(Convert.ToDateTime(d2), Convert.ToDateTime(d1));            
            if (result != 0)
            {

                for (int j = 0; j <= result; j++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    decimal Summ = 0;

                    string date1 = Convert.ToDateTime(d1).AddDays(j).ToString("yyyy-MM-dd", EngCI);
                    row1["Date"] = date1;

                    if (dt.Columns.Count != 0)
                    {

                        for (int inCx = 1; inCx < dt.Columns.Count - 1; inCx++)
                        {
                            var saleAmount = GetSaleAmount_floor(date1, dt.Columns[inCx].ColumnName);
                            if (saleAmount.Rows.Count != 0)
                            {
                                decimal saleAmount_ = 0;
                                for (int a = 0; a < saleAmount.Rows.Count; a++)
                                {
                                    if (saleAmount.Rows[a]["Amount"].ToString() != "")
                                    {
                                        saleAmount_ = saleAmount_+ Convert.ToDecimal(saleAmount.Rows[a]["Amount"].ToString());
                                    }
                                    else
                                    {
                                        saleAmount_ = saleAmount_ + 0;
                                    }
                                    Summ = Summ + saleAmount_;
                                }
                                row1[dt.Columns[inCx].ColumnName] = saleAmount_;                               
                            }                                                                                 
                        }
                    }
                    row1["Sum"] = Summ;
                    dt.Rows.Add(row1);
                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChart_CategoryLeasing()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Date", typeof(string));

            var CategoryLeasing = GetCategoryLeasing(DateTime.Now.ToString("yyyy-MM-", EngCI) + "01" , HttpContext.Current.Session["s_com_code"].ToString());
            if (CategoryLeasing.Rows.Count != 0)
            {
                for (int i = 0; i < CategoryLeasing.Rows.Count; i++)
                {
                    if (CategoryLeasing.Rows[i]["Categoryleasing"].ToString() != "Summary")
                    {
                        dt.Columns.Add(CategoryLeasing.Rows[i]["Categoryleasing"].ToString(), typeof(decimal));
                    }
                }
            }

            dt.Columns.Add("Sum", typeof(decimal));

            string d1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + "01";
            string d2 = DateTime.Now.ToString("yyyy-MM-", EngCI) +
                Convert.ToInt32(DateTime.DaysInMonth(Convert.ToInt32(DateTime.Now.ToString("yyyy", EngCI)), Convert.ToInt32(DateTime.Now.ToString("MM", EngCI)))).ToString("0#");

            double result = (Convert.ToDateTime(d2) - Convert.ToDateTime(d1)).TotalDays;// DateTime.Compare(Convert.ToDateTime(d2), Convert.ToDateTime(d1));
            if (result != 0)
            {
                for (int j = 0; j <= result; j++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    decimal Summ = 0;

                    string date1 = Convert.ToDateTime(d1).AddDays(j).ToString("yyyy-MM-dd", EngCI);
                    row1["Date"] = date1;

                    if (dt.Columns.Count != 0)
                    {

                        for (int inCx = 1; inCx < dt.Columns.Count - 1; inCx++)
                        {
                            var saleAmount = GetSaleAmount_CategoryLeasing(date1, dt.Columns[inCx].ColumnName);
                            if (saleAmount.Rows.Count != 0)
                            {
                                decimal saleAmount_ = 0;
                                for (int a = 0; a < saleAmount.Rows.Count; a++)
                                {
                                    if (saleAmount.Rows[a]["Amount"].ToString() != "")
                                    {
                                        saleAmount_ = Convert.ToDecimal(saleAmount.Rows[a]["Amount"].ToString());
                                    }
                                    else
                                    {
                                        saleAmount_ = 0;
                                    }
                                    Summ = Summ + saleAmount_;
                                }
                                row1[dt.Columns[inCx].ColumnName] = saleAmount_;
                            }                                                                                 
                        }
                    }
                    row1["Sum"] = Summ;
                    dt.Rows.Add(row1);
                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChart_IndustryGroup()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Date", typeof(string));

            var CategoryLeasing = GetIndustryGroup(DateTime.Now.ToString("yyyy-MM-", EngCI) + "01", HttpContext.Current.Session["s_com_code"].ToString());
            if (CategoryLeasing.Rows.Count != 0)
            {
                for (int i = 0; i < CategoryLeasing.Rows.Count; i++)
                {
                    if (CategoryLeasing.Rows[i]["Industrygroup"].ToString() != "Summary")
                    {
                        dt.Columns.Add(CategoryLeasing.Rows[i]["Industrygroup"].ToString(), typeof(decimal));
                    }
                }
            }

            dt.Columns.Add("Sum", typeof(decimal));

            string d1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + "01";
            string d2 = DateTime.Now.ToString("yyyy-MM-", EngCI) +
                Convert.ToInt32(DateTime.DaysInMonth(Convert.ToInt32(DateTime.Now.ToString("yyyy", EngCI)), Convert.ToInt32(DateTime.Now.ToString("MM", EngCI)))).ToString("0#");

            double result = (Convert.ToDateTime(d2) - Convert.ToDateTime(d1)).TotalDays;// DateTime.Compare(Convert.ToDateTime(d2), Convert.ToDateTime(d1));
            if (result != 0)
            {

                for (int j = 0; j <= result; j++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    decimal Summ = 0;

                    string date1 = Convert.ToDateTime(d1).AddDays(j).ToString("yyyy-MM-dd", EngCI);
                    row1["Date"] = date1;

                    if (dt.Columns.Count != 0)
                    {

                        for (int inCx = 1; inCx < dt.Columns.Count - 1; inCx++)
                        {
                            var saleAmount = GetSaleAmount_IndustryGroup(date1, dt.Columns[inCx].ColumnName);
                            if (saleAmount.Rows.Count != 0)
                            {
                                decimal saleAmount_ = 0;
                                for (int a = 0; a < saleAmount.Rows.Count; a++)
                                {
                                    if (saleAmount.Rows[a]["Amount"].ToString() != "")
                                    {
                                        saleAmount_ = saleAmount_  + Convert.ToDecimal(saleAmount.Rows[a]["Amount"].ToString());
                                    }
                                    else
                                    {
                                        saleAmount_ = saleAmount_ + 0;
                                    }
                                    Summ = Summ + saleAmount_;
                                }
                                row1[dt.Columns[inCx].ColumnName] = saleAmount_;
                            }                                                                               
                        }
                    }
                    row1["Sum"] = Summ;
                    dt.Rows.Add(row1);
                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }

        [WebMethod]
        public static List<object> GetChart_Building()
        {
            List<object> chartData = new List<object>();


            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            DataTable dt = new DataTable();
            dt.Columns.Add("Date", typeof(string));

            var Building = GetBuilding(DateTime.Now.ToString("yyyy-MM-", EngCI) + "01" , HttpContext.Current.Session["s_com_code"].ToString());
            if (Building.Rows.Count != 0)
            {
                for (int i = 0; i < Building.Rows.Count; i++)
                {
                    if (Building.Rows[i]["Building"].ToString() != "Summary")
                    {
                        dt.Columns.Add(Building.Rows[i]["Building"].ToString(), typeof(decimal));
                    }
                }
            }

            dt.Columns.Add("Sum", typeof(decimal));

            string d1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + "01";
            string d2 = DateTime.Now.ToString("yyyy-MM-", EngCI) +
                Convert.ToInt32(DateTime.DaysInMonth(Convert.ToInt32(DateTime.Now.ToString("yyyy", EngCI)), Convert.ToInt32(DateTime.Now.ToString("MM", EngCI)))).ToString("0#");

            double result = (Convert.ToDateTime(d2) - Convert.ToDateTime(d1)).TotalDays;// DateTime.Compare(Convert.ToDateTime(d2), Convert.ToDateTime(d1));
            
            if (result != 0)
            {

                for (int j = 0; j <= result; j++)
                {
                    DataRow row1 = dt.NewRow();
                    row1 = dt.NewRow();

                    decimal Summ = 0;

                    string date1 = Convert.ToDateTime(d1).AddDays(j).ToString("yyyy-MM-dd", EngCI);
                    row1["Date"] = date1;

                    if (dt.Columns.Count != 0)
                    {

                        for (int inCx = 1; inCx < dt.Columns.Count - 1; inCx++)
                        {
                            var saleAmount = GetSaleAmount_Building(date1, dt.Columns[inCx].ColumnName);
                            if (saleAmount.Rows.Count != 0)
                            {
                                decimal saleAmount_ = 0;
                                for (int a = 0; a < saleAmount.Rows.Count; a++)
                                {
                                    if (saleAmount.Rows[a]["Amount"].ToString() != "")
                                    {
                                        saleAmount_ = saleAmount_ + Convert.ToDecimal(saleAmount.Rows[a]["Amount"].ToString());
                                    }
                                    else
                                    {
                                        saleAmount_ = saleAmount_ + 0;
                                    }
                                    Summ = Summ + saleAmount_;
                                }
                                row1[dt.Columns[inCx].ColumnName] = saleAmount_;
                            }
                        }
                    }
                    row1["Sum"] = Summ;
                    dt.Rows.Add(row1);
                }
            }

            object[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();
            chartData.Add(columnNames);


            DataRow[] dr = dt.AsEnumerable().Take(dt.Rows.Count).ToArray();
            if (dr.Length != 0)
            {
                for (int d = 0; d < dr.Length; d++)
                {
                    chartData.Add(dr[d].ItemArray);
                }

            }

            return chartData;

        }










        /// <summary>
        /// Query
        /// </summary>
        /// <returns></returns>

        public static DataTable GetGroupLocation(string date,string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            //strSQL += "  select * from tblJob_chart_grouplocation where date = '" + date + "'  ";
            strSQL += " select Distinct Date , Location from tblJob_chart_grouplocation where date = '"+date+"' and Companycode in ("+Companycode+") ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetFloor(string date1 , string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select Distinct Date ,Floor from tblJob_chart_Floor where date = '" + date1 + "' and Companycode in ("+Companycode+")  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetCategoryLeasing(string date1,string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select Distinct Date , Categoryleasing from tblJob_chart_Categoryleasing where date = '" + date1 + "' and Companycode in ("+Companycode+") ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetIndustryGroup(string date1 , string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select Distinct Date , Industrygroup from tblJob_chart_industrygroup where date = '" + date1 + "' and Companycode in("+ Companycode + ") ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetBuilding(string date1 , string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            //strSQL += " select * from tblJob_chart_Building where date = '" + date1 + "' ; ";
            strSQL += " select Distinct Date , Building from tblJob_chart_Building where date = '"+date1+"' and Companycode in ("+Companycode+") ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetSaleAmount(string date1, string GrouplocationNameEN)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += "  select * from tblJob_chart_grouplocation where date = '" + date1 + "' and Location = '" + GrouplocationNameEN + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetSaleAmount_CategoryLeasing(string date1, string CategoryleasingNameEN)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblJob_chart_Categoryleasing where date = '" + date1 + "' and Categoryleasing = '" + CategoryleasingNameEN + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetSaleAmount_IndustryGroup(string date1, string IndustryGroupNameEN)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from tblJob_chart_industrygroup where date = '" + date1 + "' and Industrygroup = '" + IndustryGroupNameEN + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetSaleAmount_floor(string date1, string smart_floor)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblJob_chart_Floor where date = '" + date1 + "' and Floor = '" + smart_floor + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public static DataTable GetSaleAmount_Building(string date1, string BuildingNameEN)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from tblJob_chart_Building where date = '" + date1 + "' and Building = '" + BuildingNameEN + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


    }




}