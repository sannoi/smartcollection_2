﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for contractList_smartDLL
/// </summary>
public class contractList_smartDLL
{
    public contractList_smartDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetMapCompany(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (userid != "")
        {
            strSQL += " select  distinct company_code from tblmapp_User_Company where userid in (" + userid + ") ;  ";

        }
        else
        {
            strSQL += " select  distinct company_code from tblmapp_User_Company ;  ";

        }
        //strSQL += " select * from tblmapp_User_Company where userid = '" + userid + "';  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetCompany(string com_code)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from SAPCompany where IsActive = '1' and CompanyCode in (" + com_code + ") order by CompanyNameTH  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
    

    public DataTable GetSmart_Contract(string CompanyCode, string[] ShopName, string[] ShopGroupName, string smart_icon_staff_id,string IsActive)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string shopname = "";
        string shopgroup = "";


        if (ShopName.Length != 0)
        {
            for (int i = 0; i < ShopName.Length; i++)
            {
                shopname += " sc.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
            }

            shopname = shopname.Substring(0, shopname.Length - 2);
        }

        if (ShopGroupName.Length != 0)
        {
            for (int i = 0; i < ShopGroupName.Length; i++)
            {
                shopgroup += " (ISNULL(CONVERT(varchar(50),sg.ShopGroupNameEN),'')  like  '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or " +
                    " ISNULL(CONVERT(varchar(50), sg.ShopGroupNameTH),'')  like '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%') or";
            }

            shopgroup = shopgroup.Substring(0, shopgroup.Length - 2);
        }

        if(CompanyCode != "")
        {
            strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
              " sc.BusinessPartnerName,case when sc.smart_contract_status = '1' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, " +
              " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate " +
              " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
              " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
               "    left join tbluser u on sc.smart_icon_staff_id = u.userid " +
             " where sc.CompanyCode = '" + CompanyCode + "' and " + shopname + " and " +
               "  " + shopgroup + "  " +
               "  and sc.smart_icon_staff_id in (" + smart_icon_staff_id + ")  and smart_collection_flag_status = 'y'  and sc.smart_contract_status = '" + IsActive + "'   ";
        }
        else
        {
            strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
              " sc.BusinessPartnerName,case when sc.smart_contract_status = '1' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, " +
              " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate " +
              " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
              " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
               "    left join tbluser u on sc.smart_icon_staff_id = u.userid " +
             " where  " + shopname + " and " +
               "  " + shopgroup + "  " +
               "  and sc.smart_icon_staff_id in (" + smart_icon_staff_id + ")  and smart_collection_flag_status = 'y'  and sc.smart_contract_status = '" + IsActive + "'   ";
        }
     



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }
}