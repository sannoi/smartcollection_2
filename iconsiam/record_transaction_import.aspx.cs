﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
// using Excel = Microsoft.Office.Interop.Excel;

namespace iconsiam
{
    public partial class record_transaction_import : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        record_transactionDLL Serv = new record_transactionDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {

                    bind_rep();
                }
            }
        }


        protected void bind_rep()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var contract = Serv.GetContract(Request.QueryString["id"].ToString());
                if (contract.Rows.Count != 0)
                {
                    lbroomnum.Text = "ROOM Number  : " + contract.Rows[0]["smart_room_no"].ToString();
                    lbshopname.Text = "Shop Name : " + contract.Rows[0]["ShopName"].ToString();
                    hdd_keyintype.Value = contract.Rows[0]["smart_record_keyin_type"].ToString();

                    if (contract.Rows[0]["smart_record_keyin_type"].ToString() == "prod_sale")
                    {
                        lbkey_type.Text = "ยอดขายสินค้ารวม VAT";
                    }
                    else if (contract.Rows[0]["smart_record_keyin_type"].ToString() == "prod_service")
                    {
                        lbkey_type.Text = "ยอดขายสินค้ารวม VAT + ยอดบริการรวม VAT";
                    }
                    else if (contract.Rows[0]["smart_record_keyin_type"].ToString() == "prod_ser_charge")
                    {
                        lbkey_type.Text = "ยอดขายสินค้ารวม VAT + Service Charge รวม VAT";
                    }
                }
                else
                {
                    Response.Redirect("~/ar_monitoring.aspx");
                }
            }
            else
            {
                Response.Redirect("~/ar_monitoring.aspx");
            }

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ar_monitoring_detail.aspx?id=" + Request.QueryString["id"].ToString() + "&h=ar");
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile == false)
            {
                POPUPMSG("Please Select File");
                return;
            }

            int vat = 0;
            var v = Serv.GetVAT(Request.QueryString["id"].ToString().Substring(0, 3));
            if (v.Rows.Count != 0)
            {
                vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
            }
            else
            {
                vat = 7;
            }

            if (FileUpload1.HasFile) // || FileUpload2.HasFile || FileUpload3.HasFile
            {
                try
                {
                    if (FileUpload1.HasFile)
                    {
                        string filename1 = Path.GetFileName(FileUpload1.FileName);
                        FileInfo fi1 = new FileInfo(FileUpload1.FileName);
                        string ext = fi1.Extension;
                        if (ext == ".xls" || ext == ".xlsx")
                        {

                            int success = 0;
                            int fail = 0;

                            if (hdd_keyintype.Value == "prod_sale")
                            {

                                btnsave.Visible = false;
                                string xx = DateTime.Now.ToString("ssssHHddMM");
                                string img_name1 = "~/tmp_file_excel/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx + ext;
                                FileUpload1.SaveAs(Server.MapPath(img_name1));

                                string x = "";

                                string con =
                                     @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Server.MapPath(img_name1) + ";" +
                                     @"Extended Properties='Excel 12.0;HDR=Yes;'";
                                using (OleDbConnection connection = new OleDbConnection(con))
                                {
                                    connection.Open();
                                    OleDbCommand command = new OleDbCommand("select * from [Sheet1$]", connection);
                                    using (OleDbDataReader dr = command.ExecuteReader())
                                    {
                                        var dataTable = new DataTable();
                                        dataTable.Load(dr);
                                        if (dataTable.Rows.Count != 0)
                                        {
                                            for (int i = 4; i < dataTable.Rows.Count; i++)
                                            {
                                                if (dataTable.Rows[i][0].ToString() != "")
                                                {

                                                    string remark = "";
                                                    string nobill = "";

                                                    string xxx = "0";

                                                    if (dataTable.Rows[i][1].ToString() != "" && dataTable.Rows[i][1].ToString() != "-")
                                                    {
                                                        xxx = dataTable.Rows[i][1].ToString();
                                                    }
                                                    else
                                                    {
                                                        xxx = "0";
                                                    }


                                                    if (xxx == "0")
                                                    {

                                                        if (dataTable.Rows[i][2].ToString() == "No Bill")
                                                        {
                                                            nobill = "nobill";
                                                        }
                                                        else if (dataTable.Rows[i][2].ToString() == "ร้านปิด")
                                                        {
                                                            nobill = "close";
                                                        }
                                                        else if (dataTable.Rows[i][2].ToString() == "Other")
                                                        {
                                                            nobill = "other";
                                                        }

                                                        remark = dataTable.Rows[i][3].ToString();

                                                    }
                                                    else
                                                    {
                                                        remark = "";
                                                        nobill = "";
                                                    }

                                                    if (dataTable.Rows[i][2].ToString() == null || dataTable.Rows[i][2].ToString() == "")
                                                    {
                                                        var existing_ = Serv.getTransaction_dup(Request.QueryString["id"].ToString(), dataTable.Rows[i][0].ToString());
                                                        if (existing_.Rows.Count == 0)
                                                        {

                                                            dateconvert cc = new dateconvert();

                                                            string d2 = "";
                                                            string d1 = "";
                                                            int result = 0;

                                                            int xxxx = 0;

                                                            var cont = Serv.GetContract_ShopOper_Close(Request.QueryString["id"].ToString());
                                                            if (cont.Rows.Count != 0)
                                                            {
                                                                d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_close"]).ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");// Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result > 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_open"]).ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result < 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }

                                                                d2 = DateTime.Now.ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result > 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                else if (result == 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                            }

                                                            if (xxxx == 0)
                                                            {
                                                                Serv.Insert_transaction_record(Request.QueryString["id"].ToString(),
                                                                    Convert.ToDecimal(xxx).ToString(), "",
                                                                    "y", Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"),
                                                                    nobill, remark, vat, hdd_keyintype.Value, HttpContext.Current.Session["s_userid"].ToString(), "ar", "excel");

                                                                success = success + 1;
                                                            }


                                                        }
                                                        else
                                                        {

                                                            if (existing_.Rows[0]["status"].ToString() == "y")
                                                            {
                                                                fail = fail + 1;
                                                            }
                                                            else
                                                            {
                                                                //// Update Transaction No Bill ////

                                                                Serv.Update_transaction_record(Request.QueryString["id"].ToString(), Convert.ToDecimal(xxx).ToString(),
                                                                    "", "y",
                                                                   Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"),
                                                                    nobill, remark, existing_.Rows[0]["id"].ToString(), vat, HttpContext.Current.Session["s_userid"].ToString(), "ar", "excel");


                                                            }

                                                        }

                                                    }
                                                    else
                                                    {

                                                        var existing_ = Serv.getTransaction_dup(Request.QueryString["id"].ToString(), Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"));
                                                        if (existing_.Rows.Count == 0)
                                                        {


                                                            dateconvert cc = new dateconvert();

                                                            string d2 = "";
                                                            string d1 = "";
                                                            int result = 0;

                                                            int xxxx = 0;

                                                            var cont = Serv.GetContract_ShopOper_Close(Request.QueryString["id"].ToString());
                                                            if (cont.Rows.Count != 0)
                                                            {
                                                                d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_close"]).ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result > 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_open"]).ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result < 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                d2 = DateTime.Now.ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result > 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                else if (result == 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                            }


                                                            if (xxxx == 0)
                                                            {
                                                                Serv.Insert_transaction_record(Request.QueryString["id"].ToString(),
                                                                      Convert.ToDecimal(xxx).ToString(), "",
                                                                      "y", Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"),
                                                                      nobill, remark, vat, hdd_keyintype.Value, HttpContext.Current.Session["s_userid"].ToString(), "ar", "excel");
                                                                success = success + 1;
                                                            }

                                                        }
                                                        else
                                                        {

                                                            if (existing_.Rows[0]["status"].ToString() == "y")
                                                            {
                                                                fail = fail + 1;
                                                            }
                                                            else
                                                            {
                                                                Serv.Update_transaction_record(Request.QueryString["id"].ToString(), Convert.ToDecimal(xxx).ToString(), "", "y",
                                                                    Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"),
                                                                    nobill, remark, existing_.Rows[0]["id"].ToString(), vat, HttpContext.Current.Session["s_userid"].ToString(), "ar", "excel");


                                                            }


                                                            //fail = fail + 1;

                                                        }

                                                    }
                                                }


                                            }
                                        }

                                    }
                                }

                                if (File.Exists(Server.MapPath(img_name1)))
                                {
                                    File.Delete(Server.MapPath(img_name1));
                                }
                            }
                            else
                            {

                                btnsave.Visible = false;
                                string xx = DateTime.Now.ToString("ssssHHddMM");
                                string img_name1 = "~/tmp_file_excel/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx + ext;
                                FileUpload1.SaveAs(Server.MapPath(img_name1));

                                string x = "";

                                string con =
                                     @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Server.MapPath(img_name1) + ";" +
                                     @"Extended Properties='Excel 12.0;HDR=Yes;'";
                                using (OleDbConnection connection = new OleDbConnection(con))
                                {
                                    connection.Open();
                                    OleDbCommand command = new OleDbCommand("select * from [Sheet1$]", connection);
                                    using (OleDbDataReader dr = command.ExecuteReader())
                                    {
                                        var dataTable = new DataTable();
                                        dataTable.Load(dr);
                                        if (dataTable.Rows.Count != 0)
                                        {
                                            for (int i = 4; i < dataTable.Rows.Count; i++)
                                            {

                                                if (dataTable.Rows[i][0].ToString() != "")
                                                {
                                                    string ddd = dataTable.Rows[i][0].ToString().Replace(" มกราคม ", "/01/").Replace(" กุมภาพันธ์ ", "/02/")
                                                       .Replace(" มีนาคม ", "/03/").Replace(" เมษายน ", "/04/").Replace(" พฤษภาคม ", "/05/")
                                                       .Replace(" มิถุนายน ", "/06/").Replace(" กรกฎาคม ", "/07/").Replace(" สิงหาคม ", "/08/")
                                                       .Replace(" กันยายน ", "/09/").Replace(" ตุลาคม ", "/10/").Replace(" พฤศจิกายน ", "/11/").Replace(" ธันวาคม ", "/12/");
                                                    DateTime MyDateTime = DateTime.ParseExact(ddd, "dd/MM/yyyy", null);

                                                    string remark = "";
                                                    string nobill = "";
                                                    string xxx = "0";
                                                    string yyy = "0";
                                                    if (dataTable.Rows[i][1].ToString() != "" && dataTable.Rows[i][1].ToString() != "-")
                                                    {
                                                        xxx = dataTable.Rows[i][1].ToString();
                                                    }
                                                    else
                                                    {
                                                        xxx = "0";
                                                    }


                                                    if (dataTable.Rows[i][2].ToString() != "" && dataTable.Rows[i][2].ToString() != "-")
                                                    {
                                                        yyy = dataTable.Rows[i][2].ToString();
                                                    }
                                                    else
                                                    {
                                                        yyy = "0";
                                                    }



                                                    if (xxx == "0")
                                                    {
                                                        if (dataTable.Rows[i][2].ToString() != "" && dataTable.Rows[i][2].ToString() != "-" && dataTable.Rows[i][2].ToString() != "0")
                                                        {
                                                            remark = "";
                                                            nobill = "";
                                                        }
                                                        else
                                                        {


                                                            if (dataTable.Rows[i][3].ToString() == "No Bill")
                                                            {
                                                                nobill = "nobill";
                                                            }
                                                            else if (dataTable.Rows[i][3].ToString() == "ร้านปิด")
                                                            {
                                                                nobill = "close";
                                                            }
                                                            else if (dataTable.Rows[i][3].ToString() == "Other")
                                                            {
                                                                nobill = "other";
                                                            }

                                                            remark = dataTable.Rows[i][4].ToString();

                                                        }
                                                    }
                                                    else
                                                    {
                                                        remark = "";
                                                        nobill = "";
                                                    }


                                                    if (dataTable.Rows[i][3].ToString() == null || dataTable.Rows[i][3].ToString() == "")
                                                    {
                                                        var existing_ = Serv.getTransaction_dup(Request.QueryString["id"].ToString(), Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"));
                                                        if (existing_.Rows.Count == 0)
                                                        {

                                                            dateconvert cc = new dateconvert();

                                                            string d2 = "";
                                                            string d1 = "";
                                                            int result = 0;

                                                            int xxxx = 0;

                                                            var cont = Serv.GetContract_ShopOper_Close(Request.QueryString["id"].ToString());
                                                            if (cont.Rows.Count != 0)
                                                            {
                                                                d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_close"]).ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result > 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_open"]).ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result < 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                d2 = DateTime.Now.ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result > 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                else if (result == 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                            }

                                                            if (xxxx == 0)
                                                            {
                                                                Serv.Insert_transaction_record(Request.QueryString["id"].ToString(),
                                                                       Convert.ToDecimal(xxx).ToString(), Convert.ToDecimal(yyy).ToString(),
                                                                       "y", Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"), nobill, remark,
                                                                       vat, hdd_keyintype.Value, HttpContext.Current.Session["s_userid"].ToString(), "ar", "excel");
                                                                success = success + 1;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            if (existing_.Rows[0]["status"].ToString() == "y")
                                                            {
                                                                fail = fail + 1;
                                                            }
                                                            else
                                                            {
                                                                Serv.Update_transaction_record(Request.QueryString["id"].ToString(),
                                                                    Convert.ToDecimal(xxx).ToString(), Convert.ToDecimal(yyy).ToString(),
                                                                    //Convert.ToDecimal(dataTable.Rows[i][2].ToString()).ToString(),
                                                                    "y", Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"), nobill, remark,
                                                                    existing_.Rows[0]["id"].ToString(), vat, HttpContext.Current.Session["s_userid"].ToString(), "ar", "excel");


                                                            }
                                                            //fail = fail + 1;

                                                        }

                                                    }
                                                    else
                                                    {

                                                        var existing_ = Serv.getTransaction_dup(Request.QueryString["id"].ToString(), Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"));
                                                        if (existing_.Rows.Count == 0)
                                                        {
                                                            dateconvert cc = new dateconvert();

                                                            string d2 = "";
                                                            string d1 = "";
                                                            int result = 0;

                                                            int xxxx = 0;

                                                            var cont = Serv.GetContract_ShopOper_Close(Request.QueryString["id"].ToString());
                                                            if (cont.Rows.Count != 0)
                                                            {
                                                                d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_close"]).ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result > 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_open"]).ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result < 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                d2 = DateTime.Now.ToString("yyyy-MM-dd", EngCI);
                                                                d1 = Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd");
                                                                result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                                                                if (result > 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                                else if (result == 0)
                                                                {
                                                                    xxxx = xxxx + 1;
                                                                }
                                                            }

                                                            if (xxxx == 0)
                                                            {
                                                                Serv.Insert_transaction_record(Request.QueryString["id"].ToString(),
                                                                   Convert.ToDecimal(xxx).ToString(), Convert.ToDecimal(yyy).ToString(),
                                                                   "y", Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"),
                                                                   nobill, remark, vat, hdd_keyintype.Value, HttpContext.Current.Session["s_userid"].ToString(), "ar", "excel");

                                                                success = success + 1;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            if (existing_.Rows[0]["status"].ToString() == "y")
                                                            {
                                                                fail = fail + 1;
                                                            }
                                                            else
                                                            {
                                                                Serv.Update_transaction_record(Request.QueryString["id"].ToString(),
                                                                    Convert.ToDecimal(xxx).ToString(), Convert.ToDecimal(yyy).ToString(),
                                                                    "y", Convert.ToDateTime(dataTable.Rows[i][0].ToString()).ToString("yyyy-MM-dd"), nobill, remark,
                                                                    existing_.Rows[0]["id"].ToString(), vat, HttpContext.Current.Session["s_userid"].ToString(), "ar", "excel");

                                                            }
                                                            //fail = fail + 1;

                                                        }


                                                    }
                                                }

                                            }
                                        }

                                    }
                                }

                                if (File.Exists(Server.MapPath(img_name1)))
                                {
                                    File.Delete(Server.MapPath(img_name1));
                                }

                            }


                            if (fail != 0)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย [Success=" + success + "][Fail=" + fail + "]');window.location ='ar_monitoring_detail.aspx?id=" + Request.QueryString["id"].ToString() + "';", true);

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='ar_monitoring_detail.aspx?id=" + Request.QueryString["id"].ToString() + "';", true);

                            }



                        }
                    }


                }
                catch (Exception ex)
                {
                    POPUPMSG(ex.ToString());
                }

            }
            else
            {
                POPUPMSG("กรุณาเลือก File Excel ที่ต้องการ Import !?!");
                return;
            }
        }

    }
}