﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for contracttypeDLL
/// </summary>
public class contracttypeDLL
{
    public contracttypeDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable getcontracttypeByName(string[] contracttypeName, string Flag_active, string comcode, string role)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string contracttypeName_ = "";

        if (contracttypeName.Length != 0)
        {
            for (int i = 0; i < contracttypeName.Length; i++)
            {
                contracttypeName_ += " (c.ContractTypeNameEn like '%" + contracttypeName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%'  " +
                    " or c.ContractTypeNameTH like '%" + contracttypeName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' ) or";
            }

            contracttypeName_ = contracttypeName_.Substring(0, contracttypeName_.Length - 2);
        }

        if (role != "datacenter" && role != "super_admin")
        {
            strSQL += "  select c.*,u.username , case when c.Flag_active = '1' then 'Active' else 'Inactive' end as status " +
               " from tblcontracttype c inner join tbluser u on c.CreateId = u.userid where " +
               " " + contracttypeName_ + " " +
               " and c.Flag_active = '" + Flag_active + "'  " +
               "   and  c.id in (select ContractTypeId from TblContractType_mapping where ComGroupID in " +
               " (  select ComGroupID from TblCompanyGroupMapping where CompanyCode in (" + comcode + ") ) )  ";
        }
        else
        {
            strSQL += "  select c.*,u.username , case when c.Flag_active = '1' then 'Active' else 'Inactive' end as status " +
               " from tblcontracttype c inner join tbluser u on c.CreateId = u.userid where " +
               " " + contracttypeName_ + " " +
               " and c.Flag_active = '" + Flag_active + "' ;  ";
        }
           


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void Insertcontracttype(string ContractTypeNameTH, string ContractTypeNameEn, string Flag_active, string CreateId)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " insert into tblcontracttype(ContractTypeNameTH,ContractTypeNameEn,Flag_active,CreateDate,CreateId) " +
                " values('" + ContractTypeNameTH + "','" + ContractTypeNameEn + "','" + Flag_active + "', GETDATE(), '" + CreateId + "');   ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void Updatecontracttype(string ContractTypeNameTH, string ContractTypeNameEn, string Flag_active, string CreateId, string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Update tblcontracttype set ContractTypeNameTH = '" + ContractTypeNameTH + "', ContractTypeNameEn = '" + ContractTypeNameEn + "', " +
            " Flag_active = '" + Flag_active + "' , UpdateDate = GETDATE() , UpdateId = '" + CreateId + "' where id = '" + id + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable getcontracttypeById(string Id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontracttype where Id = '" + Id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getcontracttypeByNameEN(string ContractTypeNameEn)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontracttype where ContractTypeNameEn = '" + ContractTypeNameEn + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getcontracttypeByNameTH(string ContractTypeNameTH)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontracttype where ContractTypeNameTH = '" + ContractTypeNameTH + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getcontracttypeByNameEN_nid(string ContractTypeNameEn, string Id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontracttype where ContractTypeNameEn = '" + ContractTypeNameEn + "' and Id != '" + Id + "'  ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getcontracttypeByNameTH_nid(string ContractTypeNameTH, string Id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontracttype where ContractTypeNameTH = '" + ContractTypeNameTH + "'  and Id != '" + Id + "'   ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }







}