﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for industryGroupDLL
/// </summary>
public class industryGroupDLL
{
    public industryGroupDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public DataTable getIndustryGroupByName(string[] industryName, string IsActive, string company_code, string role)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";


        string industryName_ = "";


        if (industryName.Length != 0)
        {
            for (int i = 0; i < industryName.Length; i++)
            {
                industryName_ += " (IndustryGroupNameEN like '%" + industryName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                    " or IndustryGroupNameTH like '%" + industryName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' ) or";
            }

            industryName_ = industryName_.Substring(0, industryName_.Length - 2);
        }


        if (role != "datacenter" && role != "super_admin")
        {
            strSQL += "  select *,case when IsActive = '1' then 'Active' else 'Inactive' end as status  from TblIndustryGroup where " + industryName_ + " " +
           " and IsActive = '" + IsActive + "'  " +
           " and IndustryGroupCode in ( select IndustryGroupCode from TblIndustryGroup_mapping where ComGroupID in ( " +
           " select distinct ComGroupID from TblCompanyGroupMapping where CompanyCode in (" + company_code + ")))  " +
           " order by IndustryGroupCode ;  ";
        }
        else
        {
            strSQL += "  select *,case when IsActive = '1' then 'Active' else 'Inactive' end as status  from TblIndustryGroup where " + industryName_ + " " +
           " and IsActive = '" + IsActive + "'   " +
           " order by IndustryGroupCode ;  ";
        }




        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getIndustryGroupById(string Id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblIndustryGroup where Id = '" + Id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getIndustryGroupByGroupCode(string IndustryGroupCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblIndustryGroup where IndustryGroupCode = '" + IndustryGroupCode + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getIndustryGroupByGroupCode_nid(string IndustryGroupCode, string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblIndustryGroup where IndustryGroupCode = '" + IndustryGroupCode + "' and id != '" + id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }




    public void InsertIndustryGroup(string IndustryGroupCode, string IndustryGroupNameEN, string IndustryGroupNameTH, string IsActive, string create_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " insert into TblIndustryGroup(IndustryGroupCode,IndustryGroupNameEN,IndustryGroupNameTH,CreatedDate,IsActive,create_id) " +
                " values('" + IndustryGroupCode + "','" + IndustryGroupNameEN + "','" + IndustryGroupNameTH + "', GETDATE(), '" + IsActive + "','" + create_id + "');   ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void UpdateIndustryGroup(string IndustryGroupCode, string IndustryGroupNameEN, string IndustryGroupNameTH, string IsActive, string Id, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " update TblIndustryGroup set IndustryGroupCode = '" + IndustryGroupCode + "', " +
            " IndustryGroupNameEN = '" + IndustryGroupNameEN + "' ,IndustryGroupNameTH = '" + IndustryGroupNameTH + "', " +
            " UpdateDate = GETDATE() , IsActive = '" + IsActive + "', update_id = '" + update_id + "' " +
            " Where Id = '" + Id + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable getIndustryGroupByGroupNameEN(string IndustryGroupNameEN)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblIndustryGroup where IndustryGroupNameEN = '" + IndustryGroupNameEN + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getIndustryGroupByGroupNameEN_nid(string IndustryGroupNameEN, string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblIndustryGroup where IndustryGroupNameEN = '" + IndustryGroupNameEN + "' and id != '" + id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getIndustryGroupByGroupNameTH(string IndustryGroupNameTH)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblIndustryGroup where IndustryGroupNameTH = '" + IndustryGroupNameTH + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getIndustryGroupByGroupNameTH_nid(string IndustryGroupNameTH, string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from TblIndustryGroup where IndustryGroupNameTH = '" + IndustryGroupNameTH + "' and id != '" + id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }























}