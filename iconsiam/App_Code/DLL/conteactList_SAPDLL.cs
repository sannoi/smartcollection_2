﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for conteactList_SAPDLL
/// </summary>
public class conteactList_SAPDLL
{
    public conteactList_SAPDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetSmart_Contract(string CompanyCode, string[] ShopName, string[] ShopGroupName, string com_all)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string shopname = "";
        string shopgroup = "";


        if (ShopName.Length != 0)
        {
            for (int i = 0; i < ShopName.Length; i++)
            {
                shopname += " sc.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
            }

            shopname = shopname.Substring(0, shopname.Length - 2);
        }

        if (ShopGroupName.Length != 0)
        {
            for (int i = 0; i < ShopGroupName.Length; i++)
            {
                shopgroup += " (ISNULL(CONVERT(varchar(50),sg.ShopGroupNameEN),'')  like  '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or " +
                    " ISNULL(CONVERT(varchar(50), sg.ShopGroupNameTH),'')  like '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%') or";
            }

            shopgroup = shopgroup.Substring(0, shopgroup.Length - 2);
        }

        if (CompanyCode != "")
        {
            strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                 " sc.BusinessPartnerName,case when sc.smart_contract_status = '1' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, " +
                 " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate " +
                 " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                 " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                  "    left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                " where sc.CompanyCode = '" + CompanyCode + "' and " + shopname + " and " +
                  "  " + shopgroup + "  " +
                  " and sc.smart_icon_staff_id = ''  and smart_collection_flag_status = ''   ";

        }
        else
        {
            strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                " sc.BusinessPartnerName,case when sc.smart_contract_status = '1' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, " +
                " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate " +
                " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                 "    left join tbluser u on sc.smart_icon_staff_id = u.userid " +
               " where sc.CompanyCode in (" + com_all + ") and " + shopname + " and " +
                 "  " + shopgroup + "  " +
                 " and sc.smart_icon_staff_id = ''  and smart_collection_flag_status = ''   ";
        }




        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetSmart_Contract_owner(string smart_icon_staff_id, string CompanyCode, string[] ShopName, string[] ShopGroupName, string ar, string com_all , string status )
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string shopname = "";
        string shopgroup = "";


        if (ShopName.Length != 0)
        {
            for (int i = 0; i < ShopName.Length; i++)
            {
                shopname += " sc.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
            }

            shopname = shopname.Substring(0, shopname.Length - 2);
        }

        if (ShopGroupName.Length != 0)
        {
            for (int i = 0; i < ShopGroupName.Length; i++)
            {
                shopgroup += " (ISNULL(CONVERT(varchar(50),sg.ShopGroupNameEN),'')  like  '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or " +
                    " ISNULL(CONVERT(varchar(50), sg.ShopGroupNameTH),'')  like '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%') or";
            }

            shopgroup = shopgroup.Substring(0, shopgroup.Length - 2);
        }

        if (ar == "")
        {
            if (CompanyCode != "")
            {
                strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
              " sc.BusinessPartnerName,case when sc.smart_contract_status <> '0' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, Concat(u.Fname,' ',u.Lname) as arname, " +
              " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate, smart_record_keyin_type,smart_record_type " +
              " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
              " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
              " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
             " where sc.CompanyCode in (" + CompanyCode + ") and " + shopname + " and " +
               "  " + shopgroup + "  " +
                //" and smart_collection_flag_status = ''   ";
                " and smart_collection_flag_status = ''  ";

                if (smart_icon_staff_id != "")
                {
                    strSQL = strSQL + " and smart_icon_staff_id in (" + smart_icon_staff_id + ") ";
                }


                if (status != "")
                {
                    strSQL = strSQL + " and smart_contract_status = '" + status + "'    ";
                }

                //if (smart_icon_staff_id != "")
                //{
                //    strSQL = strSQL + " and sc.smart_icon_staff_id in (" + smart_icon_staff_id + ")  ";

                //}
            }
            else
            {
                strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
              " sc.BusinessPartnerName,case when sc.smart_contract_status <> '0' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, Concat(u.Fname,' ',u.Lname) as arname, " +
              " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate, smart_record_keyin_type,smart_record_type " +
              " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
              " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
              " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
             " where sc.CompanyCode in (" + com_all + ") and " + shopname + " and " +
               "  " + shopgroup + "   " +
               ////" and smart_collection_flag_status = ''   ";
                " and smart_collection_flag_status = '' ";

                if (smart_icon_staff_id != "")
                {
                    strSQL = strSQL + " and smart_icon_staff_id in (" + smart_icon_staff_id + ") ";
                }


                if (status != "")
                {
                    strSQL = strSQL + " and smart_contract_status = '" + status + "'    ";
                }

                //if (smart_icon_staff_id != "")
                //{
                //    strSQL = strSQL + " asnd sc.smart_icon_staff_id in (" + smart_icon_staff_id + ")  ";

                //}

            }


        }
        else
        {
            if (CompanyCode != "")
            {
                strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                       " sc.BusinessPartnerName,case when sc.smart_contract_status <> '0' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, Concat(u.Fname,' ',u.Lname) as arname, " +
                       " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate, smart_record_keyin_type,smart_record_type " +
                       " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                       " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                       " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                      " where sc.CompanyCode in (" + CompanyCode + ") and " + shopname + " and " +
                        "  " + shopgroup + "  " +
                //" and smart_collection_flag_status = ''  ";
                " and smart_collection_flag_status = '' ";

                if (ar != "")
                {
                    strSQL = strSQL + " and smart_icon_staff_id in (" + ar + ") ";
                }

                if (status != "")
                {
                    strSQL = strSQL + " and smart_contract_status = '" + status + "'    ";
                }

                //" and sc.smart_icon_staff_id = '" + ar + "'  and smart_collection_flag_status = '' and smart_contract_status = '1'   ";
            }
            else
            {
                strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                      " sc.BusinessPartnerName,case when sc.smart_contract_status <> '0' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, Concat(u.Fname,' ',u.Lname) as arname, " +
                      " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate, smart_record_keyin_type,smart_record_type " +
                      " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                      " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                      " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                     " where sc.CompanyCode in (" + com_all + ") and " + shopname + " and " +
                       "  " + shopgroup + "   " +
                //" and smart_collection_flag_status = ''   ";
                " and smart_collection_flag_status = '' ";

                if (ar != "")
                {
                    strSQL = strSQL + " and smart_icon_staff_id in (" + ar + ") ";
                }

                if (status != "")
                {
                    strSQL = strSQL + " and smart_contract_status = '" + status + "'    ";
                }


                //" and sc.smart_icon_staff_id = '" + ar + "'  and smart_collection_flag_status = '' and smart_contract_status = '1'   ";
            }

        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetSmart_Contract_owner_by_date(string smart_icon_staff_id, string CompanyCode, string[] ShopName, string[] ShopGroupName, string ar, string com_all, string status, string date1, string date2)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string shopname = "";
        string shopgroup = "";


        if (ShopName.Length != 0)
        {
            for (int i = 0; i < ShopName.Length; i++)
            {
                shopname += " sc.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
            }

            shopname = shopname.Substring(0, shopname.Length - 2);
        }

        if (ShopGroupName.Length != 0)
        {
            for (int i = 0; i < ShopGroupName.Length; i++)
            {
                shopgroup += " (ISNULL(CONVERT(varchar(50),sg.ShopGroupNameEN),'')  like  '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or " +
                    " ISNULL(CONVERT(varchar(50), sg.ShopGroupNameTH),'')  like '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%') or";
            }

            shopgroup = shopgroup.Substring(0, shopgroup.Length - 2);
        }

        if (ar == "")
        {
            if (CompanyCode != "")
            {
                strSQL += "  select DISTINCT sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
              " sc.BusinessPartnerName,case when sc.smart_contract_status <> '0' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, Concat(u.Fname,' ',u.Lname) as arname, " +
              " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate, smart_record_keyin_type,smart_record_type " +
              " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
              " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
              " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
              " inner join tbltransaction_record tr on tr.ContractNumber = sc.ContractNumber  " +
             " where sc.CompanyCode in (" + CompanyCode + ") and " + shopname + " and " +
               "  " + shopgroup + " and tr.status = 'y'   " +
                //" and smart_collection_flag_status = ''   ";
                " and smart_collection_flag_status = ''  ";

                if (date1 != "" && date2 != "")
                {
                    strSQL = strSQL + " and tr.record_date between '" + date1 + "-01 00:00:00' and '" + date2 + "-01 00:00:00'  ";
                }


                if (smart_icon_staff_id != "")
                {
                    strSQL = strSQL + " and smart_icon_staff_id in (" + smart_icon_staff_id + ") ";
                }


                if (status != "")
                {
                    strSQL = strSQL + " and smart_contract_status = '" + status + "'    ";
                }

                //if (smart_icon_staff_id != "")
                //{
                //    strSQL = strSQL + " and sc.smart_icon_staff_id in (" + smart_icon_staff_id + ")  ";

                //}
            }
            else
            {
                strSQL += "  select DISTINCT sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
              " sc.BusinessPartnerName,case when sc.smart_contract_status <> '0' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, Concat(u.Fname,' ',u.Lname) as arname, " +
              " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate, smart_record_keyin_type,smart_record_type " +
              " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
              " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
              " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
              " inner join tbltransaction_record tr on tr.ContractNumber = sc.ContractNumber " +
             " where sc.CompanyCode in (" + com_all + ") and " + shopname + " and " +
               "  " + shopgroup + " and tr.status = 'y'  " +
                ////" and smart_collection_flag_status = ''   ";
                " and smart_collection_flag_status = '' ";

                if (date1 != "" && date2 != "")
                {
                    strSQL = strSQL + " and tr.record_date between '" + date1 + "-01 00:00:00' and '" + date2 + "-01 00:00:00'  ";
                }

                if (smart_icon_staff_id != "")
                {
                    strSQL = strSQL + " and smart_icon_staff_id in (" + smart_icon_staff_id + ") ";
                }


                if (status != "")
                {
                    strSQL = strSQL + " and smart_contract_status = '" + status + "'    ";
                }

                //if (smart_icon_staff_id != "")
                //{
                //    strSQL = strSQL + " asnd sc.smart_icon_staff_id in (" + smart_icon_staff_id + ")  ";

                //}

            }


        }
        else
        {
            if (CompanyCode != "")
            {
                strSQL += "  select DISTINCT sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                       " sc.BusinessPartnerName,case when sc.smart_contract_status <> '0' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, Concat(u.Fname,' ',u.Lname) as arname, " +
                       " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate, smart_record_keyin_type,smart_record_type " +
                       " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                       " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                       " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                       " inner join tbltransaction_record tr on tr.ContractNumber = sc.ContractNumber " +
                      " where sc.CompanyCode in (" + CompanyCode + ") and " + shopname + " and " +
                        "  " + shopgroup + " and tr.status = 'y'  " +
                //" and smart_collection_flag_status = ''  ";
                " and smart_collection_flag_status = '' ";

                if (date1 != "" && date2 != "")
                {
                    strSQL = strSQL + " and tr.record_date between '" + date1 + "-01 00:00:00' and '" + date2 + "-01 00:00:00'  ";
                }

                if (ar != "")
                {
                    strSQL = strSQL + " and smart_icon_staff_id in (" + ar + ") ";
                }

                if (status != "")
                {
                    strSQL = strSQL + " and smart_contract_status = '" + status + "'    ";
                }

                //" and sc.smart_icon_staff_id = '" + ar + "'  and smart_collection_flag_status = '' and smart_contract_status = '1'   ";
            }
            else
            {
                strSQL += "  select DISTINCT sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                      " sc.BusinessPartnerName,case when sc.smart_contract_status <> '0' then 'Active' else 'Inactive' end as status,sc.smart_update_date, u.username, Concat(u.Fname,' ',u.Lname) as arname, " +
                      " CONVERT(varchar, sc.ContractStartDate , 103) as startdate, CONVERT(varchar,sc.ContractEndDate, 103) as enddate, smart_record_keyin_type,smart_record_type " +
                      " from tblsmart_Contract sc inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                      " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                      " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                      " inner join tbltransaction_record tr on tr.ContractNumber = sc.ContractNumber " +
                     " where sc.CompanyCode in (" + com_all + ") and " + shopname + " and " +
                       "  " + shopgroup + " and tr.status = 'y'  " +
                //" and smart_collection_flag_status = ''   ";
                " and smart_collection_flag_status = '' ";

                if (date1 != "" && date2 != "")
                {
                    strSQL = strSQL + " and tr.record_date between '" + date1 + "-01 00:00:00' and '" + date2 + "-01 00:00:00'  ";
                }

                if (ar != "")
                {
                    strSQL = strSQL + " and smart_icon_staff_id in (" + ar + ") ";
                }

                if (status != "")
                {
                    strSQL = strSQL + " and smart_contract_status = '" + status + "'    ";
                }


                //" and sc.smart_icon_staff_id = '" + ar + "'  and smart_collection_flag_status = '' and smart_contract_status = '1'   ";
            }

        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetSmart_Contract_owner_export(string smart_icon_staff_id, string CompanyCode, string[] ShopName, string[] ShopGroupName, string ar, string com_all , string status)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string shopname = "";
        string shopgroup = "";


        if (ShopName.Length != 0)
        {
            for (int i = 0; i < ShopName.Length; i++)
            {
                shopname += " sc.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
            }

            shopname = shopname.Substring(0, shopname.Length - 2);
        }

        if (ShopGroupName.Length != 0)
        {
            for (int i = 0; i < ShopGroupName.Length; i++)
            {
                shopgroup += " (ISNULL(CONVERT(varchar(50),sg.ShopGroupNameEN),'')  like  '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or " +
                    " ISNULL(CONVERT(varchar(50), sg.ShopGroupNameTH),'')  like '%" + ShopGroupName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%') or";
            }

            shopgroup = shopgroup.Substring(0, shopgroup.Length - 2);
        }


        if (ar == "")
        {

            if (CompanyCode != "")
            {
                strSQL += "  select  sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                     " sc.BusinessPartnerName,case when sc.smart_icon_staff_id <> '' then 'Active' else 'Inactive' end as status, " +
                     "  sc.smart_update_date, u.username, Concat(u.Fname, ' ', u.Lname) as arname,  " +
                      " CONVERT(varchar, sc.ContractStartDate, 103) as startdate, CONVERT(varchar, sc.ContractEndDate, 103) as enddate , " +
                      " sc.smart_building,ct.ContractTypeNameEn,sc.ContractTypeDescription,sc.SalesTypeName,sc.SubTypeCode,sc.SubTypeName, " +
                      "  Concat(u2.Fname, ' ', u2.Lname) as aename,cl.CategoryleasingNameEN,indust.IndustryGroupNameEN,sc.smart_floor,sc.smart_usage_name, " +
                      "  sc.smart_sqm,gl.GrouplocationNameTH,smart_record_type " +

                     "  from tblsmart_Contract sc " +
                     " inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                     " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                     " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                     " left join tbluser u2 on sc.smart_icon_ae_id = u2.userid " +
                     " inner join tblcontracttype ct on sc.smart_contract_type = ct.id " +
                     " inner join tblcategoryleasing cl on sc.smart_Category_leasing = cl.id " +
                     " inner join TblIndustryGroup indust on sc.smart_industry_group = indust.id " +
                     " inner join tblgrouplocation gl on sc.smart_group_location = gl.id " +


                       " where sc.CompanyCode in (" + CompanyCode + ") and " + shopname + " and " +
                         "  " + shopgroup + "  " +
                         "  and smart_collection_flag_status = '' and smart_contract_status = '"+ status + "'   ";

                //if (smart_icon_staff_id != "")
                //{
                //    strSQL = strSQL + " and sc.smart_icon_staff_id in (" + smart_icon_staff_id + ")  ";

                //}
            }
            else
            {
                strSQL += "  select  sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                    " sc.BusinessPartnerName,case when sc.smart_icon_staff_id <> '' then 'Active' else 'Inactive' end as status, " +
                    "  sc.smart_update_date, u.username, Concat(u.Fname, ' ', u.Lname) as arname,  " +
                     " CONVERT(varchar, sc.ContractStartDate, 103) as startdate, CONVERT(varchar, sc.ContractEndDate, 103) as enddate , " +
                     " sc.smart_building,ct.ContractTypeNameEn,sc.ContractTypeDescription,sc.SalesTypeName,sc.SubTypeCode,sc.SubTypeName, " +
                     "  Concat(u2.Fname, ' ', u2.Lname) as aename,cl.CategoryleasingNameEN,indust.IndustryGroupNameEN,sc.smart_floor,sc.smart_usage_name, " +
                     "  sc.smart_sqm,gl.GrouplocationNameTH,smart_record_type " +

                    "  from tblsmart_Contract sc " +
                    " inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                    " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                    " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                    " left join tbluser u2 on sc.smart_icon_ae_id = u2.userid " +
                    " inner join tblcontracttype ct on sc.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on sc.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup indust on sc.smart_industry_group = indust.id " +
                    " inner join tblgrouplocation gl on sc.smart_group_location = gl.id " +


                      " where sc.CompanyCode in (" + com_all + ") and " + shopname + " and " +
                        "  " + shopgroup + "  " +
                        "  and smart_collection_flag_status = '' and smart_contract_status = '"+status+"'   ";

                //if (smart_icon_staff_id != "")
                //{
                //    strSQL = strSQL + " and sc.smart_icon_staff_id in (" + smart_icon_staff_id + ")  ";

                //}
            }


        }
        else
        {
            if (CompanyCode != "")
            {
                strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                       " sc.BusinessPartnerName,case when sc.smart_icon_staff_id <> '' then 'Active' else 'Inactive' end as status, " +
                       "  sc.smart_update_date, u.username, Concat(u.Fname, ' ', u.Lname) as arname,  " +
                        " CONVERT(varchar, sc.ContractStartDate, 103) as startdate, CONVERT(varchar, sc.ContractEndDate, 103) as enddate , " +
                        " sc.smart_building,ct.ContractTypeNameEn,sc.ContractTypeDescription,sc.SalesTypeName,sc.SubTypeCode,sc.SubTypeName, " +
                        "  Concat(u2.Fname, ' ', u2.Lname) as aename,cl.CategoryleasingNameEN,indust.IndustryGroupNameEN,sc.smart_floor,sc.smart_usage_name, " +
                        "  sc.smart_sqm,gl.GrouplocationNameTH,smart_record_type " +

                       "  from tblsmart_Contract sc " +
                       " inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                       " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                       " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                       " left join tbluser u2 on sc.smart_icon_ae_id = u2.userid " +
                       " inner join tblcontracttype ct on sc.smart_contract_type = ct.id " +
                       " inner join tblcategoryleasing cl on sc.smart_Category_leasing = cl.id " +
                       " inner join TblIndustryGroup indust on sc.smart_industry_group = indust.id " +
                       " inner join tblgrouplocation gl on sc.smart_group_location = gl.id " +


                      " where sc.CompanyCode in (" + CompanyCode + ") and " + shopname + " and " +
                         "  " + shopgroup + "  " +
                         "   and smart_collection_flag_status = '' and smart_contract_status = '"+status+"'   ";
                //" and sc.smart_icon_staff_id = '" + ar + "'  and smart_collection_flag_status = '' and smart_contract_status = '1'   ";
            }
            else
            {
                strSQL += "  select sc.Id, sc.ContractNumber,sc.CompanyCode, com.CompanyNameTH,sc.smart_room_no,sg.ShopGroupNameEN,sc.ShopName, " +
                     " sc.BusinessPartnerName,case when sc.smart_icon_staff_id <> '' then 'Active' else 'Inactive' end as status, " +
                     "  sc.smart_update_date, u.username, Concat(u.Fname, ' ', u.Lname) as arname,  " +
                      " CONVERT(varchar, sc.ContractStartDate, 103) as startdate, CONVERT(varchar, sc.ContractEndDate, 103) as enddate , " +
                      " sc.smart_building,ct.ContractTypeNameEn,sc.ContractTypeDescription,sc.SalesTypeName,sc.SubTypeCode,sc.SubTypeName, " +
                      "  Concat(u2.Fname, ' ', u2.Lname) as aename,cl.CategoryleasingNameEN,indust.IndustryGroupNameEN,sc.smart_floor,sc.smart_usage_name, " +
                      "  sc.smart_sqm,gl.GrouplocationNameTH,smart_record_type " +

                     "  from tblsmart_Contract sc " +
                     " inner join SAPCompany com on sc.CompanyCode = com.CompanyCode " +
                     " left join tblshopgroup sg on sc.smart_shop_group_id = sg.id " +
                     " left join tbluser u on sc.smart_icon_staff_id = u.userid " +
                     " left join tbluser u2 on sc.smart_icon_ae_id = u2.userid " +
                     " inner join tblcontracttype ct on sc.smart_contract_type = ct.id " +
                     " inner join tblcategoryleasing cl on sc.smart_Category_leasing = cl.id " +
                     " inner join TblIndustryGroup indust on sc.smart_industry_group = indust.id " +
                     " inner join tblgrouplocation gl on sc.smart_group_location = gl.id " +


                    " where sc.CompanyCode in  (" + com_all + ") and " + shopname + " and " +
                       "  " + shopgroup + "  " +
                       " and smart_collection_flag_status = '' and smart_contract_status = '"+status+"'   ";
                //" and sc.smart_icon_staff_id = '" + ar + "'  and smart_collection_flag_status = '' and smart_contract_status = '1'   ";
            }

        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetCompany(string com_code)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from SAPCompany where IsActive = '1' and CompanyCode in (" + com_code + ") order by CompanyNameTH  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetUserByUserid(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select *,concat(Fname,' ',Lname) as name from tbluser where userid in (" + userid + ") and  flag_active = 'y'  order by Fname ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetUser()
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select *,concat(Fname,' ',Lname) as name from tbluser  where flag_active = 'y' order by Fname ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetUserAR(string companycode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL +=   " select *,concat(Fname,' ',Lname) as name from tbluser a  " +
                    " inner join tblmapp_User_Company b on b.userid = a.userid " +
                    " where a.flag_active = 'y'  and a.user_group = 'ar' and a.role = 'officer' " +
                    " and b.company_code in ("+ companycode + ")" +
                    " order by Fname ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetMapCompany(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (userid != "")
        {
            strSQL += " select  distinct ComGroupID from tblmapp_User_Company where userid in (" + userid + ") ;  ";

        }
        else
        {
            //strSQL += " select  distinct ComGroupID from tblmapp_User_Company ;  ";
            strSQL += " select companycode from TblCompanyDetail ;   ";

        }
        //strSQL += " select * from tblmapp_User_Company where userid = '" + userid + "';  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetContactPoint(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select * from tblcontact_point where contractnumber = '" + ContractNumber + "' and IsMain = 'y' and status = '1' ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetShopInfo(string ContractNumber)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select sc.BusinessPartnerCode,sc.BusinessPartnerName,sc.ShopName,sc.smart_room_no,sc.smart_sqm,sc.ContractNumber " +
                    " from tblsmart_Contract sc where sc.ContractNumber = '" + ContractNumber + "'  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetTransaction_detail(string ContractNumber, string date1, string date2 , string status)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (status != "all")
        {
            strSQL += " select tr.ContractNumber,tr.record_date," +

                      //" tr.product_amount,(tr.product_amount * 7) / 107 as vat, tr.product_amount - (tr.product_amount * 7) / 107 as exvat, " +
                      //" tr.service_serCharge_amount,(tr.service_serCharge_amount * 7) / 107 as vat2, tr.service_serCharge_amount - (tr.service_serCharge_amount * 7) / 107 as exvat2 " +

                      " tr.product_amount, tr.product_amount_vat as vat, " +
                    " tr.product_amount -  tr.product_amount_vat as exvat, " +

                     " tr.service_serCharge_amount, tr.service_serCharge_amount_vat as vat2, " +
                    " tr.service_serCharge_amount - tr.service_serCharge_amount_vat as exvat2 , com.CompanyNameTh " +





                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract smt on smt.ContractNumber = tr.ContractNumber " +
                    "  inner join TblCompanyDetail com on com.Companycode = smt.CompanyCode " +
                    " where tr.ContractNumber = '" + ContractNumber + "' and tr.status = 'y' and tr.flag_confirm = '" + status + "' " +
                    " and tr.record_date between '" + date1 + "-01 00:00:00' and '" + date2 + "-01 00:00:00' order by tr.record_date  ";
        }
        else
        {
            strSQL += " select tr.ContractNumber,tr.record_date," +

                      //" tr.product_amount,(tr.product_amount * 7) / 107 as vat, tr.product_amount - (tr.product_amount * 7) / 107 as exvat, " +
                      //" tr.service_serCharge_amount,(tr.service_serCharge_amount * 7) / 107 as vat2, tr.service_serCharge_amount - (tr.service_serCharge_amount * 7) / 107 as exvat2 " +

                      " tr.product_amount, tr.product_amount_vat as vat, " +
                    " tr.product_amount -  tr.product_amount_vat as exvat, " +

                     " tr.service_serCharge_amount, tr.service_serCharge_amount_vat as vat2, " +
                    " tr.service_serCharge_amount - tr.service_serCharge_amount_vat as exvat2 , com.CompanyNameTh " +





                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract smt on smt.ContractNumber = tr.ContractNumber " +
                    "  inner join TblCompanyDetail com on com.Companycode = smt.CompanyCode " +
                    " where tr.ContractNumber = '" + ContractNumber + "' and tr.status = 'y' " +
                    " and tr.record_date between '" + date1 + "-01 00:00:00' and '" + date2 + "-01 00:00:00' order by tr.record_date  ";
        }
        


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


}