﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class sap_rep : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        conteactList_SAPDLL Serv = new conteactList_SAPDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtstartdate.Text = DateTime.Now.ToString("yyyy-MM", EngCI);
                    bind_default();
                    bind_rep();

                }

                if (HttpContext.Current.Session["code_theme"] != null)
                {
                    this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
                }
                else
                {
                    this.MyTheme = "#CCA9DA";
                }

                if (HttpContext.Current.Session["code_Navbar"] != null)
                {
                    this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
                }
                else
                {
                    this.NavbarColor = "#2d2339";
                }

            }
        }

        protected void bind_default()
        {

            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            ////var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_userid"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }

            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;

            //}



            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }

            ddlcompany.Items.Insert(0, new ListItem("Company", ""));

            //if (HttpContext.Current.Session["role"].ToString() == "officer")
            //{
            //    ddlar.Visible = false;
            //}
            //else
            //{
            //    ddlar.Visible = true;

            //    var u = Serv.GetUserByUserid(HttpContext.Current.Session["s_user_for_shop"].ToString());
            //    if (u.Rows.Count != 0)
            //    {
            //        ddlar.DataTextField = "name";
            //        ddlar.DataValueField = "userid";
            //        ddlar.DataSource = u;
            //        ddlar.DataBind();
            //    }
            //    else
            //    {
            //        ddlar.DataSource = null;
            //        ddlar.DataBind();
            //    }
            //    ddlar.Items.Insert(0, new ListItem("User", ""));
            //}

            ddlar.Visible = true;

            //var u = Serv.GetUserByUserid(HttpContext.Current.Session["s_user_for_shop"].ToString());
            var u = Serv.GetUserAR(HttpContext.Current.Session["s_com_code"].ToString());
            if (u.Rows.Count != 0)
            {
                ddlar.DataTextField = "name";
                ddlar.DataValueField = "userid";
                ddlar.DataSource = u;
                ddlar.DataBind();
            }
            else
            {
                ddlar.DataSource = null;
                ddlar.DataBind();
            }
            ddlar.Items.Insert(0, new ListItem("User", ""));

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("All", "all"));
            ddlstatus.Items.Insert(1, new ListItem("Confirm", "y"));
            ddlstatus.Items.Insert(2, new ListItem("Unconfirmed", "n"));

        }
        protected void bind_rep()
        {
            string[] shopname = txtshopname.Text.Split(',');
            string[] shopgroup = txtshopgroup.Text.Split(',');

            string Company = "";

            if (ddlcompany.SelectedValue == "")
            {
                Company = HttpContext.Current.Session["s_com_code"].ToString();
            }
            else
            {
                Company = "'" + ddlcompany.SelectedValue + "'";
            }


            var indust = Serv.GetSmart_Contract_owner_by_date("", Company, shopname, shopgroup, ddlar.SelectedValue, HttpContext.Current.Session["s_com_code"].ToString() , "", Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"));


            if (indust.Rows.Count != 0)
            {

                ////--------------------

                if (HttpContext.Current.Session["role"].ToString() == "officer")
                {
                    GridView_List.DataSource = indust;
                    GridView_List.DataBind();

                }
                else
                {
                    GridView_List2.DataSource = indust;
                    GridView_List2.DataBind();
                }


            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();

                GridView_List2.DataSource = null;
                GridView_List2.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }




        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }


        protected void btnexport_Click1(object sender, EventArgs e)
        {
            string company = "";
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HiddenField hdd_smart_record_keyin_type = (HiddenField)row.FindControl("hdd_smart_record_keyin_type");

            DataTable dt = new DataTable();
            dt.Columns.Add("d1");
            dt.Columns.Add("d2");
            dt.Columns.Add("d3");
            dt.Columns.Add("d4");
            dt.Columns.Add("d5");
            dt.Columns.Add("d6");
            dt.Columns.Add("d7");

            var sh = Serv.GetShopInfo(hdd_id.Value);
            if (sh.Rows.Count != 0)
            {
                DataRow row1 = dt.NewRow();

                decimal s1 = 0;
                decimal s2 = 0;
                decimal s3 = 0;
                decimal s4 = 0;
                decimal s5 = 0;
                decimal s6 = 0;


                if (hdd_smart_record_keyin_type.Value == "prod_sale")
                {
                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                    if (t.Rows.Count != 0)
                    {
                        company = t.Rows[0]["CompanyNameTh"].ToString();

                        row1 = dt.NewRow();
                        row1["d1"] = "Date";
                        row1["d2"] = "Sales ex VAT";
                        row1["d3"] = "Vat";
                        row1["d4"] = "Sales inc VAT";
                        row1["d5"] = "";
                        row1["d6"] = "";
                        row1["d7"] = "";
                        dt.Rows.Add(row1);

                        for (int i = 0; i < t.Rows.Count; i++)
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);

                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                            row1["d5"] = "";
                            row1["d6"] = "";
                            row1["d7"] = "";
                            dt.Rows.Add(row1);
                        }

                        row1 = dt.NewRow();
                        row1["d1"] = "Total";
                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                        row1["d5"] = "";
                        row1["d6"] = "";
                        row1["d7"] = "";
                        dt.Rows.Add(row1);
                    }
                    else
                    {
                        POPUPMSG("ไม่พบ Transaction ของเดือน " + txtstartdate.Text);
                        return;
                    }
                }
                else if (hdd_smart_record_keyin_type.Value == "prod_service")
                {
                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                    if (t.Rows.Count != 0)
                    {
                        company = t.Rows[0]["CompanyNameTh"].ToString();

                        row1 = dt.NewRow();
                        row1["d1"] = "Date";
                        row1["d2"] = "Sales ex VAT";
                        row1["d3"] = "Service ex VAT";
                        row1["d4"] = "VAT(Sale)";
                        row1["d5"] = "Sales inc VAT";
                        row1["d6"] = "VAT(Service)";
                        row1["d7"] = "Service inc VAT";
                        dt.Rows.Add(row1);

                        for (int i = 0; i < t.Rows.Count; i++)
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["exvat2"].ToString()).ToString("#,##0.00");
                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["exvat2"].ToString());

                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["vat2"].ToString()).ToString("#,##0.00");
                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["vat2"].ToString());

                            row1["d7"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            s6 = s6 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                            dt.Rows.Add(row1);
                        }

                        row1 = dt.NewRow();
                        row1["d1"] = "Total";
                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                        row1["d7"] = Convert.ToDecimal(s6).ToString("#,##0.00");
                        dt.Rows.Add(row1);
                    }
                    else
                    {
                        POPUPMSG("ไม่พบ Transaction ของเดือน " + txtstartdate.Text);
                        return;
                    }
                }
                else
                {
                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                    if (t.Rows.Count != 0)
                    {
                        company = t.Rows[0]["CompanyNameTh"].ToString();

                        row1 = dt.NewRow();
                        row1["d1"] = "Date";
                        row1["d2"] = "Sales ex VAT";
                        row1["d3"] = "VAT";
                        row1["d4"] = "Sale before Service Charge";
                        row1["d5"] = "Service Charge";
                        row1["d6"] = "Sales inc VAT";
                        row1["d7"] = "";
                        dt.Rows.Add(row1);

                        for (int i = 0; i < t.Rows.Count; i++)
                        {
                            decimal x = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()) - Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                            row1["d4"] = Convert.ToDecimal(x).ToString("#,##0.00");
                            s3 = s3 + Convert.ToDecimal(x);

                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                            row1["d7"] = "";
                            dt.Rows.Add(row1);
                        }

                        row1 = dt.NewRow();
                        row1["d1"] = "Total";
                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                        row1["d7"] = "";// Convert.ToDecimal(s6).ToString("#,##0.00");
                        dt.Rows.Add(row1);
                    }
                    else
                    {
                        POPUPMSG("ไม่พบ Transaction ของเดือน " + txtstartdate.Text);
                        return;
                    }
                }

                if (dt.Rows.Count != 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }


                if (GridView1.Rows.Count != 0)
                {

                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment; filename = daily_sap_rep_" + sh.Rows[0]["ShopName"].ToString() + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                    //Response.AddHeader("content-disposition", "attachment;filename=Export1.xls");
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.ContentEncoding = System.Text.Encoding.Unicode;
                    Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);

                    GridView1.RenderControl(hw);

                    string headerTable = @"<Table>" +
                            "<tr align='left'><td>"+ company + "</td></tr>" +
                            "<tr align='left'><td>Daily sales Report</td><td></td><td>Type</td><td>GP</td></tr>" +
                            "<tr align='left'><td>Code</td><td>" + sh.Rows[0]["BusinessPartnerCode"].ToString() + "</td></tr>" +
                            "<tr align='left'><td>Company Name</td><td>" + sh.Rows[0]["BusinessPartnerName"].ToString() + "</td></tr>" +
                            "<tr align='left'><td>Shop Name</td><td>" + sh.Rows[0]["ShopName"].ToString() + "</td></tr>" +
                            "<tr align='left'><td>Room</td><td>" + sh.Rows[0]["smart_room_no"].ToString() + "</td></tr>" +
                            "<tr align='left'><td>Area (Sq.m)</td><td>" + sh.Rows[0]["smart_sqm"].ToString() + "</td></tr>" +
                            "<tr align='left'><td>Contract No.</td><td>" + sh.Rows[0]["ContractNumber"].ToString() + "</td></tr>" +
                            "<tr align='left'><td>Month</td><td>" + Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI) + "</td></tr>" +
                            "</Table>";
                    Response.Write(headerTable);



                    Response.Write(sw.ToString());
                    Response.End();


                }

            }
        }


        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btnexport_Click(object sender, EventArgs e)
        {
            string company = "";
            if (GridView_List.Rows.Count != 0)
            {
                DataSet ds = new DataSet();

                foreach (GridViewRow row in GridView_List.Rows) //Running all lines of grid
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
                        HiddenField hdd_ShopName = (HiddenField)row.FindControl("hdd_ShopName");
                        HiddenField hdd_smart_record_keyin_type = (HiddenField)row.FindControl("hdd_smart_record_keyin_type");
                        CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");

                        if (chkRow.Checked)
                        {

                            DataTable dt = new DataTable(hdd_ShopName.Value.Replace("/", "-"));
                            dt.Columns.Add("d1");
                            dt.Columns.Add("d2");
                            dt.Columns.Add("d3");
                            dt.Columns.Add("d4");
                            dt.Columns.Add("d5");
                            dt.Columns.Add("d6");
                            dt.Columns.Add("d7");

                            var sh = Serv.GetShopInfo(hdd_id.Value);
                            if (sh.Rows.Count != 0)
                            {
                                DataRow row1 = dt.NewRow();

                                decimal s1 = 0;
                                decimal s2 = 0;
                                decimal s3 = 0;
                                decimal s4 = 0;
                                decimal s5 = 0;
                                decimal s6 = 0;


                                if (hdd_smart_record_keyin_type.Value == "prod_sale")
                                {
                                    row1 = dt.NewRow();
                                    row1["d1"] = company;
                                    row1["d2"] = "";
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Daily sales Report";
                                    row1["d2"] = "";
                                    row1["d3"] = "Type";
                                    row1["d4"] = "GP";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Code";
                                    row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Company Name";
                                    row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Shop Name";
                                    row1["d2"] = sh.Rows[0]["ShopName"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Room";
                                    row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Area (Sq.m)";
                                    row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Contract No.";
                                    row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Month";
                                    row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Date";
                                    row1["d2"] = "Sales ex VAT";
                                    row1["d3"] = "Vat";
                                    row1["d4"] = "Sales inc VAT";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "";
                                    row1["d2"] = "";
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"),
                                        Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "Vat";
                                        row1["d4"] = "Sales inc VAT";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();

                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            row1["d5"] = "";
                                            row1["d6"] = "";
                                            row1["d7"] = "";
                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);
                                    }
                                }
                                else if (hdd_smart_record_keyin_type.Value == "prod_service")
                                {
                                    row1 = dt.NewRow();
                                    row1["d1"] = company;
                                    row1["d2"] = "";
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Daily sales Report";
                                    row1["d2"] = "";
                                    row1["d3"] = "Type";
                                    row1["d4"] = "GP";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Code";
                                    row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Company Name";
                                    row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Shop Name";
                                    row1["d2"] = sh.Rows[0]["ShopName"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Room";
                                    row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Area (Sq.m)";
                                    row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Contract No.";
                                    row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Month";
                                    row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Date";
                                    row1["d2"] = "Sales ex VAT";
                                    row1["d3"] = "Vat";
                                    row1["d4"] = "Sales inc VAT";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "";
                                    row1["d2"] = "";
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "Service ex VAT";
                                        row1["d4"] = "VAT(Sale)";
                                        row1["d5"] = "Sales inc VAT";
                                        row1["d6"] = "VAT(Service)";
                                        row1["d7"] = "Service inc VAT";
                                        dt.Rows.Add(row1);

                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();
                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["exvat2"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["exvat2"].ToString());

                                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["vat2"].ToString()).ToString("#,##0.00");
                                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["vat2"].ToString());

                                            row1["d7"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                            s6 = s6 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                                        row1["d7"] = Convert.ToDecimal(s6).ToString("#,##0.00");
                                        dt.Rows.Add(row1);
                                    }
                                }
                                else
                                {
                                    row1 = dt.NewRow();
                                    row1["d1"] = company;
                                    row1["d2"] = "";
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Daily sales Report";
                                    row1["d2"] = "";
                                    row1["d3"] = "Type";
                                    row1["d4"] = "GP";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Code";
                                    row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Company Name";
                                    row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Shop Name";
                                    row1["d2"] = sh.Rows[0]["ShopName"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Room";
                                    row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Area (Sq.m)";
                                    row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Contract No.";
                                    row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Month";
                                    row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "Date";
                                    row1["d2"] = "Sales ex VAT";
                                    row1["d3"] = "Vat";
                                    row1["d4"] = "Sales inc VAT";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    row1 = dt.NewRow();
                                    row1["d1"] = "";
                                    row1["d2"] = "";
                                    row1["d3"] = "";
                                    row1["d4"] = "";
                                    row1["d5"] = "";
                                    row1["d6"] = "";
                                    row1["d7"] = "";
                                    dt.Rows.Add(row1);

                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "VAT";
                                        row1["d4"] = "Sale before Service Charge";
                                        row1["d5"] = "Service Charge";
                                        row1["d6"] = "Sales inc VAT";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            decimal x = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()) - Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();
                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d4"] = Convert.ToDecimal(x).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(x);

                                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            row1["d7"] = "";
                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                                        row1["d7"] = "";// Convert.ToDecimal(s6).ToString("#,##0.00");
                                        dt.Rows.Add(row1);
                                    }
                                }

                            }

                            if (dt.Rows.Count != 0)
                            {
                                ds.Tables.Add(dt);
                            }
                        }
                    }
                }

                if (ds.Tables.Count != 0)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(ds);

                        for (int j = 1; j <= ds.Tables.Count; j++)
                        {
                            for (int i = 1; i <= 11; i++)
                            {
                                wb.Worksheets.Worksheet(j).Row(i).Style.Fill.BackgroundColor = XLColor.White;
                            }
                        }


                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


                        Response.AddHeader("content-disposition", "attachment;filename=daily_sap_rep_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
                else
                {
                    POPUPMSG("ร้านค้าที่ท่านเลือกไม่มียอดในเดือนนี้");
                    return;
                }



            }
            else if (GridView_List2.Rows.Count != 0)
            {
                DataSet ds = new DataSet();


                foreach (GridViewRow row in GridView_List2.Rows) //Running all lines of grid
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {


                        HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
                        HiddenField hdd_ShopName = (HiddenField)row.FindControl("hdd_ShopName");
                        HiddenField hdd_smart_record_keyin_type = (HiddenField)row.FindControl("hdd_smart_record_keyin_type");
                        CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");

                        if (chkRow.Checked)
                        {

                            DataTable dt = new DataTable(hdd_ShopName.Value.Replace("/", "-"));
                            dt.Columns.Add("d1");
                            dt.Columns.Add("d2");
                            dt.Columns.Add("d3");
                            dt.Columns.Add("d4");
                            dt.Columns.Add("d5");
                            dt.Columns.Add("d6");
                            dt.Columns.Add("d7");

                            var sh = Serv.GetShopInfo(hdd_id.Value);
                            if (sh.Rows.Count != 0)
                            {
                                DataRow row1;

                                decimal s1 = 0;
                                decimal s2 = 0;
                                decimal s3 = 0;
                                decimal s4 = 0;
                                decimal s5 = 0;
                                decimal s6 = 0;


                                if (hdd_smart_record_keyin_type.Value == "prod_sale")
                                {
                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"),
                                        Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = company;
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Daily sales Report";
                                        row1["d2"] = "";
                                        row1["d3"] = "Type";
                                        row1["d4"] = "GP";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Code";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Company Name";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Shop Name";
                                        row1["d2"] = sh.Rows[0]["ShopName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Room";
                                        row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Area (Sq.m)";
                                        row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Contract No.";
                                        row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Month";
                                        row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "";
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "Vat";
                                        row1["d4"] = "Sales inc VAT";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);


                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();

                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            row1["d5"] = "";
                                            row1["d6"] = "";
                                            row1["d7"] = "";
                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);
                                    }
                                }
                                else if (hdd_smart_record_keyin_type.Value == "prod_service")
                                {
                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = company;
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Daily sales Report";
                                        row1["d2"] = "";
                                        row1["d3"] = "Type";
                                        row1["d4"] = "GP";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Code";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Company Name";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Shop Name";
                                        row1["d2"] = sh.Rows[0]["ShopName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Room";
                                        row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Area (Sq.m)";
                                        row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Contract No.";
                                        row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Month";
                                        row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "";
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "Service ex VAT";
                                        row1["d4"] = "VAT(Sale)";
                                        row1["d5"] = "Sales inc VAT";
                                        row1["d6"] = "VAT(Service)";
                                        row1["d7"] = "Service inc VAT";
                                        dt.Rows.Add(row1);


                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();
                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["exvat2"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["exvat2"].ToString());

                                            row1["d4"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["vat2"].ToString()).ToString("#,##0.00");
                                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["vat2"].ToString());

                                            row1["d7"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                            s6 = s6 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                                        row1["d7"] = Convert.ToDecimal(s6).ToString("#,##0.00");
                                        dt.Rows.Add(row1);
                                    }
                                }
                                else
                                {
                                    var t = Serv.GetTransaction_detail(hdd_id.Value, Convert.ToDateTime(txtstartdate.Text).ToString("yyyy-MM"), Convert.ToDateTime(txtstartdate.Text).AddMonths(1).ToString("yyyy-MM"), ddlstatus.SelectedValue);
                                    if (t.Rows.Count != 0)
                                    {
                                        company = t.Rows[0]["CompanyNameTh"].ToString();

                                        row1 = dt.NewRow();
                                        row1["d1"] = company;
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Daily sales Report";
                                        row1["d2"] = "";
                                        row1["d3"] = "Type";
                                        row1["d4"] = "GP";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Code";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerCode"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Company Name";
                                        row1["d2"] = sh.Rows[0]["BusinessPartnerName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Shop Name";
                                        row1["d2"] = sh.Rows[0]["ShopName"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Room";
                                        row1["d2"] = sh.Rows[0]["smart_room_no"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Area (Sq.m)";
                                        row1["d2"] = sh.Rows[0]["smart_sqm"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Contract No.";
                                        row1["d2"] = sh.Rows[0]["ContractNumber"].ToString();
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Month";
                                        row1["d2"] = Convert.ToDateTime(txtstartdate.Text).ToString("MMMM", EngCI);
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "";
                                        row1["d2"] = "";
                                        row1["d3"] = "";
                                        row1["d4"] = "";
                                        row1["d5"] = "";
                                        row1["d6"] = "";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Date";
                                        row1["d2"] = "Sales ex VAT";
                                        row1["d3"] = "VAT";
                                        row1["d4"] = "Sale before Service Charge";
                                        row1["d5"] = "Service Charge";
                                        row1["d6"] = "Sales inc VAT";
                                        row1["d7"] = "";
                                        dt.Rows.Add(row1);


                                        for (int i = 0; i < t.Rows.Count; i++)
                                        {
                                            decimal x = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()) - Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());
                                            row1 = dt.NewRow();
                                            row1["d1"] = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                                            //row1["d1"] = t.Rows[i]["record_date"].ToString();
                                            row1["d2"] = Convert.ToDecimal(t.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                                            s1 = s1 + Convert.ToDecimal(t.Rows[i]["exvat"].ToString());

                                            row1["d3"] = Convert.ToDecimal(t.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                                            s2 = s2 + Convert.ToDecimal(t.Rows[i]["vat"].ToString());

                                            row1["d4"] = Convert.ToDecimal(x).ToString("#,##0.00");
                                            s3 = s3 + Convert.ToDecimal(x);

                                            row1["d5"] = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                                            s4 = s4 + Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString());

                                            row1["d6"] = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                                            s5 = s5 + Convert.ToDecimal(t.Rows[i]["product_amount"].ToString());

                                            row1["d7"] = "";
                                            dt.Rows.Add(row1);
                                        }

                                        row1 = dt.NewRow();
                                        row1["d1"] = "Total";
                                        row1["d2"] = Convert.ToDecimal(s1).ToString("#,##0.00");
                                        row1["d3"] = Convert.ToDecimal(s2).ToString("#,##0.00");
                                        row1["d4"] = Convert.ToDecimal(s3).ToString("#,##0.00");
                                        row1["d5"] = Convert.ToDecimal(s4).ToString("#,##0.00");
                                        row1["d6"] = Convert.ToDecimal(s5).ToString("#,##0.00");
                                        row1["d7"] = "";// Convert.ToDecimal(s6).ToString("#,##0.00");
                                        dt.Rows.Add(row1);
                                    }
                                }



                            }


                            if (dt.Rows.Count != 0)
                            {
                                ds.Tables.Add(dt);
                            }
                        }


                    }


                }

                if (ds.Tables.Count != 0)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(ds);

                        for (int j = 1; j <= ds.Tables.Count; j++)
                        {
                            for (int i = 1; i <= 11; i++)
                            {
                                wb.Worksheets.Worksheet(j).Row(i).Style.Fill.BackgroundColor = XLColor.White;
                            }
                        }


                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


                        Response.AddHeader("content-disposition", "attachment;filename=daily_sap_rep_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
                else
                {
                    POPUPMSG("ร้านค้าที่ท่านเลือกไม่มียอดในเดือนนี้");
                    return;
                }

            }

        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/sap_rep.aspx");

        }

        protected void GridView_List2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List2.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }
    }
}