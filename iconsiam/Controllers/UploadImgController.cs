﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace iconsiam.Controllers
{

    public class result
    {
        public string returncode { get; set; }
        public string desc { get; set; }
        public string tran_id { get; set; }

    }

    public class result_img
    {
        public string returncode { get; set; }
        public string desc { get; set; }
    }

    public class info
    {
        public string ContractNumber { get; set; }
        public string product_amount { get; set; }
        public string service_serCharge_amount { get; set; }
        public string status { get; set; }
        public string record_date { get; set; }
        public string no_type { get; set; }
        public string remark { get; set; }
        public string type { get; set; }
        public string userid { get; set; }

    }

    public class info_update
    {
        public string id { get; set; }
        public string ContractNumber { get; set; }
        public string product_amount { get; set; }
        public string service_serCharge_amount { get; set; }
        public string status { get; set; }
        public string record_date { get; set; }
        public string no_type { get; set; }
        public string remark { get; set; }//userid
        public string userid { get; set; }//

    }

    public class img_info
    {
        public string transaction_id { get; set; }
        public string file_type { get; set; }
        public string Contract_number { get; set; }
        public string img { get; set; }

    }
    public class img_info_adjust
    {
        public string transaction_id { get; set; }
        public string img { get; set; }
        public string userid { get; set; }
        public string file_type { get; set; }
        public string Contract_number { get; set; }

    }

    public class user_info
    {
        public string username { get; set; }
        public string password { get; set; }

    }

    public class contract_info
    {
        public string contractnumber { get; set; }

    }

    public class user_info_email
    {
        public string email { get; set; }

    }

    public class userinfo_return
    {
        public string contractnumber { get; set; }
        public string username { get; set; }
        public string smart_record_keyin_type { get; set; }
        public string Shopname { get; set; }
        public string Floor { get; set; }
        public string flag_confirm { get; set; }
        public string returncode { get; set; }
        public string desc { get; set; }
        public string flag_reset { get; set; }
        public string userid { get; set; }
        public string theme_id { get; set; }
        public string theme_color { get; set; }
        public string NavbarColor { get; set; }
    }

    public class transaction_return
    {
        public string id { get; set; }
        public string contractnumber { get; set; }
        public string product_amount { get; set; }
        public string service_serCharge_amount { get; set; }
        public string record_date { get; set; }
        public string flag_confirm { get; set; }
        public string status { get; set; }
        public string flag_adjust { get; set; }
        public string adjust_remark { get; set; }
    }

    public class noti_return
    {
        public string id { get; set; }
        public string contractnumber { get; set; }
        public string message { get; set; }
        public string index { get; set; }
    }


    public class adjust_info
    {
        public string id { get; set; }
        public string contractnumber { get; set; }
        public string remark { get; set; }
        public string userid { get; set; }
    }

    public class adjust_inf_return
    {
        public string returncode { get; set; }
        public string desc { get; set; }
    }


    public class change_password_req
    {
        public string username { get; set; }
        public string password { get; set; }
        public string contractnumber { get; set; }
        public string userid { get; set; }

    }

    public class chk_dup_req
    {
        public string contractnumber { get; set; }
        public string date1 { get; set; }

    }

    public class img_result
    {
        public string filename { get; set; }
        public string code { get; set; }

    }

    public class change_password_return
    {
        public string returncode { get; set; }
        public string desc { get; set; }

    }

    public class confirm_req
    {
        public string id_list { get; set; }
        public string contractnumber { get; set; }
    }


    public class ret_Uploadimg
    {
        public string file_name { get; set; }
        public string file_type { get; set; }
    }


    public class UploadImgController : ApiController
    {


        //======================== QAS ========================================

        //[Route("api/Files/Uploadimg")]
        //[AllowAnonymous]
        //public ret_Uploadimg PostUserImage()
        //{
        //    ret_Uploadimg a = new ret_Uploadimg();
        //    //string a = "";

        //    Dictionary<string, object> dict = new Dictionary<string, object>();
        //    try
        //    {

        //        var httpRequest = HttpContext.Current.Request;

        //        foreach (string file in httpRequest.Files)
        //        {
        //            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

        //            var postedFile = httpRequest.Files[file];

        //            Random _r = new Random();
        //            //int n = _r.Next();
        //            String n = _r.Next(0, 999999).ToString("D6");
        //            string tmp_ = "m_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + postedFile.FileName;
        //            img_result b = new img_result();


        //            if (postedFile != null && postedFile.ContentLength > 0)
        //            {

        //                int MaxContentLength = 1024 * 1024 * 5; //Size = 1 MB  

        //                IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png", ".docx", ".doc"
        //                , ".xls" , ".xlsx" , ".pdf" , ".txt"   };
        //                var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
        //                var extension = ext.ToLower();
        //                if (!AllowedFileExtensions.Contains(extension))
        //                {
        //                    var message = string.Format("Please Upload image of type .jpg,.gif,.png.");
        //                }
        //                else if (postedFile.ContentLength > MaxContentLength)
        //                {
        //                    var message = string.Format("Please Upload a file upto 5 mb.");
        //                }
        //                else
        //                {

        //                    if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
        //                    {
        //                        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
        //                    }

        //                    //=============================== File Slip ================================

        //                    var filePath = HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_);
        //                    postedFile.SaveAs(filePath);
        //                    var message1 = string.Format(DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_);

        //                    a.file_type = ext;
        //                    a.file_name = message1;
        //                    //a = message1;
        //                }
        //            }


        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        a.file_type = "";
        //        a.file_name = ex.ToString();


        //    }

        //    return a;
        //}


        /// ======================== PROD ========================================

        [Route("api/Files/Uploadimg")]
        [AllowAnonymous]
        public ret_Uploadimg PostUserImage()
        {
            ret_Uploadimg a = new ret_Uploadimg();
            //string a = "";

            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {

                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];

                    Random _r = new Random();
                    //int n = _r.Next();
                    String n = _r.Next(0, 999999).ToString("D6");
                    string tmp_ = postedFile.FileName;
                    img_result b = new img_result();


                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 5; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png", ".docx", ".doc"
                        , ".xls" , ".xlsx" , ".pdf" , ".txt"   };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 5 mb.");
                        }
                        else
                        {
                            using (new Impersonator("SmartCollectionApp", "SPWSPWEB01", "G-7/6eY2"))
                            {
                                if (Directory.Exists(ConfigurationManager.AppSettings["pathImg_slip"] + @"img\slip\" + DateTime.Now.ToString("yyyy-MM-dd")) == false)
                                {
                                    System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["pathImg_slip"] + @"img\slip\" + DateTime.Now.ToString("yyyy-MM-dd"));
                                }

                                //=============================== File Slip ================================

                                var filePath = ConfigurationManager.AppSettings["pathImg_slip"] + @"img\slip\" + DateTime.Now.ToString("yyyy-MM-dd") + @"\" + tmp_;
                                postedFile.SaveAs(filePath);
                                var message1 = string.Format(DateTime.Now.ToString("yyyy-MM-dd") + @"\" + tmp_);

                                a.file_type = ext;
                                a.file_name = message1;
                                //a = message1;
                            }
                        }
                    }


                }

            }
            catch (Exception ex)
            {
                a.file_type = "";
                a.file_name = ex.ToString();


            }

            return a;
        }




        /// <summary>
        /// ======================================= Insert Image ===============================
        /// </summary>

        [Route("api/insert_img")]
        public result_img Post_img(img_info transaction)
        {
            result_img x = new result_img();
            record_transactionDLL Serv = new record_transactionDLL();


            string icon = "";
            if (transaction.file_type.ToLower().Replace(".", "") == "jpg" || transaction.file_type.ToLower().Replace(".", "") == "gif" || transaction.file_type.ToLower().Replace(".", "") == "png")
            {
                icon = "/img/img.png";
            }
            else if (transaction.file_type.ToLower().Replace(".", "") == "doc" || transaction.file_type.ToLower().Replace(".", "") == "docx")
            {
                icon = "/img/doc.png";

            }
            else if (transaction.file_type.ToLower().Replace(".", "") == "xls" || transaction.file_type.ToLower().Replace(".", "") == "xlsx")
            {
                icon = "/img/xls.png";

            }
            else if (transaction.file_type.ToLower().Replace(".", "") == "txt")
            {
                icon = "/img/txt.png";

            }
            else if (transaction.file_type.ToLower().Replace(".", "") == "pdf")
            {
                icon = "/img/pdf.png";
            }


            string filename_ = "";

            if (transaction.Contract_number != null && transaction.Contract_number != "")
            {
                filename_ = DateTime.Now.ToString("yyyy-MM-dd") + @"\" +
                transaction.Contract_number + "_m_" + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + transaction.file_type.ToLower();
            }
            else
            {
                filename_ = transaction.img;
            }

            System.IO.File.Move(ConfigurationManager.AppSettings["pathImg_slip"] + @"img\slip\" + transaction.img,
                ConfigurationManager.AppSettings["pathImg_slip"] + @"img\slip\" + filename_);

            Serv.tbltransaction_record_img_mobile(transaction.transaction_id, icon, filename_,
                transaction.img.Replace(DateTime.Now.ToString("yyyy-MM-dd") + @"\", ""));

            x.desc = "success";
            x.returncode = "1000";

            return x;
        }

        [Route("api/insert_img_adjust")]
        public result_img Post_insert_img_adjust(img_info_adjust transaction)
        {
            result_img x = new result_img();
            record_transactionDLL Serv = new record_transactionDLL();

            string icon = "";
            if (transaction.file_type.ToLower().Replace(".", "") == "jpg" || transaction.file_type.ToLower().Replace(".", "") == "gif" || transaction.file_type.ToLower().Replace(".", "") == "png")
            {
                icon = "/img/img.png";
            }
            else if (transaction.file_type.ToLower().Replace(".", "") == "doc" || transaction.file_type.ToLower().Replace(".", "") == "docx")
            {
                icon = "/img/doc.png";

            }
            else if (transaction.file_type.ToLower().Replace(".", "") == "xls" || transaction.file_type.ToLower().Replace(".", "") == "xlsx")
            {
                icon = "/img/xls.png";

            }
            else if (transaction.file_type.ToLower().Replace(".", "") == "txt")
            {
                icon = "/img/txt.png";

            }
            else if (transaction.file_type.ToLower().Replace(".", "") == "pdf")
            {
                icon = "/img/pdf.png";
            }


            string filename_ = "";

            if (transaction.Contract_number != null && transaction.Contract_number != "")
            {
                filename_ = DateTime.Now.ToString("yyyy-MM-dd") + @"\" +
                transaction.Contract_number + "_m_" + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + transaction.file_type.ToLower();
            }
            else
            {
                filename_ = transaction.img;
            }

            System.IO.File.Move(ConfigurationManager.AppSettings["pathImg_slip"] + @"img\slip\" + transaction.img,
                ConfigurationManager.AppSettings["pathImg_slip"] + @"img\slip\" + filename_);


            Serv.tbl_edit_transaction_record_img_mobile(transaction.transaction_id, icon, filename_,
                 transaction.img.Replace(DateTime.Now.ToString("yyyy-MM-dd") + @"\", ""), transaction.userid);


            x.desc = "success";
            x.returncode = "1000";

            return x;
        }

        [Route("api/insert_info")]
        public result Post(info transaction)
        {
            result x = new result();
            record_transactionDLL Serv = new record_transactionDLL();

            try
            {

                var t = Serv.getTransaction_dup_mobile(transaction.ContractNumber, transaction.record_date);
                if (t.Rows.Count != 0)
                {

                    int vat = 0;
                    var company = Serv.GetCompany(transaction.ContractNumber);
                    if (company.Rows.Count != 0)
                    {
                        var v = Serv.GetVAT_mobile(company.Rows[0]["CompanyCode"].ToString());
                        if (v.Rows.Count != 0)
                        {
                            vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
                        }
                        else
                        {
                            vat = 7;
                        }
                    }

                    decimal xx = 0;

                    if (string.IsNullOrEmpty(transaction.service_serCharge_amount))
                    {
                        xx = 0;
                    }
                    else
                    {
                        xx = Convert.ToDecimal(transaction.service_serCharge_amount.Replace(",", "")
                                             .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                            .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                            .Replace("+", "").Replace("-", "").Replace("฿", ""));
                    }


                    Serv.Update_transaction_record_mobile(transaction.ContractNumber, transaction.product_amount.Replace(",", "")
                                             .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                            .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                            .Replace("+", "").Replace("-", "").Replace("฿", ""), Convert.ToString(xx)

                                           , "y",
                        transaction.record_date, transaction.no_type, transaction.remark, t.Rows[0]["id"].ToString(), vat, transaction.userid, "te", "mobile");


                    x.desc = "success";
                    x.returncode = "1000";
                    x.tran_id = t.Rows[0]["id"].ToString();

                }
                else
                {
                    int vat = 0;
                    var company = Serv.GetCompany(transaction.ContractNumber);
                    if (company.Rows.Count != 0)
                    {
                        var v = Serv.GetVAT_mobile(company.Rows[0]["CompanyCode"].ToString());
                        if (v.Rows.Count != 0)
                        {
                            vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
                        }
                        else
                        {
                            vat = 7;
                        }
                    }

                    decimal xx = 0;

                    if (string.IsNullOrEmpty(transaction.service_serCharge_amount))
                    {
                        xx = 0;
                    }
                    else
                    {
                        xx = Convert.ToDecimal(transaction.service_serCharge_amount.Replace(",", "")
                                             .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                            .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                            .Replace("+", "").Replace("-", "").Replace("฿", ""));
                    }


                    var indent = Serv.Insert_transaction_record_mobile(transaction.ContractNumber, transaction.product_amount.Replace(",", "")
                                             .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                            .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                            .Replace("+", "").Replace("-", "").Replace("฿", ""),

                                            Convert.ToString(xx), "y",
                        transaction.record_date, transaction.no_type, transaction.remark, vat, transaction.type, transaction.userid, "te", "mobile");


                    x.desc = "success";
                    x.returncode = "1000";
                    x.tran_id = indent.Rows[0]["last_id"].ToString();

                }
            }
            catch (Exception ex)
            {
                x.desc = ex.ToString();
                x.returncode = "3800";
                x.tran_id = "";
            }


            return x;
        }

        [Route("api/update_info")]
        public result Post_update(info_update transaction)
        {
            result x = new result();
            record_transactionDLL Serv = new record_transactionDLL();

            try
            {
                int vat = 0;
                var company = Serv.GetCompany(transaction.ContractNumber);
                if (company.Rows.Count != 0)
                {
                    var v = Serv.GetVAT_mobile(company.Rows[0]["CompanyCode"].ToString());
                    if (v.Rows.Count != 0)
                    {
                        vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
                    }
                    else
                    {
                        vat = 7;
                    }
                }

                decimal xx = 0;
                if (string.IsNullOrEmpty(transaction.service_serCharge_amount))
                {
                    xx = 0;
                }
                else
                {
                    xx = Convert.ToDecimal(transaction.service_serCharge_amount.Replace(",", "")
                                         .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                        .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                        .Replace("+", "").Replace("-", "").Replace("฿", ""));
                }




                Serv.Update_transaction_record_mobile(transaction.ContractNumber, transaction.product_amount.Replace(",", "")
                                             .Replace("@", "").Replace("#", "").Replace("!", "").Replace("#", "").Replace("$", "").Replace("%", "")
                                            .Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "").Replace(")", "").Replace("_", "")
                                            .Replace("+", "").Replace("-", "").Replace("฿", ""), Convert.ToString(xx)
                                           , "y",
                    transaction.record_date, transaction.no_type, transaction.remark, transaction.id, vat, transaction.userid, "te", "mobile");


                x.desc = "success";
                x.returncode = "1000";
                x.tran_id = transaction.id;

            }
            catch (Exception ex)
            {
                x.desc = ex.ToString();
                x.returncode = "3800";
                x.tran_id = transaction.id;
            }

            return x;
        }

        [Route("api/login")]
        public userinfo_return Post_login(user_info user_info)
        {
            userinfo_return x = new userinfo_return();

            loginDLL Serv = new loginDLL();

            string CompanyCode = "";

            var user = Serv.GetUserByUserPass_mobile(user_info.username, user_info.password);
            if (user.Rows.Count != 0)
            {
                Serv.Insert_log_login_mobile(user.Rows[0]["id"].ToString(), "tenant", "mobile");

                var contract = Serv.GetContractDetail_mobile(user.Rows[0]["contractnumber"].ToString());
                if (contract.Rows.Count != 0)
                {

                    CompanyCode = contract.Rows[0]["CompanyCode"].ToString();

                    x.returncode = "1000";
                    x.desc = "success";
                    x.contractnumber = user.Rows[0]["contractnumber"].ToString();
                    x.smart_record_keyin_type = contract.Rows[0]["smart_record_keyin_type"].ToString();
                    x.Shopname = contract.Rows[0]["ShopName"].ToString();
                    x.username = user.Rows[0]["username"].ToString();
                    x.Floor = contract.Rows[0]["smart_floor"].ToString();
                    x.flag_reset = user.Rows[0]["flag_reset"].ToString();
                    x.userid = user.Rows[0]["id"].ToString();

                    string contractnumber = user.Rows[0]["contractnumber"].ToString();
                    //string companycode = contractnumber.Substring(0, 3);

                    var comcode = Serv.getComcode_by_ContractNumber_mobile(user.Rows[0]["contractnumber"].ToString());
                    if (comcode.Rows.Count != 0)
                    {
                        var Theme = Serv.getThemeUser_tenant_mobile(comcode.Rows[0]["CompanyCode"].ToString());
                        if (Theme.Rows.Count != 0)
                        {
                            if (Theme.Rows.Count != 0)
                            {
                                x.theme_id = Theme.Rows[0]["ThemeId"].ToString();
                                x.theme_color = Theme.Rows[0]["ButtonColor"].ToString();
                                x.NavbarColor = Theme.Rows[0]["NavbarColor"].ToString();
                            }
                            else
                            {
                                x.theme_id = "0";
                                x.theme_color = "#CCA9DA";
                                x.NavbarColor = "#fff";
                            }
                        }

                    }
                    //var Theme = Serv.GetTheme_by_companycode(companycode);


                }
                else
                {
                    x.returncode = "3800";
                    x.desc = "fail";
                    x.contractnumber = "";
                    x.smart_record_keyin_type = "";
                    x.username = "";
                    x.Shopname = "";
                    x.Floor = "";
                    x.flag_reset = "";
                    x.userid = "";
                    x.theme_id = "";
                    x.theme_color = "";
                    x.NavbarColor = "";
                }
            }
            else
            {
                x.returncode = "3800";
                x.desc = "fail";
                x.contractnumber = "";
                x.smart_record_keyin_type = "";
                x.username = "";
                x.Shopname = "";
                x.Floor = "";
                x.flag_reset = "";
                x.userid = "";
                x.theme_id = "";
                x.theme_color = "";
                x.NavbarColor = "";
            }

            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            var c = Serv.Get_Confirm_new(CompanyCode);
            var c2 = Serv.Get_Confirm_new2(CompanyCode);

            if (c.Rows.Count != 0)
            {
                if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (c2.Rows.Count != 0)
                {
                    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else
                    {
                        x.flag_confirm = "n";

                    }
                }
                else
                {
                    x.flag_confirm = "n";

                }

            }
            else
            {
                x.flag_confirm = "n";

            }
            return x;
        }

        [Route("api/login_email")]
        public userinfo_return Post_login_email(user_info_email user_info)
        {
            userinfo_return x = new userinfo_return();
            sentmail mail = new sentmail();
            loginDLL Serv = new loginDLL();
            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            string CompanyCode = "";

            var user = Serv.GetUserByEmail_mobile(user_info.email);
            if (user.Rows.Count != 0) //check email
            {
                if (user.Rows.Count > 1)
                {
                    x.returncode = "3800";
                    x.desc = "ไม่สามารถใช้งานฟังก์ชั่นนี้ได้ เนื่องจากมีการผูกอีเมล์หลายร้านค้า กรุณาติดต่อ AR team";
                    x.contractnumber = "";
                    x.smart_record_keyin_type = "";
                    x.username = "";
                    x.Shopname = "";
                    x.Floor = "";
                    x.userid = "";
                    x.theme_id = "";
                    x.theme_color = "";
                    x.NavbarColor = "";
                }
                else
                {
                    var contract = Serv.GetContractDetail_mobile(user.Rows[0]["contractnumber"].ToString());
                    if (contract.Rows.Count != 0) //check contractnumber
                    {
                        CompanyCode = contract.Rows[0]["CompanyCode"].ToString();


                        var username = Serv.GetUserByContractnumber_mobile(user.Rows[0]["contractnumber"].ToString());
                        if (username.Rows.Count != 0) //check username password
                        {
                            var Floor = Serv.GetFloorfromContractNumber_mobile(user.Rows[0]["contractnumber"].ToString());
                            if (Floor.Rows.Count != 0) //getfloor
                            {

                                Serv.Insert_log_login_mobile(username.Rows[0]["id"].ToString(), "tenant", "mobile");

                                x.username = username.Rows[0]["username"].ToString();
                                x.contractnumber = user.Rows[0]["contractnumber"].ToString();
                                x.smart_record_keyin_type = contract.Rows[0]["smart_record_keyin_type"].ToString();
                                x.Shopname = contract.Rows[0]["ShopName"].ToString();
                                x.Floor = Floor.Rows[0]["FloorDescription"].ToString();
                                x.returncode = "1000";
                                x.desc = "success";
                                x.userid = username.Rows[0]["id"].ToString();

                                string contractnumber = username.Rows[0]["contractnumber"].ToString();
                                string companycode = contractnumber.Substring(0, 3);

                                var Theme = Serv.GetTheme_by_companycode_mobile(companycode);
                                if (Theme.Rows.Count != 0)
                                {
                                    x.theme_id = Theme.Rows[0]["ThemeId"].ToString();
                                    x.theme_color = Theme.Rows[0]["ButtonColor"].ToString();
                                    x.NavbarColor = Theme.Rows[0]["NavbarColor"].ToString();
                                }
                                else
                                {
                                    x.theme_id = "0";
                                    x.theme_color = "#CCA9DA";
                                    x.NavbarColor = "#fff";
                                }


                                string email_ar = "";
                                string ar_name = "";
                                string shop_name = "";
                                string room_number = "";
                                string business_name = "";
                                string co_name = "";
                                string floor = "";

                                var conDetail = Serv.GetContractDetail_mobile(user.Rows[0]["contractnumber"].ToString());
                                if (conDetail.Rows.Count != 0)
                                {
                                    var com = Serv.GetCompany(conDetail.Rows[0]["CompanyCode"].ToString());
                                    if (com.Rows.Count != 0)
                                    {
                                        co_name = com.Rows[0]["CompanyNameEn"].ToString();
                                    }
                                    shop_name = conDetail.Rows[0]["ShopName"].ToString();
                                    room_number = conDetail.Rows[0]["smart_room_no"].ToString();//
                                    business_name = conDetail.Rows[0]["BUsinessPartnerName"].ToString();//
                                    floor = conDetail.Rows[0]["smart_floor"].ToString();//

                                    var u = Serv.GetUserByUserid_mobile(conDetail.Rows[0]["smart_icon_staff_id"].ToString());
                                    if (u.Rows.Count != 0)
                                    {
                                        email_ar = u.Rows[0]["email"].ToString();
                                        ar_name = u.Rows[0]["Fname"].ToString() + " " + u.Rows[0]["Lname"].ToString();
                                    }
                                }

                                mail.CallMail_forget_password(ar_name, co_name, shop_name, business_name, floor, room_number, DateTime.Now.ToString("dd/MM/yyyy", EngCI), email_ar, conDetail.Rows[0]["CompanyCode"].ToString(), "user");


                            }
                            else
                            {
                                x.returncode = "3800";
                                x.desc = "fail";
                                x.contractnumber = "";
                                x.smart_record_keyin_type = "";
                                x.username = "";
                                x.Shopname = "";
                                x.Floor = "";
                                x.userid = "";
                                x.theme_id = "";
                                x.theme_color = "";
                                x.NavbarColor = "";
                            }

                        }
                        else
                        {
                            x.returncode = "3800";
                            x.desc = "fail";
                            x.contractnumber = "";
                            x.smart_record_keyin_type = "";
                            x.username = "";
                            x.Shopname = "";
                            x.Floor = "";
                            x.userid = "";
                            x.theme_id = "";
                            x.theme_color = "";
                            x.NavbarColor = "";
                        }
                    }
                    else
                    {
                        x.returncode = "3800";
                        x.desc = "fail";
                        x.contractnumber = "";
                        x.smart_record_keyin_type = "";
                        x.username = "";
                        x.Shopname = "";
                        x.Floor = "";
                        x.userid = "";
                        x.theme_id = "";
                        x.theme_color = "";
                        x.NavbarColor = "";
                    }
                }

            }
            else
            {
                x.returncode = "3800";
                x.desc = "fail";
                x.contractnumber = "";
                x.smart_record_keyin_type = "";
                x.username = "";
                x.Shopname = "";
                x.Floor = "";
                x.theme_id = "";
                x.theme_color = "";
                x.NavbarColor = "";
            }


            var c = Serv.Get_Confirm_new(CompanyCode);
            var c2 = Serv.Get_Confirm_new2(CompanyCode);

            if (c.Rows.Count != 0)
            {
                if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                {
                    x.flag_confirm = "y";

                }
                else if (c2.Rows.Count != 0)
                {
                    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                    {
                        x.flag_confirm = "y";

                    }
                    else
                    {
                        x.flag_confirm = "n";

                    }
                }
                else
                {
                    x.flag_confirm = "n";

                }

            }
            else
            {
                x.flag_confirm = "n";

            }


            return x;
        }

        [Route("api/get_transaction")]
        public List<transaction_return> Post_get_transaction(contract_info contract_info)
        {
            List<transaction_return> Y = new List<transaction_return>();
            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            record_transactionDLL Serv = new record_transactionDLL();

            string contract_no = "";

            var old = Serv.getOldContract_mobile(contract_info.contractnumber);
            if (old.Rows.Count != 0)
            {
                if (old.Rows[0]["Contract_old_number"].ToString() != "")
                {
                    contract_no = "" + contract_info.contractnumber + "," + old.Rows[0]["Contract_old_number"].ToString() + "";
                }
                else
                {
                    contract_no = "" + contract_info.contractnumber + "";
                }
            }
            else
            {
                contract_no = "" + contract_info.contractnumber + "";
            }

            var t = Serv.GetTransactionByContractNumber_mobile(contract_no, DateTime.Now.AddMonths(-1).ToString("yyyy-MM-01 00:00:00", EngCI),
                DateTime.Now.ToString("yyyy-MM-dd 00:00:00", EngCI));

            if (t.Rows.Count != 0)
            {
                for (int i = 0; i < t.Rows.Count; i++)
                {
                    transaction_return x = new transaction_return();


                    x.id = t.Rows[i]["id"].ToString();
                    x.contractnumber = t.Rows[i]["contractnumber"].ToString();

                    x.product_amount = Convert.ToDecimal(t.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                    x.service_serCharge_amount = Convert.ToDecimal(t.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");

                    x.record_date = Convert.ToDateTime(t.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                    x.flag_confirm = t.Rows[i]["flag_confirm"].ToString();
                    x.status = t.Rows[i]["status"].ToString();
                    x.flag_adjust = t.Rows[i]["flag_adjust"].ToString();
                    x.adjust_remark = t.Rows[i]["adjust_remark"].ToString();
                    Y.Add(x);
                }

            }
            else
            {
                transaction_return x = new transaction_return();


                x.id = "";
                x.contractnumber = "";
                x.product_amount = "";
                x.service_serCharge_amount = "";
                x.record_date = "";
                x.flag_confirm = "";
                x.status = "";
                x.flag_adjust = "";
                x.adjust_remark = "";
                Y.Add(x);
            }


            return Y;
        }

        [Route("api/insert_adjust")]
        public adjust_inf_return Post_insert_adjust(adjust_info transaction)
        {
            adjust_inf_return x = new adjust_inf_return();
            record_transactionDLL Serv = new record_transactionDLL();
            sentmail sentMail = new sentmail();
            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

            Serv.Update_Transaction_mobile(transaction.remark, transaction.id, transaction.contractnumber, transaction.userid, "te");
            var cont = Serv.GetContract_mobile(transaction.contractnumber);
            if (cont.Rows.Count != 0)
            {
                var u = Serv.GetUser_mobile(cont.Rows[0]["smart_icon_staff_id"].ToString());
                if (u.Rows.Count != 0)
                {
                    dateconvert ddd = new dateconvert();
                    var trans = Serv.GetTransaction_detail_mobile(transaction.id, transaction.contractnumber);
                    if (trans.Rows.Count != 0)
                    {

                        string comname_ = "";
                        var comname = Serv.GetCompanyCodeByCompanyCode_mobile(cont.Rows[0]["CompanyCode"].ToString());
                        if (comname.Rows.Count != 0)
                        {
                            comname_ = comname.Rows[0]["CompanyNameEN"].ToString();
                        }

                        sentMail.CallMail_adjust(u.Rows[0]["email"].ToString(),
                             "ระบบ Tenant Sales Data Collection : แก้ไขยอดขายของวันที่ " +
                             Convert.ToDateTime(trans.Rows[0]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
                             Convert.ToDateTime(trans.Rows[0]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
                             Convert.ToDateTime(trans.Rows[0]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + cont.Rows[0]["ShopName"].ToString() + " ห้อง " + cont.Rows[0]["smart_room_no"].ToString(),
                             "Tenant Sales Data Collection", u.Rows[0]["Fname"].ToString(),
                             "แก้ไขยอดขายของวันที่ " + Convert.ToDateTime(trans.Rows[0]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
                             Convert.ToDateTime(trans.Rows[0]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
                             Convert.ToDateTime(trans.Rows[0]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + cont.Rows[0]["ShopName"].ToString() + " ห้อง " + cont.Rows[0]["smart_room_no"].ToString(),

                             comname_, "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่แก้ไขยอด", "Link",
                             "", cont.Rows[0]["ShopName"].ToString(), cont.Rows[0]["BusinessPartnerName"].ToString(),
                             cont.Rows[0]["smart_floor"].ToString(), cont.Rows[0]["smart_room_no"].ToString(),
                             Convert.ToDateTime(trans.Rows[0]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
                             Convert.ToDateTime(trans.Rows[0]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
                             Convert.ToDateTime(trans.Rows[0]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(0, 4), "", transaction.remark, cont.Rows[0]["CompanyCode"].ToString(), "user");
                    }




                }
            }

            x.desc = "success";
            x.returncode = "1000";

            return x;
        }

        [Route("api/change_password")]
        public change_password_return Post_change_pass(change_password_req transaction)
        {
            change_password_return x = new change_password_return();
            record_transactionDLL Serv = new record_transactionDLL();

            Serv.Update_Password_mobile(transaction.username, transaction.contractnumber, transaction.password, transaction.userid);

            x.desc = "success";
            x.returncode = "1000";

            return x;
        }

        [Route("api/check_dup")]
        public change_password_return Post_chk_dup(chk_dup_req transaction)
        {
            change_password_return x = new change_password_return();
            record_transactionDLL Serv = new record_transactionDLL();

            var t = Serv.getTransaction_dup_mobile(transaction.contractnumber, transaction.date1);
            if (t.Rows.Count != 0)
            {
                if (t.Rows[0]["status"].ToString() == "y")
                {
                    x.desc = "Duplicate Data";
                    x.returncode = "3800";
                }
                else
                {
                    x.desc = t.Rows[0]["id"].ToString();
                    x.returncode = "3888";
                }

            }
            else
            {

                CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

                string d2;
                string d1;
                int xxx = 0;
                var cont = Serv.GetContract_ShopOper_Close_mobile(transaction.contractnumber);
                if (cont.Rows.Count != 0)
                {
                    //d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_close"]).ToString("yyyy-MM-dd", EngCI);
                    //d1 = transaction.date1;
                    //int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                    //if (result > 0)
                    //{

                    //    xxx = xxx + 1;
                    //}

                    d2 = Convert.ToDateTime(cont.Rows[0]["smart_shop_open"]).ToString("yyyy-MM-dd", EngCI);
                    d1 = transaction.date1;
                    int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));
                    if (result < 0)
                    {
                        xxx = xxx + 1;
                    }
                }


                if (xxx == 0)
                {
                    x.desc = "Available";
                    x.returncode = "1000";
                }
                else
                {
                    x.desc = "วันที่ท่านเลือกไม่อยู่ในช่วงเปิด-ปิดร้านค้าในสัญญา";
                    x.returncode = "3900";
                }




            }


            return x;
        }


        [Route("api/confirm_transaction")]
        public change_password_return Post_confirm(confirm_req transaction)
        {
            change_password_return x = new change_password_return();
            record_transactionDLL Serv = new record_transactionDLL();

            Serv.Confirm_transaction_mobile(transaction.id_list, transaction.contractnumber);

            x.desc = "success";
            x.returncode = "1000";

            return x;
        }


        [Route("api/get_noti")]
        public List<noti_return> Post_Noti(contract_info contract_info)
        {
            List<noti_return> Y = new List<noti_return>();


            record_transactionDLL Serv = new record_transactionDLL();

            var t = Serv.getNoti_mobile(contract_info.contractnumber);
            if (t.Rows.Count != 0)
            {
                for (int i = 0; i < t.Rows.Count; i++)
                {
                    noti_return x = new noti_return();

                    x.id = t.Rows[i]["id"].ToString();
                    x.contractnumber = t.Rows[i]["contractnumber"].ToString();
                    x.message = t.Rows[i]["message"].ToString();
                    x.index = Convert.ToString(i + 1);
                    Y.Add(x);

                    Serv.UpdateNoti_mobile(t.Rows[i]["id"].ToString(), t.Rows[i]["contractnumber"].ToString());
                }

            }
            else
            {
                noti_return x = new noti_return();

                x.id = "";// t.Rows[i]["id"].ToString();
                x.contractnumber = "";// t.Rows[i]["contractnumber"].ToString();
                x.message = "";// t.Rows[i]["message"].ToString();
                x.index = "";
                Y.Add(x);
            }


            return Y;
        }

        public class logo_com_group
        {
            public string CompanyGroupLogo { get; set; }
            public string ComGroupName { get; set; }

        }

        [Route("api/get_group_company")]
        public List<logo_com_group> Post_get_group_company()
        {
            List<logo_com_group> x = new List<logo_com_group>();

            loginDLL Serv = new loginDLL();
            var com = Serv.getLogo();
            if (com.Rows.Count != 0)
            {
                for (int i = 0; i < com.Rows.Count; i++)
                {
                    logo_com_group l = new logo_com_group();
                    l.ComGroupName = com.Rows[i]["ComGroupName"].ToString();
                    l.CompanyGroupLogo = ConvertImageURLToBase64(com.Rows[i]["CompanyGroupLogo"].ToString());

                    x.Add(l);
                }
            }
            else
            {
                logo_com_group l = new logo_com_group();
                l.ComGroupName = "";
                l.CompanyGroupLogo = "";

                x.Add(l);
            }



            return x;
        }

        public String ConvertImageURLToBase64(String ID)
        {

            string url = "http://10.10.6.20:8162" + ID.Replace("~", "");
            //create an object of StringBuilder type.
            StringBuilder _sb = new StringBuilder();
            //create a byte array that will hold the return value of the getImg method
            Byte[] _byte = this.GetImg(url);
            //appends the argument to the stringbulilder object (_sb)
            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));
            //return the complete and final url in a base64 format.
            return string.Format(@"data:image/png;base64,{0}", _sb.ToString());

        }

        private byte[] GetImg(string url)
        {
            //create a stream object and initialize it to null
            Stream stream = null;
            //create a byte[] object. It serves as a buffer.
            byte[] buf;
            try
            {
                //Create a new WebProxy object.
                WebProxy myProxy = new WebProxy();
                //create a HttpWebRequest object and initialize it by passing the colleague api url to a create method.
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                //Create a HttpWebResponse object and initilize it
                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                //get the response stream
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    //get the content length in integer
                    int len = (int)(response.ContentLength);
                    //Read bytes
                    buf = br.ReadBytes(len);
                    //close the binary reader
                    br.Close();
                }
                //close the stream object
                stream.Close();
                //close the response object 
                response.Close();
            }
            catch (Exception exp)
            {
                //set the buffer to null
                buf = null;
            }
            //return the buffer
            return (buf);
        }


        public class req_getImgBase64ByID
        {
            public string imageID { get; set; }
            public string type { get; set; }
        }
        public class ret_getImgBase64ByID
        {
            public string FileBase64 { get; set; }
        }


        [Route("api/getImgBase64ByID")]
        public ret_getImgBase64ByID Post_getImgBase64ByID(req_getImgBase64ByID param)
        {
            ret_getImgBase64ByID x = new ret_getImgBase64ByID();

            loginDLL Serv = new loginDLL();
            try
            {
                if (param.type == "n")
                {
                    var com = Serv.getImageById(param.imageID);
                    if (com.Rows.Count != 0)
                    {
                        using (new Impersonator("SmartCollectionApp", "SPWSPWEB01", "G-7/6eY2"))
                        {
                            string tmp_ = ConfigurationManager.AppSettings["pathImg_slip"] + @"img\slip\" + com.Rows[0]["filename"].ToString().Replace("/", @"\");

                            x.FileBase64 = ConvertImageURLToBase64_(tmp_);
                        }

                    }
                    else
                    {
                        x.FileBase64 = "";
                    }
                }
                else
                {
                    var com = Serv.getImageAdjustById(param.imageID);
                    if (com.Rows.Count != 0)
                    {
                        using (new Impersonator("SmartCollectionApp", "SPWSPWEB01", "G-7/6eY2"))
                        {
                            string tmp_ = ConfigurationManager.AppSettings["pathImg_slip"] + @"img\slip\" + com.Rows[0]["filename"].ToString().Replace("/", @"\");

                            x.FileBase64 = ConvertImageURLToBase64_(tmp_);
                        }

                    }
                    else
                    {
                        x.FileBase64 = "";
                    }
                }

            }
            catch (Exception ex)
            {
                x.FileBase64 = ex.ToString();

            }

            return x;
        }

        public String ConvertImageURLToBase64_(string uri)
        {
            string url = uri;
            StringBuilder _sb = new StringBuilder();

            byte[] bytes = System.IO.File.ReadAllBytes(uri);
            _sb.Append(Convert.ToBase64String(bytes));

            return string.Format(_sb.ToString());

        }

    }
}


namespace iconsiam
{
    #region Using directives.
    // ----------------------------------------------------------------------

    using System;
    using System.Security.Principal;
    using System.Runtime.InteropServices;
    using System.ComponentModel;

    // ----------------------------------------------------------------------
    #endregion

    /////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Impersonation of a user. Allows to execute code under another
    /// user context.
    /// Please note that the account that instantiates the Impersonator class
    /// needs to have the 'Act as part of operating system' privilege set.
    /// </summary>
    /// <remarks>    
    /// This class is based on the information in the Microsoft knowledge base
    /// article http://support.microsoft.com/default.aspx?scid=kb;en-us;Q306158
    /// 
    /// Encapsulate an instance into a using-directive like e.g.:
    /// 
    ///        ...
    ///        using ( new Impersonator( "myUsername", "myDomainname", "myPassword" ) )
    ///        {
    ///            ...
    ///            [code that executes under the new context]
    ///            ...
    ///        }
    ///        ...
    /// 
    /// Please contact the author Uwe Keim (mailto:uwe.keim@zeta-software.de)
    /// for questions regarding this class.
    /// </remarks>
    public class Impersonator :
        IDisposable
    {
        #region Public methods.
        // ------------------------------------------------------------------

        /// <summary>
        /// Constructor. Starts the impersonation with the given credentials.
        /// Please note that the account that instantiates the Impersonator class
        /// needs to have the 'Act as part of operating system' privilege set.
        /// </summary>
        /// <param name="userName">The name of the user to act as.</param>
        /// <param name="domainName">The domain name of the user to act as.</param>
        /// <param name="password">The password of the user to act as.</param>
        public Impersonator(
            string userName,
            string domainName,
            string password)
        {
            ImpersonateValidUser(userName, domainName, password);
        }

        // ------------------------------------------------------------------
        #endregion

        #region IDisposable member.
        // ------------------------------------------------------------------

        public void Dispose()
        {
            UndoImpersonation();
        }

        // ------------------------------------------------------------------
        #endregion

        #region P/Invoke.
        // ------------------------------------------------------------------

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern int LogonUser(
            string lpszUserName,
            string lpszDomain,
            string lpszPassword,
            int dwLogonType,
            int dwLogonProvider,
            ref IntPtr phToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int DuplicateToken(
            IntPtr hToken,
            int impersonationLevel,
            ref IntPtr hNewToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool RevertToSelf();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern bool CloseHandle(
            IntPtr handle);

        private const int LOGON32_LOGON_INTERACTIVE = 2;
        private const int LOGON32_PROVIDER_DEFAULT = 0;

        // ------------------------------------------------------------------
        #endregion

        #region Private member.
        // ------------------------------------------------------------------

        /// <summary>
        /// Does the actual impersonation.
        /// </summary>
        /// <param name="userName">The name of the user to act as.</param>
        /// <param name="domainName">The domain name of the user to act as.</param>
        /// <param name="password">The password of the user to act as.</param>
        private void ImpersonateValidUser(
            string userName,
            string domain,
            string password)
        {
            WindowsIdentity tempWindowsIdentity = null;
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;

            try
            {
                if (RevertToSelf())
                {
                    if (LogonUser(
                        userName,
                        domain,
                        password,
                        LOGON32_LOGON_INTERACTIVE,
                        LOGON32_PROVIDER_DEFAULT,
                        ref token) != 0)
                    {
                        if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                        {
                            tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                            impersonationContext = tempWindowsIdentity.Impersonate();
                        }
                        else
                        {
                            throw new Win32Exception(Marshal.GetLastWin32Error());
                        }
                    }
                    else
                    {
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                    }
                }
                else
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }
            }
            finally
            {
                if (token != IntPtr.Zero)
                {
                    CloseHandle(token);
                }
                if (tokenDuplicate != IntPtr.Zero)
                {
                    CloseHandle(tokenDuplicate);
                }
            }
        }

        /// <summary>
        /// Reverts the impersonation.
        /// </summary>
        private void UndoImpersonation()
        {
            if (impersonationContext != null)
            {
                impersonationContext.Undo();
            }
        }

        private WindowsImpersonationContext impersonationContext = null;

        // ------------------------------------------------------------------
        #endregion
    }

    /////////////////////////////////////////////////////////////////////////
}
