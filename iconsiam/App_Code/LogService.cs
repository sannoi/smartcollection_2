﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;


namespace iconsiam.App_Code
{
    public class LogService
    {

        public void Log(string text, string filename)
        {
            string Exepath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string Directory_tmp = System.IO.Path.GetDirectoryName(Exepath);



            if (Directory.Exists(ConfigurationManager.AppSettings["logPath"]) == false)
            {
                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["logPath"]);
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(ConfigurationManager.AppSettings["logPath"] + "" +
            DateTime.Now.ToString("yyyyMMdd") + filename + ".txt", true))
            {
                file.WriteLine(text);
            }




        }

        private static DataTable GetPath_Log()
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from tblsetting_path_log ;  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


    }

}