﻿using iconsiam.App_Code;
using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class daily_rep : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        daily_repDLL Serv = new daily_repDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtstartdate.Text = DateTime.Now.ToString("01/MM/yyyy", EngCI);
                    txtenddate.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", EngCI);

                    bind_default();
                    bind_rep();

                }
            }
        }

        protected void bind_default()
        {
            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');

            var building = Serv.GetBuilding(comcode_);
            if (building.Rows.Count != 0)
            {
                ddlbuilding.DataTextField = "BuildingNameEN";
                ddlbuilding.DataValueField = "BuildingCode";
                ddlbuilding.DataSource = building;
                ddlbuilding.DataBind();
            }
            else
            {
                ddlbuilding.DataSource = null;
                ddlbuilding.DataBind();

            }
            ddlbuilding.Items.Insert(0, new ListItem("Select Building", ""));

            var cl = Serv.GetCategory_leasing(comcode_);
            if (cl.Rows.Count != 0)
            {
                ddlcatagorLeasing.DataTextField = "CategoryleasingNameEN";
                ddlcatagorLeasing.DataValueField = "id";
                ddlcatagorLeasing.DataSource = cl;
                ddlcatagorLeasing.DataBind();
            }
            else
            {
                ddlcatagorLeasing.DataSource = null;
                ddlcatagorLeasing.DataBind();

            }
            ddlcatagorLeasing.Items.Insert(0, new ListItem("Select Category Leasing", ""));


            var ct = Serv.Getcontract_type(comcode_);
            if (ct.Rows.Count != 0)
            {
                ddlcontract_type.DataTextField = "ContractTypeNameEN";
                ddlcontract_type.DataValueField = "id";
                ddlcontract_type.DataSource = ct;
                ddlcontract_type.DataBind();
            }
            else
            {
                ddlcontract_type.DataSource = null;
                ddlcontract_type.DataBind();

            }
            ddlcontract_type.Items.Insert(0, new ListItem("Select Contract Type", ""));

            var g = Serv.Getgroup_location(comcode_);
            if (g.Rows.Count != 0)
            {
                ddlgroup_loca.DataTextField = "GrouplocationNameEN";
                ddlgroup_loca.DataValueField = "id";
                ddlgroup_loca.DataSource = g;
                ddlgroup_loca.DataBind();
            }
            else
            {
                ddlgroup_loca.DataSource = null;
                ddlgroup_loca.DataBind();

            }
            ddlgroup_loca.Items.Insert(0, new ListItem("Select Group Location", ""));

            var ig = Serv.GetIndustry_group(comcode_);
            if (ig.Rows.Count != 0)
            {
                ddlindustrygroup.DataTextField = "IndustryGroupNameEN";
                ddlindustrygroup.DataValueField = "id";
                ddlindustrygroup.DataSource = ig;
                ddlindustrygroup.DataBind();
            }
            else
            {
                ddlindustrygroup.DataSource = null;
                ddlindustrygroup.DataBind();

            }
            ddlindustrygroup.Items.Insert(0, new ListItem("Select Industry Group", ""));

            var r = Serv.GetRoomType();
            if (r.Rows.Count != 0)
            {
                ddlroomtype.DataTextField = "UsageTypeName";
                ddlroomtype.DataValueField = "UsageTypeName";
                ddlroomtype.DataSource = r;
                ddlroomtype.DataBind();
            }
            else
            {
                ddlroomtype.DataSource = null;
                ddlroomtype.DataBind();

            }
            ddlroomtype.Items.Insert(0, new ListItem("Select Room type", ""));


            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }
            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;
            //}

            if (HttpContext.Current.Session["s_user_group"].ToString() == "ae")
            {
                var comp = Serv.GetCompany_ae(HttpContext.Current.Session["s_user_for_shop"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameEN";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
                ddlcompany.Items.Insert(0, new ListItem("Company", ""));
            }
            else
            {
                if (HttpContext.Current.Session["s_com_code"] != null)
                {
                    var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                    if (comp.Rows.Count != 0)
                    {
                        ddlcompany.DataTextField = "CompanyNameTH";
                        ddlcompany.DataValueField = "CompanyCode";
                        ddlcompany.DataSource = comp;
                        ddlcompany.DataBind();
                    }
                    else
                    {
                        ddlcompany.DataSource = null;
                        ddlcompany.DataBind();

                    }
                }

                ddlcompany.Items.Insert(0, new ListItem("Company", ""));
            }

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("All", "all"));
            ddlstatus.Items.Insert(1, new ListItem("Confirm", "y"));
            ddlstatus.Items.Insert(2, new ListItem("Unconfirmed", "n"));

        }

        protected void bind_rep()
        {
            string[] customercode = txtcustomercode.Text.Split(',');
            string[] floor = txtfloor.Text.Split(',');
            string[] shopgroup = txtshopgroup.Text.Split(',');
            string[] shopname = txtshopname.Text.Split(',');

            DataTable dt = new DataTable();


            if (CheckBox1.Checked == true)
            {
                dt.Columns.Add("d1");
                dt.Columns.Add("d2");
                dt.Columns.Add("d3");
                dt.Columns.Add("d4");
                dt.Columns.Add("d5");
                dt.Columns.Add("d6");
                dt.Columns.Add("d7");
                dt.Columns.Add("d8");
                dt.Columns.Add("d9");
                dt.Columns.Add("d10");
                dt.Columns.Add("d11");
                dt.Columns.Add("d12");
                dt.Columns.Add("d13");
                dt.Columns.Add("d14");
                dt.Columns.Add("d15");
                dt.Columns.Add("d16");
                dt.Columns.Add("d17");
                dt.Columns.Add("d18");
                dt.Columns.Add("d19");
                dt.Columns.Add("d20");
                dt.Columns.Add("d21");
                dt.Columns.Add("d22");
                dt.Columns.Add("d23");
                dt.Columns.Add("d24");

                dt.Columns.Add("d33");
                dt.Columns.Add("d34");
            }
            else
            {
                dt.Columns.Add("d1");
                dt.Columns.Add("d2");
                dt.Columns.Add("d3");
                dt.Columns.Add("d4");
                dt.Columns.Add("d5");
                dt.Columns.Add("d6");
                dt.Columns.Add("d7");
                dt.Columns.Add("d8");
                dt.Columns.Add("d9");
                dt.Columns.Add("d10");
                dt.Columns.Add("d11");
                dt.Columns.Add("d12");
                dt.Columns.Add("d13");
                dt.Columns.Add("d14");
                dt.Columns.Add("d15");
                dt.Columns.Add("d16");
                dt.Columns.Add("d17");
                dt.Columns.Add("d18");
                dt.Columns.Add("d19");
                dt.Columns.Add("d20");
                dt.Columns.Add("d21");
                dt.Columns.Add("d22");

                dt.Columns.Add("d33");
                dt.Columns.Add("d34");
            }


            dateconvert cc = new dateconvert();
            DataTable rep = new DataTable();

            string Company = "";

            if (ddlcompany.SelectedValue == "")
            {
                Company = HttpContext.Current.Session["s_com_code"].ToString();
            }
            else
            {
                Company = "'" + ddlcompany.SelectedValue + "'";
            }

            if (HttpContext.Current.Session["s_user_group"].ToString() == "ae")
            {
                rep = Serv.GetRep1_ae(cc.con_date(txtstartdate.Text), cc.con_date(txtenddate.Text), shopname, Company, floor, customercode, shopgroup, ddlbuilding.SelectedValue,
                ddlroomtype.SelectedValue, ddlcontract_type.SelectedValue, ddlcatagorLeasing.SelectedValue, ddlgroup_loca.SelectedValue,
                ddlindustrygroup.SelectedValue, HttpContext.Current.Session["s_user_for_shop"].ToString(), ddlstatus.SelectedValue);
            }
            else
            {

                rep = Serv.GetRep1(cc.con_date(txtstartdate.Text), cc.con_date(txtenddate.Text), shopname, Company, floor, customercode, shopgroup, ddlbuilding.SelectedValue,
                ddlroomtype.SelectedValue, ddlcontract_type.SelectedValue, ddlcatagorLeasing.SelectedValue, ddlgroup_loca.SelectedValue,
                ddlindustrygroup.SelectedValue, HttpContext.Current.Session["s_user_for_shop"].ToString(), ddlstatus.SelectedValue);
            }



            if (rep.Rows.Count != 0)
            {
                DataRow row1 = dt.NewRow();
                string ddd = "";
                decimal xxx = 0;
                decimal yyy = 0;

                for (int i = 0; i < rep.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        ddd = Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                    }

                    if (ddd != Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI))
                    {
                        if (CheckBox1.Checked == true)
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = "Total Sale Amount";
                            row1["d2"] = "";
                            row1["d3"] = "";
                            row1["d4"] = "";
                            row1["d5"] = "";
                            row1["d6"] = "";
                            row1["d7"] = "";
                            row1["d8"] = "";
                            row1["d9"] = "";
                            row1["d10"] = "";
                            row1["d11"] = "";
                            row1["d12"] = "";
                            row1["d13"] = Convert.ToDecimal(xxx).ToString("#,##0.00");
                            row1["d14"] = "";
                            row1["d15"] = "";
                            row1["d16"] = "";
                            row1["d17"] = "";
                            row1["d18"] = "";
                            row1["d19"] = "";
                            row1["d20"] = "";
                            row1["d21"] = "";
                            row1["d22"] = "";
                            row1["d23"] = Convert.ToDecimal(yyy).ToString("#,##0.00");
                            row1["d24"] = "";

                            row1["d33"] = "";
                            row1["d34"] = "";

                            dt.Rows.Add(row1);
                        }
                        else
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = "Total Sale Amount";
                            row1["d2"] = "";
                            row1["d3"] = "";
                            row1["d4"] = "";
                            row1["d5"] = "";
                            row1["d6"] = "";
                            row1["d7"] = "";
                            row1["d8"] = "";
                            row1["d9"] = "";
                            row1["d10"] = "";
                            row1["d11"] = "";
                            row1["d12"] = "";
                            row1["d13"] = Convert.ToDecimal(xxx).ToString("#,##0.00");
                            row1["d14"] = "";
                            row1["d15"] = "";
                            row1["d16"] = "";
                            row1["d17"] = "";
                            row1["d18"] = "";
                            row1["d19"] = "";
                            row1["d20"] = "";
                            row1["d21"] = "";
                            row1["d22"] = "";

                            row1["d33"] = "";
                            row1["d34"] = "";
                            dt.Rows.Add(row1);
                        }



                        xxx = 0;
                        yyy = 0;
                        ddd = Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);


                    }

                    if (rep.Rows[i]["smart_record_keyin_type"].ToString() == "prod_sale")
                    {
                        //decimal x = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString());

                        if (CheckBox1.Checked == true)
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = rep.Rows[i]["BusinessPartnerCode"].ToString();

                            row1["d33"] = rep.Rows[i]["ContractNumber"].ToString();
                            row1["d34"] = rep.Rows[i]["smart_room_no"].ToString();

                            row1["d3"] = rep.Rows[i]["ShopName"].ToString();
                            row1["d4"] = rep.Rows[i]["BusinessPartnerName"].ToString();
                            row1["d5"] = Convert.ToDecimal(rep.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            row1["d6"] = Convert.ToDecimal(rep.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            row1["d7"] = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            row1["d8"] = "0.00";// rep.Rows[i][""].ToString();
                            row1["d9"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d10"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d11"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d12"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d13"] = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()).ToString("#,##0.00");

                            xxx = xxx + Convert.ToDecimal(rep.Rows[i]["exvat"].ToString());

                            row1["d14"] = rep.Rows[i]["ShopGroupNameTH"].ToString();
                            row1["d15"] = rep.Rows[i]["CompanyNameTH"].ToString();
                            row1["d16"] = rep.Rows[i]["BuildingNameEN"].ToString();
                            row1["d17"] = rep.Rows[i]["smart_floor"].ToString();
                            row1["d18"] = rep.Rows[i]["smart_usage_name"].ToString();
                            row1["d19"] = rep.Rows[i]["ContractTypeNameTH"].ToString();
                            row1["d20"] = rep.Rows[i]["CategoryleasingNameTH"].ToString();
                            row1["d21"] = rep.Rows[i]["IndustryGroupNameTH"].ToString();
                            row1["d22"] = rep.Rows[i]["GrouplocationNameTH"].ToString();
                            row1["d24"] = rep.Rows[i]["smart_sqm"].ToString();

                            decimal devi = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()) / Convert.ToDecimal(rep.Rows[i]["smart_sqm"].ToString());
                            row1["d23"] = Convert.ToDecimal(devi).ToString("#,##0.00");
                            yyy = yyy + devi;

                            dt.Rows.Add(row1);
                        }
                        else
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = rep.Rows[i]["BusinessPartnerCode"].ToString();

                            row1["d33"] = rep.Rows[i]["ContractNumber"].ToString();
                            row1["d34"] = rep.Rows[i]["smart_room_no"].ToString();

                            row1["d3"] = rep.Rows[i]["ShopName"].ToString();
                            row1["d4"] = rep.Rows[i]["BusinessPartnerName"].ToString();
                            row1["d5"] = Convert.ToDecimal(rep.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            row1["d6"] = Convert.ToDecimal(rep.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            row1["d7"] = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            row1["d8"] = "0.00";// rep.Rows[i][""].ToString();
                            row1["d9"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d10"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d11"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d12"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d13"] = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()).ToString("#,##0.00");

                            xxx = xxx + Convert.ToDecimal(rep.Rows[i]["exvat"].ToString());

                            row1["d14"] = rep.Rows[i]["ShopGroupNameTH"].ToString();
                            row1["d15"] = rep.Rows[i]["CompanyNameTH"].ToString();
                            row1["d16"] = rep.Rows[i]["BuildingNameEN"].ToString();
                            row1["d17"] = rep.Rows[i]["smart_floor"].ToString();
                            row1["d18"] = rep.Rows[i]["smart_usage_name"].ToString();
                            row1["d19"] = rep.Rows[i]["ContractTypeNameTH"].ToString();
                            row1["d20"] = rep.Rows[i]["CategoryleasingNameTH"].ToString();
                            row1["d21"] = rep.Rows[i]["IndustryGroupNameTH"].ToString();
                            row1["d22"] = rep.Rows[i]["GrouplocationNameTH"].ToString();


                            dt.Rows.Add(row1);
                        }

                    }
                    else if (rep.Rows[i]["smart_record_keyin_type"].ToString() == "prod_service")
                    {
                        decimal x = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()) + Convert.ToDecimal(rep.Rows[i]["exvat2"].ToString());

                        if (CheckBox1.Checked == true)
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = rep.Rows[i]["BusinessPartnerCode"].ToString();

                            row1["d33"] = rep.Rows[i]["ContractNumber"].ToString();
                            row1["d34"] = rep.Rows[i]["smart_room_no"].ToString();

                            row1["d3"] = rep.Rows[i]["ShopName"].ToString();
                            row1["d4"] = rep.Rows[i]["BusinessPartnerName"].ToString();
                            row1["d5"] = Convert.ToDecimal(rep.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            row1["d6"] = Convert.ToDecimal(rep.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            row1["d7"] = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            row1["d8"] = Convert.ToDecimal(rep.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            row1["d9"] = Convert.ToDecimal(rep.Rows[i]["vat2"].ToString()).ToString("#,##0.00");
                            row1["d10"] = Convert.ToDecimal(rep.Rows[i]["exvat2"].ToString()).ToString("#,##0.00");
                            row1["d11"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d12"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d13"] = Convert.ToDecimal(x).ToString("#,##0.00");

                            xxx = xxx + x;

                            row1["d14"] = rep.Rows[i]["ShopGroupNameTH"].ToString();
                            row1["d15"] = rep.Rows[i]["CompanyNameTH"].ToString();
                            row1["d16"] = rep.Rows[i]["BuildingNameEN"].ToString();
                            row1["d17"] = rep.Rows[i]["smart_floor"].ToString();
                            row1["d18"] = rep.Rows[i]["smart_usage_name"].ToString();
                            row1["d19"] = rep.Rows[i]["ContractTypeNameTH"].ToString();
                            row1["d20"] = rep.Rows[i]["CategoryleasingNameTH"].ToString();
                            row1["d21"] = rep.Rows[i]["IndustryGroupNameTH"].ToString();
                            row1["d22"] = rep.Rows[i]["GrouplocationNameTH"].ToString();

                            decimal devi = Convert.ToDecimal(x) / Convert.ToDecimal(rep.Rows[i]["smart_sqm"].ToString());
                            row1["d23"] = Convert.ToDecimal(devi).ToString("#,##0.00");
                            row1["d24"] = rep.Rows[i]["smart_sqm"].ToString();
                            yyy = yyy + devi;

                            dt.Rows.Add(row1);
                        }
                        else
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = rep.Rows[i]["BusinessPartnerCode"].ToString();

                            row1["d33"] = rep.Rows[i]["ContractNumber"].ToString();
                            row1["d34"] = rep.Rows[i]["smart_room_no"].ToString();

                            row1["d3"] = rep.Rows[i]["ShopName"].ToString();
                            row1["d4"] = rep.Rows[i]["BusinessPartnerName"].ToString();
                            row1["d5"] = Convert.ToDecimal(rep.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            row1["d6"] = Convert.ToDecimal(rep.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            row1["d7"] = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            row1["d8"] = Convert.ToDecimal(rep.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            row1["d9"] = Convert.ToDecimal(rep.Rows[i]["vat2"].ToString()).ToString("#,##0.00");
                            row1["d10"] = Convert.ToDecimal(rep.Rows[i]["exvat2"].ToString()).ToString("#,##0.00");
                            row1["d11"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d12"] = "0.00";//rep.Rows[i][""].ToString();
                            row1["d13"] = Convert.ToDecimal(x).ToString("#,##0.00");

                            xxx = xxx + x;

                            row1["d14"] = rep.Rows[i]["ShopGroupNameTH"].ToString();
                            row1["d15"] = rep.Rows[i]["CompanyNameTH"].ToString();
                            row1["d16"] = rep.Rows[i]["BuildingNameEN"].ToString();
                            row1["d17"] = rep.Rows[i]["smart_floor"].ToString();
                            row1["d18"] = rep.Rows[i]["smart_usage_name"].ToString();
                            row1["d19"] = rep.Rows[i]["ContractTypeNameTH"].ToString();
                            row1["d20"] = rep.Rows[i]["CategoryleasingNameTH"].ToString();
                            row1["d21"] = rep.Rows[i]["IndustryGroupNameTH"].ToString();
                            row1["d22"] = rep.Rows[i]["GrouplocationNameTH"].ToString();

                            dt.Rows.Add(row1);
                        }

                    }
                    else
                    {
                        if (CheckBox1.Checked == true)
                        {
                            decimal x = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()) - Convert.ToDecimal(rep.Rows[i]["service_serCharge_amount"].ToString());
                            //decimal y = x - (x * 7) / 107;

                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = rep.Rows[i]["BusinessPartnerCode"].ToString();

                            row1["d33"] = rep.Rows[i]["ContractNumber"].ToString();
                            row1["d34"] = rep.Rows[i]["smart_room_no"].ToString();

                            row1["d3"] = rep.Rows[i]["ShopName"].ToString();
                            row1["d4"] = rep.Rows[i]["BusinessPartnerName"].ToString();
                            row1["d5"] = Convert.ToDecimal(rep.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            row1["d6"] = Convert.ToDecimal(rep.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            row1["d7"] = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            row1["d8"] = "0.00";
                            row1["d9"] = "0.00";
                            row1["d10"] = "0.00";
                            row1["d11"] = Convert.ToDecimal(rep.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            row1["d12"] = Convert.ToDecimal(x).ToString("#,##0.00");
                            row1["d13"] = Convert.ToDecimal(x).ToString("#,##0.00");

                            xxx = xxx + x;

                            row1["d14"] = rep.Rows[i]["ShopGroupNameTH"].ToString();
                            row1["d15"] = rep.Rows[i]["CompanyNameTH"].ToString();
                            row1["d16"] = rep.Rows[i]["BuildingNameEN"].ToString();
                            row1["d17"] = rep.Rows[i]["smart_floor"].ToString();
                            row1["d18"] = rep.Rows[i]["smart_usage_name"].ToString();
                            row1["d19"] = rep.Rows[i]["ContractTypeNameTH"].ToString();
                            row1["d20"] = rep.Rows[i]["CategoryleasingNameTH"].ToString();
                            row1["d21"] = rep.Rows[i]["IndustryGroupNameTH"].ToString();
                            row1["d22"] = rep.Rows[i]["GrouplocationNameTH"].ToString();

                            decimal devi = Convert.ToDecimal(x) / Convert.ToDecimal(rep.Rows[i]["smart_sqm"].ToString());
                            row1["d23"] = Convert.ToDecimal(devi).ToString("#,##0.00");
                            row1["d24"] = rep.Rows[i]["smart_sqm"].ToString();
                            yyy = yyy + devi;

                            dt.Rows.Add(row1);
                        }
                        else
                        {
                            decimal x = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()) - Convert.ToDecimal(rep.Rows[i]["service_serCharge_amount"].ToString());
                            //decimal y = x - (x * 7) / 107;

                            row1 = dt.NewRow();
                            row1["d1"] = Convert.ToDateTime(rep.Rows[i]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                            row1["d2"] = rep.Rows[i]["BusinessPartnerCode"].ToString();

                            row1["d33"] = rep.Rows[i]["ContractNumber"].ToString();
                            row1["d34"] = rep.Rows[i]["smart_room_no"].ToString();

                            row1["d3"] = rep.Rows[i]["ShopName"].ToString();
                            row1["d4"] = rep.Rows[i]["BusinessPartnerName"].ToString();
                            row1["d5"] = Convert.ToDecimal(rep.Rows[i]["product_amount"].ToString()).ToString("#,##0.00");
                            row1["d6"] = Convert.ToDecimal(rep.Rows[i]["vat"].ToString()).ToString("#,##0.00");
                            row1["d7"] = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            row1["d8"] = "0.00";
                            row1["d9"] = "0.00";
                            row1["d10"] = "0.00";
                            row1["d11"] = Convert.ToDecimal(rep.Rows[i]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            row1["d12"] = Convert.ToDecimal(x).ToString("#,##0.00");
                            row1["d13"] = Convert.ToDecimal(x).ToString("#,##0.00");

                            xxx = xxx + x;
                            //xxx = xxx + Convert.ToDecimal(rep.Rows[i]["exvat"].ToString());

                            row1["d14"] = rep.Rows[i]["ShopGroupNameTH"].ToString();
                            row1["d15"] = rep.Rows[i]["CompanyNameTH"].ToString();
                            row1["d16"] = rep.Rows[i]["BuildingNameEN"].ToString();
                            row1["d17"] = rep.Rows[i]["smart_floor"].ToString();
                            row1["d18"] = rep.Rows[i]["smart_usage_name"].ToString();
                            row1["d19"] = rep.Rows[i]["ContractTypeNameTH"].ToString();
                            row1["d20"] = rep.Rows[i]["CategoryleasingNameTH"].ToString();
                            row1["d21"] = rep.Rows[i]["IndustryGroupNameTH"].ToString();
                            row1["d22"] = rep.Rows[i]["GrouplocationNameTH"].ToString();


                            dt.Rows.Add(row1);
                        }

                    }

                    if (i == rep.Rows.Count - 1)
                    {
                        if (CheckBox1.Checked == true)
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = "Total Sale Amount";
                            row1["d2"] = "";
                            row1["d3"] = "";
                            row1["d4"] = "";
                            row1["d5"] = "";
                            row1["d6"] = "";
                            row1["d7"] = "";
                            row1["d8"] = "";
                            row1["d9"] = "";
                            row1["d10"] = "";
                            row1["d11"] = "";
                            row1["d12"] = "";
                            row1["d13"] = Convert.ToDecimal(xxx).ToString("#,##0.00");
                            row1["d14"] = "";
                            row1["d15"] = "";
                            row1["d16"] = "";
                            row1["d17"] = "";
                            row1["d18"] = "";
                            row1["d19"] = "";
                            row1["d20"] = "";
                            row1["d21"] = "";
                            row1["d22"] = "";
                            row1["d23"] = Convert.ToDecimal(yyy).ToString("#,##0.00");
                            row1["d24"] = "";

                            row1["d33"] = "";
                            row1["d34"] = "";

                            dt.Rows.Add(row1);
                        }
                        else
                        {
                            row1 = dt.NewRow();
                            row1["d1"] = "Total Sale Amount";
                            row1["d2"] = "";
                            row1["d3"] = "";
                            row1["d4"] = "";
                            row1["d5"] = "";
                            row1["d6"] = "";
                            row1["d7"] = "";
                            row1["d8"] = "";
                            row1["d9"] = "";
                            row1["d10"] = "";
                            row1["d11"] = "";
                            row1["d12"] = "";
                            row1["d13"] = Convert.ToDecimal(xxx).ToString("#,##0.00");
                            row1["d14"] = "";
                            row1["d15"] = "";
                            row1["d16"] = "";
                            row1["d17"] = "";
                            row1["d18"] = "";
                            row1["d19"] = "";
                            row1["d20"] = "";
                            row1["d21"] = "";
                            row1["d22"] = "";

                            row1["d33"] = "";
                            row1["d34"] = "";

                            dt.Rows.Add(row1);
                        }



                    }



                }

                if (dt.Rows.Count != 0)
                {
                    if (CheckBox1.Checked == true)
                    {
                        GridView_List2.DataSource = dt;
                        GridView_List2.DataBind();

                        GridView2.DataSource = dt;
                        GridView2.DataBind();

                        GridView_List.DataSource = null;
                        GridView_List.DataBind();

                        GridView1.DataSource = null;
                        GridView1.DataBind();

                    }
                    else
                    {
                        GridView_List.DataSource = dt;
                        GridView_List.DataBind();

                        GridView1.DataSource = dt;
                        GridView1.DataBind();

                        GridView_List2.DataSource = null;
                        GridView_List2.DataBind();

                        GridView2.DataSource = null;
                        GridView2.DataBind();
                    }

                }
                else
                {
                    GridView_List2.DataSource = null;
                    GridView_List2.DataBind();

                    GridView2.DataSource = null;
                    GridView2.DataBind();

                    GridView_List.DataSource = null;
                    GridView_List.DataBind();

                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
            else
            {
                GridView_List2.DataSource = null;
                GridView_List2.DataBind();

                GridView2.DataSource = null;
                GridView2.DataBind();

                GridView_List.DataSource = null;
                GridView_List.DataBind();

                GridView1.DataSource = null;
                GridView1.DataBind();
            }
            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnexport_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count != 0)
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename = daily_report" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                System.IO.StringWriter sw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);

                GridView1.RenderControl(hw);

                string headerTable = @"<Table>" +
                          "<tr align='center'><td colspan='22'>Tenant Sales Data Collection</td></tr>" +
                          "<tr align='center'><td colspan='22'>Daily Sales Report " + txtstartdate.Text + " - " + txtenddate.Text + "</td></tr>" +
                          "<tr align='center'><td colspan='22'></td></tr>" +
                          "</Table>";


                Response.Write(headerTable);
                Response.Write(sw.ToString());
                Response.End();


            }

            else if (GridView2.Rows.Count != 0)
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename = daily_report" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                System.IO.StringWriter sw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);

                GridView2.RenderControl(hw);

                string headerTable = @"<Table>" +
                          "<tr align='center'><td colspan='24'>Tenant Sales Data Collection</td></tr>" +
                          "<tr align='center'><td colspan='24'>Daily Sales Report " + txtstartdate.Text + " - " + txtenddate.Text + "</td></tr>" +
                          "<tr align='center'><td colspan='24'></td></tr>" +
                          "</Table>";


                Response.Write(headerTable);
                Response.Write(sw.ToString());
                Response.End();
            }
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/daily_rep.aspx");
        }
    }
}