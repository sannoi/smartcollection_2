﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class transaction_tenant_detail2 : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        record_transactionDLL Serv = new record_transactionDLL();
        sentmail sentMail = new sentmail();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {

                    txtproduct_amount.Attributes.Add("onkeypress", "return isNumberKey2AndEnter(event,'" + txtproduct_amount.ClientID + "')");
                    txtservice_serCharge_amount.Attributes.Add("onkeypress", "return isNumberKey2AndEnter(event,'" + txtservice_serCharge_amount.ClientID + "')");
                    txtservice_serCharge_amount2.Attributes.Add("onkeypress", "return isNumberKey2AndEnter(event,'" + txtservice_serCharge_amount2.ClientID + "')");

                    //bind_default();
                    bind_rep();
                }
            }
        }

        //protected void bind_default()
        //{
        //    ///// Binding Reason /////


        //    ddlreason.Items.Insert(0, new ListItem("", ""));
        //    ddlreason.Items.Insert(1, new ListItem("ร้านปิด", "ร้านปิด"));
        //    ddlreason.Items.Insert(2, new ListItem("วันหยุดประจำปี", "วันหยุดประจำปี"));

        //}

        protected void bind_rep()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]) && !string.IsNullOrEmpty(Request.QueryString["cont"])
                 && !string.IsNullOrEmpty(Request.QueryString["type"]))
            {
                var contract = Serv.GetContract(Request.QueryString["cont"].ToString());
                if (contract.Rows.Count != 0)
                {
                    lbroomnum.Text = "ROOM Number  : " + contract.Rows[0]["smart_room_no"].ToString();
                    lbshopname.Text = "Shop Name : " + contract.Rows[0]["ShopName"].ToString();

                    if (contract.Rows[0]["smart_record_keyin_type"].ToString() == "prod_sale")
                    {
                        txtservice_serCharge_amount.Visible = false;
                        txtservice_serCharge_amount2.Visible = false;
                    }
                    else if (contract.Rows[0]["smart_record_keyin_type"].ToString() == "prod_service")
                    {
                        lb1.Visible = true;
                        lb1.Text = "ยอดค่าบริการ";
                        txtservice_serCharge_amount.Visible = true;
                        txtservice_serCharge_amount2.Visible = false;
                    }
                    else
                    {
                        lb1.Visible = true;
                        lb1.Text = "ยอดค่า Service Charge";
                        txtservice_serCharge_amount.Visible = false;
                        txtservice_serCharge_amount2.Visible = true;
                    }

                    var trans = Serv.GetTransaction_detail(Request.QueryString["id"].ToString(), Request.QueryString["cont"].ToString());
                    if (trans.Rows.Count != 0)
                    {
                        txtdate.Text = Convert.ToDateTime(trans.Rows[0]["record_date"].ToString()).ToString("dd/MM/yyyy", EngCI);
                        txtproduct_amount.Text = Convert.ToDecimal(trans.Rows[0]["product_amount"].ToString()).ToString("#,##0.00");

                        hdd_current_1.Value = trans.Rows[0]["product_amount"].ToString();

                        if (trans.Rows[0]["no_type"].ToString() != "")
                        {
                            Panel3.Visible = true;

                            if (trans.Rows[0]["no_type"].ToString() == "close")
                            {
                                ddlreason.Text = "ร้านปิด";
                            }
                            else if (trans.Rows[0]["no_type"].ToString() == "nobill")
                            {
                                ddlreason.Text = "No Bill";
                            }
                            else if (trans.Rows[0]["no_type"].ToString() == "other")
                            {
                                ddlreason.Text = "Other";
                            }


                            txtdetal.Text = trans.Rows[0]["remark"].ToString();
                        }
                        else
                        {
                            Panel3.Visible = false;
                        }


                        if (trans.Rows[0]["flag_confirm"].ToString() == "y")
                        {
                            Panel2.Visible = false;
                        }
                        else
                        {
                            Panel2.Visible = true;

                        }

                        if (Request.QueryString["type"].ToString() == "e")
                        {
                            txtremark.Visible = true;
                            btnsave.Visible = true;
                            Panel7.Visible = true;
                            Panel8.Visible = false;
                        }
                        else if (Request.QueryString["type"].ToString() == "n")
                        {
                            Panel7.Visible = false;
                            btnsave.Visible = false;
                            Panel2.Visible = false;
                            Panel8.Visible = true;
                            txtremark_edit.Text = trans.Rows[0]["adjust_remark"].ToString();
                        }
                        else
                        {
                            txtremark.Visible = false;
                            btnsave.Visible = false;

                        }

                        txtremark.Text = "";


                        if (txtservice_serCharge_amount.Visible == true)
                        {
                            txtservice_serCharge_amount.Text = Convert.ToDecimal(trans.Rows[0]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            hdd_current_2.Value = trans.Rows[0]["service_serCharge_amount"].ToString();
                        }
                        else if (txtservice_serCharge_amount2.Visible == true)
                        {
                            txtservice_serCharge_amount2.Text = Convert.ToDecimal(trans.Rows[0]["service_serCharge_amount"].ToString()).ToString("#,##0.00");
                            hdd_current_2.Value = trans.Rows[0]["service_serCharge_amount"].ToString();
                        }
                        else
                        {
                            hdd_current_2.Value = "0";
                        }
                    }

                    var trans_img = Serv.GetTransaction_detail_img(Request.QueryString["id"].ToString());
                    if (trans_img.Rows.Count != 0)
                    {
                        Panel1.Visible = true;

                        for (int i = 0; i < trans_img.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                Image1.ImageUrl = trans_img.Rows[i]["img"].ToString();
                                //btndel1.Visible = true;
                                hdd_img1_file.Value = trans_img.Rows[i]["filename"].ToString();
                                hdd_img1.Value = trans_img.Rows[i]["id"].ToString();
                                Image1.Visible = true;
                                LinkButton1.Visible = true;
                                LinkButton1.Text = trans_img.Rows[i]["filename_display"].ToString();
                            }
                            else if (i == 1)
                            {
                                Image2.ImageUrl = trans_img.Rows[i]["img"].ToString();
                                //btndel2.Visible = true;
                                hdd_img2_file.Value = trans_img.Rows[i]["filename"].ToString();
                                hdd_img2.Value = trans_img.Rows[i]["id"].ToString();
                                Image2.Visible = true;
                                LinkButton2.Visible = true;
                                LinkButton2.Text = trans_img.Rows[i]["filename_display"].ToString();
                            }
                            else if (i == 2)
                            {
                                Image3.ImageUrl = trans_img.Rows[i]["img"].ToString();
                                //btndel2.Visible = true;
                                hdd_img3_file.Value = trans_img.Rows[i]["filename"].ToString();
                                hdd_img3.Value = trans_img.Rows[i]["id"].ToString();
                                Image3.Visible = true;
                                LinkButton3.Visible = true;
                                LinkButton3.Text = trans_img.Rows[i]["filename_display"].ToString();
                            }
                            else if (i == 3)
                            {
                                Image4.ImageUrl = trans_img.Rows[i]["img"].ToString();
                                //btndel2.Visible = true;
                                hdd_img4_file.Value = trans_img.Rows[i]["filename"].ToString();
                                hdd_img4.Value = trans_img.Rows[i]["id"].ToString();
                                Image4.Visible = true;
                                LinkButton4.Visible = true;
                                LinkButton4.Text = trans_img.Rows[i]["filename_display"].ToString();
                            }
                            else if (i == 4)
                            {
                                Image5.ImageUrl = trans_img.Rows[i]["img"].ToString();
                                //btndel2.Visible = true;
                                hdd_img5_file.Value = trans_img.Rows[i]["filename"].ToString();
                                hdd_img5.Value = trans_img.Rows[i]["id"].ToString();
                                Image5.Visible = true;
                                LinkButton5.Visible = true;
                                LinkButton5.Text = trans_img.Rows[i]["filename_display"].ToString();
                            }
                            else if (i == 5)
                            {
                                Image6.ImageUrl = trans_img.Rows[i]["img"].ToString();
                                //btndel2.Visible = true;
                                hdd_img6_file.Value = trans_img.Rows[i]["filename"].ToString();
                                hdd_img6.Value = trans_img.Rows[i]["id"].ToString();
                                Image6.Visible = true;
                                LinkButton6.Visible = true;
                                LinkButton6.Text = trans_img.Rows[i]["filename_display"].ToString();
                            }
                        }
                    }
                    else
                    {
                        Panel1.Visible = false;
                    }

                    var pic_edit = Serv.GetTransaction_detail_edit_img(Request.QueryString["id"].ToString());
                    if (pic_edit.Rows.Count != 0)
                    {
                        Panel6.Visible = true;
                        for (int i = 0; i < pic_edit.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                Image7.ImageUrl = pic_edit.Rows[i]["img"].ToString();
                                //btndel1.Visible = true;
                                hdd_img7_file.Value = pic_edit.Rows[i]["filename"].ToString();
                                hdd_img7.Value = pic_edit.Rows[i]["id"].ToString();
                                Image7.Visible = true;
                                LinkButton7.Visible = true;
                                LinkButton7.Text = pic_edit.Rows[i]["filename_display"].ToString();
                            }
                            else if (i == 1)
                            {
                                Image8.ImageUrl = pic_edit.Rows[i]["img"].ToString();
                                //btndel2.Visible = true;
                                hdd_img8_file.Value = pic_edit.Rows[i]["filename"].ToString();
                                hdd_img8.Value = pic_edit.Rows[i]["id"].ToString();
                                Image8.Visible = true;
                                LinkButton8.Visible = true;
                                LinkButton8.Text = pic_edit.Rows[i]["filename_display"].ToString();
                            }
                        }
                    }
                    else
                    {
                        Panel6.Visible = false;
                        Panel7.Visible = false;
                    }

                    //if (trans.Rows[0]["no_type"].ToString() != "")
                    //{
                    //    ddlreason.SelectedValue = trans.Rows[0]["no_type"].ToString();
                    //    txtdetal.Text = trans.Rows[0]["remark"].ToString();
                    //}
                    //if (trans.Rows[0]["flag_adjust"].ToString() == "y")
                    //{
                    //    btnsave.Visible = true;
                    //    Panel2.Visible = true;
                    //    txtremark.Text = trans.Rows[0]["adjust_remark"].ToString();
                    //}
                    //else
                    //{
                    //    btnsave.Visible = false;
                    //    Panel2.Visible = false;
                    //    txtremark.Text = "";
                    //}
                }
                else
                {
                    Response.Redirect("~/ar_monitoring.aspx");
                }
            }
            else
            {
                Response.Redirect("~/ar_monitoring.aspx");
            }

        }


        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/transaction_list_tenant.aspx");
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            LogService l = new LogService();
            ////// Update Adjust ยอด
            if (txtremark.Text == "")
            {
                POPUPMSG("กรุณากรอกข้อมูลที่ต้องการแก้ไข");
                txtremark.Focus();
                return;
            }
            else
            {
                Serv.Update_Transaction(txtremark.Text, Request.QueryString["id"].ToString(), Request.QueryString["cont"].ToString(), HttpContext.Current.Session["s_userid"].ToString(),
                    "te");

                var cont = Serv.GetContract(Request.QueryString["cont"].ToString());
                if (cont.Rows.Count != 0)
                {
                    var u = Serv.GetUser(cont.Rows[0]["smart_icon_staff_id"].ToString());
                    if (u.Rows.Count != 0)
                    {


                        string comname_ = "";
                        var comname = Serv.GetCompanyCodeByCompanyCode(cont.Rows[0]["CompanyCode"].ToString());
                        if (comname.Rows.Count != 0)
                        {
                            comname_ = comname.Rows[0]["CompanyNameEN"].ToString();
                        }

                        sentMail.CallMail_adjust(u.Rows[0]["email"].ToString(),
                             "ระบบ Tenant Sales Data Collection : แก้ไขยอดขายของวันที่ " + txtdate.Text + " ร้าน " + lbshopname.Text + " ห้อง " + lbroomnum.Text,
                             "Tenant Sales Data Collection", u.Rows[0]["Fname"].ToString(),
                             "แก้ไขยอดขายของวันที่ " + txtdate.Text + " ร้าน " + lbshopname.Text + " ห้อง " + lbroomnum.Text,

                             comname_, "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่แก้ไขยอด", "Link",
                             "", cont.Rows[0]["ShopName"].ToString(), cont.Rows[0]["BusinessPartnerName"].ToString(),
                             cont.Rows[0]["smart_floor"].ToString(), cont.Rows[0]["smart_room_no"].ToString(),
                             txtdate.Text, "", txtremark.Text, cont.Rows[0]["CompanyCode"].ToString(),"user");
                    }
                }

                if (FileUpload1.HasFile)
                {
                    string filename1 = Path.GetFileName(FileUpload1.FileName);
                    FileInfo fi1 = new FileInfo(FileUpload1.FileName);
                    string ext = fi1.Extension;
                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                    {
                        Random _r = new Random();
                        String n = _r.Next(0, 999999).ToString("D6");
                        string tmp_ = HttpContext.Current.Session["s_userid"].ToString() + "_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                             Request.QueryString["id"].ToString() + ext;

                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");
                        //string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                        string img_name3 = "/img/img.png";



                        Bitmap originalBMP = new Bitmap(FileUpload1.FileContent);
                        // Calculate the new image dimensions
                        int origWidth = originalBMP.Width;
                        int origHeight = originalBMP.Height;

                        int newWidth;
                        int newHeight;

                        if (origWidth > origHeight)
                        {
                            newWidth = 600;
                            newHeight = 400;
                        }
                        else
                        {
                            newWidth = 600;
                            newHeight = 400;
                        }


                        // Create a new bitmap which will hold the previous resized bitmap
                        Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                        // Create a graphic based on the new bitmap
                        Graphics oGraphics = Graphics.FromImage(newBMP);

                        // Set the properties for the new graphic file
                        oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        // Draw the new graphic based on the resized bitmap
                        oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                        newBMP.Save(Server.MapPath(img_name1));
                        // Once finished with the bitmap objects, we deallocate them.
                        originalBMP.Dispose();
                        newBMP.Dispose();
                        oGraphics.Dispose();

                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                            Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                        //FileUpload1.SaveAs(Server.MapPath(img_name1));

                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                    else if (ext == ".pdf" || ext == ".PDF")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/pdf.png";


                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                    else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/doc.png";

                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }

                    else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/xls.png";


                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                    else if (ext == ".txt")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/txt.png";


                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                    else
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/other.png";


                        FileUpload1.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                }

                if (FileUpload2.HasFile)
                {
                    string filename1 = Path.GetFileName(FileUpload2.FileName);
                    FileInfo fi1 = new FileInfo(FileUpload2.FileName);
                    string ext = fi1.Extension;
                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                    {
                        Random _r = new Random();
                        String n = _r.Next(0, 999999).ToString("D6");
                        string tmp_ = HttpContext.Current.Session["s_userid"].ToString() + "_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + "_" +
                             Request.QueryString["id"].ToString() + ext;
                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": SAVE IMG " + tmp_, "Web_Transaction_insert");

                        //string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_;
                        string img_name3 = "/img/img.png";



                        Bitmap originalBMP = new Bitmap(FileUpload2.FileContent);
                        // Calculate the new image dimensions
                        int origWidth = originalBMP.Width;
                        int origHeight = originalBMP.Height;

                        int newWidth;
                        int newHeight;

                        if (origWidth > origHeight)
                        {
                            newWidth = 600;
                            newHeight = 400;
                        }
                        else
                        {
                            newWidth = 600;
                            newHeight = 400;
                        }


                        // Create a new bitmap which will hold the previous resized bitmap
                        Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                        // Create a graphic based on the new bitmap
                        Graphics oGraphics = Graphics.FromImage(newBMP);

                        // Set the properties for the new graphic file
                        oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        // Draw the new graphic based on the resized bitmap
                        oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                        newBMP.Save(Server.MapPath(img_name1));
                        // Once finished with the bitmap objects, we deallocate them.
                        originalBMP.Dispose();
                        newBMP.Dispose();
                        oGraphics.Dispose();


                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + ": Insert IMG " + filename1 + " ContractNumber " +
                      Request.QueryString["id"].ToString(), "Web_Transaction_insert");
                        //FileUpload2.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                    else if (ext == ".pdf" || ext == ".PDF")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/pdf.png";

                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                    else if (ext == ".doc" || ext == ".DOC" || ext == ".docx" || ext == ".DOCX")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/doc.png";

                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }

                    else if (ext == ".xls" || ext == ".XLS" || ext == ".xlsx" || ext == ".XLSX")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/xls.png";


                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                    else if (ext == ".txt")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/txt.png";


                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                    else
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + Request.QueryString["id"].ToString() + ext;
                        if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                        }
                        string img_name1 = "~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name2 = DateTime.Now.ToString("yyyy-MM-dd") + "/" +HttpContext.Current.Session["s_userid"].ToString() + "_" + xx;
                        string img_name3 = "/img/other.png";


                        FileUpload2.SaveAs(Server.MapPath(img_name1));
                        Serv.tbl_edit_transaction_record_img(Request.QueryString["id"].ToString(), img_name3, img_name2, filename1, HttpContext.Current.Session["s_userid"].ToString());
                    }
                }


                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='transaction_list_tenant.aspx';", true);
            }

        }
        protected void Image1_Click(object sender, ImageClickEventArgs e)
        {
            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img1_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));
        }

        protected void Image2_Click(object sender, ImageClickEventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img2_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));

        }

        protected void Image3_Click(object sender, ImageClickEventArgs e)
        {
            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img3_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));


        }

        protected void Image4_Click(object sender, ImageClickEventArgs e)
        {
            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img4_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));

        }

        protected void Image5_Click(object sender, ImageClickEventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img5_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));


        }

        protected void Image6_Click(object sender, ImageClickEventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img6_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));

        }
        protected void Image7_Click(object sender, ImageClickEventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img7_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));

        }

        protected void Image8_Click(object sender, ImageClickEventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img8_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));

        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img1_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img2_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img3_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));
        }

        protected void LinkButton4_Click(object sender, EventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img4_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));
        }

        protected void LinkButton5_Click(object sender, EventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img5_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));
        }

        protected void LinkButton6_Click(object sender, EventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img6_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));
        }

        protected void LinkButton7_Click(object sender, EventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img7_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));
        }

        protected void LinkButton8_Click(object sender, EventArgs e)
        {

            string x = ConfigurationManager.AppSettings["link_path"] + "/img/slip/" + hdd_img8_file.Value;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + x + "','_blank')", true);
            ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", x));
        }



    }
}