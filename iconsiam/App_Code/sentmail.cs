﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using Microsoft.Exchange.WebServices.Data;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using iconsiam.App_Code.DLL;
using System.Globalization;

namespace iconsiam.App_Code
{
    public class sentmail
    {
        smart_collectionDLL Serv_1 = new smart_collectionDLL();
        EmailTemplateDLL Serv_2 = new EmailTemplateDLL();
        LogService l = new LogService();
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

        string E_Link = "";
        string E_Sendfromteam = "";
        string E_Sendfromcompany = "";
        string E_Tel = "";
        string E_F_Email = "";
        string E_F_Callcenter = "";
        string codetheme = "";

        protected string MyTheme { get; set; } //btn
        protected string NavbarColor { get; set; } //txtbtn

        public string CallMail(string email, string subject, string Header, string Dear, string Title,
            string label1, string label2, string label3, string label4, string label5, string label6, string label7,
            string detail1, string detail2, string detail3, string detail4, string detail5, string detail6, string detail7, string link,
            string userid, string contractnumber, string user_type)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];

            if (userid != "") //edit_user.aspx
            {
                var Email_template = Serv_2.getTemplate_by_userid(userid);
                if (Email_template.Rows.Count != 0) //เช็คว่า user อยู่กี่ com
                {
                    if (Email_template.Rows.Count > 1)
                    {
                        var Email_template_manycom = Serv_2.getTemplate_by_userid_many_com(Email_template.Rows[0]["ComGroupID"].ToString());
                        if (Email_template_manycom.Rows.Count != 0)
                        {
                            E_Link = Email_template_manycom.Rows[0]["Link"].ToString();
                            E_Sendfromteam = Email_template_manycom.Rows[0]["Sendfromteam"].ToString();
                            E_Sendfromcompany = Email_template_manycom.Rows[0]["Sendfromcompany"].ToString();
                            E_Tel = Email_template_manycom.Rows[0]["Tel"].ToString();
                            E_F_Email = Email_template_manycom.Rows[0]["F_Email"].ToString();
                            E_F_Callcenter = Email_template_manycom.Rows[0]["F_Callcenter"].ToString();
                            codetheme = Email_template_manycom.Rows[0]["ButtonColor"].ToString();
                        }
                    }
                    else
                    {
                        E_Link = Email_template.Rows[0]["Link"].ToString();
                        E_Sendfromteam = Email_template.Rows[0]["Sendfromteam"].ToString();
                        E_Sendfromcompany = Email_template.Rows[0]["Sendfromcompany"].ToString();
                        E_Tel = Email_template.Rows[0]["Tel"].ToString();
                        E_F_Email = Email_template.Rows[0]["F_Email"].ToString();
                        E_F_Callcenter = Email_template.Rows[0]["F_Callcenter"].ToString();
                        codetheme = Email_template.Rows[0]["ButtonColor"].ToString();
                    }
                }
                else //กรณีที่เป็น Superadmin
                {
                    E_Link = ConfigurationManager.AppSettings["E_Link"];
                    E_Sendfromteam = "ทีมบัญชีลูกหนี้";
                    E_Sendfromcompany = "ไอคอนสยาม";
                    E_Tel = "061-406-1757";
                    E_F_Email = "https://qas.siamsmartcollection.com";
                    E_F_Callcenter = "061-406-1757";
                    codetheme = "#CCA9DA";
                }
            }
            else if (contractnumber != "") //shop_smart_master_exist.aspx
            {
                var comcode = Serv_2.getComcode_by_ContractNumber(contractnumber);
                if (comcode.Rows.Count != 0)
                {
                    var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(comcode.Rows[0]["CompanyCode"].ToString());
                    {
                        if (Email_template_by_CompanyCode.Rows.Count != 0)
                        {
                            E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                            E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                            E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                            E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                            E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                            E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                            codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                        }
                    }
                }

            }

            if (E_Link == "")
            {
                if (user_type == "user")
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_user"];
                }
                else
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_tenant"];
                }
            }

            string x = "";

            try
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append("<body>");
                stringBuilder.Append("<div style='width:100%; height: 70px; background-color:" + codetheme + "; margin:0px;'>");
                stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>" + Header + "</h1></center>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                stringBuilder.Append("<h3>Dear " + Dear + " ,</h3>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>" + Title + "</t>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("   <td style='width:40%;color:" + codetheme + ";'><h4 style='margin:5px; margin-left:0px; color:" + codetheme + ";'>" + label2 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail2 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;color:" + codetheme + ";'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label3 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail3 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("  <td style='width:40%;color:" + codetheme + ";'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label4 + "</h4></td>");
                stringBuilder.Append("    <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>Please change your password on first login.</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("    </table>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + " " + E_Sendfromcompany + " โทร  " + E_Tel + " ");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("<div style='width:100%; height: 40px; background:#4b4c44; margin:0px;'>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "</t> | IT Call Center " + E_F_Callcenter + " </p></center>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("</body>");

                var emailMessage = new MailMessage
                {
                    Subject = subject,
                    From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                };

                string[] allmail = email.Replace(" ", "").Split(',');
                if (allmail.Length != 0)
                {
                    for (int xxx = 0; xxx < allmail.Length; xxx++)
                    {
                        emailMessage.To.Add(new MailAddress(allmail[xxx].Trim()));
                    }
                }


                emailMessage.IsBodyHtml = true;
                emailMessage.Body = stringBuilder.ToString();

                var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                smtpClient.Send(emailMessage);

                //emailMessage.SendAndSaveCopy();

                x = "success";

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Unable to connect to the remote server"))
                {
                    x = "Unable to connect to the remote server";
                }
                else
                {
                    x = ex.ToString();
                }
            }
            finally
            {
                //exchangeService = null;
            }

            return x;


        }

        public void CallMail_new_account(string email, string subject, string Header, string Dear, string Title,
         string label1, string label2, string label3, string label4, string label5, string label6, string label7,
         string detail1, string detail2, string detail3, string detail4, string detail5, string detail6, string detail7, string link, string contractnumber, string user_type)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];

            var comcode = Serv_2.getComcode_by_ContractNumber(contractnumber);
            if (comcode.Rows.Count != 0)
            {
                var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(comcode.Rows[0]["CompanyCode"].ToString());
                {
                    if (Email_template_by_CompanyCode.Rows.Count != 0)
                    {
                        E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                        E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                        E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                        E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                        E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                        E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                        codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                    }
                }
            }

            if (E_Link == "")
            {
                if (user_type == "user")
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_user"];
                }
                else
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_tenant"];
                }
            }


            //var exchangeService = new ExchangeService(ExchangeVersion.Exchange2010)
            //{
            //    Credentials = new WebCredentials("smartcollection", "Sm@rt2018", "spwg"),
            //    Url = new Uri("https://webmail.siampiwat.com/EWS/Exchange.asmx")
            //};

            try
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append("<body>");
                stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>" + Header + "</h1></center>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                stringBuilder.Append("<h3>Dear ผู้ดูแลร้านค้า" + Dear + " ,</h3>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>" + Title + "</t>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                stringBuilder.Append("<tr>");
                stringBuilder.Append("  <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label1 + "</h4></td>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("   <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label2 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail2 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label3 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail3 + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label4 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail4 + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label5 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail5 + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label6 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail6 + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label7 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail7 + "</p></td>");
                stringBuilder.Append(" </tr>");


                stringBuilder.Append("  <tr>");
                stringBuilder.Append("  <td style='width:40%; color: " + codetheme + " ;'><h4 style='margin:5px; margin-left:0px;'></h4></td>");
                stringBuilder.Append("    <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>Please change your password on first login.</p></td>");
                stringBuilder.Append(" </tr>");



                stringBuilder.Append("    </table>");


                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + " " + E_Sendfromcompany + " โทร " + E_Tel + "</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "</t> | IT Call Center " + E_F_Callcenter + "</p></center>");
                stringBuilder.Append("</div>");



                stringBuilder.Append("</body>");

                //ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                //{
                //    return true;
                //};


                //var emailMessage = new EmailMessage(exchangeService)
                //{
                //    Subject = subject,
                //    Body = stringBuilder.ToString(),
                //};

                //emailMessage.From = "smartcollection@iconsiam.com";
                //emailMessage.Body.BodyType = BodyType.HTML;

                //string[] allmail = email.Replace(" ", "").Split(',');
                //if (allmail.Length != 0)
                //{
                //    for (int xxx = 0; xxx < allmail.Length; xxx++)
                //    {
                //        emailMessage.ToRecipients.Add(new EmailAddress(allmail[xxx].Trim()));
                //    }
                //}


                //emailMessage.SendAndSaveCopy();

                var emailMessage = new MailMessage
                {
                    Subject = subject,
                    From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                };

                string[] allmail = email.Replace(" ", "").Split(',');
                if (allmail.Length != 0)
                {
                    for (int xxx = 0; xxx < allmail.Length; xxx++)
                    {
                        emailMessage.To.Add(new MailAddress(allmail[xxx].Trim()));
                    }
                }


                emailMessage.IsBodyHtml = true;
                emailMessage.Body = stringBuilder.ToString();

                var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                smtpClient.Send(emailMessage);


            }
            catch
            {
                throw;
            }
            finally
            {
                //exchangeService = null;
            }


        }


        public void CallMail_forget_password(string Dear, string CompanyName, string ShopName, string BusinessPartner, string floor, string room, string DateNow,
            string email, string companycode, string user_type)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];

            var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(companycode);
            {
                if (Email_template_by_CompanyCode.Rows.Count != 0)
                {
                    E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                    E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                    E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                    E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                    E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                    E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                    codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                }
            }

            if (E_Link == "")
            {
                if (user_type == "user")
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_user"];
                }
                else
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_tenant"];
                }
            }

            try
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append("<body>");
                stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</h1></center>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                stringBuilder.Append("<h3>Dear " + Dear + " ,</h3>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>เนื่องจากร้านค้าผู้เช่าไม่สามารถจำ User หรือ Password ในการนำส่งยอดขายได้ " +
                    "ระบบจึงทำการแจ้งเตือนว่ายังคงอนุญาตให้ร้านค้าผู้เช่าทำการบักทึกยอดขายต่อไปได้</t>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>กรุณาทำการ Reset Password ร้านค้าผู้เช่าดังกล่าว</t>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                stringBuilder.Append("<tr>");
                stringBuilder.Append("  <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + CompanyName + "</h4></td>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("   <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ชื่อร้าน</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + ShopName + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ชื่อบริษัท</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + BusinessPartner + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ชั้น</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + floor + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ห้อง</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + room + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>วันที่ Login โดยไม่มีการระบุ User and Password</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + DateNow + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Link</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + link_ + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("    </table>");


                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + " " + E_Sendfromcompany + " โทร  " + E_Tel + " </p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "/t> | IT Call Center " + E_F_Callcenter + "</p></center>");
                stringBuilder.Append("</div>");



                stringBuilder.Append("</body>");

                var emailMessage = new MailMessage
                {
                    Subject = "ระบบ Tenant Sales Data Collection  : Forgot Password  ร้าน " + ShopName + " ห้อง " + room + "",
                    From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                };

                string[] allmail = email.Replace(" ", "").Split(',');
                if (allmail.Length != 0)
                {
                    for (int xxx = 0; xxx < allmail.Length; xxx++)
                    {
                        emailMessage.To.Add(new MailAddress(allmail[xxx].Trim()));
                    }
                }

                emailMessage.IsBodyHtml = true;
                emailMessage.Body = stringBuilder.ToString();

                var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                smtpClient.Send(emailMessage);
            }
            catch
            {
                throw;
            }
            finally
            {
                //exchangeService = null;
            }


        }

        public void CallMail_adjust(string email, string subject, string Header, string Dear, string Title,
          string label1, string label2, string label3, string label4, string label5, string label6, string label7,
          string detail1, string detail2, string detail3, string detail4, string detail5, string detail6, string detail7, string link, string companycode, string user_type)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];

            var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(companycode);
            {
                if (Email_template_by_CompanyCode.Rows.Count != 0)
                {
                    E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                    E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                    E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                    E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                    E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                    E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                    codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                }
            }

            if (E_Link == "")
            {
                if (user_type == "user")
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_user"];
                }
                else
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_tenant"];
                }
            }


            //var exchangeService = new ExchangeService(ExchangeVersion.Exchange2010)
            //{
            //    Credentials = new WebCredentials("smartcollection", "Sm@rt2018", "spwg"),
            //    Url = new Uri("https://webmail.siampiwat.com/EWS/Exchange.asmx")
            //};

            try
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append("<body>");
                stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>" + Header + "</h1></center>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                stringBuilder.Append("<h3>Dear " + Dear + " ,</h3>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>" + Title + "</t>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                stringBuilder.Append("<tr>");
                stringBuilder.Append("  <td style='width:40%; color: " + codetheme + " ;'><h4 style='margin:5px; margin-left:0px;'>" + label1 + "</h4></td>");

                if (detail1 == "")
                {
                    stringBuilder.Append("  <td></td>");

                }
                else
                {
                    stringBuilder.Append("  <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail1 + "</p></td>");

                }

                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("   <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label2 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail2 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label3 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail3 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("  <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label4 + "</h4></td>");
                stringBuilder.Append("    <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail4 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("     <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label5 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail5 + "</p></td>");
                stringBuilder.Append("  </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label6 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail6 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("     <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Remark</h4></td>");
                stringBuilder.Append("   <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + link_ + "</p></td>");
                stringBuilder.Append("  </tr>");


                stringBuilder.Append("    </table>");


                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + " " + E_Sendfromcompany + " โทร " + E_Tel + " </p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "</t> | IT Call Center " + E_F_Callcenter + "</p></center>");
                stringBuilder.Append("</div>");


                stringBuilder.Append("</body>");

                //ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                //{
                //    return true;
                //};


                var emailMessage = new MailMessage
                {
                    Subject = subject,
                    From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                };

                string[] allmail = email.Replace(" ", "").Split(',');
                if (allmail.Length != 0)
                {
                    for (int xxx = 0; xxx < allmail.Length; xxx++)
                    {
                        emailMessage.To.Add(new MailAddress(allmail[xxx].Trim()));
                    }
                }


                emailMessage.IsBodyHtml = true;
                emailMessage.Body = stringBuilder.ToString();

                var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                smtpClient.Send(emailMessage);

            }
            catch
            {
                throw;
            }
            finally
            {
                //exchangeService = null;
            }


        }

        #region Mail Template Job Schedule
        public void CallMail_noti_daily(string email, string subject, string Header, string Dear, string Title,
         string ComCode, string label2, string label3, string label4, string label5, string label6, string label7,
         string detail1, string detail2, string detail3, string detail4, string detail5, string[] detail6, string detail7, string link, string type,
         string ContractNumber, string user_type)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];

            var comcode = Serv_2.getComcode_by_ContractNumber(ContractNumber);
            if (comcode.Rows.Count != 0)
            {
                var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(comcode.Rows[0]["CompanyCode"].ToString());
                {
                    if (Email_template_by_CompanyCode.Rows.Count != 0)
                    {
                        E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                        E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                        E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                        E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                        E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                        E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                        codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                    }
                }
            }

            if (E_Link == "")
            {
                if (user_type == "user")
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_user"];
                }
                else
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_tenant"];
                }
            }

            string comname_ = "";
            var comname = Serv_1.GetCompanyCodeByCompanyCode(ComCode);
            if (comname.Rows.Count != 0)
            {
                comname_ = comname.Rows[0]["CompanyNameEN"].ToString();
            }

            try
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append("<body>");
                stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>" + Header + "</h1></center>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                stringBuilder.Append("<h3>Dear " + Dear + " ,</h3>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>" + Title + "</t>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                stringBuilder.Append("<tr>");
                stringBuilder.Append("  <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + comname_ + "</h4></td>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("   <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label2 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail2 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label3 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail3 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("  <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label4 + "</h4></td>");
                stringBuilder.Append("    <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail4 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("     <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label5 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail5 + "</p></td>");
                stringBuilder.Append("  </tr>");


                if (detail6.Length != 0)
                {
                    for (int i = 0; i < detail6.Length; i++)
                    {
                        if (detail6[i] != "")
                        {
                            if (i == 0)
                            {
                                stringBuilder.Append(" <tr>");
                                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label6 + "</h4></td>");
                                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail6[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "</p></td>");
                                stringBuilder.Append(" </tr>");
                            }
                            else
                            {
                                stringBuilder.Append(" <tr>");
                                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'></h4></td>");
                                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail6[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "</p></td>");
                                stringBuilder.Append(" </tr>");
                            }
                        }

                    }
                }

                stringBuilder.Append("    </table>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + " " + E_Sendfromcompany + " โทร " + E_Tel + "</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "</t> | IT Call Center " + E_F_Callcenter + "</p></center>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("</body>");

                var emailMessage = new MailMessage
                {
                    Subject = subject,
                    From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                };

                string[] allmail = email.Replace(" ", "").Split(',');
                if (allmail.Length != 0)
                {
                    for (int xxx = 0; xxx < allmail.Length; xxx++)
                    {
                        emailMessage.To.Add(new MailAddress(allmail[xxx].Trim()));
                    }
                }

                string userid = "";
                var ar = Serv_1.getSmart_Contract(ContractNumber);
                if (ar.Rows.Count != 0)
                {
                    for (int xc = 0; xc < ar.Rows.Count; xc++)
                    {
                        userid = userid + "'" + ar.Rows[xc]["smart_icon_staff_id"].ToString() + "'" + ",";
                    }
                    userid = userid.Substring(0, userid.Length - 1);
                }

                var user_mail = Serv_1.getAR_user(userid);
                if (user_mail.Rows.Count != 0)
                {
                    for (int g = 0; g < user_mail.Rows.Count; g++)
                    {
                        emailMessage.CC.Add(new MailAddress(user_mail.Rows[g]["email"].ToString()));
                    }
                }

                emailMessage.IsBodyHtml = true;
                emailMessage.Body = stringBuilder.ToString();

                var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                smtpClient.Send(emailMessage);

            }
            catch
            {
                throw;
            }
            finally
            {
                //exchangeService = null;
            }


        }


        public void CallMai_new_contract(string date, string link)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];



            try
            {
                var compa = Serv_1.GetAllCompany();
                if (compa.Rows.Count != 0)
                {
                    for (int y = 0; y < compa.Rows.Count; y++)
                    {
                        var contract = Serv_1.GetContract_sync_now(compa.Rows[y]["CompanyCode"].ToString(), date);
                        if (contract.Rows.Count != 0)
                        {
                            var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(compa.Rows[y]["CompanyCode"].ToString());
                            {
                                if (Email_template_by_CompanyCode.Rows.Count != 0)
                                {
                                    E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                                    E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                                    E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                                    E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                                    E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                                    E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                                    codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                                }
                            }

                            var stringBuilder = new StringBuilder();

                            stringBuilder.Append("<body>");
                            stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                            stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</h1></center>");
                            stringBuilder.Append("</div>");
                            stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                            stringBuilder.Append("<h3>Dear ทีมบัญชีลูกหนี้ ,</h3>");
                            stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>New Contract ของ " + contract.Rows[0]["ComGroupName"].ToString() + " ณ วันที่ " +
                                date.Substring(8, 2) + "/" + date.Substring(5, 2) + "/" + date.Substring(0, 4) + "</t>");
                            stringBuilder.Append("</div>");
                            stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                            stringBuilder.Append(" <table style='width:70%; margin-left:15%; margin-top:15px; border-color:#948a55; border-collapse: collapse;'> ");

                            stringBuilder.Append("  <tr style='background:#948a55; text-align:center; '> ");
                            stringBuilder.Append(" <td style=' color:#fff;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Contract No.</h4></td>  ");
                            stringBuilder.Append("   <td style=' color:#fff;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Customer Name</h4></td> ");
                            stringBuilder.Append("  <td style=' color:#fff;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Shop Name</h4></td>  ");
                            stringBuilder.Append("  <td style=' color:#fff;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Floor</h4></td> ");
                            stringBuilder.Append("  <td style=' color:#fff;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Room</h4></td> ");
                            stringBuilder.Append(" <td style=' color:#fff;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Start Date</h4></td>  ");
                            stringBuilder.Append(" <td style=' color:#fff;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>End Date</h4></td>  ");
                            stringBuilder.Append(" </tr>  ");

                            for (int k = 0; k < contract.Rows.Count; k++)
                            {
                                stringBuilder.Append(" <tr style='text-align:left; border: 1px solid black;'>  ");
                                stringBuilder.Append(" <td style='border: 1px solid black;'><h5 style='margin:5px; margin-left:8px;'>" + contract.Rows[k]["contractnumber"].ToString() + "</h5></td>  ");
                                stringBuilder.Append(" <td style='border: 1px solid black;'><h5 style='margin:5px; margin-left:8px;'>" + contract.Rows[k]["BusinessPartnerName"].ToString() + "</h5></td>  ");
                                stringBuilder.Append(" <td style='border: 1px solid black;'><h5 style='margin:5px; margin-left:8px;'>" + contract.Rows[k]["ShopName"].ToString() + "</h5></td>  ");
                                stringBuilder.Append(" <td style='border: 1px solid black;'><h5 style='margin:5px; margin-left:8px;'>" + contract.Rows[k]["smart_floor"].ToString() + "</h5></td>  ");
                                stringBuilder.Append(" <td style='border: 1px solid black;'><h5 style='margin:5px; margin-left:8px;'>" + contract.Rows[k]["smart_room_no"].ToString() + "</h5></td>  ");
                                stringBuilder.Append(" <td style='border: 1px solid black;'><h5 style='margin:5px; margin-left:8px;'>" + Convert.ToDateTime(contract.Rows[k]["ContractStartDate"]).ToString("dd/MM/yyyy") + "</h5></td>  ");
                                stringBuilder.Append(" <td style='border: 1px solid black;'><h5 style='margin:5px; margin-left:8px;'>" + Convert.ToDateTime(contract.Rows[k]["ContractEndDate"]).ToString("dd/MM/yyyy") + "</h5></td>  ");
                                stringBuilder.Append(" </tr> ");

                            }

                            stringBuilder.Append("  </table> ");

                            stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                            stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                            stringBuilder.Append("   </div>");

                            stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                            stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + "  " + E_Sendfromcompany + " โทร  " + E_Tel + " </p>");
                            stringBuilder.Append("   </div>");

                            stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                            stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                            stringBuilder.Append("   </div>");

                            stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                            stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                            stringBuilder.Append("</div>");

                            stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                            stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                            stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'> " + E_F_Email + " </t> | IT Call Center " + E_F_Callcenter + " </p></center>");
                            stringBuilder.Append("</div>");


                            stringBuilder.Append("</body>");

                            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                            {
                                return true;
                            };

                            var emailMessage = new MailMessage
                            {
                                Subject = "ระบบ Tenant Sales Data Collection : New Contract ของ " + contract.Rows[0]["ComGroupName"].ToString(),
                                From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                            };


                            string userid = "";


                            var ar = Serv_1.GetUserByComCode_distinct_email(compa.Rows[y]["CompanyCode"].ToString());
                            if (ar.Rows.Count != 0)
                            {
                                string ex_email = "";
                                for (int xc = 0; xc < ar.Rows.Count; xc++)
                                {
                                    bool contains = ex_email.Contains(ar.Rows[xc]["email"].ToString());
                                    if (contains == false)
                                    {
                                        ex_email = ex_email + ar.Rows[xc]["email"].ToString() + ";";
                                        emailMessage.To.Add(new MailAddress(ar.Rows[xc]["email"].ToString()));
                                    }

                                }
                            }

                            var ar_name = Serv_1.GetUserByComCode_distinct_name(compa.Rows[y]["CompanyCode"].ToString());
                            if (ar_name.Rows.Count != 0)
                            {
                                for (int xc = 0; xc < ar_name.Rows.Count; xc++)
                                {
                                    userid = userid + "'" + ar_name.Rows[xc]["userid"].ToString() + "'" + ",";
                                }
                                userid = userid.Substring(0, userid.Length - 1);
                            }



                            var manager = Serv_1.GetManagerEmail(userid);
                            if (manager.Rows.Count != 0)
                            {
                                string ex_email = "";
                                for (int xc = 0; xc < manager.Rows.Count; xc++)
                                {
                                    bool contains = ex_email.Contains(ar.Rows[xc]["email"].ToString());
                                    if (contains == false)
                                    {
                                        ex_email = ex_email + ar.Rows[xc]["email"].ToString() + ";";
                                        emailMessage.To.Add(new MailAddress(manager.Rows[xc]["email"].ToString()));

                                    }
                                }

                            }

                            emailMessage.IsBodyHtml = true;
                            emailMessage.Body = stringBuilder.ToString();

                            var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                            smtpClient.Send(emailMessage);

                        }
                    }
                }


            }
            catch
            {
                throw;
            }
            finally
            {
                //exchangeService = null;
            }


        }

        public void CallMailConfirm(string link, string date1, string date2)
        {
            string link_ = ConfigurationManager.AppSettings["link_path"];

            try
            {
                var contract_ = Serv_1.GetContractActive();
                if (contract_.Rows.Count != 0)
                {

                    for (int i = 0; i < contract_.Rows.Count; i++)
                    {
                        string name = "";

                        var stringBuilder = new StringBuilder();



                        string comname_ = "";
                        var comname = Serv_1.GetCompanyCodeByCompanyCode(contract_.Rows[i]["CompanyCode"].ToString());

                        var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(contract_.Rows[i]["CompanyCode"].ToString());
                        {
                            if (Email_template_by_CompanyCode.Rows.Count != 0)
                            {
                                E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                                E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                                E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                                E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                                E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                                E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                                codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                            }
                        }


                        if (comname.Rows.Count != 0)
                        {
                            comname_ = comname.Rows[0]["CompanyNameEN"].ToString();
                        }

                        stringBuilder.Append("<body>");
                        stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                        stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</h1></center>");
                        stringBuilder.Append("</div>");
                        stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                        stringBuilder.Append("<h3>Dear " + name + " ,</h3>");
                        stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>กรุณายืนยันยอดขาย</t>");
                        stringBuilder.Append("</div>");
                        stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                        stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                        stringBuilder.Append("<tr>");
                        stringBuilder.Append("  <td style='width:40%; color:#cc5600;'><h4 style='margin:5px; margin-left:0px; color: " + codetheme + ";'>" + comname_ + "</h4></td>");
                        stringBuilder.Append("  <td></td>");
                        stringBuilder.Append(" </tr>");
                        stringBuilder.Append(" <tr>");
                        stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ชื่อร้าน</h4></td>");
                        stringBuilder.Append("    <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + contract_.Rows[i]["ShopName"].ToString() + "</p></td>");
                        stringBuilder.Append(" </tr>");
                        stringBuilder.Append("  <tr>");
                        stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ชื่อบริษัท</h4></td>");
                        stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + contract_.Rows[i]["BusinessPartnerName"].ToString() + "</p></td>");
                        stringBuilder.Append(" </tr>");
                        stringBuilder.Append("  <tr>");
                        stringBuilder.Append("  <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ชั้น</h4></td>");
                        stringBuilder.Append("    <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + contract_.Rows[i]["smart_floor"].ToString() + "</p></td>");
                        stringBuilder.Append(" </tr>");
                        stringBuilder.Append(" <tr>");
                        stringBuilder.Append("     <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ห้อง</h4></td>");
                        stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + contract_.Rows[i]["smart_room_no"].ToString() + "</p></td>");
                        stringBuilder.Append("  </tr>");
                        stringBuilder.Append(" <tr>");
                        stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ยืนยันยอดขาย</h4></td>");
                        stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'></p></td>");
                        stringBuilder.Append(" </tr>");
                        stringBuilder.Append("  <tr>");
                        stringBuilder.Append("     <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>วันที่</h4></td>");
                        stringBuilder.Append("   <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + date2 + "</p></td>");
                        stringBuilder.Append("  </tr>");
                        stringBuilder.Append("  <tr>");
                        stringBuilder.Append("     <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ถึงวันที่</h4></td>");
                        stringBuilder.Append("   <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + date1 + "</p></td>");
                        stringBuilder.Append("  </tr>");

                        ////stringBuilder.Append("  <tr>");
                        ////stringBuilder.Append("     <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Link</h4></td>");
                        ////stringBuilder.Append("   <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + link_ + "</p></td>");
                        ////stringBuilder.Append("  </tr>");

                        stringBuilder.Append("    </table>");

                        stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                        stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                        stringBuilder.Append("   </div>");

                        stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                        stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + "  " + E_Sendfromcompany + " โทร " + E_Tel + " </p>");
                        stringBuilder.Append("   </div>");

                        stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                        stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                        stringBuilder.Append("   </div>");

                        stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                        stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                        stringBuilder.Append("</div>");

                        stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                        stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                        stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "</t> | IT Call Center " + E_F_Callcenter + "</p></center>");
                        stringBuilder.Append("</div>");


                        stringBuilder.Append("</body>");

                        string subject = "ระบบ Tenant Sales Data Collection : กรุณายืนยันยอดขายของวันที่ " + date2 + " ถึง " + date1 + " ร้าน " + contract_.Rows[i]["ShopName"].ToString() + " ห้อง " + contract_.Rows[i]["smart_floor"].ToString();

                        var emailMessage = new MailMessage
                        {
                            Subject = subject,
                            From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                        };


                        var cont_ = Serv_1.GetMainContact(contract_.Rows[i]["ContractNumber"].ToString());
                        if (cont_.Rows.Count != 0)
                        {
                            string ex_email = "";
                            for (int j = 0; j < cont_.Rows.Count; j++)
                            {
                                bool contains = ex_email.Contains(cont_.Rows[j]["email"].ToString());

                                if (contains == false)
                                {
                                    ex_email = ex_email + cont_.Rows[j]["email"].ToString() + ";";
                                    name = name + cont_.Rows[j]["name"].ToString() + ",";
                                    emailMessage.To.Add(new MailAddress(cont_.Rows[j]["email"].ToString()));

                                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + " Sent Email : " + cont_.Rows[j]["email"].ToString() + " ; ", "confirm_t_adhoc");

                                }
                            }
                            name = name.Substring(0, name.Length - 1);
                        }

                        emailMessage.IsBodyHtml = true;
                        emailMessage.Body = stringBuilder.ToString();

                        var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                        smtpClient.Send(emailMessage);

                    }
                }

            }
            catch (Exception ex)
            {
                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "confirm_t_adhoc");

                throw;
            }
            finally
            {
                //exchangeService = null;
            }


        }
        #endregion

        public void CallMail_bcc_ar(string email, string subject, string Header, string Dear, string Title,
         string label1, string label2, string label3, string label4, string label5, string label6, string label7,
         string detail1, string detail2, string detail3, string detail4, string detail5, string detail6, string detail7, string link, string contractnumber, string user_type)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];

            var comcode = Serv_2.getComcode_by_ContractNumber(contractnumber);
            if (comcode.Rows.Count != 0)
            {
                var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(comcode.Rows[0]["CompanyCode"].ToString());
                {
                    if (Email_template_by_CompanyCode.Rows.Count != 0)
                    {
                        E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                        E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                        E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                        E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                        E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                        E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                        codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                    }
                }

            }

            if (E_Link == "")
            {
                if (user_type == "user")
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_user"];
                }
                else
                {
                    E_Link = ConfigurationManager.AppSettings["link_path_tenant"];
                }
            }

            try
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append("<body>");
                stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>" + Header + "</h1></center>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                stringBuilder.Append("<h3>Dear " + Dear + " ,</h3>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>" + Title + "</t>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                stringBuilder.Append("<tr>");
                stringBuilder.Append("  <td style='width:40%; color:#cc5600;'><h4 style='margin:5px; margin-left:0px; color:" + codetheme + ";'>" + label1 + "</h4></td>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("   <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label2 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail2 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label3 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail3 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("  <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label4 + "</h4></td>");
                stringBuilder.Append("    <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail4 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("     <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label5 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail5 + "</p></td>");
                stringBuilder.Append("  </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label6 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail6 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("     <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label7 + "</h4></td>");
                stringBuilder.Append("   <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail7 + "</p></td>");
                stringBuilder.Append("  </tr>");
                stringBuilder.Append("    </table>");


                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + "  " + E_Sendfromcompany + " โทร  " + E_Tel + "</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "</t> | IT Call Center " + E_F_Callcenter + "</p></center>");
                stringBuilder.Append("</div>");



                stringBuilder.Append("</body>");

                var emailMessage = new MailMessage
                {
                    Subject = subject,
                    From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                };

                string[] allmail = email.Replace(" ", "").Split(',');
                if (allmail.Length != 0)
                {
                    for (int xxx = 0; xxx < allmail.Length; xxx++)
                    {
                        //emailMessage.ToRecipients.Add(new EmailAddress(allmail[xxx].Trim()));
                        emailMessage.To.Add(new MailAddress(allmail[xxx].Trim()));
                    }
                }

                var u_ar = GetUserAR_bcc(contractnumber);
                if (u_ar.Rows.Count != 0)
                {
                    for (int i = 0; i < u_ar.Rows.Count; i++)
                    {
                        //emailMessage.BccRecipients.Add(new EmailAddress(u_ar.Rows[i]["email"].ToString()));
                        emailMessage.To.Add(new MailAddress(u_ar.Rows[i]["email"].ToString()));

                    }
                }

                emailMessage.IsBodyHtml = true;
                emailMessage.Body = stringBuilder.ToString();

                var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                smtpClient.Send(emailMessage);

                //emailMessage.SendAndSaveCopy();

            }
            catch
            {
                throw;
            }
            finally
            {
                //exchangeService = null;
            }


        }

        public string Template_defalse(string email, string subject, string Header, string Dear, string Title,
            string label1, string label2, string label3, string label4, string label5, string label6, string label7,
            string detail1, string detail2, string detail3, string detail4, string detail5, string detail6, string detail7, string link, string companycode)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];

            if (companycode != "defalse")
            {
                var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(companycode);
                {
                    if (Email_template_by_CompanyCode.Rows.Count != 0)
                    {
                        E_Link = ConfigurationManager.AppSettings["link_path_user"];
                        E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                        E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                        E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                        E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                        E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                        codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                    }
                }
            }
            else
            {
                E_Link = ConfigurationManager.AppSettings["link_path_user"];
                E_Sendfromteam = "ทีมบัญชีลูกหนี้";
                E_Sendfromcompany = "ไอคอนสยาม";
                E_Tel = "061-406-1757";
                E_F_Email = "https://qas.siamsmartcollection.com";
                E_F_Callcenter = "061-406-1757";
                codetheme = "#CCA9DA";
            }


            string x = "";

            try
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append("<body>");
                stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>" + Header + "</h1></center>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                stringBuilder.Append("<h3>Dear " + Dear + " ,</h3>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>" + Title + "</t>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("   <td style='width:40%;'><h4 style='margin:5px; margin-left:0px; color:" + codetheme + ";'>" + label2 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail2 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label3 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail3 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("  <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label4 + "</h4></td>");
                stringBuilder.Append("    <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>Please change your password on first login.</p></td>");
                stringBuilder.Append(" </tr>");



                stringBuilder.Append("    </table>");


                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + " " + E_Sendfromcompany + " โทร  " + E_Tel + " ");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "</t> | IT Call Center " + E_F_Callcenter + " </p></center>");
                stringBuilder.Append("</div>");


                stringBuilder.Append("</body>");

                var emailMessage = new MailMessage
                {
                    Subject = subject,
                    From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                };

                string[] allmail = email.Replace(" ", "").Split(',');
                if (allmail.Length != 0)
                {
                    for (int xxx = 0; xxx < allmail.Length; xxx++)
                    {
                        emailMessage.To.Add(new MailAddress(allmail[xxx].Trim()));
                    }
                }


                emailMessage.IsBodyHtml = true;
                emailMessage.Body = stringBuilder.ToString();

                var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                smtpClient.Send(emailMessage);


                x = "success";

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Unable to connect to the remote server"))
                {
                    x = "Unable to connect to the remote server";
                }
                else
                {
                    x = ex.ToString();
                }
            }
            finally
            {
                //exchangeService = null;
            }

            return x;


        }
        public DataTable GetUserAR_bcc(string contractnumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select u.username ,u.email from tblsmart_Contract s inner " +
                        " join tbluser u on s.smart_icon_staff_id = u.userid " +
                        " where s.contractnumber = '" + contractnumber + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

    }
}