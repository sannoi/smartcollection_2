﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class ar_monitoring : System.Web.UI.Page
    {
        ar_monitoringDLL Serv = new ar_monitoringDLL();
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    DateTime t1 = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    DateTime t2 = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 19:00:00"));

                    if (TimeSpan.Compare(t1.TimeOfDay, t2.TimeOfDay) > 0)
                    {
                        txtstartdate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);
                    }
                    else
                    {
                        txtstartdate.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", EngCI);
                    }

                    //txtstartdate.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", EngCI);

                    bind_default();
                    bind_rep();

                    HttpContext.Current.Session["samrt_last_page"] = "ar_monitoring.aspx";
                }
            }
        }

        protected void bind_default()
        {

            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }

            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;

            //}



            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }

            ddlcompany.Items.Insert(0, new ListItem("Company", ""));
        }
        protected void bind_rep()
        {
            string[] bpname = txtbpname.Text.Split(',');
            string[] shop_name = txtshopname.Text.Split(',');

            dateconvert con_d = new dateconvert();

            if (HttpContext.Current.Session["s_user_for_shop"] != null)
            {
                if (HttpContext.Current.Session["s_user_for_shop"].ToString() != "")
                {
                    var contract = Serv.GetContract(HttpContext.Current.Session["s_user_for_shop"].ToString(), bpname, shop_name, txtroomno.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
               ddlcompany.SelectedValue, con_d.con_date(txtstartdate.Text));

                    lbfordate.Text = txtstartdate.Text;

                    if (contract.Rows.Count != 0)
                    {

                        GridView_List.DataSource = contract;
                        GridView_List.DataBind();
                    }
                    else
                    {
                        GridView_List.DataSource = null;
                        GridView_List.DataBind();
                    }
                }
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }


        }


        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }

        protected void btnrecord_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_ContractNumber = (HiddenField)row.FindControl("hdd_ContractNumber");
            HiddenField hdd_smart_record_person_type = (HiddenField)row.FindControl("hdd_smart_record_person_type");

            Response.Redirect("~/ar_monitoring_detail.aspx?id=" + hdd_ContractNumber.Value + "&h=" + hdd_smart_record_person_type.Value);
        }

        protected void btndetail_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_ContractNumber = (HiddenField)row.FindControl("hdd_ContractNumber");
            HiddenField hdd_smart_record_person_type = (HiddenField)row.FindControl("hdd_smart_record_person_type");

            Response.Redirect("~/ar_monitoring_detail.aspx?id=" + hdd_ContractNumber.Value + "&h=" + hdd_smart_record_person_type.Value);
        }


        protected void GridView_List_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                HiddenField hdd_smart_record_person_type = (HiddenField)(e.Row.FindControl("hdd_smart_record_person_type"));
                HiddenField hdd_smart_record_keyin_type = (HiddenField)(e.Row.FindControl("hdd_smart_record_keyin_type"));
                HiddenField hdd_status = (HiddenField)(e.Row.FindControl("hdd_status"));
                Button btnrecord = (Button)(e.Row.FindControl("btnrecord"));
                Button btndetail = (Button)(e.Row.FindControl("btndetail"));

                if (hdd_smart_record_person_type.Value == "te")
                {
                    btnrecord.Visible = false;
                    btndetail.Visible = true;
                }
                else
                {
                    btnrecord.Visible = true;
                    btndetail.Visible = false;
                }

                if (hdd_status.Value == "ยังไม่ส่งยอด")
                {
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }
                else if (hdd_status.Value == "ขอแก้ไขยอด")
                {
                    e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;
                }


                if (hdd_smart_record_keyin_type.Value == "prod_sale")
                {
                    e.Row.Cells[4].Text = "";
                }


            }


        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ar_monitoring.aspx");

        }

    }
}