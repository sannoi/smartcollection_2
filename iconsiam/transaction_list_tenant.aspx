﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="transaction_list_tenant.aspx.cs" Inherits="iconsiam.transaction_list_tenant" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

    <style type="text/css">
        .test .ajax__calendar_container {
            padding: 10px;
            position: absolute;
            cursor: default;
            width: 300px;
            font-size: 15px;
            text-align: center;
            font-family: tahoma,erdana,helvetica;
            background-color: lemonchiffon;
            border: 1px solid #646464;
            z-index: 10000 !important;
            text-wrap: none;
        }

        .test .ajax__calendar_dayname {
            height: 17px;
            width: 34px;
            text-align: center;
        }
    </style>


    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }


        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10005;
        }



    </script>

     <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Transaction List
            </p>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    Company :
                    <asp:Label ID="lbcompanyname" runat="server" Text="-"></asp:Label>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    Customer Name :
                    <asp:Label ID="lbbpname" runat="server" Text="-"></asp:Label>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    Shop Name :
                    <asp:Label ID="lbshopname" runat="server" Text="-"></asp:Label>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-6 col-lg-6" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Label ID="Label1" runat="server" Text="บันทึกยอดขายสิ้นวัน (Daily Close date)"></asp:Label>
                    &nbsp;
                    <asp:Button ID="btnadd" runat="server" Text="กดบันทึกยอด (Press)" OnClick="btnadd_Click" class="btn ThemeBtn" />
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
            </div>
            <hr />

            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    สถานะการส่งยอดขาย (Status)
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    จากวันที่ (From)
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    ถึงวันที่ (To)
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlstatus" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtstartdate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>

                    <asp:CalendarExtender ID="txtstartdate_CalendarExtender" runat="server"
                        BehaviorID="txtstartdate_CalendarExtender" TargetControlID="txtstartdate" Format="dd/MM/yyyy"></asp:CalendarExtender>

                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtenddate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>

                    <asp:CalendarExtender ID="txtenddate_CalendarExtender" runat="server"
                        BehaviorID="txtenddate_CalendarExtender" TargetControlID="txtenddate" Format="dd/MM/yyyy"></asp:CalendarExtender>

                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn"  Width="114px" />
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6 col-lg-6" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="btnselect_all" runat="server" Text="เลือกทั้งหมด" OnClick="btnselect_all_Click" ForeColor="Black" class="btn btn-info" Visible="false" />
                    <asp:Button ID="btndisselect_all" runat="server" Text="ยกเลิกการเลือกทั้งหมด" OnClick="btndisselect_all_Click" ForeColor="Black" class="btn btn-danger" Visible="false" />
                    <asp:Button ID="btnconfirm" runat="server" Text="ยืนยันยอด" OnClick="btnconfirm_Click" ForeColor="Black" class="btn btn-success" Width="114px" Visible="false" />
                </div>
                <div class="col-md-6 col-lg-6" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView_List_RowDataBound"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="record_date" HeaderText="วันที่ส่งยอด" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="product_amount" HeaderText="ยอดขายสินค้า (บาท)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                            <asp:BoundField DataField="service_serCharge_amount" HeaderText="ยอดค่าบริการ (บาท)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                            <asp:BoundField DataField="status" HeaderText="สถานะ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="flag_confirm" HeaderText="สถานะยืนยัน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_ContractNumber" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:HiddenField ID="hdd_status" runat="server" Value='<%# Eval("status") %>' />
                                    <asp:HiddenField ID="hdd_flag_confirm" runat="server" Value='<%# Eval("flag_confirm") %>' />
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("id") %>' />

                                    <asp:Button ID="btndetail" runat="server" Text="รายละเอียด" class="btn ThemeBtn" OnClick="btndetail_Click" Visible="true"  />

                                     <asp:Button ID="btnedit" runat="server" Text="แก้ไขยอด" class="btn ThemeBtn" OnClick="btnedit_Click" Visible="true" />

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GridView_List_confirm" runat="server" AutoGenerateColumns="false"
                        ShowFooter="false" PageSize="50" class="table mt-3" OnRowDataBound="GridView_List_confirm_RowDataBound">
                        <Columns>

                            <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="record_date" HeaderText="วันที่ส่งยอด" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="product_amount" HeaderText="ยอดขายสินค้า (บาท)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                            <asp:BoundField DataField="service_serCharge_amount" HeaderText="ยอดค่าบริการ (บาท)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                            <asp:BoundField DataField="status" HeaderText="สถานะ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="flag_confirm" HeaderText="สถานะยืนยัน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_ContractNumber" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("id") %>' />
                                    <asp:HiddenField ID="hdd_flag_confirm" runat="server" Value='<%# Eval("flag_confirm") %>' />
                                    <asp:HiddenField ID="hdd_status" runat="server" Value='<%# Eval("status") %>' />

                                    <asp:Button ID="btndetail" runat="server" Text="รายละเอียด" class="btn ThemeBtn" OnClick="btndetail_Click" Visible="true" />

                                    <asp:Button ID="btnedit" runat="server" Text="แก้ไขยอด" class="btn ThemeBtn" OnClick="btnedit_Click" Visible="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
            </div>
        </div>
    </div>



</asp:Content>
