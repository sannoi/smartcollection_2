﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CompanyGroupMgm_edit.aspx.cs" Inherits="iconsiam.CompanyGroupMgm_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }
    </script>

    <link rel="stylesheet" href="assets/css/demo.css" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Edit Company Group / Add Mapping
            </p>
        </div>
    </div>
    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">

            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-9">
                            <asp:Image ID="imgCompany" runat="server" Width="150px" ImageUrl="~/img/tmp_img.png" />
                        </div>
                        <div class="col-md-1 col-lg-1">
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-9">
                            <asp:FileUpload ID="fileupload_logo" runat="server" />
                        </div>
                        <div class="col-md-1 col-lg-1">
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <asp:TextBox ID="txtGroupName" runat="server" placeholder="Group Company Name" class="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <asp:DropDownList ID="ddlstatus" runat="server" class="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-lg-12" style="text-align: right;">
                            <asp:Button ID="btnupdateinfo" runat="server" Text="Save" OnClick="btnupdateinfo_Click" ForeColor="White" class="btn btn-success" />
                            <asp:Button ID="btnback" runat="server" Text="Back" OnClick="btnback_Click" ForeColor="White" class="btn btn-warning" />
                            <%--<asp:Button ID="btnsave" runat="server" Text="Save Mapping" OnClick="btnsave_Click" ForeColor="White" class="btn btn-info" />--%>
                        </div>
                    </div>
                    <br />
                </div>
                <%--<div class="col-md-7">
                    
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                ShowFooter="false" PageSize="50" class="table mt-3">
                                <Columns>

                                    <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#F2F3F8">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdd_CompanyCode" runat="server" Value='<%# Eval("CompanyCode") %>' />
                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="CompanyNameTH" HeaderText="Company Name" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-BackColor="#F2F3F8" ItemStyle-HorizontalAlign="Left" />

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>--%>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <asp:GridView ID="GridView_mapp" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        ShowFooter="false" PageSize="50" class="table mt-3" OnPageIndexChanging="GridView_List_PageIndexChanging"
                        OnRowDataBound="GridView_mapp_RowDataBound">
                        <Columns>

                            <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#F2F3F8">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddcheckNo" runat="server" Value='<%# Eval("chkNo") %>' />
                                    <asp:HiddenField ID="hddcheck" runat="server" Value='<%# Eval("chk") %>' />
                                    <asp:HiddenField ID="hddmapp_id" runat="server" Value='<%# Eval("Id") %>' />
                                    <asp:HiddenField ID="hdd_CompanyCode" runat="server" Value='<%# Eval("CompanyCode") %>' />
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="CompanyCode" HeaderText="Company Code" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-BackColor="#F2F3F8" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="CompanyNameTH" HeaderText="Company Name" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-BackColor="#F2F3F8" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="CompanyNameEn" HeaderText="Company Name(En)" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-BackColor="#F2F3F8" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="ComGroupName" HeaderText="Group Name" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-BackColor="#F2F3F8" ItemStyle-HorizontalAlign="Left" />


                        </Columns>
                    </asp:GridView>
                </div>
            </div>

        </div>
    </div>




</asp:Content>
