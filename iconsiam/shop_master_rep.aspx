﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="shop_master_rep.aspx.cs" Inherits="iconsiam.shop_master_rep" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />
    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Shop master report
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlcompany" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtshopgroup" runat="server" placeholder="Shop Group Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtshopname" runat="server" placeholder="Shop Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlstatus" runat="server" placeholder="Status" class="form-control"></asp:DropDownList>
                </div>

                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;"></div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;"></div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;"></div>

                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click"  class="btn ThemeBtn" Width ="25%"  />
                    <asp:Button ID="btnexport" runat="server" Text="Export" OnClick="btnexport_Click"  class="btn ThemeBtn"  Width ="25%"  />
                    <asp:Button ID="btncancel" runat="server" Text="Clear" OnClick="btncancel_Click" class="btn ThemeBtn" Width ="25%"  />
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlar" runat="server" class="form-control" Visible="false"></asp:DropDownList>
                </div>
                <%-- <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>--%>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="smart_room_no" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="startdate" HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="enddate" HeaderText="End Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="smart_record_type" HeaderText="Record Type" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />



                        </Columns>
                    </asp:GridView>


                    <asp:GridView ID="GridView_List2" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="smart_room_no" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="arname" HeaderText="AR" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="startdate" HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="enddate" HeaderText="End Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="smart_record_type" HeaderText="Record Type" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                        </Columns>
                    </asp:GridView>

                </div>
            </div>


        </div>
    </div>

    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" Visible="true"
            ShowFooter="false" class="table table-bordered">
            <Columns>

                <asp:BoundField DataField="ContractNumber" HeaderText="ContractNumber" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="CompanyCode" HeaderText="CompanyCode" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="CompanyNameTH" HeaderText="CompanyNameTH" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="smart_room_no" HeaderText="smart_room_no" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="ShopGroupNameEN" HeaderText="ShopGroupNameEN" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="ShopName" HeaderText="ShopName" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="BusinessPartnerName" HeaderText="BusinessPartnerName" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="smart_record_type" HeaderText="Record Type" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="status" HeaderText="status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="smart_update_date" HeaderText="smart_update_date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="username" HeaderText="username" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="arname" HeaderText="arname" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="startdate" HeaderText="startdate" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="enddate" HeaderText="enddate" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="smart_building" HeaderText="smart_building" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="ContractTypeNameEn" HeaderText="ContractTypeNameEn" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="ContractTypeDescription" HeaderText="ContractTypeDescription" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="SalesTypeName" HeaderText="SalesTypeName" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="SubTypeCode" HeaderText="SubTypeCode" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="SubTypeName" HeaderText="SubTypeName" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="aename" HeaderText="aename" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="CategoryleasingNameEN" HeaderText="CategoryleasingNameEN" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="IndustryGroupNameEN" HeaderText="IndustryGroupNameEN" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="smart_floor" HeaderText="smart_floor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="smart_usage_name" HeaderText="smart_usage_name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="smart_sqm" HeaderText="smart_sqm" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="GrouplocationNameTH" HeaderText="GrouplocationNameTH" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="ContactPointName" HeaderText="ContactPointName" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="ContactPointEmail" HeaderText="ContactPointEmail" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="ContactPointTel" HeaderText="ContactPointTel" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />


            </Columns>
        </asp:GridView>
    </asp:Panel>

</asp:Content>
