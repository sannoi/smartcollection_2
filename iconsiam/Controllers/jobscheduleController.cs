﻿using iconsiam.App_Code;
using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;

namespace iconsiam.Controllers
{
    public class time_req
    {
        public string time { get; set; }
    }

    public class datetime_req
    {
        public string date1 { get; set; }
        public string time { get; set; }
    }
    public class jobscheduleController : ApiController
    {
        [Route("api/dailycheck_job")]
        public void Post()
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //result x = new result();
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //dailycheck_jobDLL Serv2 = new dailycheck_jobDLL();
            //sentmail mail = new sentmail();
            //LogService l = new LogService();

            //int time = 1 * 60 * 1000;



            //while (true)
            //{
            //    l.Log(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI) + ": Job Start", "dailycheck_job");
            //    var job = Serv.getTime("daily_endofday");

            //    try
            //    {
            //        string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //        string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //        int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //        if (result > 0)
            //        {

            //            time = Convert.ToInt32(job.Rows[0]["jobrecure_min"].ToString()) * 60 * 1000;


            //            int vat = 0;
            //            var v = Serv2.GetVAT();
            //            if (v.Rows.Count != 0)
            //            {
            //                vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
            //            }
            //            else
            //            {
            //                vat = 7;
            //            }
            //            var contract = Serv2.getContract_daily_endofdate();
            //            if (contract.Rows.Count != 0)
            //            {
            //                for (int i = 0; i < contract.Rows.Count; i++)
            //                {
            //                    var trans = Serv2.getTransaction_daily_endofdate(contract.Rows[i]["ContractNumber"].ToString(),
            //                       DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI));


            //                    if (trans.Rows.Count == 0)
            //                    {
            //                        l.Log("Insert Transaction Notification : " + contract.Rows[i]["contractnumber"].ToString() + " " +
            //                             "กรุณากรอกยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI)
            //                             , "dailycheck_job");

            //                        Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n", DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI), ""
            //                                , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());

            //                        Serv2.Insert_Message(contract.Rows[i]["contractnumber"].ToString(), "กรุณากรอกยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI), "y",
            //                             DateTime.Now.ToString("yyyy-MM-dd"));

            //                        if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                        {
            //                            var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                            if (user.Rows.Count != 0)
            //                            {

            //                                mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                    "กรุณานำส่งยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                    "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                    "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่นำส่งยอดขาย", "Link",
            //                                    "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                    contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());

            //                            }
            //                        }
            //                        else
            //                        {
            //                            var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                            if (acc.Rows.Count != 0)
            //                            {
            //                                string name = "";
            //                                string email = "";
            //                                for (int j = 0; j < acc.Rows.Count; j++)
            //                                {
            //                                    name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                    email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                }
            //                                name = name.Substring(0, name.Length - 1);
            //                                email = email.Substring(0, email.Length - 1);


            //                                mail.CallMail_noti_daily(email,
            //                                  "กรุณานำส่งยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                  "Tenant Sales Data Collection", name,
            //                                  "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่นำส่งยอดขาย", "Link",
            //                                  "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                  contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                     DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());



            //                            }
            //                        }
            //                    }
            //                }
            //            }

            //            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "dailycheck_job");
            //            Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "dailycheck_job");

            //    }
            //    Thread.Sleep(time);
            //}
        }

        [Route("api/dailycheck_job_adhoc")]
        public void Post_dailycheck_job_adhoc()
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //result x = new result();
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //dailycheck_jobDLL Serv2 = new dailycheck_jobDLL();
            //sentmail mail = new sentmail();

            //LogService l = new LogService();

            //var job = Serv.getTime("daily_endofday");
            //l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "dailycheck_job_adhoc");

            //try
            //{
            //    string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //    string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //    int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //    if (result > 0)
            //    {

            //        int vat = 0;
            //        var v = Serv2.GetVAT();
            //        if (v.Rows.Count != 0)
            //        {
            //            vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
            //        }
            //        else
            //        {
            //            vat = 7;
            //        }
            //        var contract = Serv2.getContract_daily_endofdate();
            //        if (contract.Rows.Count != 0)
            //        {
            //            for (int i = 0; i < contract.Rows.Count; i++)
            //            {
            //                var trans = Serv2.getTransaction_daily_endofdate(contract.Rows[i]["ContractNumber"].ToString(),
            //                   DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI));


            //                if (trans.Rows.Count == 0)
            //                {
            //                    Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n", DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI), ""
            //                            , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());

            //                    Serv2.Insert_Message(contract.Rows[i]["contractnumber"].ToString(), "กรุณากรอกยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI), "y",
            //                         DateTime.Now.ToString("yyyy-MM-dd"));

            //                    if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                    {
            //                        var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                        if (user.Rows.Count != 0)
            //                        {

            //                            mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                "กรุณานำส่งยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่นำส่งยอดขาย", "Link",
            //                                "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                  DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());

            //                        }
            //                    }
            //                    else
            //                    {
            //                        var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                        if (acc.Rows.Count != 0)
            //                        {
            //                            string name = "";
            //                            string email = "";
            //                            for (int j = 0; j < acc.Rows.Count; j++)
            //                            {
            //                                name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                email = email + acc.Rows[j]["email"].ToString() + ",";

            //                            }
            //                            name = name.Substring(0, name.Length - 1);
            //                            email = email.Substring(0, email.Length - 1);


            //                            mail.CallMail_noti_daily(email,
            //                              "กรุณานำส่งยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                              "Tenant Sales Data Collection", name,
            //                              "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่นำส่งยอดขาย", "Link",
            //                              "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                              contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                 DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());



            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "dailycheck_job_adhoc");

            //        Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "dailycheck_job_adhoc");

            //}
        }

        [Route("api/dailycheck_job_next")]
        public void Post_next(time_req info)
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //result x = new result();
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //dailycheck_jobDLL Serv2 = new dailycheck_jobDLL();
            //sentmail mail = new sentmail();
            //LogService l = new LogService();

            //int time = 1 * 60 * 1000;

            //while (true)
            //{

            //    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "dailycheck_job_next");
            //    DataTable job = new DataTable();

            //    if (info.time == "09:00")
            //    {
            //        job = Serv.getTime("daily_nextday_900");
            //    }
            //    else if (info.time == "09:30")
            //    {
            //        job = Serv.getTime("daily_nextday_930");
            //    }
            //    else if (info.time == "10:00")
            //    {
            //        job = Serv.getTime("daily_nextday_1000");
            //    }
            //    else if (info.time == "10:30")
            //    {
            //        job = Serv.getTime("daily_nextday_1030");
            //    }

            //    try
            //    {
            //        string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //        string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //        int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //        if (result > 0)
            //        {
            //            time = Convert.ToInt32(job.Rows[0]["jobrecure_min"].ToString()) * 60 * 1000;

            //            int vat = 0;
            //            var v = Serv2.GetVAT();
            //            if (v.Rows.Count != 0)
            //            {
            //                vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
            //            }
            //            else
            //            {
            //                vat = 7;
            //            }
            //            var contract = Serv2.getContract_daily_nextday(info.time);
            //            if (contract.Rows.Count != 0)
            //            {
            //                for (int i = 0; i < contract.Rows.Count; i++)
            //                {
            //                    var trans = Serv2.getTransaction_daily_endofdate(contract.Rows[i]["ContractNumber"].ToString(),
            //                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI));

            //                    if (trans.Rows.Count == 0)
            //                    {
            //                        Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n", DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI), ""
            //                                , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());

            //                        Serv2.Insert_Message(contract.Rows[i]["contractnumber"].ToString(), "กรุณากรอกยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI), "y",
            //                             DateTime.Now.ToString("yyyy-MM-dd"));



            //                        if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                        {
            //                            var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                            if (user.Rows.Count != 0)
            //                            {

            //                                mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                   "กรุณานำส่งยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                   "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                   "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่นำส่งยอดขาย", "Link",
            //                                   "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                   contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }
            //                        else
            //                        {
            //                            var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                            if (acc.Rows.Count != 0)
            //                            {
            //                                string name = "";
            //                                string email = "";
            //                                for (int j = 0; j < acc.Rows.Count; j++)
            //                                {
            //                                    name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                    email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                }
            //                                name = name.Substring(0, name.Length - 1);
            //                                email = email.Substring(0, email.Length - 1);


            //                                mail.CallMail_noti_daily(email,
            //                                  "กรุณานำส่งยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                  "Tenant Sales Data Collection", name,
            //                                  "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่นำส่งยอดขาย", "Link",
            //                                  "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                  contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                   DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }
            //                    }
            //                }
            //            }

            //            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "dailycheck_job_next");

            //            Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "dailycheck_job_next");

            //    }
            //    Thread.Sleep(time);
            //}
        }

        [Route("api/dailycheck_job_next_adhoc")]
        public void Post_next_adhoc(time_req info)
        {
            //    CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //    result x = new result();
            //    jobscheduleDLL Serv = new jobscheduleDLL();
            //    dailycheck_jobDLL Serv2 = new dailycheck_jobDLL();
            //    sentmail mail = new sentmail();

            //    LogService l = new LogService();

            //    DataTable job = new DataTable();
            //    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "dailycheck_job_next_adhoc");

            //    if (info.time == "09:00")
            //    {
            //        job = Serv.getTime("daily_nextday_900");
            //    }
            //    else if (info.time == "09:30")
            //    {
            //        job = Serv.getTime("daily_nextday_930");
            //    }
            //    else if (info.time == "10:00")
            //    {
            //        job = Serv.getTime("daily_nextday_1000");
            //    }
            //    else if (info.time == "10:30")
            //    {
            //        job = Serv.getTime("daily_nextday_1030");
            //    }

            //    try
            //    {
            //        string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //        string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //        int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //        if (result > 0)
            //        {
            //            int vat = 0;
            //            var v = Serv2.GetVAT();
            //            if (v.Rows.Count != 0)
            //            {
            //                vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
            //            }
            //            else
            //            {
            //                vat = 7;
            //            }
            //            var contract = Serv2.getContract_daily_nextday(info.time);
            //            if (contract.Rows.Count != 0)
            //            {
            //                for (int i = 0; i < contract.Rows.Count; i++)
            //                {
            //                    var trans = Serv2.getTransaction_daily_endofdate(contract.Rows[i]["ContractNumber"].ToString(),
            //                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI));

            //                    if (trans.Rows.Count == 0)
            //                    {
            //                        Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n", DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI), ""
            //                                , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());

            //                        Serv2.Insert_Message(contract.Rows[i]["contractnumber"].ToString(), "กรุณากรอกยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI), "y",
            //                             DateTime.Now.ToString("yyyy-MM-dd"));



            //                        if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                        {
            //                            var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                            if (user.Rows.Count != 0)
            //                            {

            //                                mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                   "กรุณานำส่งยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                   "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                   "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่นำส่งยอดขาย", "Link",
            //                                   "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                   contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }
            //                        else
            //                        {
            //                            var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                            if (acc.Rows.Count != 0)
            //                            {
            //                                string name = "";
            //                                string email = "";
            //                                for (int j = 0; j < acc.Rows.Count; j++)
            //                                {
            //                                    name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                    email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                }
            //                                name = name.Substring(0, name.Length - 1);
            //                                email = email.Substring(0, email.Length - 1);


            //                                mail.CallMail_noti_daily(email,
            //                                  "กรุณานำส่งยอดขายของวันที่ " + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4) + " ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                  "Tenant Sales Data Collection", name,
            //                                  "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่นำส่งยอดขาย", "Link",
            //                                  "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                  contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                   DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(8, 2) + "/" +
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(5, 2) + "/" +
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI).Substring(0, 4), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }
            //                    }
            //                }
            //            }

            //            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "dailycheck_job_next_adhoc");

            //            Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //        }
            //    }
            //    catch (Exception ex)
            //    {

            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "dailycheck_job_next_adhoc");
            //    }

        }

        [Route("api/syncsap")]
        public void Post_getSAP()
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //result x = new result();
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //smart_collectionDLL Serv2 = new smart_collectionDLL();
            //sentmail mail = new sentmail();
            //LogService l = new LogService();

            //int time = 1 * 60 * 1000;

            //while (true)
            //{

            //    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "syncsap");
            //    var job = Serv.getTime("syncsap");

            //    try
            //    {
            //        string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //        string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //        int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //        if (result > 0)
            //        {
            //            time = Convert.ToInt32(job.Rows[0]["jobrecure_min"].ToString()) * 60 * 1000;

            //            string com = "";
            //            var com_code = Serv2.GetCompanyCode();
            //            if (com_code.Rows.Count != 0)
            //            {
            //                for (int i = 0; i < com_code.Rows.Count; i++)
            //                {
            //                    com = com + "'" + com_code.Rows[i]["CompanyCode"].ToString() + "'" + ",";
            //                }
            //                com = com.Substring(0, com.Length - 1);
            //            }



            //            var SAP_contract = Serv2.getSAPContract(com);

            //            string script_update = "";
            //            string script_create = "";

            //            try
            //            {

            //                if (SAP_contract.Rows.Count != 0)
            //                {
            //                    for (int i = 0; i < SAP_contract.Rows.Count; i++)
            //                    {
            //                        var smart_contract = Serv2.getSmart_Contract(SAP_contract.Rows[i]["ContractNumber"].ToString());
            //                        if (smart_contract.Rows.Count != 0)
            //                        {
            //                            string room_number = "";
            //                            string RentalObjectCode = "";
            //                            decimal sqm = 0;
            //                            var room = Serv2.getSAPRentalObject(SAP_contract.Rows[i]["ContractNumber"].ToString());
            //                            if (room.Rows.Count != 0)
            //                            {
            //                                for (int j = 0; j < room.Rows.Count; j++)
            //                                {
            //                                    room_number = room_number + "" + room.Rows[j]["RentalObjectDescription"].ToString() + ",";
            //                                    sqm = sqm + Convert.ToDecimal(room.Rows[j]["TotalArea"].ToString());

            //                                    if (room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1) == "K")
            //                                    {
            //                                        if (RentalObjectCode == "")
            //                                        {
            //                                            RentalObjectCode = room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1);
            //                                        }
            //                                    }
            //                                }
            //                                room_number = room_number.Substring(0, room_number.Length - 1);
            //                            }

            //                            if (SAP_contract.Rows[i]["ContractEndDate"].ToString() != "")
            //                            {
            //                                script_update = Update_script(SAP_contract.Rows[i]["ContractNumber"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["CompanyCode"].ToString(),
            //                                    SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
            //                                    SAP_contract.Rows[i]["ContractTypeDescription"].ToString().ToString().Replace("'", "''"),
            //                                    Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                    Convert.ToDateTime(SAP_contract.Rows[i]["ContractEndDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                    SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
            //                                    SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
            //                                    SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
            //                                    SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
            //                                    SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
            //                                    SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"),
            //                                    SAP_contract.Rows[i]["OwnCreditCard"].ToString(), SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
            //                                    SAP_contract.Rows[i]["UpdateDate"].ToString(),
            //                                    SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
            //                                    SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm),
            //                                    RentalObjectCode);

            //                            }
            //                            else
            //                            {
            //                                script_update = Update_script(SAP_contract.Rows[i]["ContractNumber"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["CompanyCode"].ToString(),
            //                                       SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
            //                                       SAP_contract.Rows[i]["ContractTypeDescription"].ToString().ToString().Replace("'", "''"),
            //                                       Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                       "",
            //                                       SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
            //                                       SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
            //                                       SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
            //                                       SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
            //                                       SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
            //                                       SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"),
            //                                       SAP_contract.Rows[i]["OwnCreditCard"].ToString(), SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
            //                                       SAP_contract.Rows[i]["UpdateDate"].ToString(),
            //                                       SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
            //                                       SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm),
            //                                       RentalObjectCode);

            //                            }


            //                            Serv2.Execute_query_update(script_update);
            //                        }
            //                        else
            //                        {
            //                            string room_number = "";
            //                            string RentalObjectCode = "";
            //                            string FloorDescription = "";
            //                            decimal sqm = 0;
            //                            var room = Serv2.getSAPRentalObject(SAP_contract.Rows[i]["ContractNumber"].ToString());
            //                            if (room.Rows.Count != 0)
            //                            {

            //                                FloorDescription = room.Rows[0]["FloorDescription"].ToString();

            //                                for (int j = 0; j < room.Rows.Count; j++)
            //                                {

            //                                    room_number += room.Rows[j]["RentalObjectDescription"].ToString() + ",";
            //                                    sqm = sqm + Convert.ToDecimal(room.Rows[j]["TotalArea"].ToString());

            //                                    if (room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1) == "K")
            //                                    {
            //                                        if (RentalObjectCode == "")
            //                                        {
            //                                            RentalObjectCode = room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1);
            //                                        }
            //                                    }
            //                                }

            //                                room_number = room_number.Substring(0, room_number.Length - 1);

            //                            }

            //                            if (SAP_contract.Rows[i]["ContractEndDate"].ToString() != "")
            //                            {
            //                                script_create = Insert_script(SAP_contract.Rows[i]["ContractNumber"].ToString(), SAP_contract.Rows[i]["CompanyCode"].ToString(),
            //                                          SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
            //                                          SAP_contract.Rows[i]["ContractTypeDescription"].ToString().Replace("'", "''"),
            //                                          Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                          Convert.ToDateTime(SAP_contract.Rows[i]["ContractEndDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                          SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
            //                                          SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
            //                                          SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
            //                                          SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
            //                                          SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
            //                                          SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["OwnCreditCard"].ToString(),
            //                                          SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
            //                                           Convert.ToDateTime(SAP_contract.Rows[i]["CreatedDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                           SAP_contract.Rows[i]["UpdateDate"].ToString(),
            //                                          SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
            //                                          SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm), "", "", "", "", "", "", "",
            //                                          "", "", "", "", "", "", "", "", "", "", "", "",
            //                                          "", "", "", "",
            //                                          RentalObjectCode, FloorDescription);

            //                            }
            //                            else
            //                            {
            //                                script_create = Insert_script(SAP_contract.Rows[i]["ContractNumber"].ToString(), SAP_contract.Rows[i]["CompanyCode"].ToString(),
            //                                          SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
            //                                          SAP_contract.Rows[i]["ContractTypeDescription"].ToString().Replace("'", "''"),
            //                                          Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                          "",
            //                                          SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
            //                                          SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
            //                                          SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
            //                                          SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
            //                                          SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
            //                                          SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["OwnCreditCard"].ToString(),
            //                                          SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
            //                                           Convert.ToDateTime(SAP_contract.Rows[i]["CreatedDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                           SAP_contract.Rows[i]["UpdateDate"].ToString(),
            //                                          SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
            //                                          SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm), "", "", "", "", "", "", "",
            //                                          "", "", "", "", "", "", "", "", "", "", "", "",
            //                                          "", "", "", "",
            //                                          RentalObjectCode, FloorDescription);

            //                            }


            //                            Serv2.Execute_query_insert(script_create);

            //                        }
            //                    }
            //                }
            //            }
            //            catch (Exception ex)
            //            {
            //                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "syncsap");
            //            }

            //            /// Sent Email ///
            //            /// 
            //            mail.CallMai_new_contract(DateTime.Now.ToString("yyyy-MM-dd", EngCI), "icon");


            //            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "syncsap");


            //            Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "syncsap");
            //    }
            //    Thread.Sleep(time);
            //}
        }

        [Route("api/syncsap_adhoc")]
        public void Post_getSAP_achoc()
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //result x = new result();
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //smart_collectionDLL Serv2 = new smart_collectionDLL();
            //sentmail mail = new sentmail();
            //LogService l = new LogService();

            ////int time = 1 * 60 * 1000;

            //var job = Serv.getTime("syncsap");
            //l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "syncsap_adhoc");

            //try
            //{
            //    string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //    string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //    int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //    if (result > 0)
            //    {
            //        //time = Convert.ToInt32(job.Rows[0]["jobrecure_min"].ToString()) * 60 * 1000;

            //        string com = "";
            //        var com_code = Serv2.GetCompanyCode();
            //        if (com_code.Rows.Count != 0)
            //        {
            //            for (int i = 0; i < com_code.Rows.Count; i++)
            //            {
            //                com = com + "'" + com_code.Rows[i]["CompanyCode"].ToString() + "'" + ",";
            //            }
            //            com = com.Substring(0, com.Length - 1);
            //        }



            //        var SAP_contract = Serv2.getSAPContract(com);

            //        string script_update = "";
            //        string script_create = "";

            //        try
            //        {

            //            if (SAP_contract.Rows.Count != 0)
            //            {
            //                for (int i = 0; i < SAP_contract.Rows.Count; i++)
            //                {
            //                    var smart_contract = Serv2.getSmart_Contract(SAP_contract.Rows[i]["ContractNumber"].ToString());
            //                    if (smart_contract.Rows.Count != 0)
            //                    {
            //                        string room_number = "";
            //                        string RentalObjectCode = "";
            //                        decimal sqm = 0;
            //                        var room = Serv2.getSAPRentalObject(SAP_contract.Rows[i]["ContractNumber"].ToString());
            //                        if (room.Rows.Count != 0)
            //                        {
            //                            for (int j = 0; j < room.Rows.Count; j++)
            //                            {
            //                                room_number = room_number + "" + room.Rows[j]["RentalObjectDescription"].ToString() + ",";
            //                                sqm = sqm + Convert.ToDecimal(room.Rows[j]["TotalArea"].ToString());

            //                                if (room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1) == "K")
            //                                {
            //                                    if (RentalObjectCode == "")
            //                                    {
            //                                        RentalObjectCode = room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1);
            //                                    }
            //                                }
            //                            }
            //                            room_number = room_number.Substring(0, room_number.Length - 1);
            //                        }

            //                        if (SAP_contract.Rows[i]["ContractEndDate"].ToString() != "")
            //                        {
            //                            script_update = Update_script(SAP_contract.Rows[i]["ContractNumber"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["CompanyCode"].ToString(),
            //                                SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
            //                                SAP_contract.Rows[i]["ContractTypeDescription"].ToString().ToString().Replace("'", "''"),
            //                                Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                Convert.ToDateTime(SAP_contract.Rows[i]["ContractEndDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
            //                                SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
            //                                SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
            //                                SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
            //                                SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
            //                                SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"),
            //                                SAP_contract.Rows[i]["OwnCreditCard"].ToString(), SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
            //                                SAP_contract.Rows[i]["UpdateDate"].ToString(),
            //                                SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
            //                                SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm),
            //                                RentalObjectCode);

            //                        }
            //                        else
            //                        {
            //                            script_update = Update_script(SAP_contract.Rows[i]["ContractNumber"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["CompanyCode"].ToString(),
            //                                   SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
            //                                   SAP_contract.Rows[i]["ContractTypeDescription"].ToString().ToString().Replace("'", "''"),
            //                                   Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                   "",
            //                                   SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
            //                                   SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
            //                                   SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
            //                                   SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
            //                                   SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
            //                                   SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"),
            //                                   SAP_contract.Rows[i]["OwnCreditCard"].ToString(), SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
            //                                   SAP_contract.Rows[i]["UpdateDate"].ToString(),
            //                                   SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
            //                                   SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm),
            //                                   RentalObjectCode);

            //                        }


            //                        Serv2.Execute_query_update(script_update);
            //                    }
            //                    else
            //                    {
            //                        string room_number = "";
            //                        string RentalObjectCode = "";
            //                        string FloorDescription = "";
            //                        decimal sqm = 0;
            //                        var room = Serv2.getSAPRentalObject(SAP_contract.Rows[i]["ContractNumber"].ToString());
            //                        if (room.Rows.Count != 0)
            //                        {

            //                            FloorDescription = room.Rows[0]["FloorDescription"].ToString();

            //                            for (int j = 0; j < room.Rows.Count; j++)
            //                            {

            //                                room_number += room.Rows[j]["RentalObjectDescription"].ToString() + ",";
            //                                sqm = sqm + Convert.ToDecimal(room.Rows[j]["TotalArea"].ToString());

            //                                if (room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1) == "K")
            //                                {
            //                                    if (RentalObjectCode == "")
            //                                    {
            //                                        RentalObjectCode = room.Rows[j]["RentalObjectCode"].ToString().Substring(4, 1);
            //                                    }
            //                                }
            //                            }

            //                            room_number = room_number.Substring(0, room_number.Length - 1);

            //                        }

            //                        if (SAP_contract.Rows[i]["ContractEndDate"].ToString() != "")
            //                        {
            //                            script_create = Insert_script(SAP_contract.Rows[i]["ContractNumber"].ToString(), SAP_contract.Rows[i]["CompanyCode"].ToString(),
            //                                      SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
            //                                      SAP_contract.Rows[i]["ContractTypeDescription"].ToString().Replace("'", "''"),
            //                                      Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                      Convert.ToDateTime(SAP_contract.Rows[i]["ContractEndDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                      SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
            //                                      SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
            //                                      SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
            //                                      SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
            //                                      SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
            //                                      SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["OwnCreditCard"].ToString(),
            //                                      SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
            //                                       Convert.ToDateTime(SAP_contract.Rows[i]["CreatedDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                       SAP_contract.Rows[i]["UpdateDate"].ToString(),
            //                                      SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
            //                                      SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm), "", "", "", "", "", "", "",
            //                                      "", "", "", "", "", "", "", "", "", "", "", "",
            //                                      "", "", "", "",
            //                                      RentalObjectCode, FloorDescription);

            //                        }
            //                        else
            //                        {
            //                            script_create = Insert_script(SAP_contract.Rows[i]["ContractNumber"].ToString(), SAP_contract.Rows[i]["CompanyCode"].ToString(),
            //                                      SAP_contract.Rows[i]["ContractTypeCode"].ToString(),
            //                                      SAP_contract.Rows[i]["ContractTypeDescription"].ToString().Replace("'", "''"),
            //                                      Convert.ToDateTime(SAP_contract.Rows[i]["ContractStartDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                      "",
            //                                      SAP_contract.Rows[i]["ShopName"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["IndustryCode"].ToString().ToString().Replace("'", "''"),
            //                                      SAP_contract.Rows[i]["BusinessPartnerCode"].ToString().ToString().Replace("'", "''"),
            //                                      SAP_contract.Rows[i]["BusinessPartnerName"].ToString().ToString().Replace("'", "''"),
            //                                      SAP_contract.Rows[i]["ContractTerm"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["SubTypeCode"].ToString(),
            //                                      SAP_contract.Rows[i]["SubTypeName"].ToString().ToString().Replace("'", "''"),
            //                                      SAP_contract.Rows[i]["OwnCashier"].ToString().ToString().Replace("'", "''"), SAP_contract.Rows[i]["OwnCreditCard"].ToString(),
            //                                      SAP_contract.Rows[i]["RelevantToSales"].ToString().ToString().Replace("'", "''"),
            //                                       Convert.ToDateTime(SAP_contract.Rows[i]["CreatedDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss", EngCI),
            //                                       SAP_contract.Rows[i]["UpdateDate"].ToString(),
            //                                      SAP_contract.Rows[i]["IsActive"].ToString(), SAP_contract.Rows[i]["SalesTypeCode"].ToString(),
            //                                      SAP_contract.Rows[i]["SalesTypeName"].ToString().ToString().Replace("'", "''"), room_number, Convert.ToString(sqm), "", "", "", "", "", "", "",
            //                                      "", "", "", "", "", "", "", "", "", "", "", "",
            //                                      "", "", "", "",
            //                                      RentalObjectCode, FloorDescription);

            //                        }


            //                        Serv2.Execute_query_insert(script_create);

            //                    }
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "syncsap_adhoc");

            //        }

            //        /// Sent Email ///
            //        /// 
            //        mail.CallMai_new_contract(DateTime.Now.ToString("yyyy-MM-dd", EngCI), "icon");

            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "syncsap_adhoc");

            //        Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));

            //    }
            //}
            //catch (Exception ex)
            //{
            //    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "syncsap_adhoc");
            //}
        }

        [Route("api/weekly")]
        public void Post_weekly(time_req info)
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //result x = new result();
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //dailycheck_jobDLL Serv2 = new dailycheck_jobDLL();
            //sentmail mail = new sentmail();
            //LogService l = new LogService();

            //int time = 1 * 60 * 1000;
            //while (true)
            //{

            //    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "weekly");
            //    DataTable job = new DataTable();

            //    if (info.time == "09:00")
            //    {
            //        job = Serv.getTime("weekly_900");
            //    }
            //    else if (info.time == "09:30")
            //    {
            //        job = Serv.getTime("weekly_930");
            //    }
            //    else if (info.time == "10:00")
            //    {
            //        job = Serv.getTime("weekly_1000");
            //    }
            //    else if (info.time == "10:30")
            //    {
            //        job = Serv.getTime("weekly_1030");
            //    }


            //    try
            //    {
            //        string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //        string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //        int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //        if (result > 0)
            //        {
            //            time = Convert.ToInt32(job.Rows[0]["jobrecure_min"].ToString()) * 60 * 1000;

            //            int vat = 0;
            //            var v = Serv2.GetVAT();
            //            if (v.Rows.Count != 0)
            //            {
            //                vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
            //            }
            //            else
            //            {
            //                vat = 7;
            //            }

            //            var contract = Serv2.getContract_weekly(DateTime.Now.ToString("ddd", EngCI), info.time);
            //            if (contract.Rows.Count != 0)
            //            {
            //                for (int i = 0; i < contract.Rows.Count; i++)
            //                {
            //                    var trans = Serv2.getTransaction_BackDay(contract.Rows[i]["ContractNumber"].ToString(),
            //                         DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI));

            //                    if (trans.Rows.Count == 0)
            //                    {
            //                        var trans2 = Serv2.getTransaction_LastDate(contract.Rows[i]["ContractNumber"].ToString());
            //                        if (trans2.Rows.Count != 0)
            //                        {
            //                            string d3 = Convert.ToDateTime(trans2.Rows[i]["record_date"]).ToString("yyyy-MM-dd 00:00:00", EngCI);
            //                            string d4 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00", EngCI);

            //                            double result2 = (Convert.ToDateTime(d4) - Convert.ToDateTime(d3)).TotalDays;

            //                            for (int j = 1; j <= result2; j++)
            //                            {
            //                                Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n",
            //                                        Convert.ToDateTime(trans2.Rows[i]["record_date"]).AddDays(j).ToString("yyyy-MM-dd 00:00:00", EngCI), ""
            //                                        , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());
            //                            }

            //                            Serv2.Insert_Message(contract.Rows[i]["contractnumber"].ToString(), "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน", "y", DateTime.Now.ToString("yyyy-MM-dd"));

            //                            if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                            {
            //                                var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                                if (user.Rows.Count != 0)
            //                                {


            //                                    mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                       "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                       "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                       "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                       "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                       contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                       contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());

            //                                }
            //                            }
            //                            else
            //                            {
            //                                var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                                if (acc.Rows.Count != 0)
            //                                {
            //                                    string name = "";
            //                                    string email = "";
            //                                    for (int j = 0; j < acc.Rows.Count; j++)
            //                                    {
            //                                        name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                        email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                    }
            //                                    name = name.Substring(0, name.Length - 1);
            //                                    email = email.Substring(0, email.Length - 1);


            //                                    mail.CallMail_noti_daily(email,
            //                                      "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                      "Tenant Sales Data Collection", name,
            //                                      "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                      "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                      contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                      contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                                }
            //                            }

            //                        }
            //                        else
            //                        {
            //                            Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n",
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00", EngCI), ""
            //                                        , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());

            //                            Serv2.Insert_Message(contract.Rows[i]["contractnumber"].ToString(), "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน", "y", DateTime.Now.ToString("yyyy-MM-dd"));

            //                            if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                            {
            //                                var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                                if (user.Rows.Count != 0)
            //                                {

            //                                    mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                    "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                    "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                    "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                    "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                    contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                    contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());
            //                                }
            //                            }
            //                            else
            //                            {
            //                                var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                                if (acc.Rows.Count != 0)
            //                                {
            //                                    string name = "";
            //                                    string email = "";
            //                                    for (int j = 0; j < acc.Rows.Count; j++)
            //                                    {
            //                                        name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                        email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                    }
            //                                    name = name.Substring(0, name.Length - 1);
            //                                    email = email.Substring(0, email.Length - 1);


            //                                    mail.CallMail_noti_daily(email,
            //                                      "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                      "Tenant Sales Data Collection", name,
            //                                      "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                      "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                      contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                      contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                                }
            //                            }
            //                        }

            //                    }
            //                }
            //            }

            //            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "weekly");

            //            Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "weekly");
            //    }
            //    Thread.Sleep(time);
            //}
        }

        [Route("api/weekly_adhoc")]
        public void Post_weekly_adhoc(time_req info)
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //result x = new result();
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //dailycheck_jobDLL Serv2 = new dailycheck_jobDLL();
            //sentmail mail = new sentmail();

            //LogService l = new LogService();

            //l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "start", "weekly_adhoc");
            //DataTable job = new DataTable();

            //if (info.time == "09:00")
            //{
            //    job = Serv.getTime("weekly_900");
            //}
            //else if (info.time == "09:30")
            //{
            //    job = Serv.getTime("weekly_930");
            //}
            //else if (info.time == "10:00")
            //{
            //    job = Serv.getTime("weekly_1000");
            //}
            //else if (info.time == "10:30")
            //{
            //    job = Serv.getTime("weekly_1030");
            //}


            //try
            //{
            //    string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //    string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //    int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //    if (result > 0)
            //    {

            //        int vat = 0;
            //        var v = Serv2.GetVAT();
            //        if (v.Rows.Count != 0)
            //        {
            //            vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
            //        }
            //        else
            //        {
            //            vat = 7;
            //        }

            //        var contract = Serv2.getContract_weekly(DateTime.Now.ToString("ddd", EngCI), info.time);
            //        if (contract.Rows.Count != 0)
            //        {
            //            for (int i = 0; i < contract.Rows.Count; i++)
            //            {
            //                var trans = Serv2.getTransaction_BackDay(contract.Rows[i]["ContractNumber"].ToString(),
            //                     DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI));

            //                if (trans.Rows.Count == 0)
            //                {
            //                    var trans2 = Serv2.getTransaction_LastDate(contract.Rows[i]["ContractNumber"].ToString());
            //                    if (trans2.Rows.Count != 0)
            //                    {
            //                        string d3 = Convert.ToDateTime(trans2.Rows[i]["record_date"]).ToString("yyyy-MM-dd 00:00:00", EngCI);
            //                        string d4 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00", EngCI);

            //                        double result2 = (Convert.ToDateTime(d4) - Convert.ToDateTime(d3)).TotalDays;

            //                        for (int j = 1; j <= result2; j++)
            //                        {
            //                            Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n",
            //                                    Convert.ToDateTime(trans2.Rows[i]["record_date"]).AddDays(j).ToString("yyyy-MM-dd 00:00:00", EngCI), ""
            //                                    , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());
            //                        }

            //                        Serv2.Insert_Message(contract.Rows[i]["contractnumber"].ToString(), "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน", "y", DateTime.Now.ToString("yyyy-MM-dd"));

            //                        if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                        {
            //                            var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                            if (user.Rows.Count != 0)
            //                            {


            //                                mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                   "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                   "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                   "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                   "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                   contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                   contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());

            //                            }
            //                        }
            //                        else
            //                        {
            //                            var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                            if (acc.Rows.Count != 0)
            //                            {
            //                                string name = "";
            //                                string email = "";
            //                                for (int j = 0; j < acc.Rows.Count; j++)
            //                                {
            //                                    name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                    email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                }
            //                                name = name.Substring(0, name.Length - 1);
            //                                email = email.Substring(0, email.Length - 1);


            //                                mail.CallMail_noti_daily(email,
            //                                  "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                  "Tenant Sales Data Collection", name,
            //                                  "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                  "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                  contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                  contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }

            //                    }
            //                    else
            //                    {
            //                        Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n",
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00", EngCI), ""
            //                                    , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());

            //                        Serv2.Insert_Message(contract.Rows[i]["contractnumber"].ToString(), "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน", "y", DateTime.Now.ToString("yyyy-MM-dd"));

            //                        if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                        {
            //                            var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                            if (user.Rows.Count != 0)
            //                            {

            //                                mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }
            //                        else
            //                        {
            //                            var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                            if (acc.Rows.Count != 0)
            //                            {
            //                                string name = "";
            //                                string email = "";
            //                                for (int j = 0; j < acc.Rows.Count; j++)
            //                                {
            //                                    name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                    email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                }
            //                                name = name.Substring(0, name.Length - 1);
            //                                email = email.Substring(0, email.Length - 1);


            //                                mail.CallMail_noti_daily(email,
            //                                  "กรุณาส่งยอดขายของประจำสัปดาห์ให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                  "Tenant Sales Data Collection", name,
            //                                  "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                  "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                  contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                  contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }
            //                    }

            //                }
            //            }
            //        }

            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "weekly_adhoc");

            //        Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "weekly_adhoc");
            //}
        }

        [Route("api/monthly")]
        public void Post_monthly(time_req info)
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //result x = new result();
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //dailycheck_jobDLL Serv2 = new dailycheck_jobDLL();
            //sentmail mail = new sentmail();
            //LogService l = new LogService();

            //int time = 1 * 60 * 1000;
            //while (true)
            //{

            //    DataTable job = new DataTable();

            //    if (info.time == "09:00")
            //    {
            //        job = Serv.getTime("monthly_900");
            //    }
            //    else if (info.time == "09:30")
            //    {
            //        job = Serv.getTime("monthly_930");
            //    }
            //    else if (info.time == "10:00")
            //    {
            //        job = Serv.getTime("monthly_1000");
            //    }
            //    else if (info.time == "10:30")
            //    {
            //        job = Serv.getTime("monthly_1030");
            //    }
            //    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "monthly");


            //    try
            //    {
            //        string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //        string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //        int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //        if (result > 0)
            //        {
            //            time = Convert.ToInt32(job.Rows[0]["jobrecure_min"].ToString()) * 60 * 1000;

            //            int vat = 0;
            //            var v = Serv2.GetVAT();
            //            if (v.Rows.Count != 0)
            //            {
            //                vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
            //            }
            //            else
            //            {
            //                vat = 7;
            //            }

            //            var contract = Serv2.getContract_monthly(DateTime.Now.ToString("dd", EngCI), info.time);
            //            if (contract.Rows.Count != 0)
            //            {
            //                for (int i = 0; i < contract.Rows.Count; i++)
            //                {
            //                    var trans = Serv2.getTransaction_BackDay(contract.Rows[i]["ContractNumber"].ToString(), DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI));

            //                    if (trans.Rows.Count == 0)
            //                    {
            //                        var trans2 = Serv2.getTransaction_LastDate(contract.Rows[i]["ContractNumber"].ToString());
            //                        if (trans2.Rows.Count != 0)
            //                        {
            //                            string d3 = Convert.ToDateTime(trans2.Rows[i]["record_date"]).ToString("yyyy-MM-dd 00:00:00", EngCI);
            //                            string d4 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00", EngCI);

            //                            double result2 = (Convert.ToDateTime(d4) - Convert.ToDateTime(d3)).TotalDays;

            //                            for (int j = 1; j <= result2; j++)
            //                            {
            //                                Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n",
            //                                        Convert.ToDateTime(trans2.Rows[i]["record_date"]).AddDays(j).ToString("yyyy-MM-dd 00:00:00", EngCI), ""
            //                                        , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());
            //                            }

            //                            if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                            {
            //                                var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                                if (user.Rows.Count != 0)
            //                                {

            //                                    mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                         "กรุณาส่งยอดขายของประจำเดือนให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                         "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                         "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                         "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                         contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                         contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());
            //                                }
            //                            }
            //                            else
            //                            {
            //                                var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                                if (acc.Rows.Count != 0)
            //                                {
            //                                    string name = "";
            //                                    string email = "";
            //                                    for (int j = 0; j < acc.Rows.Count; j++)
            //                                    {
            //                                        name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                        email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                    }
            //                                    name = name.Substring(0, name.Length - 1);
            //                                    email = email.Substring(0, email.Length - 1);


            //                                    mail.CallMail_noti_daily(email,
            //                                      "กรุณาส่งยอดขายของประจำเดือนให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                      "Tenant Sales Data Collection", name,
            //                                      "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                      "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                      contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                      contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                                }
            //                            }

            //                        }
            //                        else
            //                        {
            //                            Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n",
            //                                        DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00", EngCI), ""
            //                                        , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());


            //                            if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                            {
            //                                var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                                if (user.Rows.Count != 0)
            //                                {

            //                                    mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                         "กรุณาส่งยอดขายของประจำเดือนให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                         "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                         "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                         "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                         contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                         contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());
            //                                }
            //                            }
            //                            else
            //                            {
            //                                var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                                if (acc.Rows.Count != 0)
            //                                {
            //                                    string name = "";
            //                                    string email = "";
            //                                    for (int j = 0; j < acc.Rows.Count; j++)
            //                                    {
            //                                        name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                        email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                    }
            //                                    name = name.Substring(0, name.Length - 1);
            //                                    email = email.Substring(0, email.Length - 1);


            //                                    mail.CallMail_noti_daily(email,
            //                                      "กรุณาส่งยอดขายของประจำเดือนให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                      "Tenant Sales Data Collection", name,
            //                                      "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                      "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                      contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                      contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                                }
            //                            }
            //                        }

            //                    }
            //                }
            //            }

            //            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "monthly");

            //            Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "monthly");
            //    }
            //    Thread.Sleep(time);
            //}
        }

        [Route("api/monthly_adhoc")]
        public void Post_monthly_adhoc(time_req info)
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //result x = new result();
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //dailycheck_jobDLL Serv2 = new dailycheck_jobDLL();
            //sentmail mail = new sentmail();
            //LogService l = new LogService();

            //l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "monthly_adhoc");

            //DataTable job = new DataTable();

            //if (info.time == "09:00")
            //{
            //    job = Serv.getTime("monthly_900");
            //}
            //else if (info.time == "09:30")
            //{
            //    job = Serv.getTime("monthly_930");
            //}
            //else if (info.time == "10:00")
            //{
            //    job = Serv.getTime("monthly_1000");
            //}
            //else if (info.time == "10:30")
            //{
            //    job = Serv.getTime("monthly_1030");
            //}


            //try
            //{
            //    string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //    string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //    int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //    if (result > 0)
            //    {

            //        int vat = 0;
            //        var v = Serv2.GetVAT();
            //        if (v.Rows.Count != 0)
            //        {
            //            vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
            //        }
            //        else
            //        {
            //            vat = 7;
            //        }

            //        var contract = Serv2.getContract_monthly(DateTime.Now.ToString("dd", EngCI), info.time);
            //        if (contract.Rows.Count != 0)
            //        {
            //            for (int i = 0; i < contract.Rows.Count; i++)
            //            {
            //                var trans = Serv2.getTransaction_BackDay(contract.Rows[i]["ContractNumber"].ToString(), DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI));

            //                if (trans.Rows.Count == 0)
            //                {
            //                    var trans2 = Serv2.getTransaction_LastDate(contract.Rows[i]["ContractNumber"].ToString());
            //                    if (trans2.Rows.Count != 0)
            //                    {
            //                        string d3 = Convert.ToDateTime(trans2.Rows[i]["record_date"]).ToString("yyyy-MM-dd 00:00:00", EngCI);
            //                        string d4 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00", EngCI);

            //                        double result2 = (Convert.ToDateTime(d4) - Convert.ToDateTime(d3)).TotalDays;

            //                        for (int j = 1; j <= result2; j++)
            //                        {
            //                            Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n",
            //                                    Convert.ToDateTime(trans2.Rows[i]["record_date"]).AddDays(j).ToString("yyyy-MM-dd 00:00:00", EngCI), ""
            //                                    , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());
            //                        }

            //                        if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                        {
            //                            var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                            if (user.Rows.Count != 0)
            //                            {

            //                                mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                     "กรุณาส่งยอดขายของประจำเดือนให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                     "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                     "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                     "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                     contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                     contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }
            //                        else
            //                        {
            //                            var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                            if (acc.Rows.Count != 0)
            //                            {
            //                                string name = "";
            //                                string email = "";
            //                                for (int j = 0; j < acc.Rows.Count; j++)
            //                                {
            //                                    name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                    email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                }
            //                                name = name.Substring(0, name.Length - 1);
            //                                email = email.Substring(0, email.Length - 1);


            //                                mail.CallMail_noti_daily(email,
            //                                  "กรุณาส่งยอดขายของประจำเดือนให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                  "Tenant Sales Data Collection", name,
            //                                  "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                  "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                  contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                  contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }

            //                    }
            //                    else
            //                    {
            //                        Serv2.Insert_transaction_record(contract.Rows[i]["contractnumber"].ToString(), "0", "0", "n",
            //                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00", EngCI), ""
            //                                    , "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());


            //                        if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
            //                        {
            //                            var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
            //                            if (user.Rows.Count != 0)
            //                            {

            //                                mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
            //                                     "กรุณาส่งยอดขายของประจำเดือนให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                     "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
            //                                     "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                     "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                     contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                     contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }
            //                        else
            //                        {
            //                            var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
            //                            if (acc.Rows.Count != 0)
            //                            {
            //                                string name = "";
            //                                string email = "";
            //                                for (int j = 0; j < acc.Rows.Count; j++)
            //                                {
            //                                    name = name + acc.Rows[j]["name"].ToString() + ",";
            //                                    email = email + acc.Rows[j]["email"].ToString() + ",";

            //                                }
            //                                name = name.Substring(0, name.Length - 1);
            //                                email = email.Substring(0, email.Length - 1);


            //                                mail.CallMail_noti_daily(email,
            //                                  "กรุณาส่งยอดขายของประจำเดือนให้ครบถ้วน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
            //                                  "Tenant Sales Data Collection", name,
            //                                  "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "ยอดขายประจำสัปดาห์", "Link",
            //                                  "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
            //                                  contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
            //                                  contract.Rows[i]["smart_record_weekly_date"].ToString(), "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString());
            //                            }
            //                        }
            //                    }

            //                }
            //            }
            //        }

            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "monthly_adhoc");

            //        Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "monthly_adhoc");
            //}
        }


        [Route("api/confirm_t")]
        public void Post_confirm_transaction()
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //sentmail mail = new sentmail();
            //dateconvert date_con = new dateconvert();
            //LogService l = new LogService();

            //l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "confirm_t");

            //int time = 1 * 60 * 1000;
            //while (true)
            //{
            //    var job = Serv.getTime("jobconfirm");
            //    if (job.Rows.Count != 0)
            //    {
            //        try
            //        {
            //            string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //            string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //            int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //            if (result > 0)
            //            {
            //                time = Convert.ToInt32(job.Rows[0]["jobrecure_min"].ToString()) * 60 * 1000;

            //                var c = Serv.Get_Confirm();
            //                if (c.Rows.Count != 0)
            //                {
            //                    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["first_confirm"]).ToString("0#"))
            //                    {
            //                        string t1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["first_confirm"]).ToString("0#");
            //                        string t2 = DateTime.Now.ToString("yyyy-MM-", EngCI) + "01";
            //                        mail.CallMailConfirm("icon", t1.Substring(8, 2) + "/" + t1.Substring(5, 2) + "/" + t1.Substring(0, 4),
            //                            t2.Substring(8, 2) + "/" + t2.Substring(5, 2) + "/" + t2.Substring(0, 4));

            //                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "confirm_t");


            //                        Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //                    }
            //                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["second_confirm"]).ToString("0#"))
            //                    {
            //                        string t1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["second_confirm"]).ToString("0#");
            //                        string t2 = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-", EngCI) +
            //                            Convert.ToInt32(c.Rows[0]["first_confirm"]).ToString("0#")).AddDays(1).ToString("yyyy-MM-dd");

            //                        mail.CallMailConfirm("icon", t1.Substring(8, 2) + "/" + t1.Substring(5, 2) + "/" + t1.Substring(0, 4),
            //                             t2.Substring(8, 2) + "/" + t2.Substring(5, 2) + "/" + t2.Substring(0, 4));

            //                        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "confirm_t");

            //                        Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //                    }
            //                }


            //            }

            //        }
            //        catch (Exception ex)
            //        {
            //            l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "confirm_t");
            //        }
            //    }

            //    Thread.Sleep(time);
            //}

        }


        [Route("api/confirm_t_adhoc")]
        public void Post_confirm_transaction_adhoc()
        {
            //CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            //jobscheduleDLL Serv = new jobscheduleDLL();
            //sentmail mail = new sentmail();
            //dateconvert date_con = new dateconvert();

            //LogService l = new LogService();

            //l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Start", "confirm_t_adhoc");

            //var job = Serv.getTime("jobconfirm");
            //if (job.Rows.Count != 0)
            //{
            //    try
            //    {
            //        string d1 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI);
            //        string d2 = Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd HH:mm:ss", EngCI);

            //        int result = DateTime.Compare(Convert.ToDateTime(d1), Convert.ToDateTime(d2));

            //        if (result > 0)
            //        {
            //            var c = Serv.Get_Confirm();
            //            if (c.Rows.Count != 0)
            //            {
            //                if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["first_confirm"]).ToString("0#"))
            //                {
            //                    string t1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["first_confirm"]).ToString("0#");
            //                    string t2 = DateTime.Now.ToString("yyyy-MM-", EngCI) + "01";
            //                    mail.CallMailConfirm("icon", t1.Substring(8, 2) + "/" + t1.Substring(5, 2) + "/" + t1.Substring(0, 4),
            //                        t2.Substring(8, 2) + "/" + t2.Substring(5, 2) + "/" + t2.Substring(0, 4));

            //                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "confirm_t_adhoc");

            //                    Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //                }
            //                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["second_confirm"]).ToString("0#"))
            //                {
            //                    string t1 = DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["second_confirm"]).ToString("0#");
            //                    string t2 = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-", EngCI) +
            //                        Convert.ToInt32(c.Rows[0]["first_confirm"]).ToString("0#")).AddDays(1).ToString("yyyy-MM-dd");

            //                    mail.CallMailConfirm("icon", t1.Substring(8, 2) + "/" + t1.Substring(5, 2) + "/" + t1.Substring(0, 4),
            //                         t2.Substring(8, 2) + "/" + t2.Substring(5, 2) + "/" + t2.Substring(0, 4));

            //                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "confirm_t_adhoc");

            //                    Serv.UpdateJobNext_date(job.Rows[0]["id"].ToString(), Convert.ToDateTime(job.Rows[0]["jobnext_datetime"]).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss", EngCI));
            //                }


            //            }


            //        }

            //    }
            //    catch (Exception ex)
            //    {
            //        l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "confirm_t_adhoc");
            //    }
            //}


        }


        ///============================================================================================================================///
        ///
        private static string Update_script(string ContractNumber, string CompanyCode, string ContractTypeCode, string ContractTypeDescription, string ContractStartDate, string ContractEndDate, string ShopName,
             string IndustryCode, string BusinessPartnerCode, string BusinessPartnerName, string ContractTerm, string SubTypeCode, string SubTypeName, string OwnCashier, string OwnCreditCard,
             string RelevantToSales, string UpdateDate, string IsActive, string SalesTypeCode, string SalesTypeName, string smart_room_no, string smart_sqm, string smart_object_rental_code)
        {
            var str = " update tblsmart_Contract " +
                        " set CompanyCode = '" + CompanyCode + "',  " +
                        " ContractTypeCode = '" + ContractTypeCode + "', " +
                        " ContractTypeDescription = '" + ContractTypeDescription + "', " +
                        " ContractStartDate = '" + ContractStartDate + "', " +
                        " ContractEndDate = '" + ContractEndDate + "', " +
                        " ShopName = '" + ShopName + "', " +
                        " IndustryCode = '" + IndustryCode + "', " +
                        " BusinessPartnerCode = '" + BusinessPartnerCode + "', " +
                        " BusinessPartnerName = '" + BusinessPartnerName + "', " +
                        " ContractTerm = '" + ContractTerm + "', " +
                        " SubTypeCode = '" + SubTypeCode + "', " +
                        " SubTypeName = '" + SubTypeName + "', " +
                        " OwnCashier = '" + OwnCashier + "', " +
                        " OwnCreditCard = '" + OwnCreditCard + "', " +
                        " RelevantToSales = '" + RelevantToSales + "', " +
                        " UpdateDate = '" + UpdateDate + "', " +
                        " IsActive = '" + IsActive + "', " +
                        " SalesTypeCode = '" + SalesTypeCode + "', " +
                        " SalesTypeName = '" + SalesTypeName + "', " +

                        " smart_room_no = '" + smart_room_no + "', " +
                        " smart_sqm = '" + smart_sqm + "', " +
                        " smart_update_date = GETDATE(), " +
                        " smart_object_rental_code = '" + smart_object_rental_code + "' " +

                        " where ContractNumber = '" + ContractNumber + "' ;  ";




            return str;
        }

        private static string Insert_script(string ContractNumber, string CompanyCode, string ContractTypeCode, string ContractTypeDescription, string ContractStartDate, string ContractEndDate,
            string ShopName, string IndustryCode, string BusinessPartnerCode, string BusinessPartnerName, string ContractTerm, string SubTypeCode, string SubTypeName, string OwnCashier, string OwnCreditCard,
            string RelevantToSales, string CreatedDate, string UpdateDate, string IsActive, string SalesTypeCode, string SalesTypeName, string smart_room_no, string smart_sqm, string smart_update_date,
            string smart_update_user, string smart_isShop_group, string smart_shop_group_id, string smart_shop_open, string smart_shop_close, string smart_group_location, string smart_contract_type,
            string smart_Category_leasing, string smart_Industry_group, string smart_collection_flag_status, string smart_icon_staff_id, string smart_record_person_type,
            string smart_record_type, string smart_record_daily_type, string smart_record_daily_time, string smart_record_weekly_date, string smart_record_weekly_time,
            string smart_record_monthly_month, string smart_record_monthly_time, string smart_contract_status, string smart_record_keyin_type, string Contract_old_number, string smart_object_rental_code, string smart_floor)
        {
            var str = "Insert into tblsmart_Contract(ContractNumber,CompanyCode,ContractTypeCode,ContractTypeDescription,ContractStartDate,ContractEndDate,ShopName,IndustryCode, " +
                        " BusinessPartnerCode,BusinessPartnerName,ContractTerm,SubTypeCode,SubTypeName,OwnCashier,OwnCreditCard,RelevantToSales, " +
                        " CreatedDate,IsActive,SalesTypeCode,SalesTypeName,smart_room_no,smart_sqm,smart_create_date,smart_update_user, " +
                        " smart_isShop_group,smart_shop_group_id,smart_group_location,smart_contract_type, " +
                        " smart_Category_leasing,smart_Industry_group,smart_collection_flag_status,smart_icon_staff_id,smart_record_person_type, " +
                        " smart_record_type,smart_record_daily_type,smart_record_daily_time,smart_record_weekly_date,smart_record_weekly_time, " +
                        " smart_record_monthly_month,smart_record_monthly_time,smart_contract_status,smart_record_keyin_type,Contract_old_number,smart_object_rental_code,smart_floor) " +

                        " Values('" + ContractNumber + "','" + CompanyCode + "','" + ContractTypeCode + "','" + ContractTypeDescription + "','" + ContractStartDate + "','" + ContractEndDate + "','" + ShopName + "','" + IndustryCode + "', " +
                        " '" + BusinessPartnerCode + "','" + BusinessPartnerName + "','" + ContractTerm + "','" + SubTypeCode + "','" + SubTypeName + "','" + OwnCashier + "','" + OwnCreditCard + "','" + RelevantToSales + "', " +
                        "'" + CreatedDate + "','" + IsActive + "','" + SalesTypeCode + "','" + SalesTypeName + "','" + smart_room_no + "','" + smart_sqm + "',GETDATE(),'" + smart_update_user + "', " +
                        " '" + smart_isShop_group + "','" + smart_shop_group_id + "','" + smart_group_location + "','" + smart_contract_type + "', " +
                        " '" + smart_Category_leasing + "','" + smart_Industry_group + "','" + smart_collection_flag_status + "','" + smart_icon_staff_id + "','" + smart_record_person_type + "', " +
                        " '" + smart_record_type + "','" + smart_record_daily_type + "','" + smart_record_daily_time + "','" + smart_record_weekly_date + "','" + smart_record_weekly_time + "', " +
                        " '" + smart_record_monthly_month + "','" + smart_record_monthly_time + "','" + smart_contract_status + "','" + smart_record_keyin_type + "','" + Contract_old_number + "','" + smart_object_rental_code + "','" + smart_floor + "') ;  ";




            return str;
        }





















    }
}
