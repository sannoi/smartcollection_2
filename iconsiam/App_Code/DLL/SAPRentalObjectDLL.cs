﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SAPRentalObjectDLL
/// </summary>
public class SAPRentalObjectDLL
{
    public SAPRentalObjectDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetRentalOject(string RentalObject, string IsActive,string shopname,string FloorName,string ContractNumber,string RentalObjectDescription, string[] comcode)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string comcode_ = "";

        if (comcode.Length != 0)
        {
            comcode_ += "(";
            for (int i = 0; i < comcode.Length; i++)
            {
                comcode_ += " r.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
            }

            comcode_ = comcode_.Substring(0, comcode_.Length - 2);
            comcode_ = comcode_ + ")";
        }


        strSQL += " select r.ContractNumber,r.RentalObjectCode,r.RentalObjectDescription,r.BuildingName,r.FloorName " +
                    " ,case when r.IsActive = '1' then 'Active' else 'Inactive' end as status  , s.ShopName " +
                    "  from SAPRentalObject r  inner join tblsmart_contract s on r.contractnumber = s.contractnumber  " +
                    " where (r.RentalObjectCode like '%" + RentalObject.Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or r.RentalObjectDescription like '%" + 
                    RentalObject.Replace("'", "''").Replace("&", "'+char(38)+'") + "%') and r.IsActive = '" + IsActive + "' " +
                    " and (s.ShopName like '%" + shopname.Replace("'", "''").Replace("&", "'+char(38)+'") + "%') "  +
                    " and (r.ContractNumber like '%" + ContractNumber.Replace("'", "''").Replace("&", "'+char(38)+'") + "%') " +
                    " and (r.RentalObjectDescription like '%" + RentalObjectDescription.Replace("'", "''").Replace("&", "'+char(38)+'") + "%') " +
                    " and (r.FloorName like '%" + FloorName.Replace("'", "''").Replace("&", "'+char(38)+'") + "%') ";

        if (comcode_ != "")
        {
            strSQL = strSQL + " and " + comcode_;
        }

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


}