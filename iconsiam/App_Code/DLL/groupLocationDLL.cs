﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for groupLocationDLL
/// </summary>
public class groupLocationDLL
{
    public groupLocationDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable getGroupLocationByName(string[] GrouplocationName, string Flag_active, string company_code, string role)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";
        string GrouplocationName_ = "";


        if (GrouplocationName.Length != 0)
        {
            for (int i = 0; i < GrouplocationName.Length; i++)
            {
                GrouplocationName_ += " (c.GrouplocationNameEN like '%" + GrouplocationName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                    " or c.GrouplocationNameTH like '%" + GrouplocationName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' ) or";
            }

            GrouplocationName_ = GrouplocationName_.Substring(0, GrouplocationName_.Length - 2);
        }

        if (role != "datacenter" && role != "super_admin")
        {
            strSQL += "  select c.*,u.username , case when c.Flag_active = '1' then 'Active' else 'Inactive' end as status " +
               " from tblgrouplocation c inner join tbluser u on c.createid = u.userid where " +
               " " + GrouplocationName_ + " " +
               " and c.Flag_active = '" + Flag_active + "'  " +
               " and  c.id in ( select GroupLocationId from TblGroupLocation_mapping  " +
               " where ComGroupID in (  select distinct ComGroupID from TblCompanyGroupMapping " +
               "  where CompanyCode in (" + company_code + ")))   order by id ;   ";
        }
        else
        {
            strSQL += "  select c.*,u.username , case when c.Flag_active = '1' then 'Active' else 'Inactive' end as status " +
              " from tblgrouplocation c inner join tbluser u on c.createid = u.userid where " +
              " " + GrouplocationName_ + " " +
              " and c.Flag_active = '" + Flag_active + "'   order by id ;   ";
        }



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public void Inserttblgrouplocation(string GrouplocationNameTH, string GrouplocationNameEN, string Flag_active, string CreateId)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " insert into tblgrouplocation(GrouplocationNameTH,GrouplocationNameEN,Flag_active,CreateDate,CreateId) " +
                " values('" + GrouplocationNameTH + "','" + GrouplocationNameEN + "','" + Flag_active + "', GETDATE(), '" + CreateId + "');   ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable getGrouplocationById(string Id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblgrouplocation where Id = '" + Id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void Updategrouplocation(string GrouplocationNameTH, string GrouplocationNameEN, string Flag_active, string CreateId, string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Update tblgrouplocation set GrouplocationNameTH = '" + GrouplocationNameTH + "', GrouplocationNameEN = '" + GrouplocationNameEN + "', " +
            " Flag_active = '" + Flag_active + "' , UpdateDate = GETDATE() , UpdateId = '" + CreateId + "' where id = '" + id + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }



    public DataTable getGrouplocationByGrouplocationNameTH(string GrouplocationNameTH)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblgrouplocation where GrouplocationNameTH = '" + GrouplocationNameTH + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getGrouplocationByGrouplocationNameTH_nid(string GrouplocationNameTH, string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblgrouplocation where GrouplocationNameTH = '" + GrouplocationNameTH + "' and id != '" + id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable getGrouplocationByGrouplocationNameEN(string GrouplocationNameEN)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblgrouplocation where GrouplocationNameEN = '" + GrouplocationNameEN + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable getGrouplocationByGrouplocationNameEN_nid(string GrouplocationNameEN, string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblgrouplocation where GrouplocationNameEN = '" + GrouplocationNameEN + "' and id != '" + id + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

}