﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class login : System.Web.UI.Page
    {
        loginDLL Serv = new loginDLL();
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        protected string CountPic { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtusername.Focus();
                txtusername.Attributes.Add("onkeypress", "return next_tools(event,'" + txtpassword.ClientID + "')");
                txtpassword.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsubmit.ClientID + "')");            
            }

            var logo = Serv.Get_Pic_Logo_login();
            CountPic = Convert.ToString(logo.Rows.Count.ToString());
            if (logo.Rows.Count != 0)
            {
                if (CountPic == "1")
                {
                    this.imgCompany1_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "2")
                {
                    this.imgCompany2_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany2_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "3")
                {
                    this.imgCompany3_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany3_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                    this.imgCompany3_3.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "4")
                {
                    this.imgCompany4_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany4_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                    this.imgCompany4_3.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany4_4.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "5")
                {
                    this.imgCompany5_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany5_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                    this.imgCompany5_3.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany5_4.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany5_5.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                }
                else if (CountPic == "6")
                {
                    this.imgCompany6_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_3.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_4.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_5.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    this.imgCompany6_6.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                }
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtusername.Text != "" && txtpassword.Text != "")
            {

                var u = Serv.getUser_tenant(txtusername.Text, txtpassword.Text);
                if (u.Rows.Count != 0)
                {
                    if (u.Rows[0]["flag_active"].ToString() != "y")
                    {
                        POPUPMSG("User and Password is inactive ไม่สามารถเข้าใช้งานได้");
                        return;
                    }
                    else
                    {

                        Serv.Insert_log_login(u.Rows[0]["id"].ToString(), "tenant", "web");

                        if (u.Rows[0]["contractnumber"].ToString() != "")
                        {
                            var comcode = Serv.getComcode_by_ContractNumber(u.Rows[0]["contractnumber"].ToString());
                            //string comcode = u.Rows[0]["contractnumber"].ToString().Substring(0, 3);
                            if (comcode.Rows.Count != 0)
                            {
                                var theme = Serv.getThemeUser_tenant(comcode.Rows[0]["CompanyCode"].ToString());
                                if (theme.Rows.Count != 0)
                                {
                                    HttpContext.Current.Session["code_theme"] = theme.Rows[0]["ButtonColor"].ToString();
                                    HttpContext.Current.Session["code_Navbar"] = theme.Rows[0]["NavbarColor"].ToString();
                                    HttpContext.Current.Session["code_Bg"] = theme.Rows[0]["BgUri"].ToString();
                                    HttpContext.Current.Session["code_Bg2"] = theme.Rows[0]["BgUri2"].ToString();
                                    HttpContext.Current.Session["code_Bg3"] = theme.Rows[0]["BgUri3"].ToString();
                                    HttpContext.Current.Session["code_Bg4"] = theme.Rows[0]["BgUri4"].ToString();
                                    HttpContext.Current.Session["code_Bg5"] = theme.Rows[0]["BgUri5"].ToString();
                                    HttpContext.Current.Session["code_Navbar2"] = theme.Rows[0]["NavbarColor2"].ToString();
                                    HttpContext.Current.Session["PicGroupCompany"] = theme.Rows[0]["CompanyGroupLogo"].ToString();
                                }
                                else
                                {
                                    HttpContext.Current.Session["code_theme"] = "#CCA9DA";
                                    HttpContext.Current.Session["code_Navbar"] = "#fff";
                                    HttpContext.Current.Session["code_Bg"] = "#2d2339";
                                    HttpContext.Current.Session["code_Bg2"] = "#453658";
                                    HttpContext.Current.Session["code_Bg3"] = "#5f4a79";
                                    HttpContext.Current.Session["code_Bg4"] = "#7b5f9c";
                                    HttpContext.Current.Session["code_Bg5"] = "#9775c0";
                                    HttpContext.Current.Session["code_Navbar2"] = "#fff";
                                    HttpContext.Current.Session["PicGroupCompany"] = "img/siampi.png";
                                }
                            }                          
                        }

                        HttpContext.Current.Session["s_userid"] = u.Rows[0]["id"].ToString();
                        HttpContext.Current.Session["s_username"] = u.Rows[0]["username"].ToString();
                        HttpContext.Current.Session["s_user_contractNumber"] = u.Rows[0]["contractnumber"].ToString();
                        HttpContext.Current.Session["s_user_type"] = "tenant";

                        HttpContext.Current.Session["s_user_img"] = "assets/img/profile.jpg";

                        // show Head Account User
                        string contractnumber = u.Rows[0]["contractnumber"].ToString();                        
                        var company_name = Serv.getCompany_name_tenant(contractnumber);
                        if (company_name.Rows.Count != 0)
                        {
                            HttpContext.Current.Session["company_name_login"] = company_name.Rows[0]["CompanyNameTh"].ToString();
                        }

                        if (u.Rows[0]["flag_reset"].ToString() == "y")
                        {
                            Response.Redirect("~/reset_pass.aspx");
                        }
                        else
                        {
                            Response.Redirect("~/transaction_list_tenant.aspx");
                        }
                    }
                   
                }

                //=====================================================================================

                var user = Serv.getUser(txtusername.Text, txtpassword.Text);
                if (user.Rows.Count != 0)
                {
                    if (user.Rows[0]["flag_active"].ToString() != "y")
                    {
                        POPUPMSG("User and Password is inactive ไม่สามารถเข้าใช้งานได้");
                        return;
                    }
                    else
                    {
                        Serv.Insert_log_login(user.Rows[0]["userid"].ToString(), "non-tenant", "web");

                        HttpContext.Current.Session["s_userid"] = user.Rows[0]["userid"].ToString();
                        HttpContext.Current.Session["s_username"] = user.Rows[0]["username"].ToString();
                        HttpContext.Current.Session["s_employee_code"] = user.Rows[0]["employee_code"].ToString();
                        HttpContext.Current.Session["s_email"] = user.Rows[0]["email"].ToString();
                        HttpContext.Current.Session["s_user_type"] = "icon";
                        HttpContext.Current.Session["s_user_group"] = user.Rows[0]["user_group"].ToString();
                        HttpContext.Current.Session["role"] = user.Rows[0]["role"].ToString(); //<<< Smt2
                        HttpContext.Current.Session["noshow_img"] = user.Rows[0]["noshow_img"].ToString();

                        // show Head Account User
                        var comcode_user = Serv.getCompanycode_by_userid(user.Rows[0]["userid"].ToString());
                        if (comcode_user.Rows.Count != 0)
                        {
                            if (comcode_user.Rows.Count > 1) //ดึงจาก groupcompany
                            {
                                var company_name = Serv.getCompanyGroup_name_user(comcode_user.Rows[0]["company_code"].ToString());
                                if (company_name.Rows.Count != 0)
                                {
                                    HttpContext.Current.Session["company_name_login"] = company_name.Rows[0]["ComGroupName"].ToString();
                                }
                            }
                            else //ดึงจาก company
                            {
                                var company_name = Serv.getCompany_name_user(comcode_user.Rows[0]["company_code"].ToString());
                                if (company_name.Rows.Count != 0)
                                {
                                    HttpContext.Current.Session["company_name_login"] = company_name.Rows[0]["CompanyNameTh"].ToString();
                                }
                            }
                        }

                        if (user.Rows[0]["role"].ToString() == "super_admin")
                        {
                            HttpContext.Current.Session["company_name_login"] = "All Company (IT)";
                        }
                        else if (user.Rows[0]["role"].ToString() == "datacenter")
                        {
                            HttpContext.Current.Session["company_name_login"] = "All Company";
                        }

                        if (user.Rows[0]["flag_reset_password"].ToString() == "y")
                        {
                            Response.Redirect("~/reset_pass.aspx");
                        }

                        string userid = "";

                        if (HttpContext.Current.Session["role"].ToString() != "super_admin" && HttpContext.Current.Session["role"].ToString() != "datacenter")
                        {
                            var comcode = Serv.GetMapCompany(user.Rows[0]["userid"].ToString());
                            if (comcode.Rows.Count != 0)
                            {
                                string com_code = "";
                                for (int i = 0; i < comcode.Rows.Count; i++)
                                {
                                    com_code = com_code + "'" + comcode.Rows[i]["company_code"].ToString() + "',";
                                }
                                com_code = com_code.Substring(0, com_code.Length - 1);

                                HttpContext.Current.Session["s_com_code"] = com_code;

                            }
                            else
                            {
                                POPUPMSG("User นี้ยังไม่ได้ผูกกับ Company");
                                return;
                            }
                        }
                        else
                        {
                            var comcode = Serv.GetMapCompany("");
                            if (comcode.Rows.Count != 0)
                            {
                                string com_code = "";
                                for (int i = 0; i < comcode.Rows.Count; i++)
                                {
                                    com_code = com_code + "'" + comcode.Rows[i]["company_code"].ToString() + "',";
                                }
                                com_code = com_code.Substring(0, com_code.Length - 1);

                                HttpContext.Current.Session["s_com_code"] = com_code;

                            }

                        }

                        if (HttpContext.Current.Session["s_userid"] != null)
                        {
                            var Theme = Serv.Get_Theme_user(HttpContext.Current.Session["s_userid"].ToString());
                            if (Theme.Rows.Count != 0)
                            {
                                HttpContext.Current.Session["code_theme"] = Theme.Rows[0]["ButtonColor"].ToString();
                                HttpContext.Current.Session["code_Navbar"] = Theme.Rows[0]["NavbarColor"].ToString();
                                HttpContext.Current.Session["code_Bg"] = Theme.Rows[0]["BgUri"].ToString();
                                HttpContext.Current.Session["code_Bg2"] = Theme.Rows[0]["BgUri2"].ToString();
                                HttpContext.Current.Session["code_Bg3"] = Theme.Rows[0]["BgUri3"].ToString();
                                HttpContext.Current.Session["code_Bg4"] = Theme.Rows[0]["BgUri4"].ToString();
                                HttpContext.Current.Session["code_Bg5"] = Theme.Rows[0]["BgUri5"].ToString();
                                HttpContext.Current.Session["code_Navbar2"] = Theme.Rows[0]["NavbarColor2"].ToString();
                            }
                            else
                            {
                                HttpContext.Current.Session["code_theme"] = "#CCA9DA";
                                HttpContext.Current.Session["code_Navbar"] = "#fff";
                                HttpContext.Current.Session["code_Bg"] = "#2d2339";
                                HttpContext.Current.Session["code_Bg2"] = "#453658";
                                HttpContext.Current.Session["code_Bg3"] = "#5f4a79";
                                HttpContext.Current.Session["code_Bg4"] = "#7b5f9c";
                                HttpContext.Current.Session["code_Bg5"] = "#9775c0";
                                HttpContext.Current.Session["code_Navbar2"] = "#fff";
                            }

                            var PicGroupCompany = Serv.Get_Pic_Company_Group_user(HttpContext.Current.Session["s_userid"].ToString());
                            if (PicGroupCompany.Rows.Count != 0)
                            {
                                HttpContext.Current.Session["PicGroupCompany"] = PicGroupCompany.Rows[0]["CompanyGroupLogo"].ToString();
                            }
                            else
                            {
                                HttpContext.Current.Session["PicGroupCompany"] = "img/siampi.png";
                            }

                        }

                        #region smt1
                        if (user.Rows[0]["role"].ToString() == "manager")
                        {
                            var off = Serv.GetOfficerByManagerId(user.Rows[0]["userid"].ToString());
                            if (off.Rows.Count != 0)
                            {
                                for (int i = 0; i < off.Rows.Count; i++)
                                {
                                    userid = userid + "'" + off.Rows[i]["officer_id"].ToString() + "'" + ",";
                                }
                                userid = userid.Substring(0, userid.Length - 1);
                            }
                        }
                        else if (user.Rows[0]["role"].ToString() == "vp")
                        {
                            var off = Serv.GetOfficerByVPId(user.Rows[0]["userid"].ToString());
                            if (off.Rows.Count != 0)
                            {
                                for (int i = 0; i < off.Rows.Count; i++)
                                {
                                    userid = userid + "'" + off.Rows[i]["officer_id"].ToString() + "'" + ",";
                                }
                                userid = userid.Substring(0, userid.Length - 1);
                            }
                        }
                        else if (user.Rows[0]["role"].ToString() == "management")
                        {
                            var off = Serv.GetOfficer(HttpContext.Current.Session["s_com_code"].ToString());
                            if (off.Rows.Count != 0)
                            {
                                for (int i = 0; i < off.Rows.Count; i++)
                                {
                                    userid = userid + "'" + off.Rows[i]["userid"].ToString() + "'" + ",";
                                }
                                userid = userid.Substring(0, userid.Length - 1);
                            }
                        }
                        else if (user.Rows[0]["role"].ToString() == "officer")
                        {
                            userid = "'" + user.Rows[0]["userid"].ToString() + "'";
                        }
                        else if (user.Rows[0]["role"].ToString() == "admin")
                        {
                            var off = Serv.GetOfficer(HttpContext.Current.Session["s_com_code"].ToString());
                            if (off.Rows.Count != 0)
                            {
                                for (int i = 0; i < off.Rows.Count; i++)
                                {
                                    userid = userid + "'" + off.Rows[i]["userid"].ToString() + "'" + ",";
                                }
                                userid = userid.Substring(0, userid.Length - 1);
                            }
                        }
                        else if (user.Rows[0]["role"].ToString() == "super_admin")
                        {
                            var off = Serv.GetOfficer(HttpContext.Current.Session["s_com_code"].ToString());
                            if (off.Rows.Count != 0)
                            {
                                for (int i = 0; i < off.Rows.Count; i++)
                                {
                                    userid = userid + "'" + off.Rows[i]["userid"].ToString() + "'" + ",";
                                }
                                userid = userid.Substring(0, userid.Length - 1);
                            }
                        }

                        HttpContext.Current.Session["s_user_for_shop"] = userid;

                        if (user.Rows[0]["img_uri"].ToString() == "")
                        {
                            HttpContext.Current.Session["s_user_img"] = "";
                        }
                        else
                        {
                            HttpContext.Current.Session["s_user_img"] = user.Rows[0]["img_uri"].ToString();
                        }

                        if (HttpContext.Current.Session["s_user_group"].ToString() == "ae")
                        {
                            if (HttpContext.Current.Session["role"].ToString() == "officer")
                            {
                                Response.Redirect("~/daily_rep.aspx");
                            }
                            else
                            {
                                Response.Redirect("~/Default.aspx");
                            }
                        }
                        else
                        {
                            if (HttpContext.Current.Session["role"].ToString() == "datacenter")
                            {
                                Response.Redirect("~/industryGroup.aspx");

                            }
                            else
                            {
                                Response.Redirect("~/Default.aspx");

                            }
                        }

                        #endregion

                    }

                }
                else
                {
                    POPUPMSG("Username / Password ไม่ถูกต้อง");
                    return;
                    //Response.Redirect("~/login.aspx");
                }
            }
            else
            {
                POPUPMSG("กรุณากรอก Username / Password ให้ครบถ้วน");
                return;
                //Response.Redirect("~/login.aspx");
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/forget_password.aspx");

        }
    }
}