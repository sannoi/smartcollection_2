﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="monthly_rep.aspx.cs" Inherits="iconsiam.monthly_rep" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Monthly Report
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtstartdate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>
                    <asp:CalendarExtender ID="txtstartdate_CalendarExtender" runat="server"
                        BehaviorID="txtstartdate_CalendarExtender" TargetControlID="txtstartdate" Format="yyyy-MM">
                    </asp:CalendarExtender>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtenddate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>
                    <asp:CalendarExtender ID="txtenddate_CalendarExtender" runat="server"
                        BehaviorID="txtenddate_CalendarExtender" TargetControlID="txtenddate" Format="yyyy-MM">
                    </asp:CalendarExtender>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtshopname" runat="server" placeholder="Shop Name" class="form-control" autocomplete="off"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtcompanyname" runat="server" placeholder="Company Name" class="form-control" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtfloor" runat="server" placeholder="Floor" class="form-control" autocomplete="off"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtcustomercode" runat="server" placeholder="Customer Code" class="form-control" autocomplete="off"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlbuilding" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtshopgroup" runat="server" placeholder="Customer Code" class="form-control" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlroomtype" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlcontract_type" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlcatagorLeasing" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlgroup_loca" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlindustrygroup" runat="server" class="form-control"></asp:DropDownList>
                </div>             
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                   <asp:DropDownList ID="ddlstatus" runat="server" placeholder="Status" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;"></div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click"  class="btn ThemeBtn" Width ="25%" />
                    <asp:Button ID="btnexport" runat="server" Text="Export" OnClick="btnexport_Click"  class="btn ThemeBtn" Width ="25%" />
                    <asp:Button ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click" class="btn ThemeBtn" Width ="25%" />
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="true"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                    <%--    <Columns>

                            <asp:BoundField DataField="d1" HeaderText="DATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"  />
                            <asp:BoundField DataField="d2" HeaderText="Customer Code" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"  />
                            <asp:BoundField DataField="d3" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"  />
                            <asp:BoundField DataField="d4" HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"  />
                            <asp:BoundField DataField="d21" HeaderText="Industry Group" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"  />                           
                            <asp:BoundField DataField="d13" HeaderText="Total ex VAT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}"  />
                           


                        </Columns>--%>
                    </asp:GridView>

                </div>
            </div>


        </div>
    </div>

    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="true" Visible="true"
            ShowFooter="false" class="table table-bordered">
           <%-- <Columns>
                <asp:BoundField DataField="d1" HeaderText="DATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d2" HeaderText="Customer Code" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d3" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d4" HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d5" HeaderText="Sales(Inc VAT)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                <asp:BoundField DataField="d6" HeaderText="VAT 7%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                <asp:BoundField DataField="d7" HeaderText="Sales(ex VAT)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                <asp:BoundField DataField="d8" HeaderText="Service (Inc VAT)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                <asp:BoundField DataField="d9" HeaderText="VAT 7%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                <asp:BoundField DataField="d10" HeaderText="Service (ex VAT)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                <asp:BoundField DataField="d11" HeaderText="service charge" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                <asp:BoundField DataField="d12" HeaderText="Sale before Service charge" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                <asp:BoundField DataField="d13" HeaderText="Total ex VAT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                <asp:BoundField DataField="d14" HeaderText="Shop Group" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d15" HeaderText="Company Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d16" HeaderText="Building" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d17" HeaderText="Floor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d18" HeaderText="Room Type" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d19" HeaderText="Contract Type" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d20" HeaderText="Category Leasing" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d21" HeaderText="Industry Group" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d22" HeaderText="Group Location" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
            </Columns>--%>
        </asp:GridView>
    </asp:Panel>




</asp:Content>
