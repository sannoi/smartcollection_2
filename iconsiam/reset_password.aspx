﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="reset_password.aspx.cs" Inherits="iconsiam.reset_password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p class="text-center">
                Reset Password 
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-lg-3"></div>
        <div class="col-md-6 col-lg-6">
            <asp:TextBox ID="txtoldpassword" runat="server" TextMode="Password" placeholder="Old Password" class="form-control"></asp:TextBox>
        </div>
        <div class="col-3"></div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-3 col-lg-3"></div>
        <div class="col-md-6 col-lg-6">
            <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" placeholder="Password" class="form-control"></asp:TextBox>
        </div>
        <div class="col-md-3 col-lg-3"></div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-3 col-lg-3"></div>
        <div class="col-md-6 col-lg-6">
            <asp:TextBox ID="txtpassword_com" runat="server" TextMode="Password" placeholder="Confirm Password" class="form-control"></asp:TextBox>
        </div>
        <div class="col-md-3 col-lg-3"></div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-3 col-lg-3"></div>
        <div class="col-md-6 col-lg-6" style="text-align: center">
            <asp:Button ID="btnsubmit" runat="server" Text="SAVE" OnClick="btnsubmit_Click" Width="100%" ForeColor="Black" class="btn" BackColor="#f8ca3e" BorderColor="#a78a65" />
        </div>
        <div class="col-md-3 col-lg-3"></div>
    </div>


</asp:Content>
