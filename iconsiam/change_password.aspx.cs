﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class change_password : System.Web.UI.Page
    {
        change_passwordDLL Serv = new change_passwordDLL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtoldpassword.Focus();
                    txtoldpassword.Attributes.Add("onkeypress", "return next_tools(event,'" + txtnewpassword.ClientID + "')");
                    txtnewpassword.Attributes.Add("onkeypress", "return next_tools(event,'" + txtconfirmpassword.ClientID + "')");
                    txtconfirmpassword.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsave.ClientID + "')");

                }
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }



        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtoldpassword.Text == "")
            {
                POPUPMSG("กรุณากรอก Password เก่าของท่าน");
                return;
            }
            if (txtnewpassword.Text == "")
            {
                POPUPMSG("กรุณากรอก Password ใหม่ของท่าน");
                return;
            }
            if (txtnewpassword.Text.Length < 6)
            {
                POPUPMSG("Password ต้องมีความยาวอย่างน้อย 6 ตัวอักษร");
                return;
            }

            if (txtconfirmpassword.Text == "")
            {
                POPUPMSG("กรุณากรอกยืนยัน Password ของท่าน");
                return;
            }
            if(txtoldpassword.Text == txtnewpassword.Text)
            {
                POPUPMSG("Password ใหม่ต้องไม่เหมือนกับ Password ก่อนหน้าของท่าน");
                return;
            }

            var user = Serv.GetUserByID(HttpContext.Current.Session["s_userid"].ToString(), txtoldpassword.Text);
            if (user.Rows.Count != 0)
            {
                if (txtnewpassword.Text == txtconfirmpassword.Text)
                {
                    Serv.UpdatePassword(txtnewpassword.Text, HttpContext.Current.Session["s_userid"].ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='Default.aspx';", true);
                }
                else
                {
                    POPUPMSG("Password ใหม่ของท่านไม่ถูกต้อง");
                    return;
                }
            }
            else
            {
                POPUPMSG("Password เก่าของท่านไม่ถูกต้อง");
                return;
            }
        }


    }
}