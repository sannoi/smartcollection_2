﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iconsiam.App_Code.DLL
{
    public class setting_vatDLL
    {

        public DataTable getVatList(string search, string[] comcode)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " sv.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ");";
            }

            strSQL += " select sv.id,cd.Companycode,cd.CompanyNameEn,cd.CompanyNameTh,sv.vat, sv.update_date, u.username " +
                " from tblsetting_vat sv inner join TblCompanyDetail cd on sv.CompanyCode = cd.Companycode " +
                " inner join tbluser u on sv.update_id = u.userid where ( cd.CompanyNameEn like '%" + search + "%' or cd.CompanyNameTh like '%" + search + "%' ) ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable getVatByID(string id)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select sv.id,cd.Companycode,cd.CompanyNameEn,cd.CompanyNameTh,sv.vat, sv.update_date, u.username " +
                " from tblsetting_vat sv inner join TblCompanyDetail cd on sv.CompanyCode = cd.Companycode " +
                " inner join tbluser u on sv.update_id = u.userid where sv.id = '" + id + "' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable getCompany()
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblCompanyDetail where Companycode not in (select companycode from tblsetting_vat) ;  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }



        public void Insert_vat(string vat, string create_date, string update_date, string update_id, string companycode)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " insert into tblsetting_vat(vat,create_date,update_date,update_id, CompanyCode) " +
                " values('" + vat + "','" + create_date + "','" + update_date + "','" + update_id + "', '" + companycode + "') ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void update_vat(string vat, string update_date, string update_id, string id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " update tblsetting_vat set vat = '" + vat + "',update_date =  getdate() ,update_id = '" + update_id + "'  where id =  '" + id + "' ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }




    }
}