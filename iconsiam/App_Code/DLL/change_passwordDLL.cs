﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for change_passwordDLL
/// </summary>
public class change_passwordDLL
{
    public change_passwordDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetUserByID(string userid, string password)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tbluser where userid = '" + userid + "' and DecryptByPassPhrase('spwgsmart',password ) = '" + password + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public DataTable GetUser_tenantByID(string userid, string password)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += "  select * from tblcontract_account where id = '" + userid + "' and DecryptByPassPhrase('spwgsmart',password )  = '" + password + "' ;  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


    public void UpdatePassword(string password, string userid)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Update tbluser set password = ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) , " +
            " update_date = getdate(), update_id = '" + userid + "' where userid = '" + userid + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }


    public void UpdatePassword_Tenant(string password, string userid, string update_id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Update tblcontract_account set password =  ENCRYPTBYPASSPHRASE('spwgsmart','" + password + "' ) ,update_date = getdate(), update_id = '" + update_id + "' where id = '" + userid + "'  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }



}