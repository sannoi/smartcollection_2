﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="edit_Template_Email.aspx.cs" Inherits="iconsiam.edit_Template_Email" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Email Template Company
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtcompany" runat="server" class="form-control" placeholder="Company"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" OnClick="btnsearch_Click" Text="Search"  class="btn ThemeBtn" />
                     <asp:Button ID="btncancel" runat="server" Text="Clear" OnClick="btncancel_Click" class="btn ThemeBtn" />
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="Companycode" HeaderText="Company Code" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />
                            <asp:BoundField DataField="CompanyNameTh" HeaderText="Company Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="50%" />
                            <asp:BoundField DataField="username" HeaderText="Username" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Et_UpdateDate" HeaderText="Update Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />

                            <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddCompanycode" runat="server" Value='<%# Eval("Companycode") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="Edit" OnClick="btnedit_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">              
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                     <asp:Button ID="btnback" runat="server" Text="Back" OnClick="btnback_Click" class="btn ThemeBtn" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>
