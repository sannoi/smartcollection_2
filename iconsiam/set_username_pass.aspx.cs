﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class set_username_pass : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        shop_sap_masterDLL Serv = new shop_sap_masterDLL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtusername.Text != "" && txtpassword.Text != "" && txtpassword_com.Text != "")
            {
                if (txtpassword.Text == txtpassword_com.Text)
                {
                    //Serv.CreateAccount(HttpContext.Current.Session["con_code"].ToString(), txtusername.Text, txtpassword.Text, HttpContext.Current.Session["s_userid"].ToString(),
                    //    DateTime.Now.ToString("yyyy-MM-dd", EngCI));

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='conteactList_SAP.aspx';", true);

                }
                else
                {
                    POPUPMSG("Password กับ Confirm Password ไม่ตรงกัน");
                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

    }
}