﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;

namespace iconsiam
{
    public partial class add_mapping_group : System.Web.UI.Page
    {
        add_mappingDLL Serv = new add_mappingDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                }
            }
        }

        protected void bind_default()
        {
            ddlrole.Items.Insert(0, new ListItem("Admin", "admin"));
            //ddlrole.Items.Insert(0, new ListItem("Officer", "officer"));
            //ddlrole.Items.Insert(1, new ListItem("Manager", "manager"));
            //ddlrole.Items.Insert(2, new ListItem("VP", "vp"));
            //ddlrole.Items.Insert(3, new ListItem("Management", "management"));

            var u = Serv.GetUser(ddlrole.SelectedValue);
            if (u.Rows.Count != 0)
            {
                ddluser.DataSource = u;
                ddluser.DataTextField = "username";
                ddluser.DataValueField = "userid";
                ddluser.DataBind();
            }
            else
            {
                ddluser.DataSource = null;
                ddluser.DataBind();
            }
            ddluser.Items.Insert(0, new ListItem("Select User", ""));


        }

        protected void ddlrole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrole.SelectedValue != "")
            {
                var u = Serv.GetUser(ddlrole.SelectedValue);
                if (u.Rows.Count != 0)
                {
                    ddluser.DataSource = u;
                    ddluser.DataTextField = "username";
                    ddluser.DataValueField = "userid";
                    ddluser.DataBind();
                }
                else
                {
                    ddluser.DataSource = null;
                    ddluser.DataBind();
                }
                ddluser.Items.Insert(0, new ListItem("Select User", ""));
            }

        }


        protected void ddluser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddluser.SelectedValue != "")
            {
                var comp = Serv.GetCompany_Group(ddluser.SelectedValue);
                if (comp.Rows.Count != 0)
                {
                    GridView_List.DataSource = comp;
                    GridView_List.DataBind();
                }
                else
                {
                    GridView_List.DataSource = null;
                    GridView_List.DataBind();
                }
            }

        }

        protected void btnresetpass_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView_List.Rows) //Running all lines of grid
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
                    CheckBox chkRow = (CheckBox)row.FindControl("CheckBox1");
                    if (chkRow.Checked)
                    {
                        var com = Serv.GetCompanyGroupMapp(hdd_id.Value);
                        if (com.Rows.Count != 0)
                        {
                            for (int i = 0; i < com.Rows.Count; i++)
                            {
                                Serv.InsertMapping(ddluser.SelectedValue, com.Rows[i]["CompanyCode"].ToString());
                            }
                        }
                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='company_mapping_admin.aspx';", true);

        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/company_mapping_admin.aspx");
        }
    }
}