﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="record_transaction_import.aspx.cs" Inherits="iconsiam.record_transaction_import" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }


        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10005;
        }


    </script>



    <style type="text/css">
        .test .ajax__calendar_container {
            padding: 10px;
            position: absolute;
            cursor: default;
            width: 300px;
            font-size: 15px;
            text-align: center;
            font-family: tahoma,erdana,helvetica;
            background-color: lemonchiffon;
            border: 1px solid #646464;
            z-index: 10000 !important;
            text-wrap: none;
        }

        .test .ajax__calendar_dayname {
            height: 17px;
            width: 34px;
            text-align: center;
        }
    </style>




    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Add Transaction (Excel-Import)
            </p>
        </div>
    </div>
    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">

            <asp:HiddenField ID="hdd_keyintype" runat="server" />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Label ID="lbshopname" runat="server" Font-Size="18px" Text="-"></asp:Label>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Label ID="lbroomnum" runat="server" Font-Size="18px" Text="-"></asp:Label>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Label ID="lbkey_type" runat="server" ForeColor="red" Font-Size="18px" Text="-"></asp:Label>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />


            <%--            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtdate" runat="server" class="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="txtdate_CalendarExtender" runat="server" BehaviorID="txtdate_CalendarExtender" TargetControlID="txtdate"
                        OnClientShown="calendarShown" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />--%>


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:FileUpload ID="FileUpload1" runat="server" class="form-control" />
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ForeColor="White" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" ForeColor="White" class="btn btn-danger" Width="114px" />
                </div>
            </div>

        </div>
    </div>




</asp:Content>
