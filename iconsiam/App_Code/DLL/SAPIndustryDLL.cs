﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SAPIndustryDLL
/// </summary>
public class SAPIndustryDLL
{
    public SAPIndustryDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetIndustry(string IndustryNam, string IsActive)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select *,case when IsActive = '1' then 'Active' else 'Inactive' end as status " +
                "   from SAPIndustry where IsActive = '" + IsActive + "' and(IndustryNameEN like '%" + IndustryNam.Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or IndustryNameTH like '%" + IndustryNam.Replace("'", "''").Replace("&", "'+char(38)+'") + "%') " +
                " order by IndustryCode ";



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }


}