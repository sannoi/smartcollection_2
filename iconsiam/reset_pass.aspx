﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reset_pass.aspx.cs" Inherits="iconsiam.reset_pass" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tenant Sales Data Collection</title>

    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }
    </script>


    <style>
        .card-header:first-child {
            border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
            background: #565656;
            color: #fff;
        }

        .bg-dark {
            width: 100%;
            height: 100%;
            /*background: url('./img/bg.png');*/
            background: url('./img/BG-G1.jpg');
            background-position: center;
            background-size: 100% 100%;
            background-repeat: no-repeat;
        }

          @media (max-width: 425px) {
            .des_pl {
                padding-left: 0 !important;
            }

            .des_pr {
                padding-right: 0 !important;
            }

        }

        .des_pl {
            padding-left: 20%;
        }

            .des_pl img {
                width: 150px;
            }

        .des_pr {
            padding-right: 20%;
        }

            .des_pr img {
                width: 150px;
            }
    </style>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" />
    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

</head>
<body class="bg-dark">
    <div class="container" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);">

        <form id="form1" runat="server">
            <div class="row">
               <% if(CountPic == "1") { %>
                    <div class="col-md-12 col-lg-12" style="display: flex; margin-bottom: 5%;">
                    
                        <div class="col-md-12 col-lg-12">
                            <p class="text-center">
                                <%--<img src="img/siampi.png" class="logo" style="width: 150px;" />--%>
                                <asp:Image ID="imgCompany1_1" runat="server" Width="150px" />
                            </p>
                        </div>
                        
                    </div>
                <% }
                else if(CountPic == "2"){ %>
                    <div class="col-md-12 col-lg-12" style="display: flex; margin-bottom: 5%;">
                    
                        <div class="col-md-6 col-lg-6">
                            <p class="text-center des_pl">
                                <%--<img src="img/siampi.png" class="logo" style="width: 150px;" />--%>
                                <asp:Image ID="imgCompany2_1" runat="server"/>
                            </p>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <p class="text-center des_pr">
                                <%--<img src="img/one.png" class="logo" style="width: 150px;" />--%>
                                <asp:Image ID="imgCompany2_2" runat="server" />
                            </p>
                        </div>
                        
                    </div>
                <% }
                else if(CountPic == "3"){ %>
                     <div class="col-md-12 col-lg-12" style="display: flex; margin-bottom: 5%;">
                    
                        <div class="col-md-4 col-lg-4">
                            <p class="text-center">
                                <%--<img src="img/siampi.png" class="logo" style="width: 150px;" />--%>
                                <asp:Image ID="imgCompany3_1" runat="server" Width="150px" />
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <p class="text-center">
                                <%--<img src="img/one.png" class="logo" style="width: 150px;" />--%>
                                <asp:Image ID="imgCompany3_2" runat="server" Width="150px" />
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <p class="text-center">                           
                                <asp:Image ID="imgCompany3_3" runat="server" Width="150px" />
                            </p>
                        </div>
                    </div>
                <% } %>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <p class="text-center">
                        Reset Password 
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3"></div>
                <div class="col-md-6 col-lg-6">
                    <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" placeholder="Old Password" class="form-control"></asp:TextBox>

                </div>
                <div class="col-md-3 col-lg-3"></div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-3 col-lg-3"></div>
                <div class="col-md-6 col-lg-6">
                    <asp:TextBox ID="txtnewpassword" runat="server" TextMode="Password" placeholder="New Password" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3"></div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-3 col-lg-3"></div>
                <div class="col-md-6 col-lg-6">
                    <asp:TextBox ID="txtconfirmassword" runat="server" TextMode="Password" placeholder="Confirm Password" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3"></div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-3 col-lg-3"></div>
                <div class="col-md-6 col-lg-6" style="text-align: center">
                    <asp:Button ID="btnsubmit" runat="server" Text="Login" OnClick="btnsubmit_Click" Width="100%" ForeColor="white" class="btn" BackColor="#392c4a" BorderColor="#392c4a" />
                </div>
                <div class="col-md-3 col-lg-3"></div>
            </div>

        </form>
    </div>
</body>
</html>
