﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class shop_master_rep : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        conteactList_SAPDLL Serv = new conteactList_SAPDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_default();
                    bind_rep();

                }
            }
        }

        protected void bind_default()
        {

            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            ////var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_userid"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }

            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;

            //}



            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }

            ddlcompany.Items.Insert(0, new ListItem("Company", ""));

            //if (HttpContext.Current.Session["role"].ToString() == "officer")
            //{
            //    ddlar.Visible = false;
            //}
            //else
            //{
            //    ddlar.Visible = true;


            //}

            var u = Serv.GetUser();
            if (u.Rows.Count != 0)
            {
                ddlar.DataTextField = "name";
                ddlar.DataValueField = "userid";
                ddlar.DataSource = u;
                ddlar.DataBind();
            }
            else
            {
                ddlar.DataSource = null;
                ddlar.DataBind();
            }
            ddlar.Items.Insert(0, new ListItem("User", ""));

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));

        }
        protected void bind_rep()
        {
            string[] shopname = txtshopname.Text.Split(',');
            string[] shopgroup = txtshopgroup.Text.Split(',');

            string Company = "";

            if (ddlcompany.SelectedValue == "")
            {
                Company = HttpContext.Current.Session["s_com_code"].ToString();
            }
            else
            {
                Company = "'" + ddlcompany.SelectedValue + "'";
            }


            var contract = Serv.GetSmart_Contract_owner("",
              Company, shopname, shopgroup, ddlar.SelectedValue, HttpContext.Current.Session["s_com_code"].ToString(), ddlstatus.SelectedValue);

            var contract_ = Serv.GetSmart_Contract_owner_export("",
             Company, shopname, shopgroup, ddlar.SelectedValue, HttpContext.Current.Session["s_com_code"].ToString() , ddlstatus.SelectedValue);

            if (contract.Rows.Count != 0)
            {

                DataTable dt = new DataTable();
                dt.Columns.Add("ContractNumber");
                dt.Columns.Add("CompanyCode");
                dt.Columns.Add("CompanyNameTH");
                dt.Columns.Add("smart_room_no");
                dt.Columns.Add("ShopGroupNameEN");
                dt.Columns.Add("ShopName");
                dt.Columns.Add("BusinessPartnerName");
                dt.Columns.Add("status");
                dt.Columns.Add("smart_update_date");
                dt.Columns.Add("username");
                dt.Columns.Add("arname");
                dt.Columns.Add("startdate");
                dt.Columns.Add("enddate");
                dt.Columns.Add("smart_building");
                dt.Columns.Add("ContractTypeNameEn");
                dt.Columns.Add("ContractTypeDescription");
                dt.Columns.Add("SalesTypeName");
                dt.Columns.Add("SubTypeCode");
                dt.Columns.Add("SubTypeName");
                dt.Columns.Add("aename");
                dt.Columns.Add("CategoryleasingNameEN");
                dt.Columns.Add("IndustryGroupNameEN");
                dt.Columns.Add("smart_floor");
                dt.Columns.Add("smart_usage_name");
                dt.Columns.Add("smart_sqm");
                dt.Columns.Add("GrouplocationNameTH");
                dt.Columns.Add("ContactPointName");
                dt.Columns.Add("ContactPointEmail");
                dt.Columns.Add("ContactPointTel");
                dt.Columns.Add("smart_record_type");

                for (int i = 0; i < contract_.Rows.Count; i++)
                {
                    DataRow row1 = dt.NewRow();
                    row1["ContractNumber"] = contract_.Rows[i]["ContractNumber"].ToString();
                    row1["CompanyCode"] = contract_.Rows[i]["CompanyCode"].ToString();
                    row1["CompanyNameTH"] = contract_.Rows[i]["CompanyNameTH"].ToString();
                    row1["smart_room_no"] = contract_.Rows[i]["smart_room_no"].ToString();
                    row1["ShopGroupNameEN"] = contract_.Rows[i]["ShopGroupNameEN"].ToString();
                    row1["ShopName"] = contract_.Rows[i]["ShopName"].ToString();
                    row1["BusinessPartnerName"] = contract_.Rows[i]["BusinessPartnerName"].ToString();
                    row1["status"] = contract_.Rows[i]["status"].ToString();
                    row1["smart_update_date"] = contract_.Rows[i]["smart_update_date"].ToString();
                    row1["username"] = contract_.Rows[i]["username"].ToString();
                    row1["arname"] = contract_.Rows[i]["arname"].ToString();
                    row1["startdate"] = contract_.Rows[i]["startdate"].ToString();
                    row1["enddate"] = contract_.Rows[i]["enddate"].ToString();
                    row1["smart_building"] = contract_.Rows[i]["smart_building"].ToString();
                    row1["ContractTypeNameEn"] = contract_.Rows[i]["ContractTypeNameEn"].ToString();
                    row1["ContractTypeDescription"] = contract_.Rows[i]["ContractTypeDescription"].ToString();
                    row1["SalesTypeName"] = contract_.Rows[i]["SalesTypeName"].ToString();
                    row1["SubTypeCode"] = contract_.Rows[i]["SubTypeCode"].ToString();
                    row1["SubTypeName"] = contract_.Rows[i]["SubTypeName"].ToString();
                    row1["aename"] = contract_.Rows[i]["aename"].ToString();
                    row1["CategoryleasingNameEN"] = contract_.Rows[i]["CategoryleasingNameEN"].ToString();
                    row1["IndustryGroupNameEN"] = contract_.Rows[i]["IndustryGroupNameEN"].ToString();
                    row1["smart_floor"] = contract_.Rows[i]["smart_floor"].ToString();
                    row1["smart_usage_name"] = contract_.Rows[i]["smart_usage_name"].ToString();
                    row1["smart_sqm"] = contract_.Rows[i]["smart_sqm"].ToString();
                    row1["GrouplocationNameTH"] = contract_.Rows[i]["GrouplocationNameTH"].ToString();
                    row1["smart_record_type"] = contract_.Rows[i]["smart_record_type"].ToString();

                    var cont = Serv.GetContactPoint(contract_.Rows[i]["ContractNumber"].ToString());
                    if (cont.Rows.Count != 0)
                    {

                        row1["ContactPointName"] = cont.Rows[0]["name"].ToString();
                        row1["ContactPointEmail"] = cont.Rows[0]["email"].ToString();
                        row1["ContactPointTel"] = cont.Rows[0]["tel1"].ToString() + "//" + cont.Rows[0]["tel2"].ToString();
                    }
                    else
                    {
                        row1["ContactPointName"] = "";
                        row1["ContactPointEmail"] = "";
                        row1["ContactPointTel"] = "";
                    }



                    dt.Rows.Add(row1);

                }

                if (dt.Rows.Count != 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }

                ////--------------------

                if (HttpContext.Current.Session["role"].ToString() == "officer")
                {
                    GridView_List.DataSource = contract;
                    GridView_List.DataBind();

                }
                else
                {
                    GridView_List2.DataSource = contract;
                    GridView_List2.DataBind();
                }


            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();

                GridView_List2.DataSource = null;
                GridView_List2.DataBind();
            }

            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }

        }




        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_rep();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnexport_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count != 0)
            {

                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename = shop_master_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                //Response.AddHeader("content-disposition", "attachment;filename=Export1.xls");
                Response.ContentType = "application/vnd.ms-excel";
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                System.IO.StringWriter sw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);

                GridView1.RenderControl(hw);
                Response.Write(sw.ToString());
                Response.End();


            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/shop_master_rep.aspx");

        }

    }
}