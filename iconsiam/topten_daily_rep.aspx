﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="topten_daily_rep.aspx.cs" Inherits="iconsiam.topten_daily_rep" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Top 10 : Daily Report
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtstartdate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtstartdate_CalendarExtender" runat="server"
                        BehaviorID="txtstartdate_CalendarExtender" TargetControlID="txtstartdate" Format="dd/MM/yyyy" />
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtenddate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="txtenddate_CalendarExtender" runat="server"
                        BehaviorID="txtenddate_CalendarExtender" TargetControlID="txtenddate" Format="dd/MM/yyyy" />
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlindustrygroup" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlcatagorLeasing" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlcompany" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlstatus" runat="server" placeholder="Status" class="form-control"></asp:DropDownList>
                </div>              
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">  
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px; padding-right: 0px;"></div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px; padding-right: 0px;"></div>
                 <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn" Width ="25%" />
                    <asp:Button ID="btnexport" runat="server" Text="Export" OnClick="btnexport_Click" class="btn ThemeBtn" Width ="25%"/>
                    <asp:Button ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click" class="btn ThemeBtn" Width ="25%"/>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false"
                        ShowFooter="false" PageSize="50" class="table mt-3" OnRowDataBound="GridView_List_RowDataBound">
                        <Columns>

                            <asp:BoundField DataField="d0" HeaderText="No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="d1" HeaderText="DATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="d2" HeaderText="Industry Group" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:BoundField DataField="d4" HeaderText="Floor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="d5" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:BoundField DataField="d3" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:BoundField DataField="d6" HeaderText="Area" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="d_total" HeaderText="Total (Exclude VAT)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />

                            <asp:TemplateField HeaderText="" Visible="false">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_d3" runat="server" Value='<%# Eval("d3") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
            </div>


        </div>
    </div>

    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" Visible="true"
            ShowFooter="false" class="table table-bordered" OnRowDataBound="GridView1_RowDataBound">
            <Columns>

                <asp:BoundField DataField="d0" HeaderText="No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d1" HeaderText="DATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d2" HeaderText="Industry Group" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                <asp:BoundField DataField="d4" HeaderText="Floor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d5" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                <asp:BoundField DataField="d3" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                <asp:BoundField DataField="d6" HeaderText="Area" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="d_total" HeaderText="Total (Exclude VAT)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />

                <asp:TemplateField HeaderText="" Visible="false">
                    <ItemTemplate>
                        <asp:HiddenField ID="hdd_d3" runat="server" Value='<%# Eval("d3") %>' />
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </asp:Panel>



</asp:Content>
