﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class edit_shopgroup : System.Web.UI.Page
    {
        shopGroupListDLL Serv = new shopGroupListDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtshopgroup_nameEN.Focus();
                    txtshopgroup_nameEN.Attributes.Add("onkeypress", "return next_tools(event,'" + txtshopgroup_nameTH.ClientID + "')");
                    txtshopgroup_nameTH.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsave.ClientID + "')");

                    bind_default();
                    bind_data();
                }
            }
        }

        protected void bind_default()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "1"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "0"));

            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            ////var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_userid"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }

            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;

            //}


            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');

            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(comcode_);
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }
            ddlcompany.Items.Insert(0, new ListItem("", ""));

            if (HttpContext.Current.Session["role"].ToString() == "datacenter")
            {
                ddlcompany.Visible = false;
            }

        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var shopg = Serv.GetShopGroupByID(Request.QueryString["id"].ToString());
                if (shopg.Rows.Count != 0)
                {
                    txtshopgroup_nameEN.Text = shopg.Rows[0]["ShopGroupNameEN"].ToString();
                    txtshopgroup_nameTH.Text = shopg.Rows[0]["ShopGroupNameTH"].ToString();
                    ddlstatus.SelectedValue = shopg.Rows[0]["flag_active"].ToString();
                    ddlcompany.SelectedValue = shopg.Rows[0]["CompanyCode"].ToString();
                }
                else
                {
                    Response.Redirect("~/shopGroupList.aspx");
                }
            }


        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (ddlcompany.SelectedValue != "" && txtshopgroup_nameEN.Text != "" && txtshopgroup_nameTH.Text != "")
            {
                var ex1 = Serv.GetShopGroupByNameEN_nid(txtshopgroup_nameEN.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                    ddlcompany.SelectedValue, Request.QueryString["id"].ToString());
                var ex2 = Serv.GetShopGroupByNameTH_nid(txtshopgroup_nameTH.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                    ddlcompany.SelectedValue, Request.QueryString["id"].ToString());
                if (ex1.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Group Location Name(EN) ซ้ำ");
                    return;
                }
                else if (ex2.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Group Location Name(TH) ซ้ำ");
                    return;
                }
                else
                {
                    Serv.UpdateShopGroup(txtshopgroup_nameEN.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                        txtshopgroup_nameTH.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), HttpContext.Current.Session["s_userid"].ToString(), ddlcompany.SelectedValue,
                        ddlstatus.SelectedValue, Request.QueryString["id"].ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='shopGroupList.aspx';", true);

                }
            }
            else if (HttpContext.Current.Session["role"].ToString() == "datacenter")
            {
                var ex1 = Serv.GetShopGroupByNameEN_nid(txtshopgroup_nameEN.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                    ddlcompany.SelectedValue, Request.QueryString["id"].ToString());
                var ex2 = Serv.GetShopGroupByNameTH_nid(txtshopgroup_nameTH.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                    ddlcompany.SelectedValue, Request.QueryString["id"].ToString());
                if (ex1.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Group Location Name(EN) ซ้ำ");
                    return;
                }
                else if (ex2.Rows.Count != 0)
                {
                    POPUPMSG("ข้อมูล Group Location Name(TH) ซ้ำ");
                    return;
                }
                else
                {
                    Serv.UpdateShopGroup(txtshopgroup_nameEN.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                        txtshopgroup_nameTH.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), HttpContext.Current.Session["s_userid"].ToString(), "",
                        ddlstatus.SelectedValue, Request.QueryString["id"].ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='shopGroupList.aspx';", true);

                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/shopGroupList.aspx");
        }


    }
}