﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="transaction_detail.aspx.cs" Inherits="iconsiam.transaction_detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }


        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10005;
        }


    </script>



    <style type="text/css">
        .test .ajax__calendar_container {
            padding: 10px;
            position: absolute;
            cursor: default;
            width: 300px;
            font-size: 15px;
            text-align: center;
            font-family: tahoma,erdana,helvetica;
            background-color: lemonchiffon;
            border: 1px solid #646464;
            z-index: 10000 !important;
            text-wrap: none;
        }

        .test .ajax__calendar_dayname {
            height: 17px;
            width: 34px;
            text-align: center;
        }
    </style>




    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HiddenField ID="hdd_current_1" runat="server" />
    <asp:HiddenField ID="hdd_current_2" runat="server" />


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                รายละเอียดยอด
            </p>
        </div>
    </div>
    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Label ID="lbshopname" runat="server" Font-Size="18px" Text="-"></asp:Label>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Label ID="lbroomnum" runat="server" Font-Size="18px" Text="-"></asp:Label>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtdate" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="txtdate_CalendarExtender" runat="server" BehaviorID="txtdate_CalendarExtender" TargetControlID="txtdate"
                        OnClientShown="calendarShown" Format="dd/MM/yyyy"></asp:CalendarExtender>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ยอดขายรวม VAT
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtproduct_amount" runat="server" placeholder="ยอดขายรวม" class="form-control" Style="border: 1px solid #dcd135;"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Label ID="lb1" Visible="false" runat="server" Text="-"></asp:Label>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtservice_serCharge_amount" runat="server" placeholder="ยอดบริการ" class="form-control" Style="border: 1px solid #dcd135;"> </asp:TextBox>
                    <asp:TextBox ID="txtservice_serCharge_amount2" runat="server" placeholder="ยอด Service Charge" class="form-control" Visible="false" Style="border: 1px solid #dcd135;"></asp:TextBox>
                </div>
            </div>
            <br />

            <asp:Panel ID="Panel2" runat="server" Visible="false">
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        ขอแก้ไขยอด
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <asp:TextBox ID="txtremark" runat="server" TextMode="MultiLine" placeholder="รายละเอียดที่ต้องการแก้ไข" class="form-control" Style="border: 1px solid #e11919;"></asp:TextBox>
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <br />

            </asp:Panel>





            <asp:Panel ID="Panel3" runat="server" Visible="false">
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        สถานะยอดขายวันนี้ : 
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <%--<asp:TextBox ID="ddlreason" runat="server" placeholder="รายละเอียดที่ต้องการแก้ไข" ReadOnly="true" class="form-control" Style="border: 1px solid #e11919;"></asp:TextBox>--%>
                        <asp:DropDownList ID="ddlreason" runat="server" Style="border: 1px solid #dcd135;" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlreason_SelectedIndexChanged" class="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        หมายเหตุ : 
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9 col-lg-9">
                        <asp:TextBox ID="txtdetal" runat="server" ReadOnly="true" TextMode="MultiLine" class="form-control" Height="150px"></asp:TextBox>
                    </div>
                    <div class="col-md-1 col-lg-1">
                    </div>
                </div>
                <br />
            </asp:Panel>




            <asp:Panel ID="Panel1" runat="server" Visible="false">
                <div class="row">
                    <div class="col-md-10 col-lg-10">
                        <div class="row">
                            <div class="col-md-5 col-lg-5">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="Image1" runat="server" Width="50%" OnClick="Image1_Click" Visible="false" />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hdd_img1_file" runat="server" />
                                            <asp:HiddenField ID="hdd_img1" runat="server" />
                                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Visible="false">-</asp:LinkButton>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5 col-lg-5">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="Image2" runat="server" Width="50%" OnClick="Image2_Click" Visible="false" />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hdd_img2_file" runat="server" />
                                            <asp:HiddenField ID="hdd_img2" runat="server" />
                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Visible="false">-</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-lg-5">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="Image3" runat="server" Width="50%" OnClick="Image3_Click" Visible="false" />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hdd_img3_file" runat="server" />
                                            <asp:HiddenField ID="hdd_img3" runat="server" />
                                            <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click" Visible="false">-</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5 col-lg-5">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="Image4" runat="server" Width="50%" OnClick="Image4_Click" Visible="false" />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hdd_img4_file" runat="server" />
                                            <asp:HiddenField ID="hdd_img4" runat="server" />
                                            <asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton4_Click" Visible="false">-</asp:LinkButton>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-lg-5">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="Image5" runat="server" Width="50%" OnClick="Image5_Click" Visible="false" />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hdd_img5_file" runat="server" />
                                            <asp:HiddenField ID="hdd_img5" runat="server" />
                                            <asp:LinkButton ID="LinkButton5" runat="server" OnClick="LinkButton5_Click" Visible="false">-</asp:LinkButton>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5 col-lg-5">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="Image6" runat="server" Width="50%" OnClick="Image6_Click" Visible="false" />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hdd_img6_file" runat="server" />
                                            <asp:HiddenField ID="hdd_img6" runat="server" />
                                            <asp:LinkButton ID="LinkButton6" runat="server" OnClick="LinkButton6_Click" Visible="false">-</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>                   
                    </div>
                </div>
                <br />
            </asp:Panel>

            <div class="row">
                    <div class="col-md-12 col-lg-12">
                       ข้อมูลที่แก้ไขยอด : 
                    </div>
                </div>
            
            <asp:Panel ID="Panel6" runat="server" Visible="false">
                <div class="row">
                    <div class="col-md-10 col-lg-10">
                        <div class="row">
                            <div class="col-md-5 col-lg-5">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="Image7" runat="server" Width="50%" OnClick="Image7_Click" Visible="false" />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hdd_img7_file" runat="server" />
                                            <asp:HiddenField ID="hdd_img7" runat="server" />
                                            <asp:LinkButton ID="LinkButton7" runat="server" OnClick="LinkButton7_Click" Visible="false">-</asp:LinkButton>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5 col-lg-5">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="Image8" runat="server" Width="50%" OnClick="Image8_Click" Visible="false" />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hdd_img8_file" runat="server" />
                                            <asp:HiddenField ID="hdd_img8" runat="server" />
                                            <asp:LinkButton ID="LinkButton8" runat="server" OnClick="LinkButton8_Click" Visible="false">-</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>                  
                    </div>
                </div>
                <br />
            </asp:Panel>

             <div class="row">
                    <div class="col-md-10 col-lg-10">
                        <div class="row">
                            <div class="col-md-5 col-lg-5">
                                <asp:Panel ID="Panel4" runat="server">
                                    <div class="row">
                                        <div class="col-md-9 col-lg-9">
                                            <asp:FileUpload ID="FileUpload1" runat="server" class="form-control" />
                                        </div>
                                        <div class="col-md-1 col-lg-1">
                                            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                    <br />
                                </asp:Panel>
                            </div>
                            <div class="col-md-5 col-lg-5">
                                <asp:Panel ID="Panel5" runat="server">
                                    <div class="row">
                                        <div class="col-md-9 col-lg-9">
                                            <asp:FileUpload ID="FileUpload2" runat="server" class="form-control" />
                                        </div>
                                        <div class="col-md-1 col-lg-1">
                                        </div>
                                    </div>
                                    <br />
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                 </div>

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ForeColor="White" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" ForeColor="White" class="btn btn-danger" Width="114px" />
                </div>
            </div>

        </div>
    </div>





</asp:Content>
