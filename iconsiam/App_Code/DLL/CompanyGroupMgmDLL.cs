﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iconsiam.App_Code.DLL
{
    public class CompanyGroupMgmDLL
    {
        public DataTable InsertComGroupName(string ComGroupName, string CreateId, string IsActive)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " insert into TblCompanyGroup(ComGroupName,CreateDate,CreateId,IsActive,ThemeId) " +
                    " values('" + ComGroupName + "',getdate(),'" + CreateId + "','" + IsActive + "','7');   "+
                    " SELECT @@identity as last_id ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void DeleteComGroupName(string ComGroupID)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete TblCompanyGroup where ComGroupID = '" + ComGroupID + "'   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public DataTable getComGroupName(string ComGroupName, string IsActive)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select a.*,case when a.IsActive = 'y' then 'Active' else 'Inactive' end as status, " +
                    " case when b.cnt is null then 0 else b.cnt end as cnt " +
                    " from " +
                    " (select * from TblCompanyGroup) a left join " +
                    " ( " +
                    " select ComGroupID, count(CompanyCode) as cnt from TblCompanyGroupMapping " +
                    " group by ComGroupID " +
                    " ) b on a.ComGroupID = b.ComGroupID " +
                    " where a.ComGroupName like '%" + ComGroupName + "%' and IsActive = '" + IsActive + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable getComName(string CompanyName)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select  distinct cd.*, u.username from TblCompanyDetail cd left join tbluser u on cd.UpdateId = u.userid " +
                "  where CompanyNameTh like '%" + CompanyName + "%' or CompanyNameEn like '%" + CompanyName + "%' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getCompanyGroupByID(string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblCompanyGroup where ComGroupID = '" + ComGroupID + "' ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getCompanyGroupMappingByID(string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            //strSQL += "  select cm.Id,cm.CompanyCode,c.CompanyNameEn,c.CompanyNameTh from TblCompanyGroupMapping cm " +
            //    " inner join TblCompanyDetail c on cm.CompanyCode = c.Companycode " +
            //    " where cm.ComGroupID = '" + ComGroupID + "' ";

            strSQL += "  select a.*,case when b.ComGroupID is not null then 'y' else 'n' end as chk," +
                " case when b.ComGroupName is not null then b.ComGroupName " +
                " when c.ComGroupName is not null then c.ComGroupName else '' end as ComGroupName, " +
                " b.Id, case when c.ComGroupID is not null then 'y' else 'n' end as chkNo " +
                " from( select cd.Companycode, cd.CompanyNameTh, cd.CompanyNameEn " +
                " from TblCompanyDetail cd where IsActive = 'y'  " +
                " )a left join ( " +
                " select cgm.CompanyCode, cg.ComGroupID, cg.ComGroupName, cgm.Id from TblCompanyGroupMapping cgm inner " +
                " join TblCompanyGroup cg on cgm.ComGroupID = cg.ComGroupID where cgm.ComGroupID = '" + ComGroupID + "' ) " +
                " b on a.Companycode = b.CompanyCode  left join " +
                " ( select cgm.CompanyCode, cg.ComGroupID, cg.ComGroupName, cgm.Id " +
                " from TblCompanyGroupMapping cgm inner join TblCompanyGroup cg " +
                " on cgm.ComGroupID = cg.ComGroupID where cgm.ComGroupID != '" + ComGroupID + "' )c on a.Companycode = c.CompanyCode ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getCompanyCodeNotInMappingByID(string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblCompanyDetail where Companycode not in ( select cm.CompanyCode from TblCompanyGroupMapping cm  ) ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public void InsertComGroupMapping(string ComGroupID, string CompanyCode, string CreateId, string UpdateId, string IsActive)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into TblCompanyGroupMapping(ComGroupID,CompanyCode,CreateDate,UpdateDate,CreateId,UpdateId,IsActive) " +
                " values('" + ComGroupID + "','" + CompanyCode + "', getdate(), getdate(), '" + CreateId + "','" + UpdateId + "','" + IsActive + "')   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void Insert_Emailtemplate_company(string CompanyCode, string userid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into TblEmailTemplate(Companycode,Link,Sendfromteam,Sendfromcompany,Tel,F_Email,F_Callcenter,CreateDate,CreateId,UpdateDate,UpdateId,ComGroupID,TypeCompany) " +
                " values ( '"+ CompanyCode + "' ,'https://qas.siamsmartcollection.com','ทีมบัญชีลูกหนี้','สยาม','061-406-1757','siam@siam.com','061-406-1757',getdate(),'"+ userid + "',getdate(),'" + userid + "','','company')   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void Insert_Emailtemplate_companyGroup(string Companygroup, string userid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into TblEmailTemplate(Companycode,Link,Sendfromteam,Sendfromcompany,Tel,F_Email,F_Callcenter,CreateDate,CreateId,UpdateDate,UpdateId,ComGroupID,TypeCompany) " +
                " values ( '','https://qas.siamsmartcollection.com','ทีมบัญชีลูกหนี้','สยาม','061-406-1757','siam@siam.com','061-406-1757',getdate(),'" + userid + "',getdate(),'" + userid + "','"+ Companygroup + "','companygroup')   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public void DeleteComGroupMapping(string Id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete from TblCompanyGroupMapping where Id = '" + Id + " ' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable DeleteCompanyMapping_template_by_comgroup(string ComGroupID)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL = " select * from TblCompanyGroupMapping where ComGroupID = '" + ComGroupID + " ' ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;

        }

        public void DeleteComGroupMappingByComGroup(string ComGroupID)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete from TblCompanyGroupMapping where ComGroupID = '" + ComGroupID + " ' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void DeletecompanyMapping_template(string companycode)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete from TblEmailTemplate where Companycode = '" + companycode + " ' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public void UpdateCompanyGroup(string ComGroupName, string IsActive, string ComGroupID, string CompanyGroupLogo, string UpdateId)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblCompanyGroup set ComGroupName = '" + ComGroupName + "' ," +
                "  IsActive = '" + IsActive + "', CompanyGroupLogo = '" + CompanyGroupLogo + "', " +
                " UpdateDate = getdate(), UpdateId = '" + UpdateId + "' " +
                " where ComGroupID = '" + ComGroupID + "' ;  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable getComNameByID(string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblCompanyDetail where Companycode  = '" + Companycode + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getComGroupNameByID(string comgroupid)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblCompanyGroup where ComGroupID  = '" + comgroupid + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void UpdateCompanyDetail(string CompanyLogo, string CreateId, string UpdateId, string CompanyNameEn, string CompanyNameTh, string Companycode)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblCompanyDetail set  CompanyLogo = '" + CompanyLogo + "',  UpdateDate = getdate(), " +
                "  UpdateId = '" + UpdateId + "' , CompanyNameEn = '" + CompanyNameEn + "', CompanyNameTh = '" + CompanyNameTh + "' " +
                " where Companycode = '" + Companycode + "'   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void UpdateCompanyDetail_PicComgroup(string ThemeId , string CompanygroupLogo,string updateid,string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblCompanyGroup set ThemeId = '"+ ThemeId + "' , CompanyGroupLogo = '" + CompanygroupLogo + "', UpdateDate = getdate(), UpdateId = '"+ updateid + "' where ComGroupID = '"+ ComGroupID + "' ";
               


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void UpdateCompanyDetail(string ThemeId,string updateid, string ComGroupID)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblCompanyGroup set ThemeId = '" + ThemeId + "' , UpdateDate = getdate(), UpdateId = '" + updateid + "' where ComGroupID = '" + ComGroupID + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void UpdateCompanyDetail_(string CreateId, string UpdateId, string CompanyNameEn, string CompanyNameTh, string Companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblCompanyDetail set UpdateDate = getdate(), " +
                " UpdateId = '" + UpdateId + "' , CompanyNameEn = '" + CompanyNameEn + "', CompanyNameTh = '" + CompanyNameTh + "' " +
                " where Companycode = '" + Companycode + "'   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }
        public DataTable getTheme()
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblTheme where IsActive = 'y' ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }



    }
}