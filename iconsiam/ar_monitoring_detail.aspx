﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ar_monitoring_detail.aspx.cs" Inherits="iconsiam.ar_monitoring_detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

    <style type="text/css">
        .test .ajax__calendar_container {
            padding: 10px;
            position: absolute;
            cursor: default;
            width: 300px;
            font-size: 15px;
            text-align: center;
            font-family: tahoma,erdana,helvetica;
            background-color: lemonchiffon;
            border: 1px solid #646464;
            z-index: 10000 !important;
            text-wrap: none;
        }

        .test .ajax__calendar_dayname {
            height: 17px;
            width: 34px;
            text-align: center;
        }
    </style>

    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }


        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10005;
        }



    </script>

    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                AR Monitoring/Entry
                <asp:HiddenField ID="hdd_companycode" runat="server" />
                <asp:HiddenField ID="hdd_h" runat="server" />
            </p>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    Company :
                    <asp:Label ID="lbcompanyname" runat="server" Text="-"></asp:Label>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    Customer Name :
                    <asp:Label ID="lbbpname" runat="server" Text="-"></asp:Label>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    Shop Name :
                    <asp:Label ID="lbshopname" runat="server" Text="-"></asp:Label>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="btnimport" runat="server" Text="Import" OnClick="btnimport_Click" class="btn ThemeBtn"  Width="114px" />
                    <asp:Button ID="btnadd" runat="server" Text="บันทึกยอด" OnClick="btnadd_Click"  class="btn ThemeBtn"  Width="114px" />
                </div>
            </div>
            <hr />

            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlstatus" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtstartdate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>

                    <asp:CalendarExtender ID="txtstartdate_CalendarExtender" runat="server"
                        BehaviorID="txtstartdate_CalendarExtender" TargetControlID="txtstartdate" Format="dd/MM/yyyy"></asp:CalendarExtender>

                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtenddate" runat="server" placeholder="" class="form-control" autocomplete="off"></asp:TextBox>

                    <asp:CalendarExtender ID="txtenddate_CalendarExtender" runat="server"
                        BehaviorID="txtenddate_CalendarExtender" TargetControlID="txtenddate" Format="dd/MM/yyyy"></asp:CalendarExtender>

                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn"  Width="114px" />
                    <asp:Button ID="btnback" runat="server" Text="Previous" OnClick="btnback_Click"  class="btn ThemeBtn"  Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Clear" OnClick="btncancel_Click" class="btn ThemeBtn" Width="114px" />
                </div>
            </div>
            <br />
            <div class="row" id="divConfirm" runat="server">
                <div class="col-md-6 col-lg-6" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="btnselect_all" runat="server" Text="Select All" OnClick="btnselect_all_Click" ForeColor="Black" class="btn btn-info" Width="114px" Visible="false" />
                    <asp:Button ID="btndisselect_all" runat="server" Text="Diselect All" OnClick="btndisselect_all_Click" ForeColor="Black" class="btn btn-danger" Width="114px" Visible="false" />
                    <asp:Button ID="btnconfirm" runat="server" Text="Confirm ยอด" OnClick="btnconfirm_Click" ForeColor="Black" class="btn btn-success" Width="114px" Visible="false" />
                </div>
                <div class="col-md-6 col-lg-6" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false"
                        ShowFooter="false" PageSize="50" class="table mt-3" OnRowDataBound="GridView_List_RowDataBound">
                        <Columns>

                            <asp:BoundField DataField="record_date" HeaderText="วันที่ส่งยอด" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="ContractNumber" HeaderText="เลขที่สัญญา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="product_amount" HeaderText="ยอดขายรวม (บาท)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                            <asp:BoundField DataField="service_serCharge_amount" HeaderText="ยอดบริการ (บาท)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="flag_confirm" HeaderText="สถานะยืนยัน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_ContractNumber" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:HiddenField ID="hdd_status" runat="server" Value='<%# Eval("status") %>' />
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("id") %>' />
                                    <asp:HiddenField ID="hdd_flag_confirm" runat="server" Value='<%# Eval("flag_confirm") %>' />

                                    <asp:Button ID="btndetail" runat="server" Text="รายละเอียด" class="btn" OnClick="btndetail_Click" Visible="true" ForeColor="Black"
                                        BackColor="#f8ca3e" BorderColor="#a78a65" />
                                    <asp:Button ID="btnedit" runat="server" Text="แก้ไขยอด" class="btn" OnClick="btnedit_Click" Visible="true" ForeColor="Black"
                                        BackColor="#f8ca3e" BorderColor="#a78a65" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GridView_List_confirm" runat="server" AutoGenerateColumns="false"
                        ShowFooter="false" PageSize="50" class="table mt-3" OnRowDataBound="GridView_List_confirm_RowDataBound">
                        <Columns>

                            <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="record_date" HeaderText="วันที่ส่งยอด" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="ContractNumber" HeaderText="เลขที่สัญญา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="product_amount" HeaderText="ยอดขายรวม (บาท)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                            <asp:BoundField DataField="service_serCharge_amount" HeaderText="ยอดบริการ (บาท)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.00}" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="flag_confirm" HeaderText="สถานะยืนยัน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_ContractNumber" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("id") %>' />
                                    <asp:HiddenField ID="hdd_flag_confirm" runat="server" Value='<%# Eval("flag_confirm") %>' />
                                    <asp:HiddenField ID="hdd_status" runat="server" Value='<%# Eval("status") %>' />

                                    <asp:Button ID="btndetail" runat="server" Text="รายละเอียด" class="btn" OnClick="btndetail_Click" Visible="true" ForeColor="Black"
                                        BackColor="#f8ca3e" BorderColor="#a78a65" />
                                    <asp:Button ID="btnedit" runat="server" Text="แก้ไขยอด" class="btn" OnClick="btnedit_Click" Visible="true" ForeColor="Black"
                                        BackColor="#f8ca3e" BorderColor="#a78a65" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
            </div>
        </div>
    </div>



</asp:Content>
