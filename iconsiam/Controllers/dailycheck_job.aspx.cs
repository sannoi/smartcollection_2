﻿using iconsiam.App_Code;
using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam.Controllers
{
    public partial class dailycheck_job : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
                result x = new result();
                jobscheduleDLL Serv = new jobscheduleDLL();
                dailycheck_jobDLL Serv2 = new dailycheck_jobDLL();
                sentmail mail = new sentmail();

                LogService l = new LogService();

                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "_Start", "dailycheck_job_End_of_date");

                try
                {
                    string d1 = DateTime.Now.ToString("yyyy-MM-dd", EngCI);


                    #region NormalDaily_JOB
                    var contract = Serv2.getContract_daily_endofday(d1);
                    if (contract.Rows.Count != 0)
                    {
                        for (int i = 0; i < contract.Rows.Count; i++)
                        {

                            int vat = 0;
                            var v = Serv2.GetVAT(contract.Rows[i]["CompanyCode"].ToString());
                            if (v.Rows.Count != 0)
                            {
                                vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
                            }
                            else
                            {
                                vat = 7;
                            }


                            var trans = Serv2.getTransaction_daily_endofdate(contract.Rows[i]["ContractNumber"].ToString(),
                                DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd", EngCI));

                            if (trans.Rows.Count == 0)
                            {
                                /////// เริ่ม run ตั้งแต่เปิดร้าน /////////////

                                string d3 = Convert.ToDateTime(contract.Rows[i]["smart_shop_open"]).ToString("yyyy-MM-dd 00:00:00", EngCI);
                                string d4 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00", EngCI);

                                double result2 = (Convert.ToDateTime(d4) - Convert.ToDateTime(d3)).TotalDays;

                                if (result2 >= 0)
                                {
                                    for (int j = 0; j <= result2; j++)
                                    {
                                        var chk_exist = Serv2.getTransaction_BackDay(contract.Rows[i]["ContractNumber"].ToString(),
                                                Convert.ToDateTime(contract.Rows[i]["smart_shop_open"]).AddDays(j).ToString("yyyy-MM-dd"));

                                        if (chk_exist.Rows.Count == 0)
                                        {

                                            Serv2.Insert_transaction_record(contract.Rows[i]["ContractNumber"].ToString(), "0", "0", "n",
                                                        Convert.ToDateTime(contract.Rows[i]["smart_shop_open"]).AddDays(j).ToString("yyyy-MM-dd 00:00:00", EngCI),
                                                        "", "", vat, contract.Rows[i]["smart_record_keyin_type"].ToString());

                                            Serv2.Insert_Message(contract.Rows[i]["contractnumber"].ToString(), "กรุณากรอกยอดขายของวันที่ " +
                                                        Convert.ToDateTime(contract.Rows[i]["smart_shop_open"]).AddDays(j).ToString("yyyy-MM-dd 00:00:00", EngCI)
                                                        , "y", DateTime.Now.ToString("yyyy-MM-dd"));
                                        }

                                    }

                                    string date_fail = "";
                                    var d_no = Serv2.GetRecord_NO(contract.Rows[i]["ContractNumber"].ToString());
                                    if (d_no.Rows.Count != 0)
                                    {
                                        for (int ii = 0; ii < d_no.Rows.Count; ii++)
                                        {
                                            date_fail = date_fail + Convert.ToDateTime(d_no.Rows[ii]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(8, 2)
                                              + "/" + Convert.ToDateTime(d_no.Rows[ii]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(5, 2)
                                              + "/" + Convert.ToDateTime(d_no.Rows[ii]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(0, 4)
                                              + ", ";

                                        }

                                    }
                                    date_fail = date_fail.Substring(0, date_fail.Length - 1);
                                    string[] dddd = date_fail.Split(',');


                                    if (contract.Rows[i]["smart_record_person_type"].ToString() == "ar")
                                    {
                                        var user = Serv2.GetUserbyUserid(contract.Rows[i]["smart_icon_staff_id"].ToString());
                                        if (user.Rows.Count != 0)
                                        {
                                            try
                                            {
                                                mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
                                                      "ระบบ Tenant Sales Data Collection : กรุณานำส่งยอดขายรายวัน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
                                                      "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
                                                      "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่ไม่นำส่งยอดขาย", "Link",
                                                      "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
                                                      contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
                                                      dddd, "", "icon", "ar", contract.Rows[i]["ContractNumber"].ToString(),"user");
                                            }
                                            catch (Exception ex)
                                            {
                                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "dailycheck_job_End_of_date");

                                            }
                                        }
                                    }
                                    else
                                    {
                                        var acc = Serv2.GetAccount(contract.Rows[i]["contractnumber"].ToString());
                                        if (acc.Rows.Count != 0)
                                        {
                                            string name = "";
                                            string email = "";
                                            for (int jjj = 0; jjj < acc.Rows.Count; jjj++)
                                            {
                                                bool contains = email.Contains(acc.Rows[jjj]["email"].ToString());
                                                if (contains == false)
                                                {
                                                    name = name + acc.Rows[jjj]["name"].ToString() + ",";
                                                    email = email + acc.Rows[jjj]["email"].ToString() + ",";
                                                }
                                            }
                                            name = name.Substring(0, name.Length - 1);
                                            email = email.Substring(0, email.Length - 1);


                                            try
                                            {
                                                mail.CallMail_noti_daily(email,
                                                      "ระบบ Tenant Sales Data Collection : กรุณานำส่งยอดขายรายวัน ร้าน " + contract.Rows[i]["Shopname"].ToString() + " ห้อง " + contract.Rows[i]["smart_room_no"].ToString(),
                                                      "Tenant Sales Data Collection", name,
                                                      "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่ไม่นำส่งยอดขาย", "Link",
                                                      "", contract.Rows[i]["ShopName"].ToString(), contract.Rows[i]["BusinessPartnerName"].ToString(),
                                                      contract.Rows[i]["smart_floor"].ToString(), contract.Rows[i]["smart_room_no"].ToString(),
                                                      dddd, "", "icon", "te", contract.Rows[i]["ContractNumber"].ToString(),"tenant");
                                            }
                                            catch (Exception ex)
                                            {
                                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "dailycheck_job_End_of_date");

                                            }

                                        }
                                    }
                                }

                            }
                        }
                    }
                    #endregion


                    #region FindContractLastDayNotShopClose
                    var contract_ = Serv2.GetContractLastDayNotShopClose_EOD();
                    if (contract_.Rows.Count != 0)
                    {
                        for (int i = 0; i < contract_.Rows.Count; i++)
                        {

                            int vat = 0;
                            var v = Serv2.GetVAT(contract_.Rows[i]["CompanyCode"].ToString());
                            if (v.Rows.Count != 0)
                            {
                                vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
                            }
                            else
                            {
                                vat = 7;
                            }


                            var Trans = Serv2.GetChkLastTrans_ShopClose(contract_.Rows[i]["ContractNumber"].ToString(), contract_.Rows[i]["smart_shop_close"].ToString());
                            if (Trans.Rows.Count == 0)
                            {
                                var LastD = Serv2.GetLastTran(contract_.Rows[i]["ContractNumber"].ToString());

                                string d3 = LastD.Rows[0]["LastDay"].ToString();
                                string d4 = contract_.Rows[i]["smart_shop_close"].ToString();
                                double result2 = (Convert.ToDateTime(d4) - Convert.ToDateTime(d3)).TotalDays;

                                if (result2 >= 0)
                                {
                                    for (int j = 0; j <= result2; j++)
                                    {
                                        var chk_exist = Serv2.getTransaction_BackDay(contract_.Rows[i]["ContractNumber"].ToString(),
                                                Convert.ToDateTime(LastD.Rows[0]["LastDay"]).AddDays(j).ToString("yyyy-MM-dd"));

                                        if (chk_exist.Rows.Count == 0)
                                        {

                                            Serv2.Insert_transaction_record(contract_.Rows[i]["ContractNumber"].ToString(), "0", "0", "n",
                                                        Convert.ToDateTime(LastD.Rows[0]["LastDay"]).AddDays(j).ToString("yyyy-MM-dd 00:00:00", EngCI),
                                                        "", "", vat, contract_.Rows[i]["smart_record_keyin_type"].ToString());

                                            Serv2.Insert_Message(contract_.Rows[i]["ContractNumber"].ToString(), "กรุณากรอกยอดขายของวันที่ " +
                                                        Convert.ToDateTime(LastD.Rows[0]["LastDay"]).AddDays(j).ToString("yyyy-MM-dd 00:00:00", EngCI)
                                                        , "y", DateTime.Now.ToString("yyyy-MM-dd"));
                                        }

                                    }

                                    string date_fail = "";
                                    var d_no = Serv2.GetRecord_NO(contract_.Rows[i]["ContractNumber"].ToString());
                                    if (d_no.Rows.Count != 0)
                                    {
                                        for (int ii = 0; ii < d_no.Rows.Count; ii++)
                                        {
                                            date_fail = date_fail + Convert.ToDateTime(d_no.Rows[ii]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(8, 2)
                                              + "/" + Convert.ToDateTime(d_no.Rows[ii]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(5, 2)
                                              + "/" + Convert.ToDateTime(d_no.Rows[ii]["record_date"]).ToString("yyyy-MM-dd", EngCI).Substring(0, 4)
                                              + ", ";

                                        }

                                    }
                                    date_fail = date_fail.Substring(0, date_fail.Length - 1);
                                    string[] dddd = date_fail.Split(',');


                                    if (contract_.Rows[i]["smart_record_person_type"].ToString() == "ar")
                                    {
                                        var user = Serv2.GetUserbyUserid(contract_.Rows[i]["smart_icon_staff_id"].ToString());
                                        if (user.Rows.Count != 0)
                                        {
                                            try
                                            {
                                                mail.CallMail_noti_daily(user.Rows[0]["email"].ToString(),
                                                      "ระบบ Tenant Sales Data Collection : กรุณานำส่งยอดขายรายวัน ร้าน " + contract_.Rows[i]["Shopname"].ToString() + " ห้อง " + contract_.Rows[i]["smart_room_no"].ToString(),
                                                      "Tenant Sales Data Collection", user.Rows[0]["Fname"].ToString(),
                                                      "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract_.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่ไม่นำส่งยอดขาย", "Link",
                                                      "", contract_.Rows[i]["ShopName"].ToString(), contract_.Rows[i]["BusinessPartnerName"].ToString(),
                                                      contract_.Rows[i]["smart_floor"].ToString(), contract_.Rows[i]["smart_room_no"].ToString(),
                                                      dddd, "", "icon", "ar", contract_.Rows[i]["ContractNumber"].ToString(),"user");
                                            }
                                            catch (Exception ex)
                                            {
                                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "dailycheck_job_End_of_date");

                                            }
                                        }
                                    }
                                    else
                                    {
                                        var acc = Serv2.GetAccount(contract_.Rows[i]["contractnumber"].ToString());
                                        if (acc.Rows.Count != 0)
                                        {
                                            string name = "";
                                            string email = "";
                                            for (int jjj = 0; jjj < acc.Rows.Count; jjj++)
                                            {
                                                bool contains = email.Contains(acc.Rows[jjj]["email"].ToString());
                                                if (contains == false)
                                                {
                                                    name = name + acc.Rows[jjj]["name"].ToString() + ",";
                                                    email = email + acc.Rows[jjj]["email"].ToString() + ",";
                                                }
                                            }
                                            name = name.Substring(0, name.Length - 1);
                                            email = email.Substring(0, email.Length - 1);


                                            try
                                            {
                                                mail.CallMail_noti_daily(email,
                                                      "ระบบ Tenant Sales Data Collection : กรุณานำส่งยอดขายรายวัน ร้าน " + contract_.Rows[i]["Shopname"].ToString() + " ห้อง " + contract_.Rows[i]["smart_room_no"].ToString(),
                                                      "Tenant Sales Data Collection", name,
                                                      "กรุณานำส่งยอดขาย ตามรายละเอียดด้านล่าง", contract_.Rows[i]["CompanyCode"].ToString(), "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่ไม่นำส่งยอดขาย", "Link",
                                                      "", contract_.Rows[i]["ShopName"].ToString(), contract_.Rows[i]["BusinessPartnerName"].ToString(),
                                                      contract_.Rows[i]["smart_floor"].ToString(), contract_.Rows[i]["smart_room_no"].ToString(),
                                                      dddd, "", "icon", "te", contract_.Rows[i]["ContractNumber"].ToString(),"tenant");
                                            }
                                            catch (Exception ex)
                                            {
                                                l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "dailycheck_job_End_of_date");

                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "success", "dailycheck_job_End_of_date");


                }
                catch (Exception ex)
                {

                    l.Log(DateTime.Now.ToString("yyyyMMdd_HHmmss", EngCI) + "Fail : " + ex.ToString(), "dailycheck_job_End_of_date");
                }
            }

        }


    }
}