﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="edit_user.aspx.cs" Inherits="iconsiam.edit_user" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }

        function isEnglishOnly(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode >= 48 && charCode <= 122) || charCode == 8 || charCode == 13 || charCode == 46 || charCode == 32)
                return true;

            return false;
        }

    </script>

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Edit User
            </p>
        </div>
    </div>
    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtusername" runat="server" placeholder="Username" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtemployeecode" runat="server" placeholder="Employee Code" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtfname" runat="server" placeholder="First Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtlname" runat="server" placeholder="Last Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtemail" runat="server" placeholder="Email" class="form-control" onkeypress="return isEnglishOnly(event)"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtext" runat="server" placeholder="เบอร์ต่อ" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtmobile" runat="server" placeholder="Mobile" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddlstatus" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>
            <br />
            <%--<div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddlnoshow_img" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>
            <br />--%>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <asp:DropDownList ID="ddlrole" runat="server" AutoPostBack="true" class="form-control" OnSelectedIndexChanged="ddlrole_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <asp:DropDownList ID="ddlteam" runat="server" Visible="false" class="form-control"></asp:DropDownList>
                        </div>
                    </div>

                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ForeColor="White" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" ForeColor="White" class="btn btn-danger" Width="114px" />
                    <asp:Button ID="btnresetpass" runat="server" Text="Reset Password" OnClick="btnresetpass_Click" ForeColor="White" class="btn btn-info" />
                </div>
            </div>

        </div>
    </div>




</asp:Content>
