﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="change_password.aspx.cs" Inherits="iconsiam.change_password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }
    </script>

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Change Passwrod
            </p>
        </div>
    </div>
    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtoldpassword" runat="server" placeholder="Old Password" TextMode="Password" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtnewpassword" runat="server" placeholder="New Password" TextMode="Password" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtconfirmpassword" runat="server" placeholder="Confirm Password" TextMode="Password" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ForeColor="White" class="btn btn-success" Width="114px" />
                </div>
            </div>

        </div>
    </div>


</asp:Content>
