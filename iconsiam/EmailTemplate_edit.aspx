﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EmailTemplate_edit.aspx.cs" Inherits="iconsiam.EmailTemplate_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }

    function isEnglishOnly(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode >= 48 && charCode <= 122) || charCode == 8 || charCode == 13 || charCode == 46 || charCode == 32)
            return true;

        return false;
    }
    </script>

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12  col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Edit Email Template
            </p>
        </div>
    </div>

        <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">         

            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <asp:Label ID="Label16" runat="server" Text="Company Code"></asp:Label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txtcompanyCode" runat="server" placeholder="Company Code" ReadOnly="true" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <asp:Label ID="Label15" runat="server" Text="Company Name"></asp:Label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txtcompanyNameTh"  runat="server" placeholder="Company Name (Th)" ReadOnly="true" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
           <%-- <div class="row">
                <div class="col-md-12 col-lg-12">
                    <asp:Label ID="Label14" runat="server" Text="Link"></asp:Label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txtlink" runat="server" placeholder="Link" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />--%>
             <div class="row">
                 <div class="col-md-12 col-lg-12">
                    <asp:Label ID="Label13" runat="server" Text="Send from team"></asp:Label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txtsendfromteam" runat="server" placeholder="Sendfromteam" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <asp:Label ID="Label12" runat="server" Text="Send from company"></asp:Label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txtsendfromcompany" runat="server" placeholder="Sendfromcompany" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <asp:Label ID="Label11" runat="server" Text="Tel"></asp:Label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txttel" runat="server" placeholder="Tel" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <asp:Label ID="Label10" runat="server" Text="Email Footer"></asp:Label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txtemail_footer" runat="server" placeholder="Email Footer" class="form-control"  onkeypress="return isEnglishOnly(event)"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <asp:Label ID="Label9" runat="server" Text="Tel Calcenter"></asp:Label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txtcallcenter_Footer" runat="server" placeholder="Tel Calcenter" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>  
            
             <br />
             <br />
            <div class="row">
                <div class="col-md-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ForeColor="White" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" ForeColor="White" class="btn btn-danger" Width="114px" />
                </div>
            </div>

        </div>
    </div>

</asp:Content>
