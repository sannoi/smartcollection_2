﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="shop_sap_master_old.aspx.cs" Inherits="iconsiam.shop_sap_master_old" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }


        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10005;
        }

        function isEnglishOnly(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode >= 48 && charCode <= 122) || charCode == 8 || charCode == 13 || charCode == 46 || charCode == 32)
                return true;

            return false;
        }


    </script>

    <link href="css/ProgressNotifier.css" rel="stylesheet" type="text/css" />


    <style type="text/css">
        .test .ajax__calendar_container {
            padding: 10px;
            position: absolute;
            cursor: default;
            width: 300px;
            font-size: 15px;
            text-align: center;
            font-family: tahoma,erdana,helvetica;
            background-color: lemonchiffon;
            border: 1px solid #646464;
            z-index: 10000 !important;
            text-wrap: none;
        }

        .test .ajax__calendar_dayname {
            height: 17px;
            width: 34px;
            text-align: center;
        }
    </style>

    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %> !important;
            Color: <%= NavbarColor %>;
        }

        .ThemeBroder {
            border: 1px solid <%= MyTheme %>;
        }
    </style>

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hddUsageTypeName" runat="server" />
            <asp:HiddenField ID="hddBuildingCode" runat="server" />
            <asp:Panel ID="Panel1" runat="server">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-md-3 col-lg-3" style="text-align: left">
                                <p style="font-size: 25px; line-height: 1.5;">
                                    <asp:Label ID="lbheader" runat="server" Text="-"></asp:Label>
                                </p>
                            </div>
                            <div class="col-md-9 col-lg-9" style="text-align: right">
                                <asp:Button ID="Button1" runat="server" Text="คัดลอกสัญญา" OnClick="Button1_Click" class="btn  ThemeBtn" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div style="margin-left: 20px; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Company :
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtcompanyname" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Contract Code : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtcontractcode" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Customer Code : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtcustomercode" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Customer Name : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtcustomername" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        วันเริ่มต้นสัญญา : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtSAP_contract_start" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        วันสิ้นสุดสัญญา : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtSAP_contract_end" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        วันที่เปิดร้าน :                                     
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtopendate" runat="server" class="form-control ThemeBroder" AutoCompleteType="Disabled" ReadOnly="true"></asp:TextBox>

                                        <asp:CalendarExtender ID="txtopendate_CalendarExtender" runat="server"
                                            BehaviorID="txtopendate_CalendarExtender" TargetControlID="txtopendate" OnClientShown="calendarShown" Format="dd/MM/yyyy"></asp:CalendarExtender>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        วันที่ปิดร้านขาย : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtClosedate" runat="server" class="form-control ThemeBroder" AutoCompleteType="Disabled" ReadOnly="true"></asp:TextBox>

                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                            BehaviorID="txtClosedate_CalendarExtender" TargetControlID="txtClosedate" OnClientShown="calendarShown" Format="dd/MM/yyyy"></asp:CalendarExtender>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Shop Name : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtshopname" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Building : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtbuilding" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Room No. : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtroomno" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Floor : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtfloor" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Area Sqm. : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txttotalarea" ReadOnly="true" runat="server" TextMode="Number" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Room Type : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtroomtype" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Group Location :                                     
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:DropDownList ID="ddlgroup_location" runat="server" class="form-control ThemeBroder" disabled = "false"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Contract Type : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:DropDownList ID="ddlcontracttype" runat="server" class="form-control ThemeBroder" disabled = "false"></asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        SAP Contract Type : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtsapxintract_type" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px; text-align: left;">
                            <div class="col-md-6 col-lg-6" style="text-align: left;">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        เงื่อนไขยอดขาย คำนวน GP : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtcondition_gp" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Category Leasing : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:DropDownList ID="ddlcategory_leasing" runat="server" class="form-control ThemeBroder" disabled = "false"></asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Sub Type Name : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtcollection_type" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        กลุ่มร้านค้าหรือไม่ : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:DropDownList ID="ddlflag_shop_group" AutoPostBack="true" runat="server" class="form-control ThemeBroder"
                                            OnSelectedIndexChanged="ddlflag_shop_group_SelectedIndexChanged" disabled = "false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Shop Group : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:DropDownList ID="ddlshopgroup" runat="server" Visible="false" class="form-control ThemeBroder">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Industry group :                                     
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:HiddenField ID="ddlindustry_group" runat="server" />
                                        <asp:TextBox ID="txtddlindustry_group" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        Industry : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:TextBox ID="txtindustry" ReadOnly="true" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        ประเภทร้านค้าสถานที่ : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:DropDownList ID="ddlshop_type" runat="server" class="form-control ThemeBroder" disabled = "false"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6 col-lg-6" style="text-align: left;">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        AR STAFF : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:DropDownList ID="ddlicon_staff" runat="server" class="form-control ThemeBroder" disabled = "false"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 10px;">

                            <div class="col-md-6 col-lg-6" style="text-align: left;">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" style="text-align: right">
                                        AE Staff : 
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <asp:DropDownList ID="ddlae_staff" runat="server" class="form-control ThemeBroder" disabled = "false"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br />

                        <%--Email CC--%>
                        <div class="card ">
                            <div class="row" style="padding-top: 40px; padding-bottom: 10px; padding-left: 20px; padding-right: 20px;">
                                <div class="col-md-9 col-lg-9" style="text-align: left;">
                                    AR STAFF Email CC
                                </div>                                
                            </div>
                            <div class="row" style="padding-bottom: 10px; padding-left: 20px; padding-right: 20px;">
                                <div class="col-md-12 col-lg-12">
                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        ShowFooter="false" PageSize="50" BackColor="White" class="table mt-3">
                                        <Columns>
                                            <asp:BoundField DataField="name" HeaderText="ชื่อ-นามสกุล" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                                            <asp:BoundField DataField="email" HeaderText="Email" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />                                           

                                        </Columns>
                                    </asp:GridView>
                                </div>

                            </div>
                        </div>


                        <br />

                        <div class="card">
                            <div class="row" style="padding-top: 40px; padding-bottom: 10px; padding-left: 20px; padding-right: 20px;">
                                <div class="col-md-9 col-lg-9" style="text-align: left;">
                                    Tenant Contact Persons
                                </div>
                                
                            </div>
                            <div class="row" style="padding-bottom: 10px; padding-left: 20px; padding-right: 20px;">
                                <div class="col-md-12 col-lg-12">
                                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        ShowFooter="false" PageSize="50" BackColor="White" class="table mt-3">
                                        <Columns>
                                            <asp:BoundField DataField="name" HeaderText="ชื่อ-นามสกุล" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                                            <asp:BoundField DataField="status_text" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="email" HeaderText="Email" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="Tel1" HeaderText="เบอร์โทร" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="Tel2" HeaderText="เบอร์มือถือ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="main" HeaderText="ผู้ติดต่อหลัก" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                                        </Columns>
                                    </asp:GridView>
                                </div>

                            </div>
                        </div>
                        <br />
                        <%--    <<<<<<< HEAD--%>



                        <%--=======--%>
                        <div class="card ">
                            <%-->>>>>>> newnut--%>
                            <div class="row" style="padding-top: 40px; padding-bottom: 10px; padding-left: 20px; padding-right: 20px;">
                                <div class="col-md-12 col-lg-12">
                                    <p>
                                        การบันทึกยอดเข้าระบบ
                                    </p>
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 40px; padding-left: 20px; padding-right: 20px;">
                                <div class="col-md-12 col-lg-12" style="padding-left: 40px; padding-right: 40px;">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12">
                                            <div class="row">
                                                <div class="col-md-2 col-lg-2" style="float: left;">
                                                    <p style="float: right;">
                                                        ผู้รับผิดชอบ : 
                                                    </p>
                                                </div>
                                                <div class="col-md-10 col-lg-10" style="padding: 0;">
                                                    <asp:DropDownList ID="ddl_User_keyIntype" runat="server" class="form-control" disabled = "false"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px;">
                                        <div class="col-md-12 col-lg-12">
                                            <div class="row">
                                                <div class="col-md-2 col-lg-2" style="float: left !important;">
                                                    <p style="float: right;">
                                                        เวลาในการแจ้งเตือน :
                                                    </p>
                                                </div>
                                                <div class="col-md-10 col-lg-10">
                                                    <div class="row">

                                                        <asp:DropDownList ID="ddl_keyin_type" Width="200px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_keyin_type_SelectedIndexChanged" class="form-control" Style="margin-right: 1%;" disabled = "false"></asp:DropDownList>
                                                        <asp:DropDownList ID="ddl_keyin_type_sub" AutoPostBack="true" OnSelectedIndexChanged="ddl_keyin_type_sub_SelectedIndexChanged" Width="200px" runat="server" class="form-control" Style="margin-left: 1%; margin-right: 1%;" disabled = "false"></asp:DropDownList>
                                                        <asp:TextBox ID="txtkeydate" runat="server" Width="100%" class="form-control" Visible="false" Style="margin-left: 1%; margin-right: 1%;"></asp:TextBox>

                                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" BehaviorID="txtkeydate_CalendarExtender" TargetControlID="txtkeydate" OnClientShown="calendarShown" Format="dd"></asp:CalendarExtender>
                                                        <asp:DropDownList ID="ddlhh" runat="server" class="form-control" Width="100px" Style="margin-left: 1%; margin-right: 1%;" disabled = "false"></asp:DropDownList>
                                                        <asp:DropDownList ID="ddlmm" runat="server" class="form-control" Width="100px" Style="margin-left: 1%; margin-right: 1%;" disabled = "false"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table style="width: auto">
                                            <tr>
                                                <%-- <td style="text-align: right">เวลาในการแจ้งเตือน : 
                                                </td>--%>
                                                <td>
                                                    <%--   <asp:DropDownList ID="ddl_keyin_type" Width="200px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_keyin_type_SelectedIndexChanged" class="form-control"></asp:DropDownList>--%>
                                                </td>
                                                <td>
                                                    <%-- <asp:DropDownList ID="ddl_keyin_type_sub" AutoPostBack="true" OnSelectedIndexChanged="ddl_keyin_type_sub_SelectedIndexChanged" Width="200px" runat="server" class="form-control"></asp:DropDownList>
                                                    <asp:TextBox ID="txtkeydate" runat="server" Width="100%" class="form-control" Visible="false"></asp:TextBox>

                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                                        BehaviorID="txtkeydate_CalendarExtender" TargetControlID="txtkeydate" OnClientShown="calendarShown" Format="dd"></asp:CalendarExtender>--%>
                                                </td>
                                                <td>
                                                    <%-- <asp:DropDownList ID="ddlhh" runat="server" class="form-control" Width="100px"></asp:DropDownList>--%>

                                                </td>
                                                <td>
                                                    <%--  <asp:DropDownList ID="ddlmm" runat="server" class="form-control" Width="100px"></asp:DropDownList>--%>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="card ">
                            <div class="row" style="padding-top: 20px; padding-bottom: 20px; padding-left: 20px; padding-right: 20px;">
                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="row">
                                                <div class="col-md-3 col-lg-3">
                                                    <p style="float: right;">
                                                        สถานะ : 
                                                    </p>
                                                </div>
                                                <div class="col-md-9 col-lg-9">
                                                    <asp:DropDownList ID="ddlstatus" runat="server" class="form-control " disabled = "false"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="row">
                                                <div class="col-md-3 col-lg-3">
                                                    <p style="float: right;">
                                                        รูปแบบการบันทึก :
                                                    </p>
                                                </div>
                                                <div class="col-md-9 col-lg-9">
                                                    <asp:DropDownList ID="ddlrecord_type" runat="server" class="form-control " disabled = "false"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12" style="text-align: center;">                              
                                <asp:Button ID="btncancel" runat="server" Text="Back" OnClick="btncancel_Click" ForeColor="White" class="btn btn-danger" Width="114px" />
                            </div>
                        </div>
                    </div>

                </div>
            </asp:Panel>

            <asp:Panel ID="Panel3" runat="server" Visible="false">

                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <p class="text-center">
                            Create Username 
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3"></div>
                    <div class="col-md-6 col-lg-6">
                        <asp:TextBox ID="txtusername" runat="server" placeholder="Username" class="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-3 col-lg-3"></div>
                </div>
                <br />

                <div class="row">
                    <div class="col-md-3 col-lg-3"></div>
                    <div class="col-md-6 col-lg-6" style="text-align: center">
                        <asp:Button ID="btnsubmit" runat="server" Text="SAVE" OnClick="btnsubmit_Click" Width="100%" ForeColor="Black" class="btn" BackColor="#f8ca3e" BorderColor="#a78a65" />
                    </div>
                    <div class="col-md-3 col-lg-3"></div>
                </div>

            </asp:Panel>
            <asp:Panel ID="Panel4" runat="server" Visible="false">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <p style="font-size: 25px; line-height: 1.5;">
                            ค้นหา Contract Shop (SAP)
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                                <asp:DropDownList ID="ddlcompany" runat="server" class="form-control"></asp:DropDownList>
                            </div>
                            <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                                <asp:TextBox ID="txtshopname_copy" runat="server" placeholder="Shop Name" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                                <asp:TextBox ID="txtcontractnumber_copy" runat="server" placeholder="Contract Number" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                                <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" ForeColor="Black" class="btn" BackColor="#f8ca3e" BorderColor="#a78a65" Width="114px" />
                                <asp:Button ID="btnback" runat="server" Text="Back" OnClick="btnback_Click" ForeColor="Black" class="btn btn-danger" BorderColor="#a78a65" Width="114px" />
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="card">
                            <div style="margin: 20px 0px; overflow: scroll">

                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    ShowFooter="false" PageSize="50" class="table mt-3" OnRowDataBound="GridView1_RowDataBound">
                                    <Columns>

                                        <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="smart_room_no" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="startdate" HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="enddate" HeaderText="End Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>


            <asp:Panel ID="Panel5" runat="server" Visible="false">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <p style="font-size: 25px; line-height: 1.5;">
                            Edit Member(s)
                        </p>
                    </div>
                </div>
                <div class="card">
                    <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <asp:TextBox ID="txtname_edit" runat="server" placeholder="ชื่อ-นามสกุล" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <asp:Label ID="Label13" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <asp:TextBox ID="txtemail_edit" runat="server" placeholder="Email" class="form-control" onkeypress="return isEnglishOnly(event)"></asp:TextBox>
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <asp:Label ID="Label14" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <asp:TextBox ID="txttel1_edit" runat="server" placeholder="เบอร์โทร" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <asp:Label ID="Label15" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <asp:TextBox ID="txttel2_edit" runat="server" placeholder="เบอร์มือถือ" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-check">
                                    <label>ผู้ดูแลหลัก</label><br />
                                    <asp:DropDownList ID="ddlmain_contact_edit" runat="server" class="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-check">
                                    <label>Status</label><br />
                                    <asp:DropDownList ID="ddlstatus_contact_point_edit" runat="server" class="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-9 col-lg-9">                              
                                <asp:Button ID="Button3" runat="server" Text="Cancel" OnClick="Button3_Click1" ForeColor="White" class="btn btn-danger" Width="114px" />
                            </div>
                        </div>

                    </div>
                </div>

            </asp:Panel>  
             </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
        <ProgressTemplate>
            <div class="ModalProgressContainer">
                <div class="ModalProgressContent" style="text-align: center; position: absolute; font-family: Tahoma; font-size: 10pt">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/img/loading_trans.gif" Width="200px" Height="20px" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
