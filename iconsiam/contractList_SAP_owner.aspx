﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="contractList_SAP_owner.aspx.cs" Inherits="iconsiam.contractList_SAP_owner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />
     <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                My Shop (SAP)
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlcompany" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtshopgroup" runat="server" placeholder="Shop Group Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:TextBox ID="txtshopname" runat="server" placeholder="Shop Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn"/>
                    <asp:Button ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click" class="btn ThemeBtn"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlar" runat="server" class="form-control" Visible="false"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                    <asp:DropDownList ID="ddlstatus" runat="server" placeholder="Status" class="form-control"></asp:DropDownList>
                </div>
               <%-- <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>--%>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px; padding-right: 0px;">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="smart_room_no" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="startdate" HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="enddate" HeaderText="End Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="จัดการร้านค้า" OnClick="btnedit_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GridView_List1" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List1_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="smart_room_no" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="startdate" HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="enddate" HeaderText="End Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="ข้อมูลร้านค้า" OnClick="btnedit_old_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>


                    <asp:GridView ID="GridView_List2" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List2_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="smart_room_no" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="arname" HeaderText="AR" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="startdate" HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="enddate" HeaderText="End Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="จัดการร้านค้า" OnClick="btnedit_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GridView_List3" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List3_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="smart_room_no" HeaderText="Room No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="ShopName" HeaderText="Shop Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="arname" HeaderText="AR" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="startdate" HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="enddate" HeaderText="End Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("ContractNumber") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="ข้อมูลร้านค้า" OnClick="btnedit_old_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>
            </div>


        </div>
    </div>



</asp:Content>
