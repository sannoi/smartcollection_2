﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;


namespace iconsiam
{
    public partial class CompanyMgm : System.Web.UI.Page
    {
        CompanyGroupMgmDLL Serv = new CompanyGroupMgmDLL();

        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    bind_data();
                }
            }
        }

        protected void bind_data()
        {
            var comp = Serv.getComName(txtcompany.Text);
            if (comp.Rows.Count != 0)
            {
                GridView_List.DataSource = comp;
                GridView_List.DataBind();

            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hddCompanycode = (HiddenField)row.FindControl("hddCompanycode");
            Response.Redirect("~/CompanyMgm_edit.aspx?Companycode=" + hddCompanycode.Value);
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.bind_data();
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CompanyMgm.aspx");
        }

    }
}