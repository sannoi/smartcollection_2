﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for shopGroupListDLL
/// </summary>
public class shopGroupListDLL
{
    public shopGroupListDLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetShopGroupByName(string CompanyCode, string flag_active, string[] shopgroup, string company_code, string role)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string shopgroup_ = "";

        if (shopgroup.Length != 0)
        {
            for (int i = 0; i < shopgroup.Length; i++)
            {
                shopgroup_ += " (s.ShopGroupNameEN like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                    " or s.ShopGroupNameTH like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' ) or";
            }

            shopgroup_ = shopgroup_.Substring(0, shopgroup_.Length - 2);
        }





        if (CompanyCode != "")
        {
            if (role != "datacenter" && role != "super_admin")
            {
                strSQL += " select s.*,c.CompanyNameTH, case when s.flag_active = '1' then 'Active' else 'Inactive' end as status " +
                   " from tblshopgroup s  " +
                   " inner join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                   " where  s.CompanyCode = '" + CompanyCode + "' and s.flag_active = '" + flag_active + "' ";
            }
            else
            {
                strSQL += " select s.*, case when s.flag_active = '1' then 'Active' else 'Inactive' end as status " +
                   " from tblshopgroup s  " +                
                   " where  s.CompanyCode = '" + CompanyCode + "' and s.flag_active = '" + flag_active + "' ";
            }
                

            if (shopgroup_ != "")
            {
                strSQL = strSQL + " and  " + shopgroup_;
            }

            if (role != "datacenter" && role != "super_admin")
            {
                strSQL = strSQL + " and s.id in ( select ShopGroupId from TblShopGroup_mapping where ComGroupID in (  ";
                strSQL = strSQL + " select distinct ComGroupID from TblCompanyGroupMapping where CompanyCode in (" + CompanyCode + ")))  order by id  ";
            }
            else
            {
                strSQL = strSQL + "  order by id  ";
            }


        }
        else
        {
            if (role != "datacenter" && role != "super_admin")
            {
                strSQL += " select s.*,c.CompanyNameTH, case when s.flag_active = '1' then 'Active' else 'Inactive' end as status " +
                   " from tblshopgroup s  " +
                   " inner join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                   " where  s.CompanyCode = '" + CompanyCode + "' and s.flag_active = '" + flag_active + "' ";
            }
            else
            {
                strSQL += " select s.*, case when s.flag_active = '1' then 'Active' else 'Inactive' end as status " +
                   " from tblshopgroup s  " +
                   " where  s.CompanyCode = '" + CompanyCode + "' and s.flag_active = '" + flag_active + "' ";
            }

            if (shopgroup_ != "")
            {
                strSQL = strSQL + " and  " + shopgroup_;
            }

            if (role != "datacenter" && role != "super_admin")
            {
                strSQL = strSQL + " and s.id in ( select ShopGroupId from TblShopGroup_mapping where ComGroupID in (  ";
                strSQL = strSQL + " select distinct ComGroupID from TblCompanyGroupMapping where CompanyCode in (" + CompanyCode + ")))  order by id  ";
            }
            else
            {
                strSQL = strSQL + "  order by id  ";
            }


        }



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetShopGroupByID(string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select s.*,c.CompanyNameTH, case when s.flag_active = '1' then 'Active' else 'Inactive' end as status " +
                  " from tblshopgroup s inner " +
                  " join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                  " where  s.id = '" + id + "' ";

        if (HttpContext.Current.Session["role"].ToString() == "datacenter" || HttpContext.Current.Session["role"].ToString() == "super_admin")
        {
            strSQL = "";
            strSQL += " select s.*, case when s.flag_active = '1' then 'Active' else 'Inactive' end as status " +
                              " from tblshopgroup s  " +
                              " where  s.id = '" + id + "' ";
        }

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetCompany(string[] shopgroup)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        string shopcode_ = "";

        if (shopgroup.Length != 0)
        {
            shopcode_ += "(";
            for (int i = 0; i < shopgroup.Length; i++)
            {
                shopcode_ += " a.CompanyCode =" + shopgroup[i].Replace("&", "' + char(38) + '") + " or";
            }

            shopcode_ = shopcode_.Substring(0, shopcode_.Length - 2);
            shopcode_ = shopcode_ + ")";
        }


        //strSQL += " select * from SAPCompany where IsActive = '1' order by CompanyNameTH  ";

        //strSQL += " select * from SAPCompany where IsActive = '1' and CompanyCode in (" + com_code + ") order by CompanyNameTH  ";

        //NEW NUT
        strSQL += " select * from TblCompanyDetail a " +
                    " inner join TblCompanyGroupMapping b on b.CompanyCode = a.CompanyCode " +
                    " where a.IsActive = 'y' and b.IsActive = 'y' ";
        if (shopcode_ != "")
        {
            strSQL = strSQL + " and " + shopcode_;
        }



        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }



    public DataTable GetMapCompany(string userid)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        if (userid != "")
        {
            strSQL += " select  distinct ComGroupID from tblmapp_User_Company where userid in (" + userid + ") ;  ";

        }
        else
        {
            strSQL += " select  distinct ComGroupID from tblmapp_User_Company ;  ";

        }


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public void InsertShopGroup(string ShopGroupNameEN, string ShopGroupNameTH, string CreateID, string CompanyCode, string flag_active)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " insert into tblshopgroup(ShopGroupNameEN,ShopGroupNameTH,CreateDate,CreateID,CompanyCode,flag_active) " +
                " values('" + ShopGroupNameEN + "','" + ShopGroupNameTH + "', GETDATE(), '" + CreateID + "', '" + CompanyCode + "', '" + flag_active + "')  ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public void UpdateShopGroup(string ShopGroupNameEN, string ShopGroupNameTH, string UpdateID, string CompanyCode, string flag_active, string id)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = null;

        strSQL = " Update tblshopgroup set ShopGroupNameEN = '" + ShopGroupNameEN + "',ShopGroupNameTH = '" + ShopGroupNameTH + "', " +
            " UpdateDate = GETDATE(),UpdateID = '" + UpdateID + "',CompanyCode = '" + CompanyCode + "',flag_active = '" + flag_active + "' where id = '" + id + "' ";


        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        objConn.Open();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;

        objCmd.ExecuteNonQuery();

        dtAdapter = null;
        objConn.Close();
        objConn = null;

    }

    public DataTable GetShopGroupByNameEN(string ShopGroupNameEN, string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select  *  from tblshopgroup where ShopGroupNameEN = '" + ShopGroupNameEN + "' and CompanyCode = '" + CompanyCode + "' ;  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetShopGroupByNameTH(string ShopGroupNameTH, string CompanyCode)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select  *  from tblshopgroup where ShopGroupNameTH = '" + ShopGroupNameTH + "'  and CompanyCode = '" + CompanyCode + "'  ;  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetShopGroupByNameEN_nid(string ShopGroupNameEN, string CompanyCode, string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select  *  from tblshopgroup where ShopGroupNameEN = '" + ShopGroupNameEN + "' and CompanyCode = '" + CompanyCode + "' and id != '" + id + "' ;  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }

    public DataTable GetShopGroupByNameTH_nid(string ShopGroupNameTH, string CompanyCode, string id)
    {

        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        SqlDataAdapter dtAdapter = new SqlDataAdapter();

        DataSet ds = new DataSet();
        DataTable dt = null;
        string strSQL = "";

        strSQL += " select  *  from tblshopgroup where ShopGroupNameTH = '" + ShopGroupNameTH + "'  and CompanyCode = '" + CompanyCode + "' and id != '" + id + "' ;  ";

        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
        var _with1 = objCmd;
        _with1.Connection = objConn;
        _with1.CommandText = strSQL;
        _with1.CommandType = CommandType.Text;
        dtAdapter.SelectCommand = objCmd;

        dtAdapter.Fill(ds);
        dt = ds.Tables[0];

        dtAdapter = null;
        objConn.Close();
        objConn = null;

        return dt;
    }









}