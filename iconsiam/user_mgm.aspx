﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="user_mgm.aspx.cs" Inherits="iconsiam.user_mgm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

     <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                User List
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtname" runat="server" class="form-control" placeholder="Name"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlrole" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlstatus" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn"/>
                    <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click" class="btn ThemeBtn"/>
                    <asp:Button ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click" class="btn ThemeBtn"/>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="username" HeaderText="Username" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="name" HeaderText="Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="mobile" HeaderText="Mobile" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Role" HeaderText="Role" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="user_group" HeaderText="Team" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="update_date" HeaderText="Update Date" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("userid") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="Edit" OnClick="btnedit_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>
            </div>


        </div>
    </div>



</asp:Content>
