﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="industryGroupMapp.aspx.cs" Inherits="iconsiam.industryGroupMapp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Industry Group Mapping
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtindustrygroup" runat="server" class="form-control" placeholder="Industry Group"></asp:TextBox>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlcompanyList" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-6 col-lg-6" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn"/>
                    <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click" class="btn ThemeBtn"/>
                    <asp:Button ID="btncancel" runat="server" Text="Clear" OnClick="btncancel_Click" class="btn ThemeBtn"/>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">
                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>
                            <asp:BoundField DataField="IndustryGroupNameEN" HeaderText="Industry Group Name EN" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="IndustryGroupNameTH" HeaderText="Industry Group Name Th" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="username" HeaderText="Username" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="UpdateDate" HeaderText="Update Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("id") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="ลบ" OnClick="btnedit_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>

        </div>
    </div>


</asp:Content>
