﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="add_mapping_group.aspx.cs" Inherits="iconsiam.add_mapping_group" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function next_tools(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.focus();
                    return false;
                }
            }
        }
        function isNumberKey2AndEnter(event, buttonid)  // ตัวเลขอย่างเดียว
        {
            var charCode = (event.which) ? event.which : event.keyCode
            if (charCode != 13 && (charCode != 46) && (charCode != 08) && (charCode < 48 || charCode > 57)) {
                return false;
            }
            else {
                var evt = event ? event : window.event;
                var bt = document.getElementById(buttonid);
                if (bt) {
                    if (evt.keyCode == 13) {
                        bt.focus();
                        return false;
                    }
                }
                //return true;
            }
        }
    </script>

    <link rel="stylesheet" href="assets/css/demo.css" />



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Add Company group mapping
            </p>
        </div>
    </div>
    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">

            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <asp:DropDownList ID="ddlrole" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlrole_SelectedIndexChanged"
                                class="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <asp:DropDownList ID="ddluser" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddluser_SelectedIndexChanged"
                                class="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-lg-12" style="text-align: right;">
                            <asp:Button ID="btnresetpass" runat="server" Text="Save Mapping" OnClick="btnresetpass_Click" ForeColor="White" class="btn btn-info" />
                            <asp:Button ID="btnback" runat="server" Text="back" OnClick="btnback_Click" ForeColor="White" class="btn btn-info" />
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                ShowFooter="false" PageSize="50" class="table mt-3">
                                <Columns>

                                    <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#F2F3F8">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("ComGroupID") %>' />
                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="ComGroupName" HeaderText="Company Group Name" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-BackColor="#F2F3F8" ItemStyle-HorizontalAlign="Left" />

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</asp:Content>
