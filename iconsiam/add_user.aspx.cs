﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class add_user : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        add_userDLL Serv = new add_userDLL();
        sentmail sentMail = new sentmail();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {

                    txtusername.Focus();
                    txtusername.Attributes.Add("onkeypress", "return next_tools(event,'" + txtemployeecode.ClientID + "')");
                    txtemployeecode.Attributes.Add("onkeypress", "return next_tools(event,'" + txtfname.ClientID + "')");
                    txtfname.Attributes.Add("onkeypress", "return next_tools(event,'" + txtlname.ClientID + "')");
                    txtlname.Attributes.Add("onkeypress", "return next_tools(event,'" + txtemail.ClientID + "')");
                    //txtemail.Attributes.Add("onkeypress", "return next_tools(event,'" + txtext.ClientID + "')");
                    txtext.Attributes.Add("onkeypress", "return next_tools(event,'" + txtmobile.ClientID + "')");
                    txtmobile.Attributes.Add("onkeypress", "return next_tools(event,'" + txtmobile.ClientID + "')");



                    bind_default();
                }
            }
        }

        protected void bind_default()
        {
            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("Active", "y"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "n"));

            //ddlnoshow_img.Items.Insert(0, new ListItem("ไม่แสดงภาพ", "y"));
            //ddlnoshow_img.Items.Insert(1, new ListItem("แสดงภาพ", "n"));

            ddlrole.Items.Clear();

            if (HttpContext.Current.Session["role"].ToString() == "admin")
            {
                ddlrole.Items.Insert(0, new ListItem("Select Role", ""));
                //ddlrole.Items.Insert(1, new ListItem("Super Admin", "super_admin"));
                //ddlrole.Items.Insert(2, new ListItem("Datacenter", "datacenter"));
                ddlrole.Items.Insert(1, new ListItem("Admin", "admin"));
                ddlrole.Items.Insert(2, new ListItem("Officer", "officer"));
                ddlrole.Items.Insert(3, new ListItem("Manager", "manager"));
                ddlrole.Items.Insert(4, new ListItem("VP", "vp"));
                ddlrole.Items.Insert(5, new ListItem("Management", "management"));
            }
            else
            {
                ddlrole.Items.Insert(0, new ListItem("Select Role", ""));
                ddlrole.Items.Insert(1, new ListItem("Super Admin", "super_admin"));
                ddlrole.Items.Insert(2, new ListItem("Datacenter", "datacenter"));
                ddlrole.Items.Insert(3, new ListItem("Admin", "admin"));
                ddlrole.Items.Insert(4, new ListItem("Officer", "officer"));
                ddlrole.Items.Insert(5, new ListItem("Manager", "manager"));
                ddlrole.Items.Insert(6, new ListItem("VP", "vp"));
                ddlrole.Items.Insert(7, new ListItem("Management", "management"));
            }



            ddlteam.Items.Clear();
            ddlteam.Items.Insert(0, new ListItem("Select Group", ""));
            ddlteam.Items.Insert(1, new ListItem("AR", "ar"));
            ddlteam.Items.Insert(1, new ListItem("Sales", "ae"));

            var comp = Serv.GetCompany_Group();
            if (comp.Rows.Count != 0)
            {
                ddlteamAdmin.DataTextField = "ComGroupName";
                ddlteamAdmin.DataValueField = "ComGroupID";
                ddlteamAdmin.DataSource = comp;
                ddlteamAdmin.DataBind();
            }
            else
            {
                ddlteamAdmin.DataSource = null;
                ddlteamAdmin.DataBind();

            }
            ddlteamAdmin.Items.Insert(0, new ListItem("Select Company Group", ""));

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtusername.Text == "")
            {
                POPUPMSG("Please Insert Username");
                return;
            }

            if (txtemployeecode.Text == "")
            {
                POPUPMSG("Please Insert Employee Code");
                return;
            }

            if (txtfname.Text == "")
            {
                POPUPMSG("Please Insert First Name");
                return;
            }

            if (txtlname.Text == "")
            {
                POPUPMSG("Please Insert Last Name");
                return;
            }


            try
            {
                var eMailValidator = new System.Net.Mail.MailAddress(txtemail.Text);
            }
            catch (FormatException ex)
            {
                POPUPMSG("Wrong Email Format");
                return;
            }


            if (txtemail.Text == "")
            {
                POPUPMSG("Please Insert Email");
                return;
            }

            if (txtext.Text == "")
            {
                POPUPMSG("Please Insert เบอร์ต่อ");
                return;
            }

            if (txtmobile.Text == "")
            {
                POPUPMSG("Please Insert Mobile Number");
                return;
            }
            if (ddlrole.SelectedValue == "")
            {
                POPUPMSG("Please Select Role");
                return;
            }

            if (ddlrole.SelectedValue == "officer" || ddlrole.SelectedValue == "manager" || ddlrole.SelectedValue == "vp")
            {
                if (ddlteam.SelectedValue == "")
                {
                    POPUPMSG("Please Select Group");
                    return;
                }

            }


            var u = Serv.getUserByUsername(txtusername.Text);
            var u2 = Serv.getUser_tenantByUsername(txtusername.Text);
            if (u.Rows.Count != 0)
            {
                /// ซ้ำ ///
                POPUPMSG("Username ซ้ำกับผู้ใช้งานในระบบ");
            }
            else if (u2.Rows.Count != 0)
            {
                POPUPMSG("Username ซ้ำกับผู้ใช้งานในระบบ");
            }
            else
            {
                //string response = "success";
                string response = "";
                if (HttpContext.Current.Session["role"].ToString() != "admin") //superadmin
                {
                    if (ddlrole.SelectedValue == "admin" && ddlteamAdmin.SelectedValue != "") //กรณีที่ Superadmin เลือก admin
                    {
                        var ComCode = Serv.getComcode_byComGroup(ddlteamAdmin.SelectedValue);
                        if (ComCode.Rows.Count != 0)
                        {
                            response = sentMail.Template_defalse(txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), "ระบบ Tenant Sales Data Collection : New User", "Tenant Sales Data Collection",
                                                                txtfname.Text + " " + txtlname.Text, "[Tenant Sales Data Collection] Your User ID is ", "",
                                                               "Username : ",
                                                               "Password : ", "", "", "", "", "", txtusername.Text, ConfigurationManager.AppSettings["default_password"], "",
                                                               "", "", "", "icon", ComCode.Rows[0]["CompanyCode"].ToString());
                        }
                    }
                    else
                    {
                        response = sentMail.Template_defalse(txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), "ระบบ Tenant Sales Data Collection : New User", "Tenant Sales Data Collection",
                                                                txtfname.Text + " " + txtlname.Text, "[Tenant Sales Data Collection] Your User ID is ", "",
                                                               "Username : ",
                                                               "Password : ", "", "", "", "", "", txtusername.Text, ConfigurationManager.AppSettings["default_password"], "",
                                                               "", "", "", "icon", "defalse");
                    }
                }
                else //admin
                {
                    if (ddlrole.SelectedValue == "admin")
                    {
                        var ComCode = Serv.getComcode_byComGroup(ddlteamAdmin.SelectedValue);
                        if (ComCode.Rows.Count != 0)
                        {
                            response = sentMail.Template_defalse(txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), "ระบบ Tenant Sales Data Collection : New User", "Tenant Sales Data Collection",
                                                                txtfname.Text + " " + txtlname.Text, "[Tenant Sales Data Collection] Your User ID is ", "",
                                                               "Username : ",
                                                               "Password : ", "", "", "", "", "", txtusername.Text, ConfigurationManager.AppSettings["default_password"], "",
                                                               "", "", "", "icon", ComCode.Rows[0]["CompanyCode"].ToString());
                        }
                    }
                    else
                    {
                        var comcode = Serv.getComcode_byUser(HttpContext.Current.Session["s_userid"].ToString());
                        if (comcode.Rows.Count != 0)
                        {
                            response = sentMail.Template_defalse(txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), "ระบบ Tenant Sales Data Collection : New User", "Tenant Sales Data Collection",
                                                                txtfname.Text + " " + txtlname.Text, "[Tenant Sales Data Collection] Your User ID is ", "",
                                                               "Username : ",
                                                               "Password : ", "", "", "", "", "", txtusername.Text, ConfigurationManager.AppSettings["default_password"], "",
                                                               "", "", "", "icon", comcode.Rows[0]["company_code"].ToString());
                        }
                    }
                        
                }
                

                if (response == "success")
                {
                    if (ddlrole.SelectedValue != "admin" && ddlrole.SelectedValue != "management")
                    {

                        var user = Serv.InsertUser(txtfname.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), txtlname.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlrole.SelectedValue, ddlteam.SelectedValue, txtusername.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ConfigurationManager.AppSettings["default_password"], txtemployeecode.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                      "", txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), txtext.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), txtmobile.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlstatus.SelectedValue,
                      DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), "y", HttpContext.Current.Session["s_userid"].ToString());

                        if (HttpContext.Current.Session["role"].ToString() == "admin")
                        {
                            if (user.Rows.Count != 0)
                            {
                                var ComCode = Serv.getCompanyAdmin(HttpContext.Current.Session["s_userid"].ToString());
                                if (ComCode.Rows.Count != 0)
                                {
                                    for (int i = 0; i < ComCode.Rows.Count; i++)
                                    {
                                        Serv.InsertMappCom(user.Rows[0]["lastid"].ToString(), ComCode.Rows[i]["company_code"].ToString());
                                    }
                                }
                            }
                        }

                        //if (ddlrole.SelectedValue == "datacenter")
                        //{
                        //    var ComCode = Serv.getCompanyALL();
                        //    if (ComCode.Rows.Count != 0)
                        //    {
                        //        for (int i = 0; i < ComCode.Rows.Count; i++)
                        //        {
                        //            Serv.InsertMappCom(user.Rows[0]["lastid"].ToString(), ComCode.Rows[i]["CompanyCode"].ToString());
                        //        }
                        //    }
                        //}
                    }
                    else
                    {
                        var user = Serv.InsertUser(txtfname.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), txtlname.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlrole.SelectedValue, ddlrole.SelectedValue, txtusername.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ConfigurationManager.AppSettings["default_password"], txtemployeecode.Text.Replace("'", "''").Replace("&", "'+char(38)+'"),
                       "", txtemail.Text.Replace("'", "''").Replace("&", "'+char(38)+'").Replace(" ", ""), txtext.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), txtmobile.Text.Replace("'", "''").Replace("&", "'+char(38)+'"), ddlstatus.SelectedValue,
                       DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), "y", HttpContext.Current.Session["s_userid"].ToString());

                        if (HttpContext.Current.Session["role"].ToString() == "admin")
                        {
                            if (user.Rows.Count != 0)
                            {
                                var ComCode = Serv.getCompanyAdmin(HttpContext.Current.Session["s_userid"].ToString());
                                if (ComCode.Rows.Count != 0)
                                {
                                    for (int i = 0; i < ComCode.Rows.Count; i++)
                                    {
                                        Serv.InsertMappCom(user.Rows[0]["lastid"].ToString(), ComCode.Rows[i]["company_code"].ToString());
                                    }
                                }
                            }
                        }
                        else if (HttpContext.Current.Session["role"].ToString() == "super_admin")
                        {
                            if (user.Rows.Count != 0)
                            {
                                if (ddlteamAdmin.SelectedValue != "")
                                {
                                    var ComCode = Serv.getComcode_byComGroup(ddlteamAdmin.SelectedValue);
                                    if (ComCode.Rows.Count != 0)
                                    {
                                        for (int i = 0; i < ComCode.Rows.Count; i++)
                                        {
                                            Serv.InsertMappCom(user.Rows[0]["lastid"].ToString(), ComCode.Rows[i]["CompanyCode"].ToString());
                                        }
                                    }
                                }
                            }
                        }

                    }



                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='user_mgm.aspx';", true);
                }
                else
                {
                    POPUPMSG(response);
                    return;
                }


            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void btncancel_Click(object sender, EventArgs e)

        {
            Response.Redirect("~/user_mgm.aspx");
        }

        protected void ddlrole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrole.SelectedValue != "admin" && ddlrole.SelectedValue != "management" && ddlrole.SelectedValue != "super_admin"
                 && ddlrole.SelectedValue != "datacenter")
            {
                ddlteam.Visible = true;
                ddlteamAdmin.Visible = false;
            }
            else if (ddlrole.SelectedValue == "admin")
            {
                ddlteamAdmin.Visible = true;
                ddlteam.Visible = false;
            }
            else
            {
                ddlteam.Visible = false;
                ddlteamAdmin.Visible = false;
            }
        }


    }
}