﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class reset_pass : System.Web.UI.Page
    {
        loginDLL Serv = new loginDLL();
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        protected string CountPic { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtpassword.Focus();
                txtpassword.Attributes.Add("onkeypress", "return next_tools(event,'" + txtnewpassword.ClientID + "')");
                txtnewpassword.Attributes.Add("onkeypress", "return next_tools(event,'" + txtconfirmassword.ClientID + "')");
                txtconfirmassword.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsubmit.ClientID + "')");

                var logo = Serv.Get_Pic_Logo_login();
                CountPic = Convert.ToString(logo.Rows.Count.ToString());
                if (logo.Rows.Count != 0)
                {
                    if (CountPic == "1")
                    {
                        this.imgCompany1_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                    }
                    else if (CountPic == "2")
                    {
                        this.imgCompany2_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                        this.imgCompany2_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                    }
                    else if (CountPic == "3")
                    {
                        this.imgCompany3_1.ImageUrl = logo.Rows[0]["CompanyGroupLogo"].ToString();
                        this.imgCompany3_2.ImageUrl = logo.Rows[1]["CompanyGroupLogo"].ToString();
                        this.imgCompany3_3.ImageUrl = logo.Rows[2]["CompanyGroupLogo"].ToString();
                    }

                }
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["s_user_type"].ToString() == "icon")
            {
                var u = Serv.getUser(HttpContext.Current.Session["s_username"].ToString(), txtpassword.Text);
                if (u.Rows.Count != 0)
                {
                    if (txtnewpassword.Text.Length < 6)
                    {
                        POPUPMSG("Password ต้องมีความยาวอย่างน้อย 6 ตัวอักษร");
                        return;
                    }
                    else if (txtnewpassword.Text != txtconfirmassword.Text)
                    {
                        POPUPMSG("Password ใหม่ที่ยืนยันไม่ถูกต้อง");
                        return;
                    }
                    else if (txtpassword.Text == txtnewpassword.Text)
                    {
                        POPUPMSG("Password ใหม่ต้องไม่เหมือนกับ Password ก่อนหน้าของท่าน");
                        return;
                    }
                    else
                    {
                        Serv.UpdatePassword(HttpContext.Current.Session["s_userid"].ToString(), txtnewpassword.Text, HttpContext.Current.Session["s_user_type"].ToString(), HttpContext.Current.Session["s_userid"].ToString());
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='login.aspx';", true);
                    }
                }
                else
                {
                    POPUPMSG("Password เก่าของคุณไม่ถูกต้อง");
                }
            }
            else
            {
                var u = Serv.getUser_tenant(HttpContext.Current.Session["s_username"].ToString(),
               txtpassword.Text);
                if (u.Rows.Count != 0)
                {
                    if (txtnewpassword.Text != txtconfirmassword.Text)
                    {
                        POPUPMSG("Password ใหม่ที่ยืนยันไม่ถูกต้อง");
                        return;
                    }
                    else if (txtpassword.Text == txtnewpassword.Text)
                    {
                        POPUPMSG("Password ใหม่ต้องไม่เหมือนกับ Password ก่อนหน้าของท่าน");
                        return;
                    }
                    else
                    {
                        Serv.UpdatePassword(HttpContext.Current.Session["s_userid"].ToString(), txtnewpassword.Text, HttpContext.Current.Session["s_user_type"].ToString(), HttpContext.Current.Session["s_userid"].ToString());
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='login.aspx';", true);
                    }
                }
                else
                {
                    POPUPMSG("Password เก่าของคุณไม่ถูกต้อง");
                }
            }

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }
    }
}