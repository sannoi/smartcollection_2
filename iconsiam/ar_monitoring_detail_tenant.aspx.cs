﻿using iconsiam.App_Code;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class ar_monitoring_detail_tenant : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        ar_monitoring_detailDLL Serv = new ar_monitoring_detailDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtstartdate.Text = DateTime.Now.ToString("01/MM/yyyy", EngCI);
                    txtenddate.Text = DateTime.Now.ToString("dd/MM/yyyy", EngCI);
                    bind_default();
                    bind_rep();
                    bind_data();
                }
            }
        }

        protected void bind_default()
        {

            ddlstatus.Items.Insert(0, new ListItem("", ""));
            ddlstatus.Items.Insert(1, new ListItem("ยังไม่ส่งยอด", "n"));
            ddlstatus.Items.Insert(2, new ListItem("ส่งยอดแล้ว", "y"));


            //var c = Serv.Get_Confirm();
            //if (c.Rows.Count != 0)
            //{
            //    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["first_confirm"]).ToString("0#"))
            //    {
            //        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

            //        btnselect_all.Visible = true;
            //        btndisselect_all.Visible = true;
            //        btnconfirm.Visible = true;
            //    }
            //    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == DateTime.Now.ToString("yyyy-MM-", EngCI) + Convert.ToInt32(c.Rows[0]["second_confirm"]).ToString("0#"))
            //    {
            //        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

            //        btnselect_all.Visible = true;
            //        btndisselect_all.Visible = true;
            //        btnconfirm.Visible = true;
            //    }
            //    else
            //    {
            //        HttpContext.Current.Session["samrt_chkflag_confirm"] = "n";

            //        btnselect_all.Visible = false;
            //        btndisselect_all.Visible = false;
            //        btnconfirm.Visible = false;
            //    }
            //}

            var c = Serv.Get_Confirm_new(Request.QueryString["id"].ToString().Substring(0, 3));
            var c2 = Serv.Get_Confirm_new2(Request.QueryString["id"].ToString().Substring(0, 3));

            if (c.Rows.Count != 0)
            {
                if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                    btnselect_all.Visible = true;
                    btndisselect_all.Visible = true;
                    btnconfirm.Visible = true;
                }
                else if (c2.Rows.Count != 0)
                {
                    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "y";

                        btnselect_all.Visible = true;
                        btndisselect_all.Visible = true;
                        btnconfirm.Visible = true;
                    }
                    else
                    {
                        HttpContext.Current.Session["samrt_chkflag_confirm"] = "n";

                        btnselect_all.Visible = false;
                        btndisselect_all.Visible = false;
                        btnconfirm.Visible = false;
                    }
                }
                else
                {
                    HttpContext.Current.Session["samrt_chkflag_confirm"] = "n";

                    btnselect_all.Visible = false;
                    btndisselect_all.Visible = false;
                    btnconfirm.Visible = false;
                }

            }
            else
            {
                HttpContext.Current.Session["samrt_chkflag_confirm"] = "n";

                btnselect_all.Visible = false;
                btndisselect_all.Visible = false;
                btnconfirm.Visible = false;
            }



        }

        protected void bind_rep()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var contract = Serv.GetContract(Request.QueryString["id"].ToString());
                if (contract.Rows.Count != 0)
                {
                    lbcompanyname.Text = contract.Rows[0]["CompanyNameTH"].ToString();
                    lbbpname.Text = contract.Rows[0]["BusinessPartnerName"].ToString();
                    lbshopname.Text = contract.Rows[0]["ShopName"].ToString();
                }
                else
                {
                    Response.Redirect("~/ar_monitoring.aspx");
                }
            }
            else
            {
                Response.Redirect("~/ar_monitoring.aspx");
            }

        }

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                dateconvert con_d = new dateconvert();
                var t = Serv.GetTransaction(Request.QueryString["id"].ToString(), con_d.con_date(txtstartdate.Text), con_d.con_date(txtenddate.Text),
                    ddlstatus.SelectedValue);
                if (t.Rows.Count != 0)
                {

                    if (HttpContext.Current.Session["samrt_chkflag_confirm"].ToString() == "n")
                    {
                        GridView_List.DataSource = t;
                        GridView_List.DataBind();


                        GridView_List_confirm.DataSource = null;
                        GridView_List_confirm.DataBind();
                    }
                    else
                    {
                        GridView_List_confirm.DataSource = t;
                        GridView_List_confirm.DataBind();

                        GridView_List.DataSource = null;
                        GridView_List.DataBind();
                    }

                }
                else
                {
                    GridView_List.DataSource = null;
                    GridView_List.DataBind();

                    GridView_List_confirm.DataSource = null;
                    GridView_List_confirm.DataBind();

                }
            }
            else
            {
                Response.Redirect("~/ar_monitoring.aspx");
            }

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_data();
        }

        protected void btndetail_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField hdd_ContractNumber = (HiddenField)row.FindControl("hdd_ContractNumber");
            HiddenField hdd_id = (HiddenField)row.FindControl("hdd_id");
            HiddenField hdd_status = (HiddenField)row.FindControl("hdd_status");

            HttpContext.Current.Session["samrt_last_page"] = "ar_monitoring_detail_tenant";
            Response.Redirect("~/transaction_detail.aspx?id=" + hdd_id.Value + "&cont=" + hdd_ContractNumber.Value + "&s=" + hdd_status.Value);
        }

        protected void btnselect_all_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GridView_List_confirm.Rows.Count; i++)
            {
                /////// Select All Rows
            }
        }

        protected void btndisselect_all_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GridView_List_confirm.Rows.Count; i++)
            {
                /////// Diselect All Rows
            }
        }

        protected void btnconfirm_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GridView_List_confirm.Rows.Count; i++)
            {
                /////// Update flag_confirm = 'y' in checked rows
            }

            Response.Redirect("~/ar_monitoring_detail_tenant.aspx?id=" + Request.QueryString["id"].ToString());
        }

        protected void GridView_List_confirm_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                CheckBox CheckBox1 = (CheckBox)(e.Row.FindControl("CheckBox1"));
                CheckBox1.Checked = true;

                HiddenField hdd_status = (HiddenField)(e.Row.FindControl("hdd_status"));
                if (hdd_status.Value == "ยังไม่ส่งยอด")
                {
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }


                HiddenField hdd_flag_confirm = (HiddenField)(e.Row.FindControl("hdd_flag_confirm"));
                if (hdd_flag_confirm.Value == "ยังไม่ยืนยันยอด")
                {
                    e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                }

            }
        }

        protected void GridView_List_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdd_status = (HiddenField)(e.Row.FindControl("hdd_status"));
                if (hdd_status.Value == "ยังไม่ส่งยอด")
                {
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }


                HiddenField hdd_flag_confirm = (HiddenField)(e.Row.FindControl("hdd_flag_confirm"));
                if (hdd_flag_confirm.Value == "ยังไม่ยืนยันยอด")
                {
                    e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;
                }

            }


        }
    }
}