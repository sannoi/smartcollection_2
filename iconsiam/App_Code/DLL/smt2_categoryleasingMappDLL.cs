﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iconsiam.App_Code.DLL
{
    public class smt2_categoryleasingMappDLL
    {
        public DataTable getCompayGroup(string comCOde)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblCompanyGroup where ComGroupID in (select ComGroupID from TblCompanyGroupMapping where CompanyCode in (" + comCOde + ")) " +
                " and  IsActive = 'y'; ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getCompanyGroupByID(string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblCompanyGroup where ComGroupID = '" + ComGroupID + "' ; ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;

        }
        public DataTable getCategoryLeasing_mapping(string ComGroupID, string ContractTypeName)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select icM.id,c.CategoryleasingNameEN,c.CategoryleasingNameTH , icM.IsActive,icM.UpdateDate, u.username " +
                " from TblCategoryLeasing_mapping icM inner join tblcategoryleasing c on icM.CategoryleasingId = c.id " +
                " inner join tbluser u on icM.UpdateId = u.userid where icM.ComGroupID = '" + ComGroupID + "' " +
                " and(c.CategoryleasingNameEN like '%" + ContractTypeName + "%' or c.CategoryleasingNameTH like '%" + ContractTypeName + "%'); ;  ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void DeleteCategoryLeasing_mapping(string id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Delete from TblCategoryLeasing_mapping where id = '" + id + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable getCategoryLeasing_mapping(string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select a.*,case when ComGroupID is not null then 'y' else 'n' end chk " +
                " from ( select id, CategoryleasingNameEN, CategoryleasingNameTH from tblcategoryleasing " +
                " where Flag_active = '1' ) a left join( " +
                " select ComGroupID, CategoryleasingId from TblCategoryLeasing_mapping where ComGroupID = '" + ComGroupID + "' )b on a.id = CategoryleasingId  ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public DataTable getCategoryLeasing_Notmapping(string ComGroupID)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from tblcategoryleasing where id not in ( " +
                " select inGm.CategoryleasingId from TblCategoryLeasing_mapping inGm " +
                " inner join tblcategoryleasing sap on inGm.CategoryleasingId = sap.id where ComGroupID = '" + ComGroupID + "' ) ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void InsertCategoryLeasing_mapping(string CategoryleasingId, string ComGroupID, string CreateId, string UpdateId, string IsActive)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into TblCategoryLeasing_mapping(CategoryleasingId,ComGroupID,CreateDate,UpdateDate,CreateId,UpdateId,IsActive) " +
                " values('" + CategoryleasingId + "','" + ComGroupID + "', getdate(), getdate(),'" + CreateId + "','" + UpdateId + "','" + IsActive + "')   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void DeleteCategoryLeasing_mappingByGroup(string ComGroupID)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete from TblCategoryLeasing_mapping where  ComGroupID = '" + ComGroupID + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }





    }
}