﻿using iconsiam.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class topten_monthly_rep : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        monthly_repDLL Serv = new monthly_repDLL();
        protected string MyTheme { get; set; }
        protected string NavbarColor { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    txtstartdate.Text = DateTime.Now.ToString("yyyy-MM", EngCI);
                    txtenddate.Text = DateTime.Now.ToString("yyyy-MM", EngCI);

                    bind_default();
                    bind_rep();

                }
            }
        }
        protected void bind_default()
        {
            string comcode = HttpContext.Current.Session["s_com_code"].ToString();
            string[] comcode_ = comcode.Split(',');

            var cl = Serv.GetCategory_leasing(comcode_);
            if (cl.Rows.Count != 0)
            {
                ddlcatagorLeasing.DataTextField = "CategoryleasingNameTH";
                ddlcatagorLeasing.DataValueField = "id";
                ddlcatagorLeasing.DataSource = cl;
                ddlcatagorLeasing.DataBind();
            }
            else
            {
                ddlcatagorLeasing.DataSource = null;
                ddlcatagorLeasing.DataBind();

            }
            ddlcatagorLeasing.Items.Insert(0, new ListItem("Select Category Leasing", ""));           

            var ig = Serv.GetIndustry_group(comcode_);
            if (ig.Rows.Count != 0)
            {
                ddlindustrygroup.DataTextField = "IndustryGroupNameTH";
                ddlindustrygroup.DataValueField = "id";
                ddlindustrygroup.DataSource = ig;
                ddlindustrygroup.DataBind();
            }
            else
            {
                ddlindustrygroup.DataSource = null;
                ddlindustrygroup.DataBind();

            }
            ddlindustrygroup.Items.Insert(0, new ListItem("Select Industry Group", ""));

            //var comcode = Serv.GetMapCompany(HttpContext.Current.Session["s_user_for_shop"].ToString());
            //if (comcode.Rows.Count != 0)
            //{
            //    string com_code = "";
            //    for (int i = 0; i < comcode.Rows.Count; i++)
            //    {
            //        com_code += "'" + comcode.Rows[i]["company_code"].ToString() + "',";
            //    }
            //    com_code = com_code.Substring(0, com_code.Length - 1);
            //    HttpContext.Current.Session["s_com_code"] = com_code;
            //}



            if (HttpContext.Current.Session["s_com_code"] != null)
            {
                var comp = Serv.GetCompany(HttpContext.Current.Session["s_com_code"].ToString());
                if (comp.Rows.Count != 0)
                {
                    ddlcompany.DataTextField = "CompanyNameTH";
                    ddlcompany.DataValueField = "CompanyCode";
                    ddlcompany.DataSource = comp;
                    ddlcompany.DataBind();
                }
                else
                {
                    ddlcompany.DataSource = null;
                    ddlcompany.DataBind();

                }
            }

            ddlcompany.Items.Insert(0, new ListItem("Company", ""));

            ddlstatus.Items.Clear();
            ddlstatus.Items.Insert(0, new ListItem("All", "all"));
            ddlstatus.Items.Insert(1, new ListItem("Confirm", "y"));
            ddlstatus.Items.Insert(2, new ListItem("Unconfirmed", "n"));

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void bind_rep()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("d0");
            dt.Columns.Add("d1");
            dt.Columns.Add("d2");
            dt.Columns.Add("d3");
            dt.Columns.Add("d4");
            dt.Columns.Add("d5");
            dt.Columns.Add("d6");
            dt.Columns.Add("d_total", typeof(decimal));

            int xx = Convert.ToInt32(Convert.ToDateTime(txtenddate.Text).ToString("MM")) - Convert.ToInt32(Convert.ToDateTime(txtstartdate.Text).ToString("MM"));

            if (xx < 0)
            {
                POPUPMSG("Start Date ห้ามมากกว่า End Date");
                return;
            }
            else
            {

                string Company = "";

                if (ddlcompany.SelectedValue == "")
                {
                    Company = HttpContext.Current.Session["s_com_code"].ToString();
                }
                else
                {
                    Company = "'" + ddlcompany.SelectedValue + "'";
                }


                for (int j = 0; j <= xx; j++)
                {
                    int d_end = DateTime.DaysInMonth(Convert.ToInt32(Convert.ToDateTime(txtstartdate.Text).AddMonths(j).ToString("yyyy")),
                        Convert.ToInt32(Convert.ToDateTime(txtstartdate.Text).AddMonths(j).ToString("MM")));

                    var rep = Serv.GetRep2(Convert.ToDateTime(txtstartdate.Text + "-01").AddMonths(j).ToString("yyyy-MM-dd"),
                        Convert.ToDateTime(txtstartdate.Text).AddMonths(j).ToString("yyyy-MM") + "-" + d_end,
                        ddlcatagorLeasing.SelectedValue, ddlindustrygroup.SelectedValue, Company , ddlstatus.SelectedValue);

                    if (rep.Rows.Count != 0)
                    {
                        decimal sum = 0;
                        for (int i = 0; i < rep.Rows.Count; i++)
                        {
                            DataRow row1 = dt.NewRow();
                            row1["d0"] = i + 1;
                            row1["d1"] = rep.Rows[i]["record_date"].ToString();
                            row1["d2"] = rep.Rows[i]["IndustryGroupNameTH"].ToString();
                            row1["d3"] = rep.Rows[i]["ShopName"].ToString();
                            row1["d4"] = rep.Rows[i]["smart_floor"].ToString();
                            row1["d5"] = rep.Rows[i]["smart_room_no"].ToString();
                            row1["d6"] = rep.Rows[i]["smart_sqm"].ToString();
                            //row1["d_total"] = Convert.ToDecimal(rep.Rows[i]["exvat"].ToString()).ToString("#,##0.00");
                            row1["d_total"] = Convert.ToDecimal(rep.Rows[i]["total_ex_var"].ToString()).ToString("#,##0.00");
                            sum = sum + Convert.ToDecimal(rep.Rows[i]["total_ex_var"].ToString());

                            dt.Rows.Add(row1);

                            if (i == rep.Rows.Count - 1)
                            {
                                row1 = dt.NewRow();
                                row1["d0"] = "";
                                row1["d1"] = "";
                                row1["d2"] = "";
                                row1["d3"] = "";
                                row1["d4"] = "";
                                row1["d5"] = "";
                                row1["d6"] = "";
                                row1["d_total"] = sum;

                                dt.Rows.Add(row1);
                            }
                        }


                    }
                }

                if (dt.Rows.Count != 0)
                {
                    //dt.DefaultView.Sort = "d_total DESC";

                    GridView_List.DataSource = dt;
                    GridView_List.DataBind();

                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView_List.DataSource = null;
                    GridView_List.DataBind();

                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }


            }
            if (HttpContext.Current.Session["code_theme"] != null)
            {
                this.MyTheme = HttpContext.Current.Session["code_theme"].ToString();
            }
            else
            {
                this.MyTheme = "#CCA9DA";
            }

            if (HttpContext.Current.Session["code_Navbar"] != null)
            {
                this.NavbarColor = HttpContext.Current.Session["code_Navbar"].ToString();
            }
            else
            {
                this.NavbarColor = "#2d2339";
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            bind_rep();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnexport_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count != 0)
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename = top10monthly_report" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                //Response.AddHeader("content-disposition", "attachment;filename=Export1.xls");
                Response.ContentType = "application/vnd.ms-excel";
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

                System.IO.StringWriter sw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);

                GridView1.RenderControl(hw);
                string headerTable = @"<Table>" +
                            "<tr align='center'><td colspan='8'>Tenant Sales Data Collection</td></tr>" +
                            "<tr align='center'><td colspan='8'>TOP TEN MONTHLY " + txtstartdate.Text + " - " + txtenddate.Text + "</td></tr>" +
                            "<tr align='center'><td colspan='8'></td></tr>" +
                            "</Table>";
                Response.Write(headerTable);
                Response.Write(sw.ToString());
                Response.End();


            }
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/topten_monthly_rep.aspx");
        }
        protected void GridView_List_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdd_d3 = (HiddenField)(e.Row.FindControl("hdd_d3"));
                if (hdd_d3.Value == "")
                {
                    e.Row.BackColor = System.Drawing.Color.LightPink;
                }//

            }
        }



    }
}