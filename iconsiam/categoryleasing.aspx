﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="categoryleasing.aspx.cs" Inherits="iconsiam.categoryleasing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />
     <style>
        .ThemeBtn {
            background-color: <%= MyTheme %>;
            Color: <%= NavbarColor %>;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Category Leasing
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtcategoryleasing_name" runat="server" placeholder="Category Leasing Name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlstatus" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" class="btn ThemeBtn"/>
                    <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click"  class="btn ThemeBtn"/>
                    <asp:Button ID="btncancel" runat="server" Text="Clear" OnClick="btncancel_Click" class="btn ThemeBtn"/>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="CategoryleasingNameEN" HeaderText="Category Leasing Name(EN)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                            <asp:BoundField DataField="CategoryleasingNameTH" HeaderText="Category Leasing Name(TH)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="30%" />
                            <asp:BoundField DataField="UpdateDate" HeaderText="Last Modify Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="username" HeaderText="Modify By" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />

                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_id" runat="server" Value='<%# Eval("Id") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="Edit" OnClick="btnedit_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>
            </div>


        </div>
    </div>



</asp:Content>
