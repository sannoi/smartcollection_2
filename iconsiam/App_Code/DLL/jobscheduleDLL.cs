﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace iconsiam.App_Code.DLL
{
    public class jobscheduleDLL
    {
        public DataTable getTime(string job_name, string CompanyCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select *  from tbljob where job_name  = '" + job_name + "' and CompanyCode = '" + CompanyCode + "' ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable Get_Confirm()
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "select * from tblsetting_confirm ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public void InsertJob(string jobid, string status)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = "  Insert into tbljob_log(jobid,status,createdate) " +
                " values('" + jobid + "','" + status + "',GETDATE()) ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public void UpdateJobNext_date(string jobid, string jobnext_datetime)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " update tbljob set jobnext_datetime = '" + jobnext_datetime + "' , update_date = getdate() " +
                " where id = '" + jobid + "' ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable GetContract_Expire()
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from tblsmart_contract " +
                " where smart_shop_close < GETDATE() and smart_collection_flag_status <> 'y' and smart_contract_status = '1' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable Find_new_Contract(string contractnumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from tblsmart_contract where Contract_old_number = '" + contractnumber + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void Update_Account(string flag_reset, string flag_active, string contractnumber, string update_id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = "  Update tblcontract_account set flag_reset = '" + flag_reset + "' , flag_active = '" + flag_active + "',update_date = getdate(), update_id = '" + update_id + "' " +
                " where contractnumber = '" + contractnumber + "'    ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public void Update_Transaction(string contractnumber_new, string record_date, string contractnumber)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = "delete tbltransaction_record where  contractnumber = '" + contractnumber_new + "' and   record_date >= '" + record_date + "'; " +
                " update tbltransaction_record set contractnumber = '" + contractnumber_new + "' where  record_date >= '" + record_date + "' and contractnumber = '" + contractnumber + "' ;   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public void Update_status_Contract(string smart_contract_status, string contractnumber)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;


            strSQL = "  Update tblsmart_contract set smart_contract_status = '" + smart_contract_status + "'  where contractnumber = '" + contractnumber + "'    ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        //================================ Tenant Sales Data Collection #2 ===================================




        public DataTable getCompany()
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblCompanyDetail ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable getSAPIndustryGroup()
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from SAPIndustryGroup ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable getTblIndustryGroup(string IndustryGroupCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblIndustryGroup where IndustryGroupCode = '" + IndustryGroupCode + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
        public void Execute_query_insert(string quesry)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = quesry;


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }











    }
}