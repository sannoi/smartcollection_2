﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iconsiam
{
    public partial class reset_password : System.Web.UI.Page
    {
        reset_passwordDLL Serv = new reset_passwordDLL();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {

                    txtoldpassword.Focus();
                    txtoldpassword.Attributes.Add("onkeypress", "return next_tools(event,'" + txtpassword.ClientID + "')");
                    txtpassword.Attributes.Add("onkeypress", "return next_tools(event,'" + txtpassword_com.ClientID + "')");
                    txtpassword_com.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsubmit.ClientID + "')");
                }
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtoldpassword.Text != "" && txtpassword.Text != "" && txtpassword_com.Text != "")
            {
                if(txtpassword.Text.Length < 6)
                {
                    POPUPMSG("Password ต้องมีความยาวอย่างน้อย 6 ตัวอักษร");
                    return;
                }
                else if (txtpassword.Text != txtpassword_com.Text)
                {
                    POPUPMSG("Password กับ Confirm Password ไม่ถูกต้อง");
                    return;
                }
                else if (txtoldpassword.Text == txtpassword.Text)
                {
                    POPUPMSG("Password ใหม่ต้องไม่เหมือนกับ Password ก่อนหน้าของท่าน");
                    return;
                }
                else
                {
                    var u = Serv.GetUserByPassword(HttpContext.Current.Session["s_userid"].ToString(), txtoldpassword.Text);
                    if (u.Rows.Count != 0)
                    {
                        Serv.UpdatePassword(HttpContext.Current.Session["s_userid"].ToString(), txtpassword.Text);


                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='login.aspx';", true);
                    }
                    else
                    {
                        POPUPMSG("Password เก่าของท่านไม่ถูกต้อง");
                        return;
                    }
                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
                return;
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


    }
}