﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;
using System.Text;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Drawing;

namespace iconsiam
{
    public partial class CompanyMgm_edit : System.Web.UI.Page
    {
        CompanyGroupMgmDLL Serv = new CompanyGroupMgmDLL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    //bind_defult();
                    bind_data();

                }
            }
        }

        //protected void bind_defult()
        //{

        //    var theme = Serv.getTheme();
        //    if (theme.Rows.Count != 0)
        //    {
        //        ddlTheme.DataTextField = "ThemeName";
        //        ddlTheme.DataValueField = "ThemeId";
        //        ddlTheme.DataSource = theme;
        //        ddlTheme.DataBind();
        //    }
        //    else
        //    {
        //        ddlTheme.DataSource = null;
        //        ddlTheme.DataBind();

        //    }
        //    ddlTheme.Items.Insert(0, new ListItem("Theme", ""));
        //}

        protected void bind_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Companycode"]))
            {
                var com = Serv.getComNameByID(Request.QueryString["Companycode"].ToString());
                if (com.Rows.Count != 0)
                {
                    //if (com.Rows[0]["CompanyLogo"].ToString() != "")
                    //{
                    //    imgCompany.ImageUrl = com.Rows[0]["CompanyLogo"].ToString().Replace("~", "");
                    //}

                    txtcompanyCode.Text = com.Rows[0]["Companycode"].ToString();
                    txtcompanyNameTh.Text = com.Rows[0]["CompanyNameTh"].ToString();
                    txtcompanyNameEn.Text = com.Rows[0]["CompanyNameEn"].ToString();
                    //ddlTheme.SelectedValue = com.Rows[0]["ThemeId"].ToString();

                    //string pathpic = getPicTheme(com.Rows[0]["ThemeId"].ToString());
                    //Image1.ImageUrl = pathpic;
                }
                else
                {
                    Response.Redirect("~/CompanyMgm.aspx");
                }
            }
            else
            {
                Response.Redirect("~/CompanyMgm.aspx");
            }
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtcompanyNameTh.Text != "" && txtcompanyCode.Text != "")
            {

                Serv.UpdateCompanyDetail_(HttpContext.Current.Session["s_userid"].ToString(), HttpContext.Current.Session["s_userid"].ToString(),
                       txtcompanyNameEn.Text, txtcompanyNameTh.Text, Request.QueryString["Companycode"].ToString());

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location = 'CompanyMgm.aspx' ;", true);

                //if (fileupload_logo.HasFile)
                //{
                //    string filename1 = Path.GetFileName(fileupload_logo.FileName);
                //    FileInfo fi1 = new FileInfo(fileupload_logo.FileName);
                //    string ext = fi1.Extension.ToLower();
                //    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                //    {
                //        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;

                //        string img_name1 = ConfigurationManager.AppSettings["imgLogo"] + Request.QueryString["Companycode"].ToString() + "_" + xx;
                //        fileupload_logo.SaveAs(Server.MapPath(img_name1));

                //        Serv.UpdateCompanyDetail(img_name1, HttpContext.Current.Session["s_userid"].ToString(), HttpContext.Current.Session["s_userid"].ToString(),
                //            txtcompanyNameEn.Text, txtcompanyNameTh.Text, Request.QueryString["Companycode"].ToString());

                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location = 'CompanyMgm.aspx' ;", true);

                //    }
                //    else
                //    {
                //        POPUPMSG("Logo ต้องเป็นภาพเท่านั้น");
                //        return;
                //    }

                //}
                //else
                //{

                //    Serv.UpdateCompanyDetail_(HttpContext.Current.Session["s_userid"].ToString(), HttpContext.Current.Session["s_userid"].ToString(),
                //        txtcompanyNameEn.Text, txtcompanyNameTh.Text, Request.QueryString["Companycode"].ToString());

                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location = 'CompanyMgm.aspx' ;", true);
                //}
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ถูกต้อง");
                return;
            }

        }
        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CompanyMgm.aspx");

        }

        //protected void ddlTheme_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string themeid = ddlTheme.SelectedValue;          
        //    Image1.ImageUrl = getPicTheme(themeid);
        //}

        //protected string getPicTheme(string themeid)
        //{
        //    string Urlpic = "";
        //    var pic = Serv.getTheme();
        //    if (pic.Rows.Count != 0)
        //    {
        //        if (themeid == "1")
        //        {
        //            Urlpic = pic.Rows[0]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "2")
        //        {
        //            Urlpic = pic.Rows[1]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "3")
        //        {
        //            Urlpic = pic.Rows[2]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "4")
        //        {
        //            Urlpic = pic.Rows[3]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "5")
        //        {
        //            Urlpic = pic.Rows[4]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "6")
        //        {
        //            Urlpic = pic.Rows[5]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "7")
        //        {
        //            Urlpic = pic.Rows[6]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "8")
        //        {
        //            Urlpic = pic.Rows[7]["Urlpictheme"].ToString();
        //        }             
        //        else if (themeid == "10")
        //        {
        //            Urlpic = pic.Rows[8]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "11")
        //        {
        //            Urlpic = pic.Rows[9]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "12")
        //        {
        //            Urlpic = pic.Rows[10]["Urlpictheme"].ToString();
        //        }               
        //        else if (themeid == "17")
        //        {
        //            Urlpic = pic.Rows[11]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "18")
        //        {
        //            Urlpic = pic.Rows[12]["Urlpictheme"].ToString();
        //        }
        //        else if (themeid == "19")
        //        {
        //            Urlpic = pic.Rows[13]["Urlpictheme"].ToString();
        //        }
        //        else
        //        {
        //            Urlpic = "~/img/Pic_color/Defult.jpg";
        //        }
        //    }

        //    return Urlpic;

        //}

    }
}