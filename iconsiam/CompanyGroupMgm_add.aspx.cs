﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iconsiam.App_Code.DLL;


namespace iconsiam
{
    public partial class CompanyGroupMgm_add : System.Web.UI.Page
    {
        CompanyGroupMgmDLL Serv = new CompanyGroupMgmDLL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["s_userid"] == null)
                {
                    Response.Redirect("~/login.aspx");
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtcompanyGroupName.Text == "")
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
            else
            {
               var comgroup_id =  Serv.InsertComGroupName(txtcompanyGroupName.Text, HttpContext.Current.Session["s_userid"].ToString(), "y");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย กรุณาเลือกบริษัทที่อยู๋ภายใต้กรุ๊ป');window.location ='CompanyGroupMgm.aspx';", true);

                //Email template
                if (comgroup_id.Rows.Count != 0)
                {
                    Serv.Insert_Emailtemplate_companyGroup(comgroup_id.Rows[0]["last_id"].ToString(), HttpContext.Current.Session["s_userid"].ToString());
                }
                
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CompanyGroupMgm.aspx");
        }

       

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


    }
}