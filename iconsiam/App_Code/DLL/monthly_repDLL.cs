﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace iconsiam.App_Code.DLL
{
    public class monthly_repDLL
    {
        public DataTable GetRep1_tmp(string d1, string d2, string[] ShopName, string[] companyname, string[] floor,
            string[] cuscode, string[] shopgroup, string building, string roomtype,
        string contract_type, string cat_leasing, string group_location, string industry_group, string user_group, string com_code , string status)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";
            string shopname_ = "";
            string companyname_ = "";
            string floor_ = "";
            string cuscode_ = "";
            string shopgroup_ = "";

            if (ShopName.Length > 0)
            {
                for (int i = 0; i < ShopName.Length; i++)
                {
                    if (ShopName[i] != "")
                    {
                        shopname_ += " s.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }

                }
                if (shopname_ != "")
                {
                    shopname_ = " and " + shopname_.Substring(0, shopname_.Length - 2);

                }
            }


            if (companyname.Length > 0)
            {
                for (int i = 0; i < companyname.Length; i++)
                {
                    if (companyname[i] != "")
                    {
                        companyname_ += " com.CompanyNameTH like '%" + companyname[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                            " or com.CompanyNameEN like '%" + companyname[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";

                    }
                }
                if (companyname_ != "")
                {
                    companyname_ = " and " + companyname_.Substring(0, companyname_.Length - 2);

                }
            }


            if (floor.Length > 0)
            {
                for (int i = 0; i < floor.Length; i++)
                {
                    if (floor[i] != "")
                    {
                        floor_ += " s.smart_floor like '%" + floor[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (floor_ != "")
                {
                    floor_ = " and " + floor_.Substring(0, floor_.Length - 2);
                }
            }

            if (cuscode.Length > 0)
            {
                for (int i = 0; i < cuscode.Length; i++)
                {
                    if (cuscode[i] != "")
                    {
                        cuscode_ += " s.BusinessPartnerCode like '%" + cuscode[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (cuscode_ != "")
                {
                    cuscode_ = " and " + cuscode_.Substring(0, cuscode_.Length - 2);
                }
            }

            if (shopgroup.Length > 0)
            {
                for (int i = 0; i < shopgroup.Length; i++)
                {
                    if (shopgroup[i] != "")
                    {
                        shopgroup_ += " sg.ShopGroupNameTH like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                            " or sg.ShopGroupNameEN like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (shopgroup_ != "")
                {
                    shopgroup_ = " and " + shopgroup_.Substring(0, shopgroup_.Length - 2);
                }
            }

            if (building != "")
            {
                building = " and s.smart_building = '" + building + "' ";

            }
            else
            {
                building = "";
            }

            if (roomtype != "")
            {
                roomtype = " and s.smart_usage_name = '" + roomtype + "' ";

            }
            else
            {
                roomtype = "";
            }

            if (contract_type != "")
            {
                contract_type = " and s.smart_contract_type = '" + contract_type + "' ";

            }
            else
            {
                contract_type = "";
            }//

            if (cat_leasing != "")
            {
                cat_leasing = " and s.smart_Category_leasing  = '" + cat_leasing + "' ";

            }
            else
            {
                cat_leasing = "";
            }//

            if (group_location != "")
            {
                group_location = " and s.smart_group_location  = '" + group_location + "' ";

            }
            else
            {
                group_location = "";
            }//

            if (industry_group != "")
            {
                industry_group = " and s.smart_industry_group  = '" + industry_group + "' ";

            }
            else
            {
                industry_group = "";
            }//

            string ar_ = "";
            if (user_group != "")
            {
                //ar_ = "  and s.smart_icon_staff_id in (" + user_group + ") ";
                ar_ = "";
            }
            else
            {
                ar_ = "";
            }

            if (status != "all")
            {
                strSQL = "   select distinct s.ContractNumber, s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,   " +
                    "   s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor,  " +
                    "   s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH,  " +
                    "   ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm   " +

                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber  and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " + building +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 00:00:00'  and tr.flag_confirm = '" + status + "' " + shopname_ + " " + companyname_ + " " + floor_ + " " + cuscode_ +
                    " " + shopgroup_ + " " + roomtype + " " + contract_type + " " + cat_leasing + " " + group_location + " " + industry_group + " " + ar_ +
                    "  and com.CompanyCode in (" + com_code + ")  Order by s.ShopName ";
            }
            else
            {
                strSQL = "   select distinct s.ContractNumber, s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,   " +
                    "   s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor,  " +
                    "   s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH,  " +
                    "   ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm   " +

                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber  and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " + building +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 00:00:00'  " + shopname_ + " " + companyname_ + " " + floor_ + " " + cuscode_ +
                    " " + shopgroup_ + " " + roomtype + " " + contract_type + " " + cat_leasing + " " + group_location + " " + industry_group + " " + ar_ +
                    "  and com.CompanyCode in (" + com_code + ")  Order by s.ShopName ";
            }



            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable GetRep1_tmp_ae(string d1, string d2, string[] ShopName, string[] companyname, string[] floor, string[] cuscode, string[] shopgroup, string building, string roomtype,
       string contract_type, string cat_leasing, string group_location, string industry_group, string user_group, string com_code , string status)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";
            string shopname_ = "";
            string companyname_ = "";
            string floor_ = "";
            string cuscode_ = "";
            string shopgroup_ = "";

            if (ShopName.Length > 0)
            {
                for (int i = 0; i < ShopName.Length; i++)
                {
                    if (ShopName[i] != "")
                    {
                        shopname_ += " s.ShopName like '%" + ShopName[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }

                }
                if (shopname_ != "")
                {
                    shopname_ = " and " + shopname_.Substring(0, shopname_.Length - 2);

                }
            }


            if (companyname.Length > 0)
            {
                for (int i = 0; i < companyname.Length; i++)
                {
                    if (companyname[i] != "")
                    {
                        companyname_ += " com.CompanyNameTH like '%" + companyname[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                            " or com.CompanyNameEN like '%" + companyname[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";

                    }
                }
                if (companyname_ != "")
                {
                    companyname_ = " and " + companyname_.Substring(0, companyname_.Length - 2);

                }
            }


            if (floor.Length > 0)
            {
                for (int i = 0; i < floor.Length; i++)
                {
                    if (floor[i] != "")
                    {
                        floor_ += " s.smart_floor like '%" + floor[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (floor_ != "")
                {
                    floor_ = " and " + floor_.Substring(0, floor_.Length - 2);
                }
            }

            if (cuscode.Length > 0)
            {
                for (int i = 0; i < cuscode.Length; i++)
                {
                    if (cuscode[i] != "")
                    {
                        cuscode_ += " s.BusinessPartnerCode like '%" + cuscode[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (cuscode_ != "")
                {
                    cuscode_ = " and " + cuscode_.Substring(0, cuscode_.Length - 2);
                }
            }

            if (shopgroup.Length > 0)
            {
                for (int i = 0; i < shopgroup.Length; i++)
                {
                    if (shopgroup[i] != "")
                    {
                        shopgroup_ += " sg.ShopGroupNameTH like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' " +
                            " or sg.ShopGroupNameEN like '%" + shopgroup[i].Replace("'", "''").Replace("&", "'+char(38)+'") + "%' or";
                    }
                }
                if (shopgroup_ != "")
                {
                    shopgroup_ = " and " + shopgroup_.Substring(0, shopgroup_.Length - 2);
                }
            }

            if (building != "")
            {
                building = " and s.smart_building = '" + building + "' ";

            }
            else
            {
                building = "";
            }

            if (roomtype != "")
            {
                roomtype = " and s.smart_usage_name = '" + roomtype + "' ";

            }
            else
            {
                roomtype = "";
            }

            if (contract_type != "")
            {
                contract_type = " and s.smart_contract_type = '" + contract_type + "' ";

            }
            else
            {
                contract_type = "";
            }//

            if (cat_leasing != "")
            {
                cat_leasing = " and s.smart_Category_leasing  = '" + cat_leasing + "' ";

            }
            else
            {
                cat_leasing = "";
            }//

            if (group_location != "")
            {
                group_location = " and s.smart_group_location  = '" + group_location + "' ";

            }
            else
            {
                group_location = "";
            }//

            if (industry_group != "")
            {
                industry_group = " and s.smart_industry_group  = '" + industry_group + "' ";

            }
            else
            {
                industry_group = "";
            }//

            string ae_ = "";
            if (user_group != "")
            {
                ae_ = "  and s.smart_icon_ae_id in (" + user_group + ") ";
            }
            else
            {
                ae_ = "";
            }

            if (status != "all")
            {
                strSQL = "   select distinct s.ContractNumber, s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,   " +
                    "   s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor,  " +
                    "   s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH,  " +
                    "   ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm   " +

                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber  and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " + building +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 00:00:00' and tr.flag_confirm = '" + status + "' " + shopname_ + " " + companyname_ + " " + floor_ + " " + cuscode_ +
                    " " + shopgroup_ + " " + roomtype + " " + contract_type + " " + cat_leasing + " " + group_location + " " + industry_group + " " + ae_ + "" +
                    "   and com.CompanyCode in (" + com_code + ")  Order by s.ShopName ";
            }
            else
            {
                strSQL = "   select distinct s.ContractNumber, s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,   " +
                    "   s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor,  " +
                    "   s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH,  " +
                    "   ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm   " +

                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber  and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " + building +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 00:00:00' " + shopname_ + " " + companyname_ + " " + floor_ + " " + cuscode_ +
                    " " + shopgroup_ + " " + roomtype + " " + contract_type + " " + cat_leasing + " " + group_location + " " + industry_group + " " + ae_ + "" +
                    "   and com.CompanyCode in (" + com_code + ")  Order by s.ShopName ";
            }
            



            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetRep1(string d1, string d2, string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL = "   select FORMAT(record_date, 'MMM-yy', 'en-us') as record_date ,s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,   " +

                     " case when s.smart_record_keyin_type = 'prod_sale' then sum(tr.product_amount - tr.product_amount_vat)  " +
                    " when s.smart_record_keyin_type = 'prod_service' then sum((tr.product_amount - tr.product_amount_vat) + " +
                    " (tr.service_serCharge_amount - tr.service_serCharge_amount_vat)) else   " +
                    " sum((tr.product_amount - tr.product_amount_vat) - tr.service_serCharge_amount)  " +
                    " end as total_ex_var,  " +

                    //"   sum(tr.product_amount) as product_amount,sum((tr.product_amount * 7) / 107) as vat,  " +
                    //"   sum(tr.product_amount - (tr.product_amount * 7) / 107) as exvat,   " +
                    //"   sum(tr.service_serCharge_amount) as service_serCharge_amount,sum((tr.service_serCharge_amount * 7) / 107) as vat2,   " +
                    //"   sum(tr.service_serCharge_amount - (tr.service_serCharge_amount * 7) / 107) as exvat2, " +



                    " s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor, " +
                    " s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH, " +
                    " ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm  " +
                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber and tr.ContractNumber = '" + ContractNumber + "'  and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                     " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 00:00:00' " +
                    "   group by FORMAT(record_date, 'MMM-yy', 'en-us'),s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName, " +
                      "   s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor,  " +
                      "   s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH,  " +
                      "   ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type ,s.smart_room_no,s.smart_sqm  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetRep2(string d1, string d2, string cat_leasing, string industry_group, string company , string status)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";
            string companyname_ = "";


            if (cat_leasing != "")
            {
                cat_leasing = " and s.smart_Category_leasing  = '" + cat_leasing + "' ";

            }
            else
            {
                cat_leasing = "";
            }//


            if (industry_group != "")
            {
                industry_group = " and s.smart_industry_group  = '" + industry_group + "' ";

            }
            else
            {
                industry_group = "";
            }//
            if (company != "")
            {
                companyname_ += " and com.CompanyCode in (" + company + ") ";

            }

            if (status != "all")
            {
                strSQL = "   select top(10) FORMAT(record_date, 'MMM-yy', 'en-us') as record_date ,s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,   " +


                    " case when s.smart_record_keyin_type = 'prod_sale' then sum(tr.product_amount - tr.product_amount_vat)  " +
                    " when s.smart_record_keyin_type = 'prod_service' then sum((tr.product_amount - tr.product_amount_vat) + " +
                    " (tr.service_serCharge_amount - tr.service_serCharge_amount_vat)) else   " +
                    " sum((tr.product_amount - tr.product_amount_vat) - tr.service_serCharge_amount)  " +
                    " end as total_ex_var,  " +


                    //"   sum(tr.product_amount) as product_amount,sum((tr.product_amount * 7) / 107) as vat,  " +
                    //"   sum(tr.product_amount - (tr.product_amount * 7) / 107) as exvat,   " +
                    //"   sum(tr.service_serCharge_amount) as service_serCharge_amount,sum((tr.service_serCharge_amount * 7) / 107) as vat2,   " +
                    //"   sum(tr.service_serCharge_amount - (tr.service_serCharge_amount * 7) / 107) as exvat2, " +



                    " s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor, " +
                    " s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH, " +
                    " ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm  " +
                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber  and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 00:00:00' and tr.flag_confirm = '" + status + "' " +
                    " " + " " + cat_leasing + " " + industry_group + " " + companyname_ + " " +

                    "   group by FORMAT(record_date, 'MMM-yy', 'en-us'),s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName, " +
                      "   s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor,  " +
                      "   s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH,  " +
                      "   ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm  order by total_ex_var desc  ";
            }
            else
            {
                strSQL = "   select top(10) FORMAT(record_date, 'MMM-yy', 'en-us') as record_date ,s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName,   " +


                    " case when s.smart_record_keyin_type = 'prod_sale' then sum(tr.product_amount - tr.product_amount_vat)  " +
                    " when s.smart_record_keyin_type = 'prod_service' then sum((tr.product_amount - tr.product_amount_vat) + " +
                    " (tr.service_serCharge_amount - tr.service_serCharge_amount_vat)) else   " +
                    " sum((tr.product_amount - tr.product_amount_vat) - tr.service_serCharge_amount)  " +
                    " end as total_ex_var,  " +


                    //"   sum(tr.product_amount) as product_amount,sum((tr.product_amount * 7) / 107) as vat,  " +
                    //"   sum(tr.product_amount - (tr.product_amount * 7) / 107) as exvat,   " +
                    //"   sum(tr.service_serCharge_amount) as service_serCharge_amount,sum((tr.service_serCharge_amount * 7) / 107) as vat2,   " +
                    //"   sum(tr.service_serCharge_amount - (tr.service_serCharge_amount * 7) / 107) as exvat2, " +



                    " s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor, " +
                    " s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH, " +
                    " ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm  " +
                    " from tbltransaction_record tr " +
                    " inner join tblsmart_Contract s on tr.ContractNumber = s.ContractNumber  and tr.status = 'y' " +
                    " left join tblshopgroup sg on s.smart_shop_group_id = sg.id " +
                    " inner join SAPCompany com on s.CompanyCode = com.CompanyCode " +
                    " inner join SAPBuilding b on s.smart_building = b.BuildingCode " +
                    " inner join tblcontracttype ct on s.smart_contract_type = ct.id " +
                    " inner join tblcategoryleasing cl on s.smart_Category_leasing = cl.id " +
                    " inner join TblIndustryGroup ig on s.smart_industry_group = ig.id " +
                    " inner join tblgrouplocation g on s.smart_group_location = g.id " +
                    " where tr.record_date between '" + d1 + " 00:00:00' and '" + d2 + " 00:00:00' " +
                    " " + " " + cat_leasing + " " + industry_group + " " + companyname_ + " " +

                    "   group by FORMAT(record_date, 'MMM-yy', 'en-us'),s.BusinessPartnerCode,s.ShopName,s.BusinessPartnerName, " +
                      "   s.smart_shop_group_id,sg.ShopGroupNameTH,com.CompanyNameTH,s.smart_building,b.BuildingNameEN,s.smart_floor,  " +
                      "   s.smart_usage_name,s.smart_contract_type,ct.ContractTypeNameTH,cl.CategoryleasingNameTH,  " +
                      "   ig.IndustryGroupNameTH,g.GrouplocationNameTH,s.smart_record_keyin_type,s.smart_room_no,s.smart_sqm  order by total_ex_var desc  ";
            }
            


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetBuilding(string[] comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " a.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            //strSQL += " select * from TblIndustryGroup  ";

            strSQL += " select distinct a.* from SAPBuilding a  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " where " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetRoomType()
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select distinct UsageTypeName from SAPRentalObject  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable Getcontract_type(string[] comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " c.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            strSQL += " select distinct a.* from tblcontracttype a  " +
                        " inner join TblContractType_mapping b on b.ContractTypeId = a.id  " +
                        " inner join TblCompanyGroupMapping c on c.ComGroupID = b.ComGroupID  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetCategory_leasing(string[] comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " c.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            strSQL += " select distinct a.* from tblcategoryleasing a  " +
                      " inner join TblCategoryLeasing_mapping b on b.CategoryleasingId = a.id  " +
                      " inner join TblCompanyGroupMapping c on c.ComGroupID = b.ComGroupID  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable Getgroup_location(string[] comcode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " c.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            strSQL += " select distinct a.* from tblgrouplocation a   " +
                        " inner join TblGroupLocation_mapping b on b.GroupLocationId = a.id  " +
                        " inner join TblCompanyGroupMapping c on c.ComGroupID = b.ComGroupID  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetIndustry_group(string[] comcode)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string comcode_ = "";

            if (comcode.Length != 0)
            {
                comcode_ += "(";
                for (int i = 0; i < comcode.Length; i++)
                {
                    comcode_ += " c.CompanyCode =" + comcode[i].Replace("&", "' + char(38) + '") + " or";
                }

                comcode_ = comcode_.Substring(0, comcode_.Length - 2);
                comcode_ = comcode_ + ")";
            }

            //strSQL += " select * from TblIndustryGroup  ";

            strSQL += " select distinct a.* from TblIndustryGroup a  " +
                                   " inner join TblIndustryGroup_mapping b on b.IndustryGroupCode = a.IndustryGroupCode " +
                                   " inner join TblCompanyGroupMapping c on c.ComGroupID = b.ComGroupID  ";

            if (comcode_ != "")
            {
                strSQL = strSQL + " and " + comcode_;
            }

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetMapCompany(string userid)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            if (userid != "")
            {
                strSQL += " select  distinct ComGroupID from tblmapp_User_Company where userid in (" + userid + ") ;  ";

            }
            else
            {
                strSQL += " select  distinct ComGroupID from tblmapp_User_Company ;  ";

            }
            //strSQL += " select * from tblmapp_User_Company where userid = '" + userid + "';  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable GetCompany(string com_code)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from SAPCompany where IsActive = '1' and CompanyCode in (" + com_code + ") order by CompanyNameTH  ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }









    }
}